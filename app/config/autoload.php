<?php

/**
 * Auto Load Class files by namespace
 *
 * @eg
 	'namespace' => '/path/to/dir'
 */

$autoload = [
	'Events\Api' => $dir . '/library/events/api/',
	'Micro\Messages' => $dir . '/library/micro/messages/',
	'Utilities\Debug' => $dir . '/library/utilities/debug/',
  'Utilities\Guid' => $dir . '/library/utilities/guid/',
	'Security\Hmac' => $dir . '/library/security/hmac/',
  'Security\Jwt' => $dir . '/library/security/jwt/',
	'Application' => $dir . '/library/application/',
	'Interfaces' => $dir . '/library/interfaces/',
	'Controllers' => $dir . '/controllers/',
	'Models' => $dir . '/models/',
	'Phalcon\Db\Adapter\Pdo' => $dir . '/library/Phalcon/Db/Adapter/Pdo',
	'Phalcon\Db\Result\PdoSqlsrv' => $dir . '/library/Phalcon/Db/Result',
	'Phalcon\Db\Dialect' => $dir . '/library/Phalcon/Db/Dialect',
	'Phalcon\Db\Result' => $dir . '/library/Phalcon/Db/Result',
	'Airaghi\PhalconPHP\MSQQL\Adapter' => $dir . '/library/MSSQL/Adapter',
	'Airaghi\PhalconPHP\MSSQL\Dialect' => $dir . '/library/MSSQL/Dialect'

];

return $autoload;
