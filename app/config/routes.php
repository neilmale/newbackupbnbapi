<?php

/**
 * @author Jete O'Keeffe
 * @version 1.0
 * @link http://docs.phalconphp.com/en/latest/reference/micro.html#defining-routes
 * @eg.
 */
 $routes[] = [
 	'method' => 'get',
 	'route' => '/checkthisout',
 	'handler' => ['Controllers\ExampleController', 'checkthisoutAction'],
     'authentication' => FALSE
 ];

$routes[] = [
	'method' => 'post',
	'route' => '/ping',
	'handler' => ['Controllers\ExampleController', 'pingAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/authenticate/get',
    'handler' => ['Controllers\AuthenticateController', 'getAccessTokenAction'],
    'authentication' => FALSE
];

$routes[] = [
	'method' => 'post',
	'route' => '/test/{id}',
	'handler' => ['Controllers\ExampleController', 'testAction']
];

$routes[] = [
	'method' => 'post',
	'route' => '/skip/{name}',
	'handler' => ['Controllers\ExampleController', 'skipAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/map/marker/upload',
    'handler' => ['Controllers\MapController', 'uploadPicsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/map/create/info',
    'handler' => ['Controllers\MapController', 'saveMapMarkerAction'],
    'authentication' => FALSE
];

//rainier routers

//pages routers
$routes[] = [
    'method' => 'post',
    'route' => '/pages/saveimage',
    'handler' => ['Controllers\PagesController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/listimages',
    'handler' => ['Controllers\PagesController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/create',
    'handler' => ['Controllers\PagesController', 'createPagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pages/managepage/{num}/{off}/{keyword}',
    'handler' => ['Controllers\PagesController', 'managePagesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/updatepagestatus/{status}/{pageid}/{keyword}',
    'handler' => ['Controllers\PagesController', 'pageUpdatestatusAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'get',
    'route' => '/page/pagedelete/{pageid}',
    'handler' => ['Controllers\PagesController', 'pagedeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/pageedit/{pageid}',
    'handler' => ['Controllers\PagesController', 'pageeditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/pagerightitem/{pageid}',
    'handler' => ['Controllers\PagesController', 'pagerightitemAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/pages/saveeditedpage',
    'handler' => ['Controllers\PagesController', 'saveeditedPagesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/getpage/{pageslugs}',
    'handler' => ['Controllers\PagesController', 'getPageAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/page/leftsidebarid/{pageid}',
 'handler' => ['Controllers\PagesController', 'leftsidebaridAction'],
 'authentication' => FALSE
];


$routes[] = [
 'method' => 'get',
 'route' => '/page/leftsidebar/{offset}/{sidebarid}',
 'handler' => ['Controllers\PagesController', 'listleftsidebarAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/page/rightsidebar/{offset}/{pageid}',
 'handler' => ['Controllers\PagesController', 'listrightsidebarAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/page/initialawakening',
 'handler' => ['Controllers\PagesController', 'pageinitialawakeningAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/page/related/{pageslugs}',
 'handler' => ['Controllers\PagesController', 'pagerelatedAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/fe/page/view/{pageslugs}',
 'handler' => ['Controllers\PagesController', 'fepageviewAction'],
 'authentication' => FALSE
];



//news routers
$routes[] = [
    'method' => 'post',
    'route' => '/news/saveimage',
    'handler' => ['Controllers\NewsController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listimages',
    'handler' => ['Controllers\NewsController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/listcategory',
    'handler' => ['Controllers\NewsController', 'listcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/create',
    'handler' => ['Controllers\NewsController', 'createNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/newscenter/create',
    'handler' => ['Controllers\NewsController', 'createNewsCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/managenews/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'manageNewsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewsstatus/{status}/{newsid}/{keyword}',
    'handler' => ['Controllers\NewsController', 'newsUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatenewscenterstatus/{status}/{newsid}/{keyword}',
    'handler' => ['Controllers\NewsController', 'newsCenterUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/newsdelete/{newsid}',
    'handler' => ['Controllers\NewsController', 'newsdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/newscenterdelete/{newsid}',
    'handler' => ['Controllers\NewsController', 'newscenterdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/newsedit/{newsid}',
    'handler' => ['Controllers\NewsController', 'newseditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/newscenteredit/{newsid}',
    'handler' => ['Controllers\NewsController', 'newscentereditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/edit',
    'handler' => ['Controllers\NewsController', 'editNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/newscenter/edit',
    'handler' => ['Controllers\NewsController', 'editNewsCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/createnews/workshoptitles',
    'handler' => ['Controllers\NewsController', 'workshoptitlesAction'],
    'authentication' => FALSE
];



$routes[] = [
    'method' => 'get',
    'route' => '/news/managecategory/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsController', 'managecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/savecategory',
    'handler' => ['Controllers\NewsController', 'createcategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updatecategorynames',
    'handler' => ['Controllers\NewsController', 'updatecategoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/validate/categ/{categ}',
    'handler' => ['Controllers\NewsController', 'validatecategAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/categorydelete/{id}',
    'handler' => ['Controllers\NewsController', 'categorydeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listcategory',
 'handler' => ['Controllers\NewsController', 'listcategoryfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/randomcategoryfrontend',
 'handler' => ['Controllers\NewsController', 'randomcategoryfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/featurednews',
 'handler' => ['Controllers\NewsController', 'featurednewsfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/latest',
 'handler' => ['Controllers\NewsController', 'latestnewsfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/founderswisdom',
 'handler' => ['Controllers\NewsController', 'founderswisdomfrontendAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/showcategoryname/{categoryslugs}',
 'handler' => ['Controllers\NewsController', 'showcategorynameAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/listnewsbycategory/{categoryslugs}/{offset}',
 'handler' => ['Controllers\NewsController', 'listnewsbycategoryAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/showtotalnewscategoryitem/{categoryslugs}',
 'handler' => ['Controllers\NewsController', 'showtotalnewscategoryitemAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/deletenewsimg/{id}',
 'handler' => ['Controllers\NewsController', 'deletenewsimgAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/listcenter',
 'handler' => ['Controllers\NewsController', 'socialUpdatestatusAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];
//validate news if already taken
$routes[] = [
 'method' => 'get',
 'route' => '/news/validate/{title}',
 'handler' => ['Controllers\NewsController', 'Validatenewstitlesction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/newsslugs/validate/{slugs}',
 'handler' => ['Controllers\NewsController', 'ValidateNewsSlugsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/newsslugss/validate/{slugs}/{centerid}',
 'handler' => ['Controllers\NewsController', 'ValidateNewssSlugsAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/fe/bnbbuzz/index',
 'handler' => ['Controllers\NewsController', 'febnbbuzzindexAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/fe/bnbbuzz/view/{newsslugs}',
 'handler' => ['Controllers\NewsController', 'febnbbuzzviewAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/fe/bnbbuzz/category/{categoryslugs}/{offset}',
 'handler' => ['Controllers\NewsController', 'febnbbuzzcategoryAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/regionmanagerlist',
 'handler' => ['Controllers\CenterController', 'listregionmanagerAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/listregion',
 'handler' => ['Controllers\CenterController', 'listregionAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/editcenter/districtlist',
 'handler' => ['Controllers\CenterController', 'editlistdistrictAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/centermanagerlist',
 'handler' => ['Controllers\CenterController', 'listcentermanagerAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/statelist',
 'handler' => ['Controllers\CenterController', 'liststateAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/citylist/{statecode}',
 'handler' => ['Controllers\CenterController', 'listcityAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
 'method' => 'get',
 'route' => '/center/ziplist/{cityname}',
 'handler' => ['Controllers\CenterController', 'getzipAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
 'method' => 'post',
 'route' => '/center/createcenter',
 'handler' => ['Controllers\CenterController', 'savecenterAction'],
 'authentication' => TRUE,
 'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/managecenter/{num}/{off}/{keyword}/{userid}',
    'handler' => ['Controllers\CenterController', 'manageCenterAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/managecenterdropdown/{num}/{off}/{keyword}/{userid}',
    'handler' => ['Controllers\CenterController', 'dropdownmanageCenterAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/updatecenterstatus/{status}/{centerid}/{keyword}',
    'handler' => ['Controllers\CenterController', 'centerUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/centerdelete/{centerid}',
    'handler' => ['Controllers\CenterController', 'centerdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/loadcenter/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadCenterAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/saveeditedcenter',
    'handler' => ['Controllers\CenterController', 'saveeditedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/calendar/createevent',
    'handler' => ['Controllers\CenterController', 'saveEventAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/calendar/listevent/{num}/{off}/{keyword}/{centerid}',
    'handler' => ['Controllers\CenterController', 'manageEventAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/updateeventstatus/{status}/{activityid}/{keyword}',
    'handler' => ['Controllers\CenterController', 'eventUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/eventdelete/{activityid}',
    'handler' => ['Controllers\CenterController', 'eventdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/calendar/loadevent/{activityid}',
    'handler' => ['Controllers\CenterController', 'loadEventAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/calendar/updateevent',
    'handler' => ['Controllers\CenterController', 'eventupdateAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/location/savelocation',
    'handler' => ['Controllers\CenterController', 'savelocationAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/location/loadlocation/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadLocationAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/openhours/saveopenhour',
    'handler' => ['Controllers\CenterController', 'saveopenhourAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/openhours/loadhour/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadopenhourAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/social/savesocial',
    'handler' => ['Controllers\CenterController', 'savesociallinkAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/social/loadsocial/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadsocialAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/social/deletesocial/{linkid}',
    'handler' => ['Controllers\CenterController', 'deletesociallinkAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/social/updatesocialstatus/{status}/{linkid}/{keyword}',
    'handler' => ['Controllers\CenterController', 'socialUpdatestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/centerview/loadcenter/{centerid}',
    'handler' => ['Controllers\CenterController', 'centerviewloadcenterAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/social/loadlinksbyid/{linkid}',
    'handler' => ['Controllers\CenterController', 'loadlinksbyidAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerdetails/updatesociallink',
    'handler' => ['Controllers\CenterController', 'updatesociallinkAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerdetails/updatesocialurlstatus',
    'handler' => ['Controllers\CenterController', 'updatesocialurlstatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];



$routes[] = [
    'method' => 'post',
    'route' => '/center/social/updatelinksbyid',
    'handler' => ['Controllers\CenterController', 'updatelinksbyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/spnl/{centerid}',
    'handler' => ['Controllers\CenterController', 'spnlAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/setspnl/{centerid}/{spnlstatus}',
    'handler' => ['Controllers\CenterController', 'setspnlAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/offer/saveoffer',
    'handler' => ['Controllers\CenterController', 'saveofferAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/offer/updateoffer',
    'handler' => ['Controllers\CenterController', 'updateofferAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/offer/updateofferwithupload',
    'handler' => ['Controllers\CenterController', 'updateofferWithUploadAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/offer/loadoffer/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadofferAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/beneplace/viewreferrallink',
    'handler' => ['Controllers\BeneplaceController', 'viewreferrallinkAction'],
    'authentication' => false
];

$routes[] = [
    'method' => 'post',
    'route' => '/be/beneplace/savereferral',
    'handler' => ['Controllers\BeneplaceController', 'addreferralAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/offer/deleteoffer/{offerid}',
    'handler' => ['Controllers\CenterController', 'deleteofferAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/offer/editoffer/{offerid}',
    'handler' => ['Controllers\CenterController', 'editofferAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/loadfee/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadfeeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/pricing/savefee',
    'handler' => ['Controllers\CenterController', 'savefeeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/statusregfee/{centerid}/{status}',
    'handler' => ['Controllers\CenterController', 'statusregfeeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/statusregclass/{centerid}/{status}',
    'handler' => ['Controllers\CenterController', 'statusregclassAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/statussessionfee/{centerid}/{status}',
    'handler' => ['Controllers\CenterController', 'statussessionfeeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/pricing/savemembership',
    'handler' => ['Controllers\CenterController', 'savemembershipAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/loadmembership/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadmembershipAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/loadmembershipbyid/{membershipid}',
    'handler' => ['Controllers\CenterController', 'loadmembershipbyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/pricing/savemembershipbyid',
    'handler' => ['Controllers\CenterController', 'savemembershipbyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/deletemembershipbyid/{membershipid}',
    'handler' => ['Controllers\CenterController', 'deletemembershipbyidAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/pricing/loadprivatesession/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadprivatesessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/pricing/saveprivatesession',
    'handler' => ['Controllers\CenterController', 'saveprivatesessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


$routes[] = [
    'method' => 'get',
    'route' => '/center/schedule/loadschedule/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadscheduleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/schedule/saveschedule',
    'handler' => ['Controllers\CenterController', 'savescheduleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/schedule/loadschedulebyid/{sessionid}',
    'handler' => ['Controllers\CenterController', 'loadschedulebyidAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/schedule/updateschedule',
    'handler' => ['Controllers\CenterController', 'updatescheduleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/schedule/deleteschedule/{sessionid}',
    'handler' => ['Controllers\CenterController', 'deletescheduleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/news/newslist/{num}/{off}/{keyword}/{centerid}',
    'handler' => ['Controllers\CenterController', 'newslistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/phone/getcenterphonenumber/{centerid}',
    'handler' => ['Controllers\CenterController', 'getcenterphonenumberAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/phone/savecenterphonenumber',
    'handler' => ['Controllers\CenterController', 'savecenterphonenumberAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/offer/updateofferstatus/{status}/{offerid}',
    'handler' => ['Controllers\CenterController', 'offerUpdatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/listnewsbycenter/{centerslugs}/{offset}',
    'handler' => ['Controllers\CenterController', 'listnewsbycenterAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/fullnewsbycenter/{centerslugs}/{newsslugs}',
 'handler' => ['Controllers\CenterController', 'fullnewsbycenterAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/listsuccesssotriesbycenter/{centerslugs}/{offset}',
    'handler' => ['Controllers\CenterController', 'listsuccesssotriesbycenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/listeventsbycenter/{centerslugs}/{offset}',
    'handler' => ['Controllers\CenterController', 'listeventsbycentercenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/saveregion',
    'handler' => ['Controllers\CenterController', 'saveregionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/loadregion/{num}/{off}/{keyword}',
    'handler' => ['Controllers\CenterController', 'loadregionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/deleteregion',
    'handler' => ['Controllers\CenterController', 'deleteregionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/updateregion',
    'handler' => ['Controllers\CenterController', 'updateregionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/savedistrict',
    'handler' => ['Controllers\CenterController', 'savedistrictAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/loadallregion',
    'handler' => ['Controllers\CenterController', 'loadallregionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/loaddistrict/{num}/{off}/{keyword}',
    'handler' => ['Controllers\CenterController', 'loaddistrictAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/deletedistrict',
    'handler' => ['Controllers\CenterController', 'deletedistrictAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/loaddistrictbyid',
    'handler' => ['Controllers\CenterController', 'loaddistrictbyidAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/updateistrict',
    'handler' => ['Controllers\CenterController', 'updateistrictAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/email/getcenteremail/{centerid}',
    'handler' => ['Controllers\CenterController', 'getcenteremailAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/email/saveemail',
    'handler' => ['Controllers\CenterController', 'saveemailAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/centerpagedata/{centerslugs}',
    'handler' => ['Controllers\CenterController', 'centerpagedataAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/location/searchcenterlocation',
    'handler' => ['Controllers\CenterlocationController', 'searchlocationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/paypal/paypalreturn',
    'handler' => ['Controllers\PaypalController', 'paypalreturnAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/introsessionlist/{num}/{off}/{keyword}/{centerid}',
    'handler' => ['Controllers\GetstartedController', 'introsessionlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/starterspackage/authorize',
    'handler' => ['Controllers\GetstartedController', 'authorizeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/getcenterdetails/{centerid}',
    'handler' => ['Controllers\GetstartedController', 'getcenterdetailsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/loadsession/{sessionid}/{userid}',
    'handler' => ['Controllers\GetstartedController', 'loadsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/groupclasssessionlist/{num}/{off}/{keyword}/{centerid}',
    'handler' => ['Controllers\GetstartedController', 'groupclasssessionlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/loadgroupsession/{sessionid}/{userid}',
    'handler' => ['Controllers\GetstartedController', 'loadgroupsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/price/loadprice',
    'handler' => ['Controllers\CenterController', 'loadpriceAction'],
    'authentication' => false
];

$routes[] = [
    'method' => 'get',
    'route' => '/price/loadbeneplaceprice',
    'handler' => ['Controllers\CenterController', 'loadbeneplacepriceAction'],
    'authentication' => false
];

$routes[] = [
    'method' => 'post',
    'route' => '/price/saveintro',
    'handler' => ['Controllers\CenterController', 'saveintroAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/price/beneplacesaveintro',
    'handler' => ['Controllers\CenterController', 'beneplacesaveintroAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/getstarted/loadcenter/{centerid}',
    'handler' => ['Controllers\GetstartedController', 'loadcenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/beneplace/beneplacelist/{num}/{off}/{keyword}/{centerid}',
    'handler' => ['Controllers\BeneplaceController', 'beneplacelistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/beneplace/loadbeneplacesession/{sessionid}/{userid}',
    'handler' => ['Controllers\BeneplaceController', 'loadbeneplacesessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/beneplace/loadbenecancelledsession/{sessionid}/{userid}',
    'handler' => ['Controllers\BeneplaceController', 'loadbenecancelledsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/beneplace/loadunverifiedsession/{sessionid}/{userid}',
    'handler' => ['Controllers\BeneplaceController', 'loadunverifiedsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/allbeneplace/allbeneplacelist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\BeneplaceController', 'allbeneplacelistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/be/allbeneplace/loadclasscancelledlist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\BeneplaceController', 'loadclasscancelledlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/allbeneplace/allunverifiedclasslist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\BeneplaceController', 'allunverifiedclasslist'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/allintrosession/allintrosession/{num}/{off}/{keyword}',
    'handler' => ['Controllers\GetstartedController', 'allintrosessionlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/allgroupclasssession/allgroupclasssession/{num}/{off}/{keyword}',
    'handler' => ['Controllers\GetstartedController', 'allgroupclasssessionlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/be/managercontract/savecontact',
    'handler' => ['Controllers\CenterController', 'savecontactAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/managercontract/loadcontact/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadcontacttAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/be/managercontract/testsmsmessage',
    'handler' => ['Controllers\CenterController', 'testsmsmessageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/dashboard/counttotalpost',
    'handler' => ['Controllers\DashboardController', 'counttotalpostAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/getip/visit/{ip}',
    'handler' => ['Controllers\VisitController', 'visitAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/dashboard/countvisit',
    'handler' => ['Controllers\VisitController', 'countAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/listnotification/{userid}/{offset}/{filter}',
    'handler' => ['Controllers\NotificationController', 'listnotificationAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/changestatus/{notificationid}/{userid}',
    'handler' => ['Controllers\NotificationController', 'changestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/notificationcount/{userid}',
    'handler' => ['Controllers\NotificationController', 'notificationcountAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/notificationcountzero/{userid}',
    'handler' => ['Controllers\NotificationController', 'notificationcountzeroAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/loadnotification/{num}/{off}/{keyword}/{userid}',
    'handler' => ['Controllers\NotificationController', 'loadnotificationAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/notification/deletenotification/{notificationid}',
    'handler' => ['Controllers\NotificationController', 'deletenotificationAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/auditlogs/auditloglist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\AuditlogController', 'auditloglistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/auditlogs/auditloglistcsv/{num}/{off}/{keyword}',
    'handler' => ['Controllers\AuditlogController', 'auditloglistcsvAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/centeroffer/offer',
    'handler' => ['Controllers\CenterController', 'getofferAction'],
    'authentication' => FALSE
];



/** NEIL ROUTER

**/
$routes[] = [
 'method' => 'get',
 'route' => '/fe/index/properties',
 'handler' => ['Controllers\IndexController', 'FEindexPropertiesAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/view/{newsslugs}',
 'handler' => ['Controllers\NewsController', 'showFullNewsAction'],
 'authentication' => FALSE
];


$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/sidebarmostviewednews',
 'handler' => ['Controllers\NewsController', 'showSidebarMostViewedAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/frontend/sidebarlatestnews',
 'handler' => ['Controllers\NewsController', 'showSidebarLatestNewsAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/news/updateviews/{newsslugs}',
    'handler' => ['Controllers\NewsController', 'newsViewsCountUpdateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/popularnews',
    'handler' => ['Controllers\NewsController', 'displayPopularNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/news/rss/{offset}',
 'handler' => ['Controllers\NewsController', 'newrssAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/mainnews/workshoptitles/{newsid}',
    'handler' => ['Controllers\NewsController', 'mainnewsworkshoprelatedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/mainnews/addrelated',
    'handler' => ['Controllers\NewsController', 'mainnewsaddrelatedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/mainnews/removerelated',
    'handler' => ['Controllers\NewsController', 'mainnewsremoverelatedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/centernews/workshoprelated/{newsid}',
    'handler' => ['Controllers\NewsController', 'centernewsworkshoprelatedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centernews/addrelated',
    'handler' => ['Controllers\NewsController', 'centernewsaddrelatedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centernews/removerelated',
    'handler' => ['Controllers\NewsController', 'centernewsremoverelatedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


$routes[] = [
    'method' => 'get',
    'route' => '/user/setstatus/{userid}/{userstatus}',
    'handler' => ['Controllers\UserController', 'userSetStatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/centerlist',
    'handler' => ['Controllers\CenterController', 'listOfCentersAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/statelist',
    'handler' => ['Controllers\CenterController', 'liststateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/success-stories/submitstory',
    'handler' => ['Controllers\TestimoniesController', 'submitStoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/centerlistviastate/{state_code}',
    'handler' => ['Controllers\CenterController', 'centerListViaStateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/stories/storieslist',
    'handler' => ['Controllers\TestimoniesController', 'storiesListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/stories/managestories/{num}/{off}/{keyword}',
    'handler' => ['Controllers\TestimoniesController', 'manageStoriesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/stories/managestoriespercenter/{centerid}/{num}/{off}/{keyword}',
    'handler' => ['Controllers\TestimoniesController', 'manageStoriesPerCenterAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/story/information/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'getStoryAction'],
    'authentication' => FALSE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/tagpage/{tag}/{offset}',
    'handler' => ['Controllers\TestimoniesController', 'fetagAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/story/savereviewedstory',
    'handler' => ['Controllers\TestimoniesController', 'saveReviewedStoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/story/delete/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'deleteStoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/spotlight',
    'handler' => ['Controllers\TestimoniesController', 'spotlightAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/totallateststories/{offset}',
    'handler' => ['Controllers\TestimoniesController', 'totalLatestStoriesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/success-stories/frontend/showtotallateststoriesitem',
    'handler' => ['Controllers\TestimoniesController', 'showTotalLatestStoriesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/story/details/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'viewStoryDetailsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/index/locations/states',
    'handler' => ['Controllers\LocationsController', 'locationStatesAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/saveimagesforslider',
    'handler' => ['Controllers\CenterController', 'centerSaveImagesForSliderAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/news/frontend/view',
    'handler' => ['Controllers\NewsController', 'viewAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/center/loadcenterimages/{centerid}',
    'handler' => ['Controllers\CenterController', 'loadCenterImagesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerslider/delete/{img}',
    'handler' => ['Controllers\CenterController', 'deleteimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/center/getlocation',
    'handler' => ['Controllers\CenterController', 'loadCenterPagelocationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/successstories/{centerslugs}/{storyslugs}/{ssid}',
    'handler' => ['Controllers\CenterController', 'loadCenterPageStoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/centerinfo/{centerslugs}',
    'handler' => ['Controllers\CenterController', 'loadCenterPageInfoAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/classschedule/{centerslugs}',
    'handler' => ['Controllers\CenterController', 'loadCenterPageClassScheduleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/center/pricing/{centerslugs}',
    'handler' => ['Controllers\CenterController', 'loadCenterPagePricingAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/loginuser/centermanager/{username}',
    'handler' => ['Controllers\UserController', 'loginCenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/user/available/regdiscen/{userid}',
    'handler' => ['Controllers\UserController', 'availableRegDisCenListAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/success/write/statelist',
    'handler' => ['Controllers\CenterController', 'FEloadstateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/success-stories/view/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'FEsuccessstoriespageviewAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/story/addmetatags',
    'handler' => ['Controllers\TestimoniesController', 'storyaddmetatagAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/story/workshoptitles/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'workshoptitlesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/story/addrelated',
    'handler' => ['Controllers\TestimoniesController', 'storyaddrelatedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/story/removerelated',
    'handler' => ['Controllers\TestimoniesController', 'storyremoverelatedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/story/metatags/{storyid}',
    'handler' => ['Controllers\TestimoniesController', 'storymetatagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/story/updatemetatags',
    'handler' => ['Controllers\TestimoniesController', 'storyupdatemetatagAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];



$routes[] = [
    'method' => 'post',
    'route' => '/be/workshop/createvenue',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopCreatevenueAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/listofvenues/{num}/{off}/{keyword}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopListofvenuesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// //dati to, pwd na siguro idelete soon (12-28-15)
// $routes[] = [
//     'method' => 'get',
//     'route' => '/BE/workshop/checkvenuename/{venuename}',
//     'handler' => ['Controllers\WorkshopController', 'BEworkshopCheckvenuenameAction'],
//     'authentication' => TRUE,
//     'level' => 'admin'
// ];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/deleteworkshopvenue/{workshopvenueid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopDeleteworkshopvenueAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/workshop/editworkshopvenue/{workshopvenueid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopEditworkshopvenueAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
// //dati to, pwd na siguro idelete soon (12-29-15)
// $routes[] = [
//     'method' => 'get',
//     'route' => '/BE/workshop/checkvenuenamewexception/{venuename}/{workshopvenueid}',
//     'handler' => ['Controllers\WorkshopController', 'BEworkshopCheckvenuenamewexceptionAction'],
//     'authentication' => TRUE,
//     'level' => 'admin'
// ];

$routes[] = [
    'method' => 'post',
    'route' => '/be/workshop/updatevenue',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopUpdatevenueAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/be/workshop/createtitle',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopCreatetitleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/workshop/listoftitles/{num}/{off}/{keyword}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopListoftitlesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/be/workshop/updatetitle',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopUpdatetitleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/removetitle/{titleid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopremovetitleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/checktitlename/{titleslugs}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopChecktitlenameAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/lists',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopListAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/titles',
    'handler' => ['Controllers\WorkshopController', 'workshopTitlesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/centersnvenues',
    'handler' => ['Controllers\WorkshopController', 'centersNVenuesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/be/workshop/createworkshop',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopCreateworkshopAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/BE/workshop/listofworkshop/{num}/{off}/{keyword}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopListofworkshopAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/workshop/removeworkshop/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopRemoveworkshopAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/workshop/edit/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopEditworkshopAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/be/workshop/update',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopUpdateworkshopAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// $routes[] = [
//     'method' => 'post',
//     'route' => '/BE/workshop/update/{workshopid}',
//     'handler' => ['Controllers\WorkshopController', 'BEworkshopUpdateworkshopAction'],
//     'authentication' => FALSE
// ];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/lists',
    'handler' => ['Controllers\WorkshopController', 'BEworkshopListAction'], //acting like universal action :]
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/loadcity/viastatecode/{statecode}',
    'handler' => ['Controllers\WorkshopController', 'loadcityViastatecodeAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/workshopschedules/{titleslugs}/{offset}',
    'handler' => ['Controllers\WorkshopController', 'workshopschedulePaginationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/workshop/schedule/pagination',
    'handler' => ['Controllers\WorkshopController', 'workshopschedulepaginationangularAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/workshop/schedulelist/{titleslugs}/{offset}',
    'handler' => ['Controllers\WorkshopController', 'workshopSchedulelistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/detail/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'workshopdetailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/location/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'workshoplocationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/registrationinfo/{workshopid}',
    'handler' => ['Controllers\WorkshopController', 'workshopdetailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/workshop/registrant',
    'handler' => ['Controllers\WorkshopController', 'registrantAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/workshop/registrantslist',
    'handler' => ['Controllers\WorkshopController', 'registrantslistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/removeregistrant/{registrantid}',
    'handler' => ['Controllers\WorkshopController', 'removeRegistrantAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/overview',
    'handler' => ['Controllers\WorkshopController', 'overviewAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/workshop/related',
    'handler' => ['Controllers\WorkshopController', 'relatedAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// WORKSHOP ^

$routes[] = [
    'method' => 'get',
    'route' => '/sidebar/contents',
    'handler' => ['Controllers\SidebarController', 'contentsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/oneonone',
    'handler' => ['Controllers\CenterincomeController','oneononeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filteroneonone',
    'handler' => ['Controllers\CenterincomeController','filteroneononeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/groupsession',
    'handler' => ['Controllers\CenterincomeController','groupsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filtergroupsession',
    'handler' => ['Controllers\CenterincomeController','filtergroupsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/chartperday',
    'handler' => ['Controllers\CenterincomeController','chartperdayAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/chartpermonth',
    'handler' => ['Controllers\CenterincomeController','chartpermonthAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filterchartperday',
    'handler' => ['Controllers\CenterincomeController','filterchartperdayAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filterchartpermonth',
    'handler' => ['Controllers\CenterincomeController','filterchartpermonthAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/oneononefranchise',
    'handler' => ['Controllers\CenterincomeController','oneononefranchiseAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/allfranchise',
    'handler' => ['Controllers\CenterincomeController','allfranchiseAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filterallfranchise',
    'handler' => ['Controllers\CenterincomeController','filterallfranchiseAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filteroneononefranchise',
    'handler' => ['Controllers\CenterincomeController','filteroneononefranchiseAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/centerincome/groupfranchise',
    'handler' => ['Controllers\CenterincomeController','groupfranchiseAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerincome/filtergroupfranchise',
    'handler' => ['Controllers\CenterincomeController','filtergroupfranchiseAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/details/{centerid}',
    'handler' => ['Controllers\CenterController','detailsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/centerdetails/savedescription',
    'handler' => ['Controllers\CenterController','savedescriptionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/center/saveordering',
    'handler' => ['Controllers\CenterController','saveorderingAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];




/*NEIL ENDS*/



/** Kyben
 */
/* Validation for username exist */
$routes[] = [
    'method' => 'get',
    'route' => '/validate/username/{name}',
    'handler' => ['Controllers\UserController', 'userExistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
/* Validation for email exist */
$routes[] = [
    'method' => 'get',
    'route' => '/validate/email/{email}',
    'handler' => ['Controllers\UserController', 'emailExistAction'],
    'authentication' => FALSE
];
/* Submit User Regester */
$routes[] = [
    'method' => 'post',
    'route' => '/user/register',
    'handler' => ['Controllers\UserController', 'registerUserAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

/* List all User */
$routes[] = [
    'method' => 'get',
    'route' => '/user/list/{num}/{off}/{keyword}',
    'handler' => ['Controllers\UserController', 'userListAction'],
    'authentication' => FALSE
];
/* User Info */
$routes[] = [
    'method' => 'get',
    'route' => '/user/info/{id}',
    'handler' => ['Controllers\UserController', 'userInfoction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/user/update',
    'handler' => ['Controllers\UserController', 'userUpdateAction'],
    'authentication' => FALSE
];
/* DELETE User */
$routes[] = [
    'method' => 'get',
    'route' => '/user/delete/{id}',
    'handler' => ['Controllers\UserController', 'deleteUserAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/user/activation',
    'handler' => ['Controllers\UserController', 'activationAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/user/login/{username}/{password}',
    'handler' => ['Controllers\UserController', 'loginAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/beneplace/trialaccount/{zipcode}/{textcity}/{state}/{lat}/{lon}',
    'handler' => ['Controllers\BeneplaceController', 'benePlaceAction'],
    'authentication' => FALSE
];



/**SIMULA NG ROTA NI RYAN JERIC.................. CONTACT US**/

$routes[] = [
    'method' => 'post',
    'route' => '/contactsend/form',
    'handler' => ['Controllers\ContactusController', 'sendAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/managecontacts/list/{num}/{off}/{keyword}/{status}',
    'handler' => ['Controllers\ContactusController', 'listcontactsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/managecontacts/messagedelete/{id}',
    'handler' => ['Controllers\ContactusController', 'messagedeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/managecontacts/listreplies/{id}',
    'handler' => ['Controllers\ContactusController', 'listreplyAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/message/view/{id}',
    'handler' => ['Controllers\ContactusController', 'viewreplyAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/message/reply',
    'handler' => ['Controllers\ContactusController', 'replyrequestAction'],
    'authentication' => FALSE
];


/**SIMULA NG ROTA ng FORGOT PASSWORD =Ryanjeric= **/
$routes[] = [
    'method' => 'post',
    'route' => '/forgotpassword/send/{email}',
    'handler' => ['Controllers\UserController', 'forgotpasswordAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/checktoken/check/{email}/{token}',
    'handler' => ['Controllers\UserController', 'checktokenAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/updatepassword/token',
    'handler' => ['Controllers\UserController', 'updatepasswordtokenAction'],
    'authentication' => FALSE
];

//SIMULA NG ROTA ng News Letter - Subscribers <Ryanjeric>

$routes[] = [
    'method' => 'get',
    'route' => '/subscriber/emailcheck/{email}',
    'handler' => ['Controllers\SubscribersController', 'checkemailAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/subscriber/add',
    'handler' => ['Controllers\SubscribersController', 'addsubscriberAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/subscriber/list/{num}/{off}/{keyword}',
    'handler' => ['Controllers\SubscribersController', 'subscriberslistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/subscriber/delete/{id}',
    'handler' => ['Controllers\SubscribersController', 'deletesubscriberAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// $routes[] = [
//     'method' => 'get',
//     'route' => '/delete/transaction',
//     'handler' => ['Controllers\WorkshopController', 'deletetransactionAction'],
//     'authentication' => FALSE
// ];


//SIMULA NG ROTA ng News Letter - Newsletter <Ryanjeric>



//IMAGE UPLOAD AND LIST
$routes[] = [
    'method' => 'post',
    'route' => '/newsletter/saveimage',
    'handler' => ['Controllers\NewsletterController', 'saveimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/listimages',
    'handler' => ['Controllers\NewsletterController', 'listimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/delete/{imgid}',
    'handler' => ['Controllers\NewsletterController', 'deleteimageAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/newsletter/add',
    'handler' => ['Controllers\NewsletterController', 'savenewsletterAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/checktitle/{title}',
    'handler' => ['Controllers\NewsletterController', 'checktitleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

//PDF UPLOAD AND LIST
$routes[] = [
    'method' => 'post',
    'route' => '/newsletter/savepdf',
    'handler' => ['Controllers\NewsletterController', 'savepdfAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/pdffiles',
    'handler' => ['Controllers\NewsletterController', 'listpdfAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/pdfdelete/{imgid}',
    'handler' => ['Controllers\NewsletterController', 'deletepdfAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/managenewsletter/list/{num}/{off}/{keyword}',
    'handler' => ['Controllers\NewsletterController', 'manageNewsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/deletenewspdf/{id}',
    'handler' => ['Controllers\NewsletterController', 'deletenewspdfAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/deletenewsletter/{id}',
    'handler' => ['Controllers\NewsletterController', 'deletenewsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/changestatus/{stat}/{id}',
    'handler' => ['Controllers\NewsletterController', 'changestatus1Action'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletterpdf/changestatus/{stat}/{id}',
    'handler' => ['Controllers\NewsletterController', 'changestatusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/edit/{id}',
    'handler' => ['Controllers\NewsletterController', 'newslettereditoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/newsletter/editpdf/{id}',
    'handler' => ['Controllers\NewsletterController', 'newspdfeditoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/newsletter/updatenewsletter',
    'handler' => ['Controllers\NewsletterController', 'updateNewsletterAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/newsletter/updatenewsletterpdf',
    'handler' => ['Controllers\NewsletterController', 'updateNewspdfAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/pdf/pdfcheck/{filename}',
    'handler' => ['Controllers\NewsletterController', 'checkfileAction'],
    'authentication' => FALSE
];

//NEWS LETTER FRONT END
$routes[] = [
    'method' => 'get',
    'route' => '/fe/enewsletter/popularnews/listsuccesssotries',
    'handler' => ['Controllers\NewsletterController', 'loadStoryAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/enewslettershow/{id}',
    'handler' => ['Controllers\NewsletterController', 'fenewslettershowAction'],
    'authentication' => FALSE
];


$routes[] = [
    'method' => 'get',
    'route' => '/fe/newsletterlist/{num}/{off}',
    'handler' => ['Controllers\NewsletterController', 'fenewsletterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/pdfnewsletterlist/{num}/{off}',
    'handler' => ['Controllers\NewsletterController', 'fepdfnewsletterAction'],
    'authentication' => FALSE
];


//    Settings - Simula ng ROTA ni Ryanjeric


$routes[] = [
    'method' => 'get',
    'route' => '/settings/managesettings',
    'handler' => ['Controllers\SettingsController', 'managesettingsAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/settings/maintenanceon',
    'handler' => ['Controllers\SettingsController', 'maintenanceonAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/settings/maintenanceoff',
    'handler' => ['Controllers\SettingsController', 'maintenanceoffAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/uploadlogo',
    'handler' => ['Controllers\SettingsController', 'uploadlogoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/settings/logolist',
    'handler' => ['Controllers\SettingsController', 'listlogoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/settings/delete/{img}',
    'handler' => ['Controllers\SettingsController', 'deletelogoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/settings/savedefaultlogo',
    'handler' => ['Controllers\SettingsController', 'savedefaultlogoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/googleanalytics',
    'handler' => ['Controllers\SettingsController', 'scriptAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/settings/loadscript',
    'handler' => ['Controllers\SettingsController', 'loadscriptAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/settings/script',
    'handler' => ['Controllers\SettingsController', 'displaytAction'],
    'authentication' => FALSE
];

// GHOST ROUTES
$routes[] = [
    'method' => 'post',
    'route' => '/user/changepassword',
    'handler' => ['Controllers\UserController', 'changepasswordAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

//jeevon
//product backend
$routes[] = [
    'method' => 'post',
    'route' => '/add/product',
    'handler' => ['Controllers\ProductController', 'addproductAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadimages/{id}',
    'handler' => ['Controllers\ProductController', 'loadimagesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/product/saveimage/{id}',
    'handler' => ['Controllers\ProductController', 'saveimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deleteimage/{id}',
    'handler' => ['Controllers\ProductController', 'deleteimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadaddproduct',
    'handler' => ['Controllers\ProductController', 'loadproductAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/load/{data}',
    'handler' => ['Controllers\ProductController', 'autocompleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/product/loadlist/{num}/{off}/{keyword}/{find}',
    'handler' => ['Controllers\ProductController', 'getlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/validatecode/{id}/{name}',
    'handler' => ['Controllers\ProductController', 'validatepcodeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/product/validatename/{id}/{name}',
    'handler' => ['Controllers\ProductController', 'validatenameAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/delete/{id}',
    'handler' => ['Controllers\ProductController', 'deleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadedit/{id}',
    'handler' => ['Controllers\ProductController', 'loadeditAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route'=> '/product/edit',
    'handler' => ['Controllers\ProductController', 'editAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/updatestatus/{id}/{status}',
    'handler' => ['Controllers\ProductController', 'updatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/validateslug/{id}/{slugs}',
    'handler' => ["Controllers\ProductController", 'validateslugAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/addquantity/{id}',
    'handler' => ['Controllers\ProductController', 'addquantityAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/adddiscount/{id}',
    'handler' => ['Controllers\ProductController', 'adddiscountAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/viewproduct/{id}',
    'handler' => ['Controllers\ProductController', 'viewproductAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/viewdetails/{id}/{type}/{page}',
    'handler' => ['Controllers\ProductController', 'viewdetailsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/selectimage',
    'handler' => ['Controllers\ProductController', 'selectimageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadcategorylist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\ProductController', 'loadcategorylistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/validatecategory/{id}/{category}',
    'handler' => ['Controllers\ProductController', 'validatecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadsubcateogry',
    'handler' => ['Controllers\ProductController', 'loadsubcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/saveCategory',
    'handler' => ['Controllers\ProductController', 'savecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deleteCategory/{id}',
    'handler' => ['Controllers\ProductController', 'deletecategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadeditcategory/{id}',
    'handler' => ['Controllers\ProductController', 'loadeditcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/editcategory',
    'handler' => ['Controllers\ProductController', 'editcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadtags/{num}/{off}/{keyword}',
    'handler' => ['Controllers\ProductController', 'loadtagslistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deletetags/{id}',
    'handler' => ['Controllers\ProductController', 'deletetagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/validatetags/{id}/{tags}',
    'handler' => ['Controllers\ProductController', 'validatetagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/saveTag',
    'handler' => ['Controllers\ProductController', 'savetagAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/updatetags',
    'handler' => ['Controllers\ProductController', 'updatetagsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadsubcategory/{num}/{off}/{keyword}',
    'handler' => ['Controllers\ProductController', 'loadsubcatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/validatesubcategory/{id}/{subcategory}',
    'handler' => ['Controllers\ProductController', 'validatesubcatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadtypes',
    'handler' => ['Controllers\ProductController', 'loadtypesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/addsubcategory',
    'handler' => ['Controllers\ProductController', 'addsubcatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deletesubcategory/{id}',
    'handler' => ['Controllers\ProductController', 'deletesubcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadeditsubcategory/{id}',
    'handler' => ['Controllers\ProductController', 'loadeditsubcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/updatesubcategory',
    'handler' => ['Controllers\ProductController', 'updatesubcategoryAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadtypes/{num}/{off}/{keyword}',
    'handler' => ['Controllers\ProductController', 'loadtypelistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deletetype/{id}',
    'handler' => ['Controllers\ProductController', 'deletetypeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/updatetype',
    'handler' => ['Controllers\ProductController', 'updatetypeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' =>'/product/validatetype/{type}',
    'handler' => ['Controllers\ProductController', 'validatetypeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/savetype',
    'handler' => ['Controllers\ProductController', 'savetypeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/getsubcat',
    'handler' => ['Controllers\ProductController', 'getsubcatAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/gettypes',
    'handler' => ['Controllers\ProductController', 'gettypesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/getReport/{id}',
    'handler' => ['Controllers\ProductController', 'getreportAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadtagssize',
    'handler' => ['Controllers\ProductController', 'loadtagssizeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/loadsize/{num}/{off}/{keyword}',
    'handler' => ['Controllers\ProductController', 'loadsizeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/deletesize/{id}',
    'handler' => ['Controllers\ProductController', 'deletesizeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/product/updatesize',
    'handler' => ['Controllers\ProductController', 'updatesizeAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/product/getproductreport',
    'handler' => ['Controllers\ProductController', 'productreportAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// frontend login and registration
$routes[] = [
    'method' => 'post',
    'route' => '/member/login',
    'handler' => ['Controllers\MemberController', 'loginAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/member/validateemail/{email}',
    'handler' => ['Controllers\MemberController', 'validateemailAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/member/register',
    'handler' => ['Controllers\MemberController', 'registerAction'],
    'authentication' => FALSE
];
// $routes[] = [
//     'method' => 'get',
//     'route' => '/member/getstates',
//     'handler' => ['Controllers\MemberController', 'getstatesAction'],
//     'authentication' => FALSE
// ];
// $routes[] = [
//     'method' => 'get',
//     'route' => '/member/getcenter/{state}',
//     'handler' => ['Controllers\MemberController', 'getcenterAction'],
//     authentication  => FALSE
// ];
$routes[] = [
    'method' => 'get',
    'route' => '/center-page/view/validate/{dataslugs}',
    'handler' => ['Controllers\PagesController', 'pageviewvalidateAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/bnbbuzz/index/{category}/{page}',
    'handler' => ['Controllers\NewsController', 'bnbbuzzindexAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/bnbbuzz/index/getnewscategories',
    'handler' => ['Controllers\NewsController', 'getnewscategoriesAction'],
    'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/fe/bnbbuzz/newsview/{newsslugs}',
 'handler' => ['Controllers\NewsController', 'febnbbuzznewsviewAction'],
 'authentication' => FALSE
];

$routes[] = [
 'method' => 'get',
 'route' => '/fe/starterspackage/loadstatewithcenters',
 'handler' => ['Controllers\CenterController', 'loadstatewithcentersAction'],
 'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/starterspackage/searchlocation',
    'handler' => ['Controllers\GetstartedController', 'searchlocationAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/starterspackage/searchzip/{zip}',
    'handler' => ['Controllers\GetstartedController', 'searchzipAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/starterspackage/getcenter/{slug}',
    'handler' => ['Controllers\GetstartedController', 'getcenterAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/starterspackage/getstarterloadscheduleAction',
    'handler' => ['Controllers\CenterController', 'getstarterloadscheduleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/starterspackage/savetemppaypaltransactionAction',
    'handler' => ['Controllers\GetstartedController', 'savetemppaypaltransactionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/starterspackage/getsuccesstransactionAction/{transactionid}/{moduletype}',
    'handler' => ['Controllers\GetstartedController', 'getsuccesstransactionAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/center/main/properties',
    'handler' => ['Controllers\LocationController', 'fe_centermainpropAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/center/monthly/sched',
    'handler' => ['Controllers\LocationController', 'monthlyschedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/center/main/getScheduleAction',
    'handler' => ['Controllers\CenterController', 'getScheduleAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/center/main/geteventAction',
    'handler' => ['Controllers\LocationController', 'geteventAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/center/main/getScheduleAction',
    'handler' => ['Controllers\CenterController', 'getScheduleAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/member/loginfb',
    'handler' => ['Controllers\MemberController', 'loginfbAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/member/logingplus',
    'handler' => ['Controllers\MemberController', 'logingplusAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/location/regions',
    'handler' => ['Controllers\LocationController', 'regionsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/location/selectstate',
    'handler' => ['Controllers\LocationController', 'selectstateAction'],
    'authentication' => FALSE
];

/*backend - ORDER*/
$routes[] = [
    'method' => 'get',
    'route' => '/order/getproducts',
    'handler' => ['Controllers\OrderController', 'getproductsAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/getaccountinfo/{email}',
    'handler' => ['Controllers\OrderController', 'getaccountinfAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/order/offline/add/{centerid}',
    'handler' => ['Controllers\OrderController', 'offlineorderAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/_testimonies/index/list/{offset}',
    'handler' => ['Controllers\TestimoniesController', '_indexListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/_testimonies/searchresult/list/{keyword}/{offset}',
    'handler' => ['Controllers\TestimoniesController', '_searchResultListAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/_testimonies/details/{ssid}',
    'handler' => ['Controllers\TestimoniesController', '_detailsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/order/list/{num}/{off}/{keyword}/{filter}',
    'handler' => ['Controllers\OrderController', 'orderlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/get/{id}',
    'handler' => ['Controllers\OrderController', 'getorderAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/order/comment/add',
    'handler' => ['Controllers\OrderController', 'addcommentAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/order/updatestatus',
    'handler' => ['Controllers\OrderController', 'updatestatusAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/order/getreport',
    'handler' => ['Controllers\OrderController', 'getreportAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// front end shop
$routes[] = [
    'method' => 'post',
    'route' => '/shop/get',
    'handler' => ['Controllers\OrderController', 'getshopAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/shop/getproduct/{slug}',
    'handler' => ['Controllers\OrderController', 'getproductAction'],
    'authentication' => FALSE
];

// PRESS PAGE
$routes[] = [
    'method' => 'post',
    'route' => '/presspage/create',
    'handler' => ['Controllers\PresspageController', 'createNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/presspage/managenews/{num}/{off}/{keyword}',
    'handler' => ['Controllers\PresspageController', 'manageNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/presspage/newsedit/{newsid}',
    'handler' => ['Controllers\PresspageController', 'newseditoAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/read/presspage/{slug}',
    'handler' => ['Controllers\PresspageController', 'readPressAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/presspage/edit',
    'handler' => ['Controllers\PresspageController', 'editNewsAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/pressgage/delete/{newsid}',
    'handler' => ['Controllers\PresspageController', 'newscenterdeleteAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/press/savebanner',
    'handler' => ['Controllers\PresspageController', 'saveVidBannerAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/press/bannersave',
    'handler' => ['Controllers\PresspageController', 'saveBannerAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/press/logosave',
    'handler' => ['Controllers\PresspageController', 'saveLogoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/press/listbanner',
    'handler' => ['Controllers\PresspageController', 'listBannerAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/press/listlogo',
    'handler' => ['Controllers\PresspageController', 'listLogoAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
 'method' => 'get',
 'route' => '/press/deletebanner/{id}',
 'handler' => ['Controllers\PresspageController', 'deleteBannerAction'],
 'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
 'method' => 'get',
 'route' => '/press/deletelogo/{id}',
 'handler' => ['Controllers\PresspageController', 'deleteLogoAction'],
 'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/loadpress/{page}',
    'handler' => ['Controllers\PresspageController', 'loadpressAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/gallery/save',
    'handler' => ['Controllers\SliderController', 'saveSlideAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/gallery/list',
    'handler' => ['Controllers\SliderController', 'imglistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/gallery/delete',
    'handler' => ['Controllers\SliderController', 'deleteAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/gallery/updateorder',
    'handler' => ['Controllers\SliderController', 'updateorderAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/gallery/updateUrl',
    'handler' => ['Controllers\SliderController', 'updateurlAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];


$routes[] = [
    'method' => 'post',
    'route' => '/shop/addtocart',
    'handler' => ['Controllers\OrderController', 'addtocartAction'],
    'authentication' => TRUE,
    'level' => 'member'
];
$routes[] = [
    'method' => 'get',
    'route' => '/shop/getmemberbag/{id}',
    'handler' => ['Controllers\OrderController', 'getmemeberbagAction'],
    'authentication' => TRUE,
    'level' => 'member'
];
$routes[] = [
    'method' => 'post',
    'route' => '/shop/getbag',
    'handler' => ['Controllers\OrderController', 'getbagAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/shop/removefrombag/{id}',
    'handler' => ['Controllers\OrderController', 'removefrombagAction'],
    'authentication' => TRUE,
    'level' => 'member'
];
$routes[] = [
    'method' => 'post',
    'route' => '/shop/addmultipletocart/{id}',
    'handler' => ['Controllers\OrderController', 'addmultipletocartAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/shop/updateQuantity/{id}/{quantity}',
    'handler' => ['Controllers\OrderController', 'updateqtyAction'],
    'authentication' => TRUE,
    'level' => 'member'
];
$routes[] = [
    'method' => 'get',
    'route' => '/shop/getbillingaddr/{id}',
    'handler' => ['Controllers\OrderController', 'getbillingaddrAction'],
    'authentication' => TRUE,
    'level' => 'member'
];
$routes[] = [
    'method' => 'post',
    'route' => '/shop/placeorder/{id}',
    'handler' => ['Controllers\OrderController', 'placeorderAction'],
    'authentication' => TRUE,
    'level' => 'member'
];
$routes[] = [
    'method' => 'get',
    'route' => '/_workshops/index',
    'handler' => ['Controllers\WorkshopController', '_workshopsIndexAction'],
    'authentication' => FALSE,
];
$routes[] = [
    'method' => 'post',
    'route' => '/_workshops/findworkshop',
    'handler' => ['Controllers\WorkshopController', '_workshopsFindWorkshopAction'],
    'authentication' => FALSE,
];
$routes[] = [
    'method' => 'post',
    'route' => '/_workshops/thisworkshop',
    'handler' => ['Controllers\WorkshopController', '_workshopsThisWorkshopAction'],
    'authentication' => FALSE,
];
$routes[] = [
    'method' => 'get',
    'route' => '/_workshops/titlecheck/{title}',
    'handler' => ['Controllers\WorkshopController', '_workshopsTitleCheckAction'],
    'authentication' => FALSE,
];
$routes[] = [
    'method' => 'post',
    'route' => '/_workshops/submitpayment',
    'handler' => ['Controllers\WorkshopController', '_workshopsSubmitPaymentAction'],
    'authentication' => FALSE,
];

$routes[] = [
    'method' => 'post',
    'route' => '/shop/savepaypaltemp',
    'handler' => ['Controllers\OrderController', 'savepaypaltempAction'],
    'authentication' => TRUE,
    'level' => 'member'
];
$routes[] = [
    'method' => 'post',
    'route' => '/shop/savebillinginfo/{id}',
    'handler' => ['Controllers\OrderController', 'savebillinginfoAction'],
    'authentication' => TRUE,
    'level' => 'member'
];

//backend temp
$routes[] = [
    'method' => 'get',
    'route' => '/be/allintrosession/unverified/{num}/{off}/{keyword}',
    'handler' => ['Controllers\GetstartedController', 'allintrosessionunverifiedlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/be/allintrosession/loadcanceledclasslist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\GetstartedController', 'allintrosessioncanceledlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/loadunverifiedsession/{sessionid}',
    'handler' => ['Controllers\GetstartedController', 'loadunverifiedsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/loadcanceledsession/{sessionid}',
    'handler' => ['Controllers\GetstartedController', 'loadcanceledsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/be/getstarted/verifysession',
    'handler' => ['Controllers\GetstartedController', 'verifysessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/be/allgroupclasssession/allgroupclasssessionunverfied/{num}/{off}/{keyword}',
    'handler' => ['Controllers\GetstartedController', 'allgroupclasssessionunverifiedlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/be/allgroupclasssession/allgroupclasssessioncanceledlist/{num}/{off}/{keyword}',
    'handler' => ['Controllers\GetstartedController', 'allgroupclasssessioncanceledlistAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/loadunverifiedgroupsession/{sessionid}',
    'handler' => ['Controllers\GetstartedController', 'loadunverifiedgroupsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/be/getstarted/loadcanceledgroupsession/{sessionid}',
    'handler' => ['Controllers\GetstartedController', 'loadcanceledgroupsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/be/getstarted/verifygroupsession',
    'handler' => ['Controllers\GetstartedController', 'verifygroupsessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/be/getstarted/verifybeneplacesession',
    'handler' => ['Controllers\BeneplaceController', 'verifybeneplacesessionAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/contactus/centerlist',
    'handler' => ['Controllers\ContactusController', 'centerlistAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/contactus/centercontact',
    'handler' => ['Controllers\ContactusController', 'centercontactAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/presspage/changestatus',
    'handler' => ['Controllers\PresspageController', 'changestatusAction'],
    'authentication' => FALSE
];

// frontent shop slider
$routes[] = [
    'method' => 'get',
    'route' => '/shop/slider',
    'handler' => ['Controllers\SliderController', 'getshopsliderAction'],
    'authentication' => FALSE
];
// frontent home slider
$routes[] = [
    'method' => 'get',
    'route' => '/home/slider',
    'handler' => ['Controllers\SliderController', 'gethomesliderAction'],
    'authentication' => FALSE
];

//mssql routes
$routes[] = [
   'method' => 'get',
   'route' => '/bmrnet/addmembertomailchimp',
   'handler' => ['Controllers\BmrnetController', 'addmembertomailchimpAction'],
   'authentication' => FALSE
];
//deleteoldnotifications
$routes[] = [
    'method' => 'get',
    'route' => '/notification/deleteoldnotiAction',
    'handler' => ['Controllers\NotificationController', 'deleteoldnotiAction'],
    'authentication' => FALSE
];

//export auditlog
$routes[] = [
    'method' => 'get',
    'route' => '/auditlog/exportcsv',
    'handler' => ['Controllers\AuditlogController', 'exporttableAction'],
    'authentication' => FALSE
];

// press thumbnail
$routes[] = [
    'method' => 'get',
    'route' => '/press/listthumbnail',
    'handler' => ['Controllers\PresspageController', 'listthumbnailAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/press/thumbnailsave',
    'handler' => ['Controllers\PresspageController', 'savethumbnailAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/press/deletethumbnail/{id}',
    'handler' => ['Controllers\PresspageController', 'deletethumbnailAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/press/savethumbnailvid',
    'handler' => ['Controllers\PresspageController', 'savethumbnailvidAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/be/auditlogs/deletecsvfile/{fileid}',
    'handler' => ['Controllers\AuditlogController', 'deletecsvfileAction'],
    'authentication' => FALSE
];

// <<<<<<< ROUTES FOR BANNER MODULE
$routes[] = [
    'method' => 'post',
    'route' => '/save/image',
    'handler' => ['Controllers\UniversalController', 'saveImageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/loadimages/{page}',
    'handler' => ['Controllers\UniversalController', 'loadImagesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/removeimage/{id}',
    'handler' => ['Controllers\UniversalController', 'removeImageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/editimage/{id}',
    'handler' => ['Controllers\UniversalController', 'editImageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'post',
    'route' => '/update/image',
    'handler' => ['Controllers\UniversalController', 'updateImageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];
$routes[] = [
    'method' => 'get',
    'route' => '/togglestatus/image/{id}',
    'handler' => ['Controllers\UniversalController', 'toggleStatusImageAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/loadsinglebanner/image/{page}',
    'handler' => ['Controllers\UniversalController', 'loadSingleBannerImageAction'],
    'authentication' => FALSE,
];

$routes[] = [
    'method' => 'get',
    'route' => '/page/loadimages/{pageslug}',
    'handler' => ['Controllers\PagesController', 'loadbannerAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/slider/updateorder',
    'handler' => ["Controllers\UniversalController", 'updateorderAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

// END OF ROUTES FOR BANNER MODULE >>>>>>>

$routes[] = [
    'method' => 'get',
    'route' => '/news/setThumbnail',
    'handler' => ['Controllers\NewsController', 'setThumbnailAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/locations/index',
    'handler' => ['Controllers\LocationsController', 'feLocationsIndexAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/tryaclass/index',
    'handler' => ['Controllers\GetstartedController', 'feTryAClassIndexAction'],
    'authentication' => FALSE
];

$routes[] = [
  'method' => 'get',
  'route' => '/fe/{centerslugs}/newspost/{centernewsslugs}',
  'handler' => ['Controllers\LocationController', 'feCenternewsAction'],
  'authentication' => FALSE
];

$routes[] = [
  'method' => 'get',
  'route' => '/fe/{centerslugs}/fullnews',
  'handler' => ['Controllers\LocationController', 'feCenterFullnewsAction'],
  'authentication' => FALSE
];

$routes[] = [
  'method' => 'get',
  'route' => '/fe/{centerslugs}/fulltestimonials/{offset}',
  'handler' => ['Controllers\LocationController', 'feCenterFulltestimonialsAction'],
  'authentication' => FALSE
];

$routes[] = [
  'method' => 'post',
  'route' => '/fe/contactus/sendmail',
  'handler' => ['Controllers\ContactusController', 'sendmailAction'],
  'authentication' => FALSE
];

// $routes[] = [
//   'method' => 'post',
//   'route' => '/esign',
//   'handler' => ['Controllers\UniversalController', 'esignAction'],
//   'authentication' => FALSE
// ];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/membership',
    'handler' => ['Controllers\LocationController', 'femembershipAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/getevents/{keyword}',
    'handler' => ['Controllers\LocationController', 'fegeteventsAction'],
    'authentication' => FALSE
];

$routes[] = [
    "method" => "post",
    "route" => "/account/personalinfo/update",
    "handler" => ["Controllers\MemberController","account_personalinfo_update"],
    "authentication" => FALSE
];

$routes[] = [
    'method' => 'get',
    'route' => '/fe/getcurrentuser/{memberid}',
    'handler' => ['Controllers\MemberController', 'feGetcurrentuserAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/account/resetpassword',
    'handler' => ['Controllers\MemberController', 'feResetPasswordAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/account/forgotpassword',
    'handler' => ['Controllers\MemberController', 'feForgotPasswordAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'get',
    'route' => '/changepassword/validate/{requestcode}',
    'handler' => ['Controllers\MemberController', 'feChangepasswordValidateAction'],
    'authentication' => FALSE
];
$routes[] = [
    'method' => 'post',
    'route' => '/changepassword',
    'handler' => ['Controllers\MemberController', 'feChangepasswordAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/fe/viewsched',
    'handler' => ['Controllers\CenterController', 'viewschedAction'],
    'authentication' => FALSE
];

$routes[] = [
    'method' => 'post',
    'route' => '/centermanager/mycenter',
    'handler' => ['Controllers\CenterController', 'centermanageMycenterAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/class/validate/{id}/{class}',
    'handler' => ['Controllers\CenterController', 'validateClassAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/class/save',
    'handler' => ['Controllers\CenterController', 'saveclassAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/class/get/{num}/{off}/{keyword}',
    'handler' => ['Controllers\CenterController', 'getclassesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/class/update',
    'handler' => ['Controllers\CenterController', 'updateclassAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/class/delete',
    'handler' => ['Controllers\CenterController', 'deleteclassAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'get',
    'route' => '/center/getclasses',
    'handler' => ['Controllers\CenterController', 'getcenterclassesAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/workshop/new/title',
    'handler' => ['Controllers\WorkshopController', 'newwstitleAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

$routes[] = [
    'method' => 'post',
    'route' => '/settings/updatesociallink',
    'handler' => ['Controllers\SettingsController', 'updatesociallinkAction'],
    'authentication' => TRUE,
    'level' => 'admin'
];

return $routes;
