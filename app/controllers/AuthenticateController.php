<?php

namespace Controllers;

use \Controllers\ControllerBase as CB;

class AuthenticateController extends \Phalcon\Mvc\Controller {

    public function getAccessTokenAction() {


        $request = new \Phalcon\Http\Request();

        $jwt = new \Security\Jwt\JWT();

        $app = new CB;

        $parsetoken = explode(" ",$request->getHeader('Authorization'));

        $token = $jwt->decode($parsetoken[1], $app->config->hashkey, array('HS256'));
        echo json_encode($token);

    }

    public function skipAction($name) {
        echo "auth skipped ($name)";
    }
}
