<?php

namespace Controllers;

use \Models\Auditlog as Auditlog;
use \Models\Auditlogexportedcsv as Auditlogexportedcsv;
use \Controllers\ControllerBase as CB;

class AuditlogController extends \Phalcon\Mvc\Controller {

	public function auditloglistAction($num, $page, $keyword){
		if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM auditlog LEFT JOIN users ON auditlog.userid=users.id  ORDER BY auditlog.datetime DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM auditlog LEFT JOIN users on auditlog.userid=users.id");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totallogs = count($searchresult1);
        } 
        else {

        	$offsetfinal = ($page * 10) - 10;

        	$db = \Phalcon\DI::getDefault()->get('db');
        	$stmt = $db->prepare("SELECT * FROM auditlog LEFT JOIN users ON auditlog.userid=users.id WHERE users.username LIKE '%".$keyword."%' or auditlog.module LIKE '%".$keyword."%' or auditlog.event LIKE '%".$keyword."%' or auditlog.title LIKE '%".$keyword."%' ORDER BY auditlog.datetime DESC LIMIT " . $offsetfinal . ",10");

        	$stmt->execute();
        	$searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


        	$db1 = \Phalcon\DI::getDefault()->get('db');
        	$stmt1 = $db1->prepare("SELECT * FROM auditlog LEFT JOIN users on auditlog.userid=users.id WHERE users.username LIKE '%".$keyword."%' or auditlog.module LIKE '%".$keyword."%' or auditlog.event LIKE '%".$keyword."%' or auditlog.title LIKE '%".$keyword."%'");

        	$stmt1->execute();
        	$searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

        	$totallogs = count($searchresult1);

         
    
        }

      
        echo json_encode(array('loglist' => $searchresult, 'index' =>$page, 'total_items' => $totallogs));
	}

  public function auditloglistcsvAction($num, $page, $keyword){
    if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM auditlogexportedcsv ORDER BY datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM auditlogexportedcsv");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totallogs = count($searchresult1);
        } 
        else {

          $offsetfinal = ($page * 10) - 10;

          $db = \Phalcon\DI::getDefault()->get('db');
          $stmt = $db->prepare("SELECT * FROM auditlogexportedcsv WHERE filename LIKE '%".$keyword."%' or datecreated LIKE '%".$keyword."%' ORDER BY datecreated DESC LIMIT " . $offsetfinal . ",10");

          $stmt->execute();
          $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


          $db1 = \Phalcon\DI::getDefault()->get('db');
          $stmt1 = $db1->prepare("SELECT * FROM auditlogexportedcsv WHERE filename LIKE '%".$keyword."%' or datecreated LIKE '%".$keyword."%'");

          $stmt1->execute();
          $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

          $totallogs = count($searchresult1);

         
    
        }

      
        echo json_encode(array('loglist' => $searchresult, 'index' =>$page, 'total_items' => $totallogs));
  }

  public function exporttableAction(){

    $csvfilename = '';
    $csvfilelocation = 'csvfiles/';


    $getlatestauditlogexportedcsv = CB::bnbQuery("SELECT * FROM auditlogexportedcsv ORDER BY datecreated DESC");

    if($getlatestauditlogexportedcsv){
      $csvfilename = $getlatestauditlogexportedcsv[0]['datecreated']."_to_".date("Y-m-d").'_auditlog.csv';
    }
    else{
       $csvfilename = date("Y-m-d")."_to_".date("Y-m-d").'_auditlog.csv';
    }

    $auditlogs = CB::bnbQuery("SELECT * FROM auditlog");

    $output = fopen($csvfilelocation.$csvfilename, 'w');
    
    fputcsv($output, array('logid', 'datetime', 'userid', ' module', 'event', 'title'));

    foreach ($auditlogs as $auditlogs) {
      fputcsv($output, $auditlogs);
    }

    fclose($output);

    $guid = new \Utilities\Guid\Guid();
    $id = $guid->GUID();

    $saveauditlogexportedcsv = new Auditlogexportedcsv();
    $saveauditlogexportedcsv->fileid = $id;
    $saveauditlogexportedcsv->filename = $csvfilename;
    $saveauditlogexportedcsv->datecreated = date("Y-m-d");

    if($saveauditlogexportedcsv->save()){
      if (file_exists($csvfilelocation.$csvfilename)) {
        $truncatetauditlogs = CB::bnbQuerydelete("TRUNCATE auditlog");
      }
    }

  }

  public function deletecsvfileAction($fileid){

    $csvfilelocation = 'csvfiles/';

    $checkfile = Auditlogexportedcsv::findFirst("fileid='" . $fileid . "'");
    if($checkfile){
      if($checkfile->delete()){
        if (file_exists($csvfilelocation.$checkfile->filename)) {
          unlink($csvfilelocation.$checkfile->filename);
          $data['status'] = 'success';
          $data['msg'] = 'File Deleted!';
        }
      }
      else{
        $data['status'] = 'danger';
        $data['msg'] = 'Something went wrong please try again later!';
      }
      
    }
    else{
      $data['status'] = 'danger';
      $data['msg'] = 'Something went wrong please try again later!';
    }

    echo json_encode($data);
  }
	
	
}
