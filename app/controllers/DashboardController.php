<?php

namespace Controllers;

use \Models\News as News;
use \Models\Centercalendar as Centercalendar;
use \Models\Groupclassintrosession as Groupclassintrosession;
use \Models\Centerlocation as Centerlocation;
use \Models\Beneplace as Beneplace;
use \Models\Centeremail as Centeremail;
use \Models\Introsession as Introsession;
use \Models\Centernews as Centernews;
use \Models\Centerdistrict as Centerdistrict;
use \Models\Centerregion as Centerregion;
use \Models\Centerpricingregfee as Centerpricingregfee;
use \Models\Centerphonenumber as Centerphonenumber;
use \Models\Centermembership as Centermembership;
use \Models\Centerpricingregclass as Centerpricingregclass;
use \Models\Centerpricingsessionfee as Centerpricingsessionfee;
use \Models\Centerhours as Centerhours;
use \Models\Centersociallinks as Centersociallinks;
use \Models\Center as Center;
use \Models\Centerimages as Centerimages;
use \Models\Centeroffer as Centeroffer;
use \Models\Users as Users;
use \Models\States as States;
use \Models\Centersession1 as Centersession1;
use \Models\Centersession2 as Centersession2;
use \Models\Centersession3 as Centersession3;
use \Models\Centersession4 as Centersession4;
use \Models\Cities as Cities;
use \Models\Cities_extended as Cities_extended;
use \Models\Testimonies as Testimonies;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class DashboardController extends \Phalcon\Mvc\Controller {

	public function counttotalpostAction(){
		$datenow = date("Y-m-d");
		$db = \Phalcon\DI::getDefault()->get('db');
		$stmt = $db->prepare("
			select * from 
			(
				select title as title from centernews WHERE date <= '".$datenow."'
				UNION
				select title as title from news WHERE date <= '".$datenow."'
			) 
		as news");

		$stmt->execute();
		$searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		echo json_encode(array('totalcount'=>count($searchresult)));
	}

}
