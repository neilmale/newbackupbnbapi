<?php

namespace Controllers;

use \Models\News as News;
use \Models\Notification as Notification;
use \Models\Centercalendar as Centercalendar;
use \Models\Groupclassintrosession as Groupclassintrosession;
use \Models\Centerlocation as Centerlocation;
use \Models\Beneplace as Beneplace;
use \Models\Centeremail as Centeremail;
use \Models\Introsession as Introsession;
use \Models\Centernews as Centernews;
use \Models\Centerdistrict as Centerdistrict;
use \Models\Centerregion as Centerregion;
use \Models\Centerpricingregfee as Centerpricingregfee;
use \Models\Centerphonenumber as Centerphonenumber;
use \Models\Centermembership as Centermembership;
use \Models\Centerpricingregclass as Centerpricingregclass;
use \Models\Centerpricingsessionfee as Centerpricingsessionfee;
use \Models\Centerhours as Centerhours;
use \Models\Centersociallinks as Centersociallinks;
use \Models\Center as Center;
use \Models\Centerimages as Centerimages;
use \Models\Centeroffer as Centeroffer;
use \Models\Users as Users;
use \Models\States as States;
use \Models\Centersession1 as Centersession1;
use \Models\Centersession2 as Centersession2;
use \Models\Centersession3 as Centersession3;
use \Models\Centersession4 as Centersession4;
use \Models\Cities as Cities;
use \Models\Cities_extended as Cities_extended;
use \Models\Testimonies as Testimonies;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class CenterlocationController extends \Phalcon\Mvc\Controller {

    public function searchlocationAction(){
        $request = new \Phalcon\Http\Request();
        $cb = new CB;
        
        if($request->isPost()){


            $state = $request->getPost('state');
            $city = $request->getPost('city');
            $zip = $request->getPost('zip');

            if($zip != "" && $city == "" && $state == "")
            {
                $zipfrom = Cities_extended::findFirst('zip = "'.$zip.'"');
                if($zipfrom){
                   $searchresult = array();
                   $db = \Phalcon\DI::getDefault()->get('db');
                   $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.status = 1");

                   $stmt->execute();
                   $center = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                    $getdis = array();
                    $data = array();
                   foreach ($center as $key) {
                      if( $cb->distance($key['lat'], $key['lon'], $zipfrom->latitude, $zipfrom->longitude, 'Miles')<= 50){
                        $getdis[] = $key['centerid'];
                      }
                   }

                   $fsearchresult = array();
                   foreach ($getdis as $centerid) {
                      
                        $db = \Phalcon\DI::getDefault()->get('db');
                        $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerid = '".$centerid."' AND center.status = 1");

                        $stmt->execute();
                        $ssearchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                        foreach ($ssearchresult as $key) {
                      
                       
                            $data[] = array(
                              "centerid" => $key['centerid'],
                              "centertitle" =>  $key['centertitle'],
                              "centerslugs" =>  $key['centerslugs'],
                              "centerdesc" => $key['centerdesc'],
                              "openingdate" => $key['openingdate'],
                              "centerregion" =>$key['centerregion'],
                              "centerdistrict" => $key['centerdistrict'],
                              "centeraddress" => $key['centeraddress'],
                              "centerstate" => $key['centerstate'],
                              "centercity" => $key['centercity'],
                              "centerzip" => $key['centerzip'],
                              "centertype" => $key['centertype'],
                              "metatitle" => $key['metatitle'],
                              "metadesc" => $key['metadesc'],
                              "centermanager" => $key['centermanager'],
                              "centertype" => $key['centertype'],
                              "status" => $key['status'],
                              "centertelnumber" => $key['centertelnumber'],
                              "authorizeid" => $key['authorizeid'],
                              "authorizekey" => $key['authorizekey'],
                              "paypalid" => $key['paypalid'],
                              "origsessionprice" => $key['origsessionprice'],
                              "dissessionprice" => $key['dissessionprice'],
                              "origgroupprice" => $key['origgroupprice'],
                              "disgroupprice" => $key['disgroupprice'],
                              "orig1monthprice" => $key['orig1monthprice'],
                              "dis1monthprice" => $key['dis1monthprice'],
                              "orig3monthprice" => $key['orig3monthprice'],
                              "dis3monthprice" => $key['dis3monthprice'],
                              "datecreated" => $key['datecreated'],
                              "dateedited" => $key['dateedited'],
                              "locationid" => $key['locationid'],
                              "lat" =>$key['lat'],
                              "lon" => $key['lon'],
                              "phonenumber" => $key['phonenumber'],
                              "email" => $key['email'],
                              "fdistance" => intval($cb->distance($key['lat'], $key['lon'], $zipfrom->latitude, $zipfrom->longitude, 'Miles'))
                            );
                         }
                    }

                   $searchresult =  $data;
                }
            }
            else{

              $exploded = explode(",",$city);
              $city = strtolower($exploded[0]);
              $state = strtoupper(trim($exploded[1]));
             

            $db = \Phalcon\DI::getDefault()->get('db');
            $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerstate Like '%".$state."%' AND center.centercity Like '%".$city."%' AND center.centerzip Like '%".$zip."%' AND center.status = 1");

            $stmt->execute();
            $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            }
        }
        echo json_encode($searchresult);
    }

    
   
}