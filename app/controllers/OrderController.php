<?php

namespace Controllers;

use \Models\Product as Product;
use \Models\Members as Members;
use \Models\Orderlist as Orderlist;
use \Models\Billinginfo as Billinginfo;
use \Models\Shippinginfo as Shippinginfo;
use \Models\Orderprod as Orderprod;
use \Models\Ordercomment as Ordercomment;
use \Models\Shopbag as Shopbag;
use \Models\Ordertemp as Ordertemp;
use \Models\Orderprodtemp as Orderprodtemp;
use \Models\Prodcat as Prodcat;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class OrderController extends \Phalcon\Mvc\Controller {
  
  public function getproductsAction(){
    $app = new CB();
    $p = $app->bnbQuery("SELECT 
      name,
      price,
      quantity,
      discount,
      discount_from,
      discount_to,
      minquantity,
      maxquantity,
      productid FROM product WHERE quantity>0");

    if($p){
      echo json_encode($p, JSON_NUMERIC_CHECK);
    }
  }

  public function getaccountinfAction($email) {
    $app = new CB();
    $m = $app->bnbQuery("SELECT * FROM members WHERE email='$email'");
    if($m){
      $m = $app->bnbQuery("SELECT * FROM billinginfo WHERE memberid='" . $m[0]['memberid'] . "'");
      echo json_encode($m[0]);
    }
  }

  public function offlineorderAction($centerid) {
    $app = new CB();
    $request = new \Phalcon\Http\Request();
    
    if($request->isPost()){
      try {
        $transactionManager = new TransactionManager();
        $transaction = $transactionManager->get();
        $invoiceno = hexdec(substr(uniqid(),0,9));
                
        $m = Members::findFirst("email='" . $request->getPost('email') . "'");
        if(!$m){
          $m = new Members();
          $m->setTransaction($transaction);
          $guid = new \Utilities\Guid\Guid();
          $m->assign(array(
            'memberid' => $guid->GUID(),
            'email' => $request->getPost('billinginfo')['email'],
            'firstname' => $request->getPost('billinginfo')['firstname'],
            'lastname' => $request->getPost('billinginfo')['lastname'],
            'address' => $request->getPost('billinginfo')['address'],
            'address2' => $request->getPost('billinginfo')['address2'],
            'city' => $request->getPost('billinginfo')['city'],
            'country' => $request->getPost('billinginfo')['country'],
            'phoneno' => $request->getPost('billinginfo')['phoneno'],
            'state' => $request->getPost('billinginfo')['state'],
            'zipcode' => $request->getPost('billinginfo')['zipcode'],
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
             ));
          if ($m->save() == false) {
            $data = [];
            foreach ($m->getMessages() as $message) {
              $data[] = $message;
            }
            $transaction->rollback();
          }
        }

        $b = Billinginfo::findFirst("memberid='" . $m->memberid . "'");
        if(!$b){
          $b = new Billinginfo();
          $b->setTransaction($transaction);
          $guid = new \Utilities\Guid\Guid();
          $b->assign(array(
              'id' => $guid->GUID(),
              'memberid' => $m->memberid,
              'firstname' => $request->getPost('billinginfo')['firstname'],
              'lastname' => $request->getPost('billinginfo')['lastname'],
              'address' => $request->getPost('billinginfo')['address'],
              'address2' => $request->getPost('billinginfo')['address2'],
              'city' => $request->getPost('billinginfo')['city'],
              'country' => $request->getPost('billinginfo')['country'],
              'phoneno' => $request->getPost('billinginfo')['phoneno'],
              'state' => $request->getPost('billinginfo')['state'],
              'zipcode' => $request->getPost('billinginfo')['zipcode'],
          ));
          if($b->save() == false){
            $data = [];
            foreach ($b->getMessages() as $message) {
              $data[] = $message;
            }
            $transaction->rollback();
          }
        }

        $o = new Orderlist();
        $o->setTransaction($transaction);
        $guid = new \Utilities\Guid\Guid();
        $amount = $request->getPost('totalamount');
        if($request->getPost('shippinginfo')['firstname']!=''){
          $amount = $amount + $request->getPost('shippingfee');
        }
        $o->assign(array(
          'id' => $guid->GUID(),
          'invoiceno' => $invoiceno,
          'memberid' => $m->memberid,
          'amount' => $amount,
          'paymenttype'=> $request->getPost('paymenttype'),
          'status' => $request->getPost('shippinginfo')['firstname']!='' ? 'processing' :'completed',
          'created_at' => date("Y-m-d H:i:s"),
          'firstname' => $request->getPost('billinginfo')['firstname'],
          'lastname' => $request->getPost('billinginfo')['lastname'],
          'address' => $request->getPost('billinginfo')['address'],
          'address2' => $request->getPost('billinginfo')['address2'],
          'city' => $request->getPost('billinginfo')['city'],
          'country' => $request->getPost('billinginfo')['country'],
          'phoneno' => $request->getPost('billinginfo')['phoneno'],
          'state' => $request->getPost('billinginfo')['state'],
          'zipcode' => $request->getPost('billinginfo')['zipcode']
          ));
        if($o->save() == false){
          $data = [];
          foreach ($o->getMessages() as $message) {
            $data[] = $message; 
          }
          $transaction->rollback();
        }

        if($request->getPost('shippinginfo')['firstname']!=''){
          $s = new Shippinginfo();
          $s->setTransaction($transaction);
          $guid = new \Utilities\Guid\Guid();
          $s->assign(array(
            'id' => $guid->GUID(),
            'orderid' => $o->id,
            'firstname' => $request->getPost('shippinginfo')['firstname'],
            'lastname' => $request->getPost('shippinginfo')['lastname'],
            'address' => $request->getPost('shippinginfo')['address'],
            'address2' => $request->getPost('shippinginfo')['address2'],
            'city' => $request->getPost('shippinginfo')['city'],
            'country' => $request->getPost('shippinginfo')['country'],
            'phoneno' => $request->getPost('shippinginfo')['phoneno'],
            'state' => $request->getPost('shippinginfo')['state'],
            'zipcode' => $request->getPost('shippinginfo')['zipcode'],
            ));
          if($s->save() == false){
            $data = [];
            foreach ($s->getMessages() as $message) {
              $data[] = $message; 
            }
            $transaction->rollback();
          }
        }

        foreach ($request->getPost('order') as $key => $value) {
          $p = Product::findFirst("productid='" . $value['productid'] . "'");
          if($p->quantity >= $value['orderquantity']){
            $p->setTransaction($transaction);
            $p->quantity = $p->quantity - $value['orderquantity'];
            if($p->save() == false){
              $data = [];
              foreach ($op->getMessages() as $message) {
                $data[] = $message; 
              }
              $transaction->rollback();
            }

            $op = new Orderprod();
            $op->setTransaction($transaction);
            $guid = new \Utilities\Guid\Guid();
            $op->assign(array(
              'id' => $guid->GUID(),
              'orderid' => $o->id,
              'productid' => $value['productid'],
              'quantity' => $value['orderquantity'] 
              ));
            if($op->save() == false){
              $data = [];
              foreach ($op->getMessages() as $message) {
                $data[] = $message; 
              }
              $transaction->rollback();
            }
          }
        }

        $transaction->commit();

        if($request->getPost('paymenttype') != 'cash'){          
          $app->creditcardPayment($request->getPost('billinginfo'), $request->getPost('payment'), $request->getPost('paymenttype'), $request->getPost('shippingfee'), $request->getPost('tax'), $request->getPost('totalamount'), $invoiceno, $centerid, 'Offline Order');
        }

        if(isset($data['success']) || $request->getPost('paymenttype') == 'cash'){
          $body = "<p>Order Information</p>";
          $body .= "<p>Invoice no. ".$o->invoiceno."</p>";
          $body .= "<p>Total amount : " . $amount . "</p>";
          $body .= "<p class='text-bold'>Billing Information</p>";
          $body .= "<p>Name : " . $request->getPost('billinginfo')['lastname'] . ", "  . $request->getPost('billinginfo')['firstname'] . "</p>";
          $body .= "<p>Address : " . $request->getPost('billinginfo')['address'] . " </p>";
          $body .= "<p>Phone no. : " . $request->getPost('billinginfo')['phoneno'] . " </p>";
          $body .= "<table style='width:100%; text-align: center;'>
                      <thead>
                          <tr>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total Price</th>
                          </tr>
                      </thead>
                      <tbody>";
          foreach ($request->getPost('order') as $key => $value) {
            $body .= "<tr>";
            $body .= "<td>" . $value['name'] . "</td>";
            $body .= "<td>" . $value['price'] . "</td>";
            $body .= "<td>" . $value['orderquantity'] . "</td>";
            $body .= "<td>" . $value['total'] . "</td>";
            $body .= "</tr>";
          }
                        
          $body .= "</tbody>
                  </table>";

          if($request->getPost('shippinginfo')['firstname']!=''){
            $body .= "<p class='text-bold'>Shipping Information</p>";
            $body .= "<p>Name : " . $request->getPost('shippinginfo')['lastname'] . ", "  . $request->getPost('shippinginfo')['firstname'] . "</p>";
            $body .= "<p>Address : " . $request->getPost('shippinginfo')['address'] . " </p>";
            $body .= "<p>Phone no. : " . $request->getPost('shippinginfo')['phoneno'] . " </p>";
          }
          $app->sendMail($request->getPost('billinginfo')['email'], 'Body and Brain Order Confirmation', $body);
        }
        echo json_encode(array('success' => 'Order has been successfully added' ));

      }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
          die( json_encode(array('401' => $e->getMessage())) );
      }
    }
  }

  public function orderlistAction($offset, $page, $keyword, $filter){
    $app = new CB();
    $offsetfinal = ($page * 10) - 10;

    if($offsetfinal < 0){
      $offsetfinal = 0;
    }

    $sql = "SELECT orderlist.id,
      orderlist.invoiceno,
      orderlist.amount,
      orderlist.status,
      orderlist.created_at,
      orderlist.firstname,
      orderlist.lastname,
      orderlist.paymenttype,
      shippinginfo.firstname AS shipfirstname,
      shippinginfo.lastname AS shiplastname FROM orderlist INNER JOIN members ON orderlist.memberid=members.memberid 
      LEFT JOIN shippinginfo ON orderlist.id=shippinginfo.orderid ";

    $sqlCount = "SELECT COUNT(*) FROM orderlist INNER JOIN members ON orderlist.memberid=members.memberid LEFT JOIN billinginfo on members.memberid=billinginfo.memberid ";

    if($keyword != "null"){
      $sqlWhere = "WHERE orderlist.invoiceno LIKE '%" . $keyword . "%' OR orderlist.amount LIKE '%" . $keyword . "%' OR orderlist.status LIKE '%" . $keyword . "%'";
      $sqlWhere .= " OR orderlist.created_at LIKE '%" . $keyword . "%'";
      $sqlWhere .= " OR orderlist.lastname LIKE '%" . $keyword . "%' OR orderlist.firstname LIKE '%" . $keyword . "%' OR orderlist.paymenttype LIKE '%" . $keyword . "%'";

      $sql .= $sqlWhere;
      $sqlCount .= $sqlWhere;
    }

    if($keyword != "null" && $filter != "null"){
      $sql .= " AND ";
      $sqlCount .= " AND ";
    }else if($keyword == "null" && $filter != "null") {
      $sql .= " WHERE ";
      $sqlCount .= " WHERE ";
    } 

    if($filter != "null"){
      if($filter == 'today'){
        $sql .= "orderlist.created_at LIKE '%" . date('Y-m-d') . "%'";
        $sqlCount .= "orderlist.created_at LIKE '%" . date('Y-m-d') . "%'";
      }else {
        $sql .= "orderlist.status='$filter'";
        $sqlCount .= "orderlist.status='$filter'";
      }
    }

    $sql .= " ORDER BY orderlist.created_at DESC LIMIT " . $offsetfinal . ", 10";
    $searchresult = $app->bnbQuery($sql);
    $total_items = $app->bnbQuery($sqlCount);
    // die(var_dump(array('sql' => $searchresult, 'sqlcount' => $total_items )));

    $total_processing = $app->bnbQuery("SELECT COUNT(*) FROM orderlist WHERE status='processing'");
    $total_completed = $app->bnbQuery("SELECT COUNT(*) FROM orderlist WHERE status='completed'");
    $total_pending = $app->bnbQuery("SELECT COUNT(*) FROM orderlist WHERE status='pending'");
    $total_today = $app->bnbQuery("SELECT COUNT(*) FROM orderlist WHERE created_at LIKE '%" . date('Y-m-d') . "%'");

    echo json_encode(array(
      'orders' => $searchresult, 
      'index' =>$page, 
      'total_items' => $total_items[0]['COUNT(*)'], 
      'processing' => $total_processing[0]['COUNT(*)'],
      'completed' => $total_completed[0]['COUNT(*)'],
      'pending' => $total_pending[0]['COUNT(*)'],
      'today' => $total_today[0]['COUNT(*)']), JSON_NUMERIC_CHECK);
  }

  public function getorderAction($id){
    $app = new CB();
    $o = Orderlist::findFirst("id='$id'");
    if($o){
      $ai = Members::findFirst("memberid='" . $o->memberid . "'");
      $s = Shippinginfo::findFirst("orderid='" . $o->id . "'");
      $c = Ordercomment::find("orderid='" . $o->id . "'");
      $p = $app->bnbQuery("SELECT product.name,
        product.discount,
        product.discount_from,
        product.discount_to,
        product.price,
        orderprod.quantity AS orderquantity,
        orderlist.amount FROM orderlist INNER JOIN orderprod ON orderlist.id=orderprod.orderid INNER JOIN product ON orderprod.productid=product.productid WHERE orderlist.id='" . $o->id . "'");
      echo json_encode(array('order' => $o, 'acctinfo' => $ai, 'shippinginfo' => $s, 'products'=>$p, 'comments' => $c->toArray()), JSON_NUMERIC_CHECK);
    }
  }

  public function addcommentAction() {
    $request = new \Phalcon\Http\Request();
    $app = new CB();

    if($request->isPost()){
      $oc = new Ordercomment();
      $guid = new \Utilities\Guid\Guid();
      $oc->assign(array(
        'id' => $guid->GUID(),
        'comment' => $request->getPost('comment'),
        'status' => $request->getPost('status'),
        'created_at' => date("Y-m-d H:i:s"),
        'orderid' => $request->getPost('id') ));

      if($oc->save() == false){
        $data = [];
        foreach ($oc->getMessages() as $message) {
          $data[] = $message;
        }
      }else {

        $order = $app->bnbQuery("SELECT members.firstname,
            members.lastname,
            members.email FROM orderlist INNER JOIN members ON orderlist.memberid=members.memberid WHERE orderlist.id='" . $request->getPost('id') . "'");

        $body = 'Name: '. $order[0]['firstname'] . ' '. $order[0]['lastname'] . ' <br> Email: '. $order[0]['email'] . ' <br><br> Message: <br><br>'. $request->getPost('comment') . '';
        $app->sendMail($order[0]['email'], 'Body and Brain Order update', $body);

        $c = Ordercomment::find(array("orderid='" . $request->getPost('id') . "'", "ORDER" => 'created_at DESC'));
        echo json_encode($c->toArray(), JSON_NUMERIC_CHECK);
      }
    }
  }

  public function updatestatusAction() {
    $request = new \Phalcon\Http\Request();
    $app = new CB();

    if($request->isPost()){
      $c = Orderlist::findFirst("id='" . $request->getPost('id') . "'");
      if($c){
        $c->status = $request->getPost('status');
        if($c->save() == false){
          $data = [];
          foreach ($c->getMessages() as $message) {
            $data[] = $message;
          }
        } else {
          $data = array('success' => 'success' );
        }

        if($request->getPost('status') == 'processing'){
          $m = Members::findFirst("memberid='$c->memberid'");
          $body = "<p>Order Information</p>";
          $body .= "<p>Invoice no. ".$c->invoiceno."</p>";
          $body .= "<p>Total amount : " . $c->amount . "</p>";
          $body .= "<p class='text-bold'>Billing Information</p>";
          $body .= "<p>Name : " . $c->lastname . ", "  . $c->firstname . "</p>";
          $body .= "<p>Address : " . $c->address . " </p>";
          $body .= "<p>Phone no. : " . $c->phoneno . " </p>";
          $body .= "<h4>Message: Your order has been shipped : " . date("Y-m-d H:i:s") . "</h4>";
          $body .= "<table style='width:100%; text-align: center;'>
                      <thead>
                          <tr>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total Price</th>
                          </tr>
                      </thead>
                      <tbody>";
          $order = $app->bnbQuery("SELECT product.name,
              product.price,
              orderprod.quantity,
              product.discount,
              product.discount_from,
              product.discount_to FROM product INNER JOIN orderprod ON product.productid=orderprod.productid WHERE orderprod.orderid='" . $c->id . "'");
          foreach ($order as $key => $value) {
            $body .= "<tr>";
            $body .= "<td>" . $value['name'] . "</td>";
            if((strtotime($value['discount_from']) <= strtotime(date("Y-m-d H:i:s"))) && (strtotime($value['discount_to']) >=  strtotime(date("Y-m-d H:i:s")))){
              $body .= "<td>" . $value['price'] . "(-" . $value['discount'] . "%) </td>";
            }else {
              $body .= "<td>" . $value['price'] . "</td>";
            }
            $body .= "<td>" . $value['quantity'] . "</td>";
            $body .= "<td>" . $value['total'] . "</td>";
            $body .= "</tr>";
          }
                        
          $body .= "</tbody>
                  </table>";
          $si = Shippinginfo::findFirst("orderid='" . $order->id . "'");
          if($si){
            $body .= "<p class='text-bold'>Shipping Information</p>";
            $body .= "<p>Name : " . $si->lastname . ", "  . $si->firstname . "</p>";
            $body .= "<p>Address : " . $si->address . " </p>";
            $body .= "<p>Phone no. : " . $si->phoneno . " </p>";
          }
           $app->sendMail($m->email, 'Body and Brain Order update', $body);
        }

        echo json_encode($data);
      }
    }
  }

  public function getreportAction() {
    $app = new CB();

    $data = $app->bnbQuery("SELECT orderlist.id,
      orderlist.invoiceno,
      orderlist.amount,
      orderlist.status,
      orderlist.created_at,
      billinginfo.firstname,
      billinginfo.lastname,
      shippinginfo.firstname AS shipfirstname,
      shippinginfo.lastname AS shiplastname FROM orderlist INNER JOIN billinginfo ON orderlist.id=billinginfo.orderid 
      LEFT JOIN shippinginfo ON orderlist.id=shippinginfo.orderid  ORDER BY orderlist.created_at DESC");
    echo json_encode($data);
  }


  // front end shop
  public function getshopAction() {
    $request = new \Phalcon\Http\Request();
    
    if($request->isPost()){
      $app = new CB();

      $featured = $app->bnbQuery("SELECT id, category FROM productcategory");
      foreach ($featured as $key => $value) {
        $featured[$key]['subcategories'] = $app->bnbQuery("SELECT productsubcategory.subcategory, productsubcategory.id FROM prodsubcat INNER JOIN productsubcategory ON prodsubcat.subcategoryid = productsubcategory.id WHERE prodsubcat.categoryid='" . $value['id'] . "'");        
        foreach ($featured[$key]['subcategories'] as $key2 => $value2) {
          $featured[$key]['subcategories'][$key2]['types'] = $app->bnbQuery("SELECT producttype.type FROM prodtype INNER JOIN producttype ON prodtype.typeid = producttype.id WHERE prodtype.subcategoryid='" . $value2['id'] . "'");
        }
      }
      $filter = $request->getPost('filter');
      if($filter){
        $sql = "SELECT product.productid,
          product.name,
          product.shortdesc,
          product.price,
          product.discount,
          product.discount_from,
          product.discount_to,
          product.slugs,
          productimages.filename";

        if($filter == 'category' || $filter == 'subcategory' || $filter == 'type'){
          $sqlselect = ", productcategory.category";
          $sqljoin = "INNER JOIN  prodcat ON product.productid=prodcat.productid INNER JOIN productcategory ON prodcat.categoryid=productcategory.id ";
          $sqlWhere = "AND productcategory.category='" . $request->getPost('category') . "'";
        }

        if($filter == 'subcategory' || $filter == 'type'){
          $sqlselect = ", productsubcategory.subcategory";
          $sqljoin .= "INNER JOIN prodsubcat2 ON product.productid=prodsubcat2.productid INNER JOIN productsubcategory ON prodsubcat2.subcategoryid=productsubcategory.id ";
          $sqlWhere .= " AND productsubcategory.subcategory='" . $request->getPost('subcategory') . "'";
        }

        if($filter == 'type'){
          $sqlselect = ", producttype.type";
          $sqljoin .= "INNER JOIN prodtype2 ON product.productid=prodtype2.productid INNER JOIN producttype ON prodtype2.typeid=producttype.id ";
          $sqlWhere .= " AND producttype.type='" . $request->getPost('type') . "'";
        }

        $sql .= $sqlselect;
        $sql .= " FROM product INNER JOIN productimages ON product.productid=productimages.productid ";
        $sql .= $sqljoin;
        $sql .= "WHERE productimages.status='1' ";
        $sql .= $sqlWhere;
        $sql .= " ORDER BY product.name";
        $p = $app->bnbQuery($sql);
      }else {
        $p = $app->bnbQuery("SELECT product.productid,
          product.name,
          product.shortdesc,
          product.price,
          product.discount,
          product.discount_from,
          product.discount_to,
          product.slugs,
          productimages.filename FROM product INNER JOIN productimages ON product.productid=productimages.productid WHERE productimages.status='1'
          ORDER BY product.name LIMIT 9");
      }

      foreach ($p as $key => $value) {
        $p[$key]['category'] = $app->bnbQuery("SELECT productcategory.category FROM prodcat INNER JOIN productcategory ON prodcat.categoryid=productcategory.id WHERE prodcat.productid='" . $value['productid'] . "'");
      }
      echo json_encode(array('featured' => $featured, 'products' => $p), JSON_NUMERIC_CHECK);
    }
  }

  public function getproductAction($slug) {
    $app = new CB();
    $featured = $app->bnbQuery("SELECT id, category FROM productcategory");
    foreach ($featured as $key => $value) {
      $featured[$key]['subcategories'] = $app->bnbQuery("SELECT productsubcategory.subcategory, productsubcategory.id FROM prodsubcat INNER JOIN productsubcategory ON prodsubcat.subcategoryid = productsubcategory.id WHERE prodsubcat.categoryid='" . $value['id'] . "'");        
      foreach ($featured[$key]['subcategories'] as $key2 => $value2) {
        $featured[$key]['subcategories'][$key2]['types'] = $app->bnbQuery("SELECT producttype.type FROM prodtype INNER JOIN producttype ON prodtype.typeid = producttype.id WHERE prodtype.subcategoryid='" . $value2['id'] . "'");
      }
    }
    $p = $app->bnbQuery("SELECT product.name,
        product.price,
        product.discount,
        product.discount_from,
        product.discount_to,
        product.productid,
        product.minquantity,
        product.shortdesc,
        product.description,
        product.maxquantity FROM product WHERE product.slugs='$slug'");
    $p[0]['images'] = $app->bnbQuery("SELECT filename, status FROM productimages WHERE productid='" . $p[0]['productid'] . "'");

    $cat = Prodcat::findFirst("productid='" . $p[0]['productid'] . "'")->toArray();

    $s = $app->bnbQuery("SELECT product.name,
        product.productid,
        product.slugs,
        productimages.filename,
        productcategory.category FROM product INNER JOIN prodcat ON product.productid=prodcat.productid INNER JOIN productimages ON
        product.productid=productimages.productid INNER JOIN productcategory ON prodcat.categoryid=productcategory.id WHERE prodcat.categoryid='" . $cat['categoryid'] . "' AND productimages.status=1 AND product.productid!='" . $p[0]['productid'] . "' LIMIT 3");
    echo json_encode(array('featured' => $featured, 'product' => $p[0], 'suggestions' => $s), JSON_NUMERIC_CHECK);
  }

  public function addtocartAction() {
    $request = new \Phalcon\Http\Request();
    if($request->isPost()){

      $s = new Shopbag();
      $guid = new \Utilities\Guid\Guid();
      $s->assign(array(
        'id' => $guid->GUID(),
        'productid' => $request->getPost('id'),
        'quantity' => $request->getPost('quantity'),
        'memberid' => $request->getPost('memberid'),
        'created_at' => date("Y-m-d H:i:s")));

      if ($s->save() == false) {
        $data = [];
        foreach ($m->getMessages() as $message) {
          $data[] = $message;
        }
      }else {
        $data = array('success' => 'success' );
      }
      echo json_encode($data);
    }
  }

  public function getbagAction() {
    $request = new \Phalcon\Http\Request();

    if($request->isPost()){
      $app = new CB();
      $data = [];

      foreach ($request->getPost('bag') as $key => $value) {
        if(!$value['bagquantity']){
          $guid = new \Utilities\Guid\Guid();
          $res = $app->bnbQuery("SELECT product.name,
          product.price,
          product.discount,
          product.discount_to,
          product.discount_from,
          product.productid,
          product.quantity,
          product.minquantity,
          product.maxquantity,
          productimages.filename FROM product INNER JOIN productimages ON product.productid=productimages.productid WHERE product.productid='" . $value['id'] . "' AND productimages.status=1");
          $res[0]['bagquantity'] = $value['quantity'];
          $res[0]['id'] = $guid->GUID();
          $data[] = $res[0];
        }else {
          $data[] = $value;
        }
      }

      echo json_encode($data, JSON_NUMERIC_CHECK);
    }
  }

  public function getmemeberbagAction($id) {
    $app = new CB();

    $p = $app->bnbQuery("SELECT product.name,
        product.price,
        product.discount,
        product.discount_to,
        product.discount_from,
        product.productid,
        shopbag.quantity as bagquantity,
        product.quantity,
        product.minquantity,
        product.maxquantity,
        shopbag.id,
        productimages.filename FROM shopbag INNER JOIN product ON shopbag.productid=product.productid INNER JOIN productimages ON product.productid=productimages.productid WHERE shopbag.memberid='" . $id . "' AND productimages.status=1 ORDER BY created_at ASC");

    $b = Billinginfo::findFirst("memberid='$id'");

    echo json_encode(array('bag' => $p, 'billinginfo' => $b), JSON_NUMERIC_CHECK);
  }

  public function removefrombagAction($id) {
    $s = Shopbag::findFirst("id='$id'");

    if($s){
      if($s->delete()){
        echo json_encode(array('success' => 'success' ));
      }
    }
  }

  public function addmultipletocartAction($id) {
    $request = new \Phalcon\Http\Request();

    if($request->isPost()){
      foreach ($request->getPost('bag') as $key => $value) {
        $s = new Shopbag();
        $guid = new \Utilities\Guid\Guid();
        $s->assign(array(
          'id' => $guid->GUID(),
          'productid' => $value['id'],
          'quantity' => $value['quantity'],
          'memberid' => $id,
          'created_at' => date("Y-m-d H:i:s")));

        if ($s->save() == false) {
          $data = [];
          foreach ($m->getMessages() as $message) {
            $data[] = $message;
          }
          die(json_encode($data));
        }
      }

      echo json_encode(array('success' => 'success' ));
    }
  }

  public function updateqtyAction($id, $quantity) {
    $s = Shopbag::findFirst("id='$id'");
    if($s){
      $s->quantity = $quantity;
      if($s->save()){
        $data = array('success' => 'Bag has been updated.' );
      } else {
        $data = array('error' => 'An error occurred please try again later.' );
      }
    }else {
      $data = array('error' => 'An error occurred please try again later.' );
    }
    echo json_encode($data);
  }

  public function getbillingaddrAction($id) {
    $b = Billinginfo::findFirst("memberid='$id'");
    echo json_encode($b);
  }

  public function placeorderAction($id) {
    $app = new CB();
    $request = new \Phalcon\Http\Request();

    if($request->isPost()){
      try {
        $transactionManager = new TransactionManager();
        $transaction = $transactionManager->get();

        $m = Members::findFirst("memberid='" . $id . "'");
        $b = Billinginfo::findFirst("memberid='$id'");

        $o = new Orderlist();
        $o->setTransaction($transaction);
        $guid = new \Utilities\Guid\Guid();
        $amount = $request->getPost('totalamount') + 200;
        $invoiceno = $request->getPost('invoice');
        $memberid = $id;
        if($b){
          $o->assign(array(
            'id' => $guid->GUID(),
            'invoiceno' => $invoiceno,
            'memberid' => $memberid,
            'amount' => $amount,
            'paymenttype'=> $request->getPost('paymenttype'),
            'status' => 'pending',
            'created_at' => date("Y-m-d H:i:s"),
            'firstname' => $b->firstname,
            'lastname' => $b->lastname,
            'address' => $b->address,
            'address2' => $b->address2,
            'city' => $b->city,
            'country' => $b->country,
            'phoneno' => $b->phoneno,
            'state' => $b->state,
            'zipcode' => $b->zipcode
            ));
        }

        if($o->save() == false){
          $data = [];
          foreach ($o->getMessages() as $message) {
            $data[] = $message; 
          }
          $transaction->rollback();
        }

        if($request->getPost('shippinginfo')['firstname']!=''){
          $s = new Shippinginfo();
          $s->setTransaction($transaction);
          $guid = new \Utilities\Guid\Guid();
          $s->assign(array(
            'id' => $guid->GUID(),
            'orderid' => $o->id,
            'firstname' => $request->getPost('shippinginfo')['firstname'],
            'lastname' => $request->getPost('shippinginfo')['lastname'],
            'address' => $request->getPost('shippinginfo')['address'],
            'address2' => $request->getPost('shippinginfo')['address2'],
            'city' => $request->getPost('shippinginfo')['city'],
            'country' => $request->getPost('shippinginfo')['country'],
            'state' => $request->getPost('shippinginfo')['state'],
            'zipcode' => $request->getPost('shippinginfo')['zipcode'],
            ));
          if($s->save() == false){
            $data = [];
            foreach ($s->getMessages() as $message) {
              $data[] = $message; 
            }
            $transaction->rollback();
          }
        }

        foreach ($request->getPost('order') as $key => $value) {
          $p = Product::findFirst("productid='" . $value['productid'] . "'");
          if($p->quantity >= $value['orderquantity']){
            $p->setTransaction($transaction);
            $p->quantity = $p->quantity - $value['orderquantity'];
            if($p->save() == false){
              $data = [];
              foreach ($p->getMessages() as $message) {
                $data[] = $message; 
              }
              $transaction->rollback();
            }

            $op = new Orderprod();
            $op->setTransaction($transaction);
            $guid = new \Utilities\Guid\Guid();
            $op->assign(array(
              'id' => $guid->GUID(),
              'orderid' => $o->id,
              'productid' => $value['productid'],
              'quantity' => $value['bagquantity'] 
              ));
            if($op->save() == false){
              $data = [];
              foreach ($op->getMessages() as $message) {
                $data[] = $message; 
              }
              $transaction->rollback();
            }
          }
        }

        $data = $app->creditcardPayment($b->toArray(), 
          $request->getPost('payment'), 
          $request->getPost('paymenttype'), 
          $request->getPost('shippingfee'), 
          $request->getPost('tax'), 
          $request->getPost('totalamount'), 
          $invoiceno, 'null', 'Body and Brain Online Shop');

        $shopbag = Shopbag::find("memberid='$id'");
        if($shopbag){
          if($shopbag->delete() == false){
            $data = [];
            foreach ($op->getMessages() as $message) {
              $data[] = $message; 
            }
            die(json_encode($data));
          }
        }

        $transaction->commit();
        if(isset($data['success'])){
          $body = "<p>Order Information</p>";
          $body .= "<p>Invoice no. ".$o->invoiceno."</p>";
          $body .= "<p>Total amount : " . $amount . "</p>";
          $body .= "<p class='text-bold'>Billing Information</p>";
          $body .= "<p>Name : " . $b->lastname . ", "  . $b->firstname . "</p>";
          $body .= "<p>Address : " . $b->address . " </p>";
          $body .= "<p>Phone no. : " . $b->phoneno . " </p>";
          $body .= "<table>
                      <thead>
                          <tr>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total Price</th>
                          </tr>
                      </thead>
                      <tbody>";
          foreach ($request->getPost('order') as $key => $value) {
            $body .= "<tr>";
            $body .= "<td>" . $value['name'] . "</td>";
            $body .= "<td>" . $value['price'] . "</td>";
            $body .= "<td>" . $value['orderquantity'] . "</td>";
            $body .= "<td>" . $value['total'] . "</td>";
            $body .= "</tr>";
          }
                        
          $body .= "</tbody>
                  </table>";

          if($request->getPost('shippinginfo')['firstname']!=''){
            $body .= "<p class='text-bold'>Shipping Information</p>";
            $body .= "<p>Name : " . $request->getPost('shippinginfo')['lastname'] . ", "  . $request->getPost('shippinginfo')['firstname'] . "</p>";
            $body .= "<p>Address : " . $request->getPost('shippinginfo')['address'] . " </p>";
            $body .= "<p>Phone no. : " . $request->getPost('shippinginfo')['phoneno'] . " </p>";
          }
          $app->sendMail($request->getPost('billinginfo')['email'], 'Body and Brain Order Confirmation', $body);

        }

        echo json_encode(array('success' => 'Order has been successfully added' ));
      } catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
        die( json_encode(array('401' => $e->getMessage())) );
      }
    }
  }

  public function savepaypaltempAction(){
    $request = new \Phalcon\Http\Request();

    if($request->isPost()){
      try{
        $transactionManager = new TransactionManager();
        $transaction = $transactionManager->get();

        $ot = new Ordertemp();
        $guid = new \Utilities\Guid\Guid();
        $ot->setTransaction($transaction);
        $ot->assign(array(
          'id' => $guid->GUID(),
          'invoiceno' => $request->getPost('invoice'),
          'memberid' => $request->getPost('member')['id'],
          'amount' => $request->getPost('totalamount'),
          'paymenttype' => 'paypal',
          'status' => 'pending',
          'created_at' => date("Y-m-d H:i:s"),
          'firstname' => $request->getPost('shippinginfo')['firstname'],
          'lastname' => $request->getPost('shippinginfo')['lastname'],
          'address' => $request->getPost('shippinginfo')['address'],
          'address2' => $request->getPost('shippinginfo')['address2'],
          'city' => $request->getPost('shippinginfo')['city'],
          'country' => $request->getPost('shippinginfo')['country'],
          'state' => $request->getPost('shippinginfo')['state'],
          'zipcode' => $request->getPost('shippinginfo')['zipcode'],
          'phoneno' => $request->getPost('shippinginfo')['phoneno']));

        if($ot->save() == false){
          $data = [];
          foreach ($ot->getMessages() as $message) {
            $data[] = $message;
          }
          $transaction->rollback();
        }

        foreach ($request->getPost('orders') as $key => $value) {
          $opt = new Orderprodtemp();
          $guid = new \Utilities\Guid\Guid();
          $opt->setTransaction($transaction);
          $opt->assign(array(
            'id' => $guid->GUID(),
            'orderid' => $request->getPost('invoice'),
            'productid' => $value['productid'],
            'quantity' => $value['bagquantity'] ));
          if($opt->save() == false){
            $data = [];
            foreach ($opt->getMessages() as $message) {
              $data[] = $message;
            }
            $transaction->rollback();
          }
        }

        $bag = Shopbag::find("memberid='" . $request->getPost('member')['id'] . "'");
        if($bag){
          if($bag->delete() == false){
            $data = [];
            foreach ($bag->getMessages() as $message) {
              $data[] = $message;
            }
            $transaction->rollback();
          }
        }

        $transaction->commit();
        echo json_encode(array('success' => 'success'));
      } catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
        die( json_encode(array('401' => $e->getMessage())) );
      }
    }
  }

  public function savebillinginfoAction($id) {
    $request = new \Phalcon\Http\Request();

    if($request->isPost()){
      $b = new Billinginfo();
      $guid = new \Utilities\Guid\Guid();
      $b->assign(array(
        'id' => $guid->GUID(),
        'memberid' => $id,
        'firstname' => $request->getPost('firstname'),
        'lastname' => $request->getPost('lastname'),
        'address' => $request->getPost('address'),
        'address2' => $request->getPost('address2'),
        'city' => $request->getPost('city'),
        'country' => $request->getPost('country'),
        'state' => $request->getPost('state'),
        'zipcode' => $request->getPost('zipcode'),
        'phoneno' => $request->getPost('phoneno') ));
      if($b->save() == false){
        $data = [];
        foreach ($b->getMessages() as $message) {
          $data[] = $message;
        }
        echo json_encode($data);
      }else {
        echo json_encode(array('success' => 'success' ));
      }
    }
  }
}
