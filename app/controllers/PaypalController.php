<?php

namespace Controllers;

use \Models\Groupclassintrosession as Groupclassintrosession;
use \Models\Groupclassintrosessiontemp as Groupclassintrosessiontemp;
use \Models\Groupclassintrosessioncanceled as Groupclassintrosessioncanceled;
use \Models\Centerlocation as Centerlocation;
use \Models\Beneplace as Beneplace;
use \Models\Beneplacetemp as Beneplacetemp;
use \Models\Beneplacecanceled as Beneplacecanceled;
use \Models\Centeremail as Centeremail;
use \Models\Notification as Notification;
use \Models\Introsession as Introsession;
use \Models\Introsessiontemp as Introsessiontemp;
use \Models\Introsessioncanceled as Introsessioncanceled;
use \Models\Centerdistrict as Centerdistrict;
use \Models\Centerregion as Centerregion;
use \Models\Centerpricingregfee as Centerpricingregfee;
use \Models\Centerphonenumber as Centerphonenumber;
use \Models\Center as Center;
use \Models\Centeroffer as Centeroffer;
use \Models\States as States;
use \Models\Cities as Cities;
use \Models\Cities_extended as Cities_extended;
use \Models\Ordertemp as Ordertemp;
use \Models\Orderlist as Orderlist;
use \Models\Orderprodtemp as Orderprodtemp;
use \Models\Orderprod as Orderprod;
use \Models\Shippinginfo as Shippinginfo;
use \Models\Billinginfo as Billinginfo;
use \Models\Members as Members;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class PaypalController extends \Phalcon\Mvc\Controller {

    public function paypalreturnAction(){
        // $request = new \Phalcon\Http\Request();
        // $temptransactionid = $request->getPost('id');
        // PaypalController::shopsaving($temptransactionid, 'Paypal');
        // die();
        $raw_post_data = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval) {
          $keyval = explode ('=', $keyval);
          if (count($keyval) == 2)
             $myPost[$keyval[0]] = urldecode($keyval[1]);
        }

        $req = 'cmd=_notify-validate';
        if(function_exists('get_magic_quotes_gpc')) {
           $get_magic_quotes_exists = true;
        }
        foreach ($myPost as $key => $value) {
            if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            }
            else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }


        $ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
        // $ch = curl_init('https://www.paypal.com/cgi-bin/webscr');

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

        if( !($res = curl_exec($ch)) ) {
            curl_close($ch);
            exit;
        }
        curl_close($ch);

        $data = explode('|', $_POST['custom']);
        $moduletype = $data[0];
        $temptransactionid = $data[1];
        $txn_id = $_POST['txn_id'];
        $payment_status = $_POST['payment_status'];
        $parent_txn_id = $_POST['parent_txn_id'];


        if (strcmp ($res, "VERIFIED") == 0){
          var_dump($temptransactionid);
           if($moduletype == 'intro'){
                if($payment_status == 'Completed'){
                    PaypalController::introsessionsaving($temptransactionid, $txn_id, 'Paypal');
                }
                else if($payment_status == 'Refunded'){
                    PaypalController::introsessioncanceledsaving($temptransactionid, $txn_id, 'Paypal',$parent_txn_id);
                }
           }
           else if($moduletype == 'group'){
                if($payment_status == 'Completed'){
                    PaypalController::groupsessionsaving($temptransactionid, $txn_id, 'Paypal');
                }
                else if($payment_status == 'Refunded'){
                    PaypalController::groupsessioncanceledsaving($temptransactionid, $txn_id, 'Paypal',$parent_txn_id);
                }
           }
           else if($moduletype == 'beneplace'){
                if($payment_status == 'Completed'){
                    PaypalController::beneplacesaving($temptransactionid, $txn_id, 'Paypal');
                }
                else if($payment_status == 'Refunded'){
                    PaypalController::beneplacecanceledsaving($temptransactionid, $txn_id, 'Paypal',$parent_txn_id);
                }
           }
           else if($moduletype == 'shop'){
                PaypalController::shopsaving($temptransactionid, 'Paypal');
           }

        }
        else if (strcmp ($res, "INVALID") == 0) {
            echo "The response from IPN was: <b>" .$res ."</b>";
        }
    }

    public function introsessionsaving($temptransactionid, $txn_id, $paymenttype){
        $tempintrosession = CB::bnbQueryFirst("SELECT * FROM introsessiontemp WHERE confirmationnumber = '".$temptransactionid."'");
        $centerdetails = CB::bnbQueryFirst("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid LEFT JOIN centermanagerphone ON center.centerid = centermanagerphone.centerid WHERE center.centerid = '".$tempintrosession['centerid']."' ");
        if($tempintrosession){
            $movetointrosession = new Introsession();
            $movetointrosession->sessionid = $tempintrosession['sessionid'];
            $movetointrosession->centerid = $tempintrosession['centerid'];
            $movetointrosession->confirmationnumber = $txn_id;
            $movetointrosession->name = $tempintrosession['name'];
            $movetointrosession->email = $tempintrosession['email'];
            $movetointrosession->phone = $tempintrosession['phone'];
            $movetointrosession->day = $tempintrosession['day'];
            $movetointrosession->hour = $tempintrosession['hour'];
            $movetointrosession->comment = $tempintrosession['comment'];
            $movetointrosession->quantity = $tempintrosession['quantity'];
            $movetointrosession->device = $tempintrosession['device'];
            $movetointrosession->deviceos = $tempintrosession['deviceos'];
            $movetointrosession->devicebrowser = $tempintrosession['devicebrowser'];
            $movetointrosession->datecreated = date('Y-m-d H:i:s');
            $movetointrosession->centersessionstatus = 0;
            $movetointrosession->regionsessionstatus = 0;
            $movetointrosession->adminsessionstatus = 0;
            $movetointrosession->payment = $paymenttype;
            $movetointrosession->paid = $tempintrosession['paid'];

            if($movetointrosession->save()){

                $changeverificationstatus = Introsessiontemp::findFirst("confirmationnumber = '".$temptransactionid."'");
                $changeverificationstatus->verificationstatus = 1;
                if($changeverificationstatus){
                    if($changeverificationstatus->save()){

                    }
                }

                $guid = new \Utilities\Guid\Guid();
                $notificationid = $guid->GUID();

                $notification = new Notification();
                $notification->notificationid = $notificationid;
                $notification->centerid = $tempintrosession['centerid'];
                $notification->itemid = $tempintrosession['sessionid'];
                $notification->name = $tempintrosession['name'];
                $notification->datecreated = date('Y-m-d H:i:s');
                $notification->adminstatus = 0;
                $notification->regionstatus = 0;
                $notification->centerstatus = 0;
                $notification->type = 'introsession';
                if ($notification->save()) {

                }

                // email for users
                $bodyforuser = '<p style="color:#49AFCD;font-size:23px;">Your Appointment</p>
                <p style="color: #999 !important">Name: <span  style="color: #000;">'. $tempintrosession['name'] .'</span></p>
                <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($tempintrosession['day'])).','.$tempintrosession['hour'].'</span><em>(Your appointment is subject to availability.)</em></p>
                <p style="color: #999 !important">Where: <span class="bluetext">'.$centerdetails['centertitle'].'</span><br>
                <em>'.$centerdetails['centeraddress'].', '.$centerdetails['centerzip'].'</em><br>
                <em>Phone: '.$centerdetails['phonenumber'].'</em>
                </p>
                <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$txn_id.'</span></p>
                <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$tempintrosession['paid'].' '.$paymenttype.'</span></p>
                <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$centerdetails['lat'].', '.$centerdetails['lon'].'&zoom=14&size=1200x500&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$centerdetails['lat'].', '.$centerdetails['lon'].'" width="100%" height="auto">';

                $contentforuser = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforuser);
                $send = CB::sendMail($tempintrosession['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforuser);

                // email for centers
                $bodyforcenter = 'Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                <br>1. Name: '. $tempintrosession['name'] .'<br>
                2. Phone: '.$tempintrosession['phone'].'<br>
                3. Email: '.$tempintrosession['email'].'<br>
                4. Appointment Date: '.date("D, F d Y", strtotime($tempintrosession['day'])).','.$tempintrosession['hour'].'<br>
                <p>5. Comment: '.$tempintrosession['comment'].'</p><br>
                6. Payment: Paid ($'.$tempintrosession['paid'].') Quantity: '.$tempintrosession['quantity'].'<br>
                7. Application Code: '.$txn_id.'<br>
                8.Type: 1-on-1 Intro Session <br><br>
                Please contact this person to confirm the appointment as soon as possible.<br>
                Thank you';

                $contentforcenter = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforcenter);
                $send = CB::sendMail($centerdetails['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);

                $DI = \Phalcon\DI::getDefault();
                $app = $DI->get('application');
                foreach($app->config->email as $bnbemail) {
                    $send = CB::sendMail($bnbemail, 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);
                }

                // sms email for centers
                $emailcarrier = CB::emailCarrier($centerdetails['carrier']);
                $emailnumber = $centerdetails['phone1'].$centerdetails['phone2'].$centerdetails['phone3'].$emailcarrier;
                $textmsg = '(1-on-1 Intro:Paid($'.$tempintrosession['paid'].'))/Name:'.$tempintrosession['name'].'/Phone:'.$tempintrosession['phone'].'/Date:'.$tempintrosession['day'].'/Hour:'.$tempintrosession['hour'].'. Confirm it. Check email '. $tempintrosession['comment'];
                $sendsmsemail = CB::sendMail($emailnumber, 'BodyandBrain', $textmsg);

                return 'success';

            }
            else{
                return 'error';
            }

        }
    }


    public function introsessioncanceledsaving($temptransactionid, $txn_id, $paymenttype,$parent_txn_id){
        $tempintrosession = CB::bnbQueryFirst("SELECT * FROM introsessiontemp WHERE confirmationnumber = '".$temptransactionid."'");
        $centerdetails = CB::bnbQueryFirst("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid LEFT JOIN centermanagerphone ON center.centerid = centermanagerphone.centerid WHERE center.centerid = '".$tempintrosession['centerid']."' ");
        if($tempintrosession){
            $movetointrosessioncanceled = new Introsessioncanceled();
            $movetointrosessioncanceled->sessionid = $tempintrosession['sessionid'];
            $movetointrosessioncanceled->centerid = $tempintrosession['centerid'];
            $movetointrosessioncanceled->confirmationnumber = $txn_id;
            $movetointrosessioncanceled->name = $tempintrosession['name'];
            $movetointrosessioncanceled->email = $tempintrosession['email'];
            $movetointrosessioncanceled->phone = $tempintrosession['phone'];
            $movetointrosessioncanceled->day = $tempintrosession['day'];
            $movetointrosessioncanceled->hour = $tempintrosession['hour'];
            $movetointrosessioncanceled->comment = $tempintrosession['comment'];
            $movetointrosessioncanceled->quantity = $tempintrosession['quantity'];
            $movetointrosessioncanceled->device = $tempintrosession['device'];
            $movetointrosessioncanceled->deviceos = $tempintrosession['deviceos'];
            $movetointrosessioncanceled->devicebrowser = $tempintrosession['devicebrowser'];
            $movetointrosessioncanceled->datecreated = $tempintrosession['datecreated'];
            $movetointrosessioncanceled->daterefunded = date('Y-m-d H:i:s');
            $movetointrosessioncanceled->centersessionstatus = 0;
            $movetointrosessioncanceled->regionsessionstatus = 0;
            $movetointrosessioncanceled->adminsessionstatus = 0;
            $movetointrosessioncanceled->payment = $paymenttype;
            $movetointrosessioncanceled->paid = $tempintrosession['paid'];

            if($movetointrosessioncanceled->save()){

                $changeverificationstatus = Introsessiontemp::findFirst("confirmationnumber = '".$temptransactionid."'");
                $changeverificationstatus->verificationstatus = 1;
                if($changeverificationstatus){
                    if($changeverificationstatus->save()){

                    }
                }

                $deletesession = Introsession::findFirst("confirmationnumber = '".$parent_txn_id."'");
                if($deletesession){
                    $deletesession->delete();
                }

                // $guid = new \Utilities\Guid\Guid();
                // $notificationid = $guid->GUID();

                // $notification = new Notification();
                // $notification->notificationid = $notificationid;
                // $notification->centerid = $tempintrosession['centerid'];
                // $notification->itemid = $tempintrosession['sessionid'];
                // $notification->name = $tempintrosession['name'];
                // $notification->datecreated = date('Y-m-d H:i:s');
                // $notification->adminstatus = 0;
                // $notification->regionstatus = 0;
                // $notification->centerstatus = 0;
                // $notification->type = 'introsession';
                // if ($notification->save()) {

                // }

                // // email for users
                // $bodyforuser = '<p style="color:#49AFCD;font-size:23px;">Your Appointment</p>
                // <p style="color: #999 !important">Name: <span  style="color: #000;">'. $tempintrosession['name'] .'</span></p>
                // <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($tempintrosession['day'])).','.$tempintrosession['hour'].'</span><em>(Your appointment is subject to availability.)</em></p>
                // <p style="color: #999 !important">Where: <span class="bluetext">'.$centerdetails['centertitle'].'</span><br>
                // <em>'.$centerdetails['centeraddress'].', '.$centerdetails['centerzip'].'</em><br>
                // <em>Phone: '.$centerdetails['phonenumber'].'</em>
                // </p>
                // <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$txn_id.'</span></p>
                // <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$tempintrosession['paid'].' Paypal</span></p>
                // <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$centerdetails['lat'].', '.$centerdetails['lon'].'&zoom=14&size=1200x500&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$centerdetails['lat'].', '.$centerdetails['lon'].'" width="100%" height="auto">';

                // $contentforuser = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforuser);
                // $send = CB::sendMail($tempintrosession['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforuser);

                // // email for centers
                // $bodyforcenter = 'Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                // <br>1. Name: '. $tempintrosession['name'] .'<br>
                // 2. Phone: '.$tempintrosession['phone'].'<br>
                // 3. Email: '.$tempintrosession['email'].'<br>
                // 4. Appointment Date: '.date("D, F d Y", strtotime($tempintrosession['day'])).','.$tempintrosession['hour'].'<br>
                // <p>5. Comment: '.$tempintrosession['comment'].'</p><br>
                // 6. Payment: Paid ($'.$tempintrosession['paid'].')<br>
                // 7. Application Code: '.$txn_id.'<br>
                // 8.Type: 1-on-1 Intro Session <br><br>
                // Please contact this person to confirm the appointment as soon as possible.<br>
                // Thank you';

                // $contentforcenter = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforcenter);
                // $send = CB::sendMail($centerdetails['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);

                // $DI = \Phalcon\DI::getDefault();
                // $app = $DI->get('application');
                // foreach($app->config->email as $bnbemail) {
                //     $send = CB::sendMail($bnbemail, 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);
                // }

                // // sms email for centers
                // $emailcarrier = CB::emailCarrier($centerdetails['carrier']);
                // $emailnumber = $centerdetails['phone1'].$centerdetails['phone2'].$centerdetails['phone3'].$emailcarrier;
                // $textmsg = '(1-on-1 Intro:Paid($'.$tempintrosession['paid'].'))/Name:'.$tempintrosession['name'].'/Phone:'.$tempintrosession['phone'].'/Date:'.$tempintrosession['day'].'/Hour:'.$tempintrosession['hour'].'. Confirm it. Check email '. $tempintrosession['comment'];
                // $sendsmsemail = CB::sendMail($emailnumber, 'BodyandBrain', $textmsg);

                return 'success';

            }
            else{
                return 'error';
            }

        }
    }

    public function groupsessionsaving($temptransactionid, $txn_id, $paymenttype){
        $tempintrosession = CB::bnbQueryFirst("SELECT * FROM groupclassintrosessiontemp WHERE confirmationnumber = '".$temptransactionid."'");
        $centerdetails = CB::bnbQueryFirst("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid LEFT JOIN centermanagerphone ON center.centerid = centermanagerphone.centerid WHERE center.centerid = '".$tempintrosession['centerid']."' ");
        if($tempintrosession){
            $movetogroupsession = new Groupclassintrosession();
            $movetogroupsession->sessionid = $tempintrosession['sessionid'];
            $movetogroupsession->centerid = $tempintrosession['centerid'];
            $movetogroupsession->confirmationnumber = $txn_id;
            $movetogroupsession->name = $tempintrosession['name'];
            $movetogroupsession->email = $tempintrosession['email'];
            $movetogroupsession->phone = $tempintrosession['phone'];
            $movetogroupsession->day = $tempintrosession['day'];
            $movetogroupsession->hour = $tempintrosession['hour'];
            $movetogroupsession->comment = $tempintrosession['comment'];
            $movetogroupsession->quantity = $tempintrosession['quantity'];
            $movetogroupsession->device = $tempintrosession['device'];
            $movetogroupsession->deviceos = $tempintrosession['deviceos'];
            $movetogroupsession->devicebrowser = $tempintrosession['devicebrowser'];
            $movetogroupsession->datecreated = date('Y-m-d H:i:s');
            $movetogroupsession->centersessionstatus = 0;
            $movetogroupsession->regionsessionstatus = 0;
            $movetogroupsession->adminsessionstatus = 0;
            $movetogroupsession->payment = $paymenttype;
            $movetogroupsession->paid = $tempintrosession['paid'];

            if($movetogroupsession->save()){

                $changeverificationstatus = Groupclassintrosessiontemp::findFirst("confirmationnumber = '".$temptransactionid."'");
                $changeverificationstatus->verificationstatus = 1;
                if($changeverificationstatus){
                    if($changeverificationstatus->save()){

                    }
                }

                $guid = new \Utilities\Guid\Guid();
                $notificationid = $guid->GUID();

                $notification = new Notification();
                $notification->notificationid = $notificationid;
                $notification->centerid = $tempintrosession['centerid'];
                $notification->itemid = $tempintrosession['sessionid'];
                $notification->name = $tempintrosession['name'];
                $notification->datecreated = date('Y-m-d H:i:s');
                $notification->adminstatus = 0;
                $notification->regionstatus = 0;
                $notification->centerstatus = 0;
                $notification->type = 'groupsession';
                if ($notification->save()) {

                }

                // email for users
                $bodyforuser = '<p style="color:#49AFCD;font-size:23px;">Your Appointment</p>
                <p style="color: #999 !important">Name: <span  style="color: #000;">'. $tempintrosession['name'] .'</span></p>
                <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($tempintrosession['day'])).','.$tempintrosession['hour'].'</span><em>(Your appointment is subject to availability.)</em></p>
                <p style="color: #999 !important">Where: <span class="bluetext">'.$centerdetails['centertitle'].'</span><br>
                <em>'.$centerdetails['centeraddress'].', '.$centerdetails['centerzip'].'</em><br>
                <em>Phone: '.$centerdetails['phonenumber'].'</em>
                </p>
                <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$txn_id.'</span></p>
                <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$tempintrosession['paid'].' '.$paymenttype.'</span></p>
                <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$centerdetails['lat'].', '.$centerdetails['lon'].'&zoom=14&size=1200x500&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$centerdetails['lat'].', '.$centerdetails['lon'].'" width="100%" height="auto">';

                $contentforuser = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforuser);
                $send = CB::sendMail($tempintrosession['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforuser);

                // email for centers
                $bodyforcenter = 'Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                <br>1. Name: '. $tempintrosession['name'] .'<br>
                2. Phone: '.$tempintrosession['phone'].'<br>
                3. Email: '.$tempintrosession['email'].'<br>
                4. Appointment Date: '.date("D, F d Y", strtotime($tempintrosession['day'])).','.$tempintrosession['hour'].'<br>
                <p>5. Comment: '.$tempintrosession['comment'].'</p><br>
                6. Payment: Paid ($'.$tempintrosession['paid'].') Quantity: '.$tempintrosession['quantity'].'<br>
                7. Application Code: '.$txn_id.'<br>
                8.Type: 1 Group Class + 1-on-1 Intro <br><br>
                Please contact this person to confirm the appointment as soon as possible.<br>
                Thank you';

                $contentforcenter = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforcenter);
                $send = CB::sendMail($centerdetails['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);

                $DI = \Phalcon\DI::getDefault();
                $app = $DI->get('application');
                foreach($app->config->email as $bnbemail) {
                    $send = CB::sendMail($bnbemail, 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);
                }

                // sms email for centers
                $emailcarrier = CB::emailCarrier($centerdetails['carrier']);
                $emailnumber = $centerdetails['phone1'].$centerdetails['phone2'].$centerdetails['phone3'].$emailcarrier;
                $textmsg = '(1 Group Class + 1-on-1 Intro:Paid($'.$tempintrosession['paid'].'))/Name:'.$tempintrosession['name'].'/Phone:'.$tempintrosession['phone'].'/Date:'.$tempintrosession['day'].'/Hour:'.$tempintrosession['hour'].'. Confirm it. Check email '. $tempintrosession['comment'];
                $sendsmsemail = CB::sendMail($emailnumber, 'BodyandBrain', $textmsg);

                return 'success';

            }
            else{
                return 'error';
            }

        }
        else{
            return 'error';
        }
    }

    public function groupsessioncanceledsaving($temptransactionid, $txn_id, $paymenttype,$parent_txn_id){
        $tempintrosession = CB::bnbQueryFirst("SELECT * FROM groupclassintrosessiontemp WHERE confirmationnumber = '".$temptransactionid."'");
        $centerdetails = CB::bnbQueryFirst("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid LEFT JOIN centermanagerphone ON center.centerid = centermanagerphone.centerid WHERE center.centerid = '".$tempintrosession['centerid']."' ");
        if($tempintrosession){
            $movetogroupsessioncanceled = new Groupclassintrosessioncanceled();
            $movetogroupsessioncanceled->sessionid = $tempintrosession['sessionid'];
            $movetogroupsessioncanceled->centerid = $tempintrosession['centerid'];
            $movetogroupsessioncanceled->confirmationnumber = $txn_id;
            $movetogroupsessioncanceled->name = $tempintrosession['name'];
            $movetogroupsessioncanceled->email = $tempintrosession['email'];
            $movetogroupsessioncanceled->phone = $tempintrosession['phone'];
            $movetogroupsessioncanceled->day = $tempintrosession['day'];
            $movetogroupsessioncanceled->hour = $tempintrosession['hour'];
            $movetogroupsessioncanceled->comment = $tempintrosession['comment'];
            $movetogroupsessioncanceled->quantity = $tempintrosession['quantity'];
            $movetogroupsessioncanceled->device = $tempintrosession['device'];
            $movetogroupsessioncanceled->deviceos = $tempintrosession['deviceos'];
            $movetogroupsessioncanceled->devicebrowser = $tempintrosession['devicebrowser'];
            $movetogroupsessioncanceled->datecreated = $tempintrosession['datecreated'];
            $movetogroupsessioncanceled->daterefunded = date('Y-m-d H:i:s');
            $movetogroupsessioncanceled->centersessionstatus = 0;
            $movetogroupsessioncanceled->regionsessionstatus = 0;
            $movetogroupsessioncanceled->adminsessionstatus = 0;
            $movetogroupsessioncanceled->payment = $paymenttype;
            $movetogroupsessioncanceled->paid = $tempintrosession['paid'];

            if($movetogroupsessioncanceled->save()){

                $changeverificationstatus = Groupclassintrosessiontemp::findFirst("confirmationnumber = '".$temptransactionid."'");
                $changeverificationstatus->verificationstatus = 1;
                if($changeverificationstatus){
                    if($changeverificationstatus->save()){

                    }
                }

                $deletesession = Groupclassintrosession::findFirst("confirmationnumber = '".$parent_txn_id."'");
                if($deletesession){
                    $deletesession->delete();
                }

                // $guid = new \Utilities\Guid\Guid();
                // $notificationid = $guid->GUID();

                // $notification = new Notification();
                // $notification->notificationid = $notificationid;
                // $notification->centerid = $tempintrosession['centerid'];
                // $notification->itemid = $tempintrosession['sessionid'];
                // $notification->name = $tempintrosession['name'];
                // $notification->datecreated = date('Y-m-d H:i:s');
                // $notification->adminstatus = 0;
                // $notification->regionstatus = 0;
                // $notification->centerstatus = 0;
                // $notification->type = 'groupsession';
                // if ($notification->save()) {

                // }

                // // email for users
                // $bodyforuser = '<p style="color:#49AFCD;font-size:23px;">Your Appointment</p>
                // <p style="color: #999 !important">Name: <span  style="color: #000;">'. $tempintrosession['name'] .'</span></p>
                // <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($tempintrosession['day'])).','.$tempintrosession['hour'].'</span><em>(Your appointment is subject to availability.)</em></p>
                // <p style="color: #999 !important">Where: <span class="bluetext">'.$centerdetails['centertitle'].'</span><br>
                // <em>'.$centerdetails['centeraddress'].', '.$centerdetails['centerzip'].'</em><br>
                // <em>Phone: '.$centerdetails['phonenumber'].'</em>
                // </p>
                // <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$txn_id.'</span></p>
                // <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$tempintrosession['paid'].' Paypal</span></p>
                // <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$centerdetails['lat'].', '.$centerdetails['lon'].'&zoom=14&size=1200x500&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$centerdetails['lat'].', '.$centerdetails['lon'].'" width="100%" height="auto">';

                // $contentforuser = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforuser);
                // $send = CB::sendMail($tempintrosession['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforuser);

                // // email for centers
                // $bodyforcenter = 'Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                // <br>1. Name: '. $tempintrosession['name'] .'<br>
                // 2. Phone: '.$tempintrosession['phone'].'<br>
                // 3. Email: '.$tempintrosession['email'].'<br>
                // 4. Appointment Date: '.date("D, F d Y", strtotime($tempintrosession['day'])).','.$tempintrosession['hour'].'<br>
                // <p>5. Comment: '.$tempintrosession['comment'].'</p><br>
                // 6. Payment: Paid ($'.$tempintrosession['paid'].')<br>
                // 7. Application Code: '.$txn_id.'<br>
                // 8.Type: 1 Group Class + 1-on-1 Intro <br><br>
                // Please contact this person to confirm the appointment as soon as possible.<br>
                // Thank you';

                // $contentforcenter = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforcenter);
                // $send = CB::sendMail($centerdetails['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);

                // $DI = \Phalcon\DI::getDefault();
                // $app = $DI->get('application');
                // foreach($app->config->email as $bnbemail) {
                //     $send = CB::sendMail($bnbemail, 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);
                // }

                // // sms email for centers
                // $emailcarrier = CB::emailCarrier($centerdetails['carrier']);
                // $emailnumber = $centerdetails['phone1'].$centerdetails['phone2'].$centerdetails['phone3'].$emailcarrier;
                // $textmsg = '(1 Group Class + 1-on-1 Intro:Paid($'.$tempintrosession['paid'].'))/Name:'.$tempintrosession['name'].'/Phone:'.$tempintrosession['phone'].'/Date:'.$tempintrosession['day'].'/Hour:'.$tempintrosession['hour'].'. Confirm it. Check email '. $tempintrosession['comment'];
                // $sendsmsemail = CB::sendMail($emailnumber, 'BodyandBrain', $textmsg);

                return 'success';

            }
            else{
                return 'error';
            }

        }
    }

    public function beneplacesaving($temptransactionid, $txn_id, $paymenttype){
        $tempbeneplace = CB::bnbQueryFirst("SELECT * FROM beneplacetemp WHERE confirmationnumber = '".$temptransactionid."'");
        $centerdetails = CB::bnbQueryFirst("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid LEFT JOIN centermanagerphone ON center.centerid = centermanagerphone.centerid WHERE center.centerid = '".$tempbeneplace['centerid']."' ");
        if($tempbeneplace){
            $movetobeneplace = new Beneplace();
            $movetobeneplace->sessionid = $tempbeneplace['sessionid'];
            $movetobeneplace->centerid = $tempbeneplace['centerid'];
            $movetobeneplace->confirmationnumber = $txn_id;
            $movetobeneplace->name = $tempbeneplace['name'];
            $movetobeneplace->email = $tempbeneplace['email'];
            $movetobeneplace->phone = $tempbeneplace['phone'];
            $movetobeneplace->day = $tempbeneplace['day'];
            $movetobeneplace->hour = $tempbeneplace['hour'];
            $movetobeneplace->comment = $tempbeneplace['comment'];
            $movetobeneplace->quantity = $tempbeneplace['quantity'];
            $movetobeneplace->device = $tempbeneplace['device'];
            $movetobeneplace->deviceos = $tempbeneplace['deviceos'];
            $movetobeneplace->devicebrowser = $tempbeneplace['devicebrowser'];
            $movetobeneplace->datecreated = date('Y-m-d H:i:s');
            $movetobeneplace->centersessionstatus = 0;
            $movetobeneplace->regionsessionstatus = 0;
            $movetobeneplace->adminsessionstatus = 0;
            $movetobeneplace->payment = $paymenttype;
            $movetobeneplace->paid = $tempbeneplace['paid'];
            $movetobeneplace->program = $tempbeneplace['program'];

            if($movetobeneplace->save()){

                $changeverificationstatus = Beneplacetemp::findFirst("confirmationnumber = '".$temptransactionid."'");
                $changeverificationstatus->verificationstatus = 1;
                if($changeverificationstatus){
                    if($changeverificationstatus->save()){

                    }
                }

                $guid = new \Utilities\Guid\Guid();
                $notificationid = $guid->GUID();

                $notification = new Notification();
                $notification->notificationid = $notificationid;
                $notification->centerid = $tempbeneplace['centerid'];
                $notification->itemid = $tempbeneplace['sessionid'];
                $notification->name = $tempbeneplace['name'];
                $notification->datecreated = date('Y-m-d H:i:s');
                $notification->adminstatus = 0;
                $notification->regionstatus = 0;
                $notification->centerstatus = 0;
                $notification->type = 'beneplace';
                if ($notification->save()) {

                }


                // email for users
                $bodyforuser = '<p style="color:#49AFCD;font-size:23px;">Your Appointment</p>
                <p style="color: #999 !important">Name: <span  style="color: #000;">'. $tempbeneplace['name'] .'</span></p>
                <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($tempbeneplace['day'])).','.$tempbeneplace['hour'].'</span><em>(Your appointment is subject to availability.)</em></p>
                <p style="color: #999 !important">Where: <span class="bluetext">'.$centerdetails['centertitle'].'</span><br>
                <em>'.$centerdetails['centeraddress'].', '.$centerdetails['centerzip'].'</em><br>
                <em>Phone: '.$centerdetails['phonenumber'].'</em>
                </p>
                <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$txn_id.'</span></p>
                <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$tempbeneplace['paid'].' '.$paymenttype.'</span></p>
                <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$centerdetails['lat'].', '.$centerdetails['lon'].'&zoom=14&size=1200x500&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$centerdetails['lat'].', '.$centerdetails['lon'].'" width="100%" height="auto">';

                $contentforuser = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforuser);
                $send = CB::sendMail($tempbeneplace['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforuser);

                if($tempbeneplace['program'] == "intro"){
                  $classtitle =  "1-on-1 Intro Session";
                }
                else if($tempbeneplace['program'] == "group"){
                  $classtitle =  "1 Group Class + 1-on-1 Intro";
                }
                // email for centers
                $bodyforcenter = 'Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                <br>1. Name: '. $tempbeneplace['name'] .'<br>
                2. Phone: '.$tempbeneplace['phone'].'<br>
                3. Email: '.$tempbeneplace['email'].'<br>
                4. Appointment Date: '.date("D, F d Y", strtotime($tempbeneplace['day'])).','.$tempbeneplace['hour'].'<br>
                <p>5. Comment: '.$tempbeneplace['comment'].'</p><br>
                6. Payment: Paid ($'.$tempbeneplace['paid'].') Quantity: '.$tempbeneplace['quantity'].'<br>
                7. Application Code: '.$txn_id.'<br>
                8.Type:Beneplace ('.$classtitle.')<br><br>
                Please contact this person to confirm the appointment as soon as possible.<br>
                Thank you';

                $contentforcenter = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforcenter);
                $send = CB::sendMail($centerdetails['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);

                $DI = \Phalcon\DI::getDefault();
                $app = $DI->get('application');
                foreach($app->config->email as $bnbemail) {
                    $send = CB::sendMail($bnbemail, 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);
                }

                // sms email for centers
                $emailcarrier = CB::emailCarrier($centerdetails['carrier']);
                $emailnumber = $centerdetails['phone1'].$centerdetails['phone2'].$centerdetails['phone3'].$emailcarrier;
                $textmsg = '(1-on-1 Intro:Paid($'.$tempbeneplace['paid'].'))/Name:'.$tempbeneplace['name'].'/Phone:'.$tempbeneplace['phone'].'/Date:'.$tempbeneplace['day'].'/Hour:'.$tempbeneplace['hour'].'. Confirm it. Check email '. $tempbeneplace['comment'];
                $sendsmsemail = CB::sendMail($emailnumber, 'BodyandBrain', $textmsg);

                return 'success';

            }
            else{
                return 'error';
            }

        }
        else{
            return 'error';
        }
    }

    public function beneplacecanceledsaving($temptransactionid, $txn_id, $paymenttype,$parent_txn_id){
        $tempbeneplace = CB::bnbQueryFirst("SELECT * FROM beneplacetemp WHERE confirmationnumber = '".$temptransactionid."'");
        $centerdetails = CB::bnbQueryFirst("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid LEFT JOIN centermanagerphone ON center.centerid = centermanagerphone.centerid WHERE center.centerid = '".$tempbeneplace['centerid']."' ");
        if($tempbeneplace){
            $movetobeneplace = new Beneplacecanceled();
            $movetobeneplace->sessionid = $tempbeneplace['sessionid'];
            $movetobeneplace->centerid = $tempbeneplace['centerid'];
            $movetobeneplace->confirmationnumber = $txn_id;
            $movetobeneplace->name = $tempbeneplace['name'];
            $movetobeneplace->email = $tempbeneplace['email'];
            $movetobeneplace->phone = $tempbeneplace['phone'];
            $movetobeneplace->day = $tempbeneplace['day'];
            $movetobeneplace->hour = $tempbeneplace['hour'];
            $movetobeneplace->comment = $tempbeneplace['comment'];
            $movetobeneplace->quantity = $tempbeneplace['quantity'];
            $movetobeneplace->device = $tempbeneplace['device'];
            $movetobeneplace->deviceos = $tempbeneplace['deviceos'];
            $movetobeneplace->devicebrowser = $tempbeneplace['devicebrowser'];
            $movetobeneplace->datecreated = $tempintrosession['datecreated'];
            $movetobeneplace->daterefunded = date('Y-m-d H:i:s');
            $movetobeneplace->centersessionstatus = 0;
            $movetobeneplace->regionsessionstatus = 0;
            $movetobeneplace->adminsessionstatus = 0;
            $movetobeneplace->payment = $paymenttype;
            $movetobeneplace->paid = $tempbeneplace['paid'];
            $movetobeneplace->program = $tempbeneplace['program'];

            if($movetobeneplace->save()){

                $changeverificationstatus = Beneplacetemp::findFirst("confirmationnumber = '".$temptransactionid."'");
                $changeverificationstatus->verificationstatus = 1;
                if($changeverificationstatus){
                    if($changeverificationstatus->save()){

                    }
                }

                $deletesession = Beneplace::findFirst("confirmationnumber = '".$parent_txn_id."'");
                if($deletesession){
                    $deletesession->delete();
                }

                // $guid = new \Utilities\Guid\Guid();
                // $notificationid = $guid->GUID();
                //
                // $notification = new Notification();
                // $notification->notificationid = $notificationid;
                // $notification->centerid = $tempbeneplace['centerid'];
                // $notification->itemid = $tempbeneplace['sessionid'];
                // $notification->name = $tempbeneplace['name'];
                // $notification->datecreated = date('Y-m-d H:i:s');
                // $notification->adminstatus = 0;
                // $notification->regionstatus = 0;
                // $notification->centerstatus = 0;
                // $notification->type = 'beneplace';
                // if ($notification->save()) {
                //
                // }


                // email for users
                // $bodyforuser = '<p style="color:#49AFCD;font-size:23px;">Your Appointment</p>
                // <p style="color: #999 !important">Name: <span  style="color: #000;">'. $tempbeneplace['name'] .'</span></p>
                // <p style="color: #999 !important">When: <span  style="color: #000;">'.date("D, F d Y", strtotime($tempbeneplace['day'])).','.$tempbeneplace['hour'].'</span><em>(Your appointment is subject to availability.)</em></p>
                // <p style="color: #999 !important">Where: <span class="bluetext">'.$centerdetails['centertitle'].'</span><br>
                // <em>'.$centerdetails['centeraddress'].', '.$centerdetails['centerzip'].'</em><br>
                // <em>Phone: '.$centerdetails['phonenumber'].'</em>
                // </p>
                // <p style="color: #999 !important">Confirmation Number: <span  style="color: #000;">'.$txn_id.'</span></p>
                // <p style="color: #999 !important">Payment: <span  style="color: #000;">Paid $'.$tempbeneplace['paid'].' Paypal</span></p>
                // <img src="http://maps.googleapis.com/maps/api/staticmap?center='.$centerdetails['lat'].', '.$centerdetails['lon'].'&zoom=14&size=1200x500&maptype=roadmap&markers=color:red%7Clabel:C%7C'.$centerdetails['lat'].', '.$centerdetails['lon'].'" width="100%" height="auto">';

                // $contentforuser = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforuser);
                // $send = CB::sendMail($tempbeneplace['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforuser);

                // email for centers
                // $bodyforcenter = 'Congratulations! You just got an appointment for your center '.$searchresult['centertitle'].'.
                // <br>1. Name: '. $tempbeneplace['name'] .'<br>
                // 2. Phone: '.$tempbeneplace['phone'].'<br>
                // 3. Email: '.$tempbeneplace['email'].'<br>
                // 4. Appointment Date: '.date("D, F d Y", strtotime($tempbeneplace['day'])).','.$tempbeneplace['hour'].'<br>
                // <p>5. Comment: '.$tempbeneplace['comment'].'</p><br>
                // 6. Payment: Paid ($'.$tempbeneplace['paid'].')<br>
                // 7. Application Code: '.$txn_id.'<br>
                // 8.Type:Beneplace <br><br>
                // Please contact this person to confirm the appointment as soon as possible.<br>
                // Thank you';

                // $contentforcenter = CB::mailTemplate('BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $bodyforcenter);
                // $send = CB::sendMail($centerdetails['email'], 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);

                // $DI = \Phalcon\DI::getDefault();
                // $app = $DI->get('application');
                // foreach($app->config->email as $bnbemail) {
                //     $send = CB::sendMail($bnbemail, 'BodynBrain : Appointment for '.$centerdetails['centertitle'].' Center', $contentforcenter);
                // }

                // sms email for centers
                // $emailcarrier = CB::emailCarrier($centerdetails['carrier']);
                // $emailnumber = $centerdetails['phone1'].$centerdetails['phone2'].$centerdetails['phone3'].$emailcarrier;
                // $textmsg = '(1-on-1 Intro:Paid($'.$tempbeneplace['paid'].'))/Name:'.$tempbeneplace['name'].'/Phone:'.$tempbeneplace['phone'].'/Date:'.$tempbeneplace['day'].'/Hour:'.$tempbeneplace['hour'].'. Confirm it. Check email '. $tempbeneplace['comment'];
                // $sendsmsemail = CB::sendMail($emailnumber, 'BodyandBrain', $textmsg);

                return 'success';

            }
            else{
                return 'error';
            }

        }
    }

    public function shopsaving($transactionid, $paymenttype) {
        $ordert = Ordertemp::findFirst("invoiceno='$transactionid'");
        if($ordert){
            $b = Billinginfo::findFirst("memberid='" . $ordert->memberid . "'");
            if($b){
                try {
                    $transactionManager = new TransactionManager();
                    $transaction = $transactionManager->get();

                    $order = new Orderlist();
                    $guid = new \Utilities\Guid\Guid();
                    $order->setTransaction($transaction);
                    $order->assign(array(
                        'id' => $guid->GUID(),
                        'invoiceno' => $transactionid,
                        'memberid' => $b->memberid,
                        'amount' => $ordert->amount,
                        'paymenttype' => $ordert->paymenttype,
                        'status' => $ordert->status,
                        'created_at' => date("Y-m-d H:i:s"),
                        'firstname' => $b->firstname,
                        'lastname' => $b->lastname,
                        'address' => $b->address,
                        'address2' => $b->address2,
                        'city' => $b->city,
                        'country' => $b->country,
                        'state' => $b->state,
                        'zipcode' => $b->zipcode,
                        'phoneno' => $b->phoneno ));

                    if($order->save() == false){
                        $data = [];
                        foreach ($order->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    $sh = new Shippinginfo();
                    $guid = new \Utilities\Guid\Guid();
                    $sh->assign(array(
                        'id' => $guid->GUID(),
                        'orderid' => $order->id,
                        'firstname' => $ordert->firstname,
                        'lastname' => $ordert->lastname,
                        'address' => $ordert->address,
                        'address2' => $ordert->address2,
                        'city' => $ordert->city,
                        'country' => $ordert->country,
                        'state' => $ordert->state,
                        'zipcode' => $ordert->zipcode,
                        'phoneno' => $ordert->phoneno ));

                    if($sh->save() == false){
                        $data = [];
                        foreach ($sh->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    //delete temporary order
                    $ordert->setTransaction($transaction);
                    if($ordert->delete() == false){
                        $data = [];
                        foreach ($ordert->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    //save orderlist
                    $orderprodt = Orderprodtemp::find("orderid='$transactionid'");
                    if($orderprodt){
                        foreach ($orderprodt->toArray() as $key => $value) {
                            $guid = new \Utilities\Guid\Guid();
                            $orderprod = new Orderprod();
                            $orderprod->setTransaction($transaction);
                            $orderprod->assign(array(
                                'id' => $guid->GUID(),
                                'orderid' => $order->id,
                                'productid' => $value['productid'],
                                'quantity' => $value['quantity'] ));
                            if($orderprod->save() == false){
                                $data = [];
                                foreach ($orderprod->getMessages() as $message) {
                                    $data[] = $message;
                                }
                                $transaction->rollback();
                            }

                            $ot = Orderprodtemp::findFirst("id='" . $value['id'] . "'");
                            $ot->setTransaction($transaction);
                            if($ot->delete() == false){
                                $data = [];
                                foreach ($ot->getMessages() as $message) {
                                    $data[] = $message;
                                }
                                $transaction->rollback();
                            }
                        }
                    }

                    $transaction->commit();

                    $body = "<p>Order Information</p>";
                    $body .= "<p>Invoice no. ".$transactionid."</p>";
                    $body .= "<p>Total amount : " . $order->amount . "</p>";
                    $body .= "<p class='text-bold'>Billing Information</p>";
                    $body .= "<p>Name : " . $b->firstname . " "  . $b->lastname . "</p>";
                    $body .= "<p>Address : " . $b->address . " </p>";
                    $body .= "<p>Phone no. : " . $b->phoneno . " </p>";
                    $body .= "<table>
                                  <thead>
                                      <tr>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total Price</th>
                                      </tr>
                                  </thead>
                                  <tbody>";

                    $bag = CB::bnbQuery("SELECT product.name,
                        product.price,
                        orderprod.quantity,
                        product.discount,
                        product.discount_from,
                        product.discount_to FROM orderprod INNER JOIN product ON orderprod.productid=product.productid WHERE orderprod.orderid='" . $order->id . "'");
                    foreach ($bag as $key => $value) {
                        $body .= "<tr>";
                        $body .= "<td>" . $value['name'] . "</td>";

                        if($value['discount'] != null && (strtotime(date("Y-m-d H:i:s")) >= strtotime($value["discount_from"]) && strtotime(date("Y-m-d H:i:s")) <= strtotime($value["discount_to"]))){
                            $body .= "<td>" . $value['price'] . "(-" . $value['discount'] . "%)</td>";
                        }else {
                            $body .= "<td>" . $value['price'] . "</td>";
                        }
                        $body .= "<td>" . $value['quantity'] . "</td>";
                        if($value['discount'] != null && (strtotime(date("Y-m-d H:i:s")) >= strtotime($value["discount_from"]) && strtotime(date("Y-m-d H:i:s")) <= strtotime($value["discount_to"]))){
                            $price = ($value['price'] - ($value['price'] * ($value['discount'] / 100)));
                            $total = $price * $value['quantity'];
                            $body .= "<td>" . $total . "</td>";
                        }else {
                            $total = $value['price'] * $value['quantity'];
                            $body .= "<td>" . $total . "</td>";
                        }
                        $body .= "</tr>";
                    }

                    $body .= "</tbody>
                              </table>";

                    $body .= "<p class='text-bold'>Shipping Information</p>";
                    $body .= "<p>Name : " . $sh->firstname . " "  . $sh->lastname . "</p>";
                    $body .= "<p>Address : " . $sh->address . " </p>";
                    $body .= "<p>Phone no. : " . $sh->phoneno . " </p>";

                    $m = Members::findFirst("memberid='$order->memberid'");

                    CB::sendMail($m->email, 'Body and Brain Order Confirmation', $body);

                } catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
                    die( json_encode(array('401' => $e->getMessage())) );
                }
            }
        }
    }
}
