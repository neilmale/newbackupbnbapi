<?php

namespace Controllers;

use \Models\Notification as Notification;
use \Models\Groupclassintrosession as Groupclassintrosession;
use \Models\Groupclassintrosessiontemp as Groupclassintrosessiontemp;
use \Models\Groupclassintrosessioncanceled as Groupclassintrosessioncanceled;
use \Models\Introsessiontemp as Introsessiontemp;
use \Models\Centerlocation as Centerlocation;
use \Models\Beneplace as Beneplace;
use \Models\Beneplacetemp as Beneplacetemp;
use \Models\Beneplacecanceled as Beneplacecanceled;
use \Models\Centeremail as Centeremail;
use \Models\Introsession as Introsession;
use \Models\Introsessioncanceled as Introsessioncanceled;
use \Models\Centerphonenumber as Centerphonenumber;
use \Models\Centermembership as Centermembership;
use \Models\Center as Center;
use \Models\Users as Users;
use \Models\States as States;
use \Models\Cities as Cities;
use \Models\Cities_extended as Cities_extended;
use \Models\Testimonies as Testimonies;
use \Controllers\ControllerBase as CB;
use \Controllers\PaypalController as Paypalctrl;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class GetstartedController extends \Phalcon\Mvc\Controller {

    public function searchlocationAction(){
        $request = new \Phalcon\Http\Request();
        $cb = new CB;

        if($request->isPost()){

            $state = $request->getPost('locationstate');
            $city = $request->getPost('locationcity');
            $zip = $request->getPost('locationzip');

            if ($zip != "" && $city == "" && $state == "") {
                $zipfrom = Cities_extended::findFirst('zip = "'.$zip.'"');

                if($zipfrom){
                  $searchresult = array();
                  $centers = CB::bnbQuery("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.status = 1 ");

                  foreach ($centers as $key => $value) {
                    $fdistance = $cb->distance($value['lat'], $value['lon'], $zipfrom->latitude, $zipfrom->longitude, 'Miles');
                    if( $fdistance <= 50 ) {

                      $value['fdistance'] = $fdistance;
                      $searchresult[] = $value;

                    }
                  }
                  // $getdis = array();
                  // $data = array();
                  //
                  // foreach ($center as $center) {
                  //   if( $cb->distance($center['lat'], $center['lon'], $zipfrom->latitude, $zipfrom->longitude, 'Miles')<= 50){
                  //     $getdis[] = $center['centerid'];
                  //   }
                  // }
                  //
                  // $fsearchresult = array();
                  // $final = array();
                  // foreach ($getdis as $centerid) {
                  //
                  //   $ssearchresult = CB::bnbQuery("SELECT * FROM center LEFT JOIN states ON states.state_code=center.centerstate LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerid = '".$centerid."'");
                  //
                  //   foreach ($ssearchresult as $ssearchresult) {
                  //     $ssearchresult["fdistance"] = intval($cb->distance($key['lat'], $key['lon'], $zipfrom->latitude, $zipfrom->longitude, 'Miles'));
                  //     $final[]=$ssearchresult;
                  //   }
                  // }
                  //  $searchresult =  $final;
                }
            } elseif ( ($zip != "" && $city != "" && $state != "") ||
                       ($zip == "" && $city != "" && $state != "") ||
                       ($zip != "" && $city == "" && $state != "") ||
                       ($zip != "" && $city != "" && $state == "") ||
                       ($zip == "" && $city == "" && $state != "")
                      ) {

              $center = CB::bnbQuery("SELECT * FROM center LEFT JOIN states ON states.state_code=center.centerstate LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerstate Like '%".$state."%' AND center.centercity Like '%".$city."%' AND center.centerzip Like '%".$zip."%' AND center.status = 1 ORDER BY center.centertitle");
              if($center) { $searchresult = $center; }
              else { $searchresult['error'] = 'no result found!ssss'; }

            } elseif ($zip == "" && $city != "" && $state == "") {

              $exploded = explode(",",$city);
              $city = strtolower($exploded[0]);
              $state = strtoupper(trim($exploded[1]));

              $centers = CB::bnbQuery("SELECT * FROM center LEFT JOIN states ON states.state_code=center.centerstate LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerstate Like '%".$state."%' AND center.centercity Like '%".$city."%' AND center.centerzip Like '%".$zip."%' AND center.status = 1");
              if($centers) { $searchresult = $centers; }
              else { $searchresult['error'] = 'no result found!'; }

            } else {
              $searchresult['error'] = 'no result found!';
            }
        }
        echo json_encode($searchresult);
    }


    public function introsessionlistAction($num, $page, $keyword,$centerid){


        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM introsession LEFT JOIN center ON introsession.centerid = center.centerid WHERE introsession.centerid = '" .$centerid. "' ORDER BY introsession.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT introsession.centerid FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid WHERE introsession.centerid = '" .$centerid. "' ORDER BY introsession.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        } else {

         $offsetfinal = ($page * 10) - 10;

         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT * FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid  WHERE introsession.centerid = '" .$centerid. "' and introsession.name LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.quantity LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and center.centertitle LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.payment LIKE '%" . $keyword . "%' ORDER BY introsession.datecreated DESC LIMIT " . $offsetfinal . ",10");

         $stmt->execute();
         $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


         $db1 = \Phalcon\DI::getDefault()->get('db');
         $stmt1 = $db1->prepare("SELECT introsession.centerid FROM   introsession LEFT JOIN center ON introsession.centerid=center.centerid WHERE introsession.centerid = '" .$centerid. "' and introsession.name LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.quantity LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.email LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and center.centertitle LIKE '%" . $keyword . "%' or introsession.centerid = '" .$centerid. "' and introsession.payment LIKE '%" . $keyword . "%' ORDER BY introsession.datecreated DESC ");

         $stmt1->execute();
         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

         $totalitem = count($searchresult1);

        }


        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }


    public function getcenterdetailsAction($centerid){
         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerid = '".$centerid."' ");

         $stmt->execute();
         $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
         echo json_encode(array('data' => $searchresult));
    }

    public function loadsessionAction($sessionid,$userid){
        $usertype = Users:: findFirst('id="'.$userid.'"');
        $changestatus = Introsession::findFirst("sessionid = '".$sessionid."'");
        if($changestatus){
            if($usertype->task == 'Administrator'){
                $changestatus->adminsessionstatus = 1;
            }
            else if(($usertype->task == 'Center Manager')){
                $changestatus->centersessionstatus = 1;
            }

            if (!$changestatus->save()) {
                $errors = array();
                foreach ($changestatus->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['msg'] = "status successfully saved!";
                $data['type'] = "success";

            }


        }

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM introsession WHERE sessionid = '".$sessionid."' ");

        $stmt->execute();
        $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
        echo json_encode($searchresult);
    }


     public function groupclasssessionlistAction($num, $page, $keyword,$centerid){


        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid = center.centerid WHERE groupclassintrosession.centerid = '" .$centerid. "' ORDER BY groupclassintrosession.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosession.centerid FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid WHERE groupclassintrosession.centerid = '" .$centerid. "' ORDER BY groupclassintrosession.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        }
        else {

           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid  WHERE groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.name LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.quantity LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and center.centertitle LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.payment LIKE '%" . $keyword . "%' ORDER BY groupclassintrosession.datecreated DESC LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosession.centerid FROM   groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid WHERE groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.name LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.quantity LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.email LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and center.centertitle LIKE '%" . $keyword . "%' or groupclassintrosession.centerid = '" .$centerid. "' and groupclassintrosession.payment LIKE '%" . $keyword . "%' ORDER BY groupclassintrosession.datecreated DESC ");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);

       }


        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }

    public function loadgroupsessionAction($sessionid,$userid){
        $usertype = Users:: findFirst('id="'.$userid.'"');
        $changestatus = Groupclassintrosession::findFirst("sessionid = '".$sessionid."'");
        if($changestatus){
            if($usertype->task == 'Administrator'){
                $changestatus->adminsessionstatus = 1;
            }
            else if(($usertype->task == 'Center Manager')){
                $changestatus->centersessionstatus = 1;
            }
            if (!$changestatus->save()) {
                $errors = array();
                foreach ($changestatus->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['msg'] = "status successfully saved!";
                $data['type'] = "success";

            }


        }

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM groupclassintrosession WHERE sessionid = '".$sessionid."' ");

        $stmt->execute();
        $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
        echo json_encode($searchresult);
    }

    public function loadcenterAction($centerid){
       $db = \Phalcon\DI::getDefault()->get('db');
       $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerid = '".$centerid."'");

       $stmt->execute();
       $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);

       echo json_encode($searchresult);
    }


    public function allintrosessionlistAction($num, $page, $keyword){

        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT *,introsession.datecreated as 'classdatecreated' FROM introsession LEFT JOIN center ON introsession.centerid = center.centerid ORDER BY introsession.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT introsession.centerid FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid ORDER BY introsession.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        } else {

         $offsetfinal = ($page * 10) - 10;

         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT *,introsession.datecreated as 'classdatecreated' FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid  WHERE  introsession.name LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  introsession.quantity LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  introsession.payment LIKE '%" . $keyword . "%' or  introsession.confirmationnumber LIKE '%" . $keyword . "%' ORDER BY introsession.datecreated DESC LIMIT " . $offsetfinal . ",10");

         $stmt->execute();
         $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


         $db1 = \Phalcon\DI::getDefault()->get('db');
         $stmt1 = $db1->prepare("SELECT introsession.centerid FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid WHERE  introsession.name LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  introsession.quantity LIKE '%" . $keyword . "%' or  introsession.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  introsession.payment LIKE '%" . $keyword . "%' or  introsession.confirmationnumber LIKE '%" . $keyword . "%' ORDER BY introsession.datecreated DESC ");

         $stmt1->execute();
         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

         $totalitem = count($searchresult1);

        }


        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }

    public function allgroupclasssessionlistAction($num, $page, $keyword){


        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT *,groupclassintrosession.datecreated as 'classdatecreated' FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid = center.centerid ORDER BY groupclassintrosession.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosession.centerid FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid ORDER BY groupclassintrosession.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        }
        else {

           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT *,groupclassintrosession.datecreated as 'classdatecreated' FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid  WHERE  groupclassintrosession.name LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  groupclassintrosession.quantity LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  groupclassintrosession.payment LIKE '%" . $keyword . "%' ORDER BY groupclassintrosession.datecreated DESC LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosession.centerid FROM   groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid WHERE  groupclassintrosession.name LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  groupclassintrosession.quantity LIKE '%" . $keyword . "%' or  groupclassintrosession.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  groupclassintrosession.payment LIKE '%" . $keyword . "%' ORDER BY groupclassintrosession.datecreated DESC ");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);

       }


        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }

    public function savetemppaypaltransactionAction(){
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {

            $guid = new \Utilities\Guid\Guid();
            $sessionid = $guid->GUID();
            $centerid = $request->getPost("centerid");
            $transactionid = $request->getPost("transactionid");
            $name = $request->getPost("name");
            $email = $request->getPost("email");
            $phone = $request->getPost("phone");
            $day = $request->getPost("day");
            $hour = $request->getPost("hour");
            $comment = $request->getPost("comment");
            $qty = $request->getPost("qty");
            $device = $request->getPost("device");
            $device = $request->getPost("device");
            $deviceos = $request->getPost("deviceos");
            $devicebrowser = $request->getPost("devicebrowser");
            $payment_amount = $request->getPost("payment_amount");
            $classtype = $request->getPost("classtype");
            $classtypebeneplace = $request->getPost("classtypebeneplace");

            if($classtype == 'intro'){

              $tempsession = new Introsessiontemp();
              $tempsession->sessionid = $sessionid;
              $tempsession->centerid = $centerid;
              $tempsession->confirmationnumber = $transactionid;
              $tempsession->name = $name;
              $tempsession->email = $email;
              $tempsession->phone = $phone;
              $tempsession->day = $day;
              $tempsession->hour = $hour;
              $tempsession->comment = $comment;
              $tempsession->quantity = $qty;
              $tempsession->device = $device;
              $tempsession->deviceos = $deviceos;
              $tempsession->devicebrowser = $devicebrowser;
              $tempsession->datecreated = date('Y-m-d H:i:s');
              $tempsession->centersessionstatus = 0;
              $tempsession->regionsessionstatus = 0;
              $tempsession->adminsessionstatus = 0;
              $tempsession->payment = 'Paypal';
              $tempsession->paid = $payment_amount;
              $tempsession->verificationstatus = 0;

              if($tempsession->save()){
                  $data['success'] = 'temp data successfully saved';
              }
              else{
                  $data['error'] = 'temp data not saved';
              }

            }
            else if($classtype == 'group'){

              $tempgroupsession = new Groupclassintrosessiontemp();
              $tempgroupsession->sessionid = $sessionid;
              $tempgroupsession->centerid = $centerid;
              $tempgroupsession->confirmationnumber = $transactionid;
              $tempgroupsession->name = $name;
              $tempgroupsession->email = $email;
              $tempgroupsession->phone = $phone;
              $tempgroupsession->day = $day;
              $tempgroupsession->hour = $hour;
              $tempgroupsession->comment = $comment;
              $tempgroupsession->quantity = $qty;
              $tempgroupsession->device = $device;
              $tempgroupsession->deviceos = $deviceos;
              $tempgroupsession->devicebrowser = $devicebrowser;
              $tempgroupsession->datecreated = date('Y-m-d H:i:s');
              $tempgroupsession->centersessionstatus = 0;
              $tempgroupsession->regionsessionstatus = 0;
              $tempgroupsession->adminsessionstatus = 0;
              $tempgroupsession->payment = 'Paypal';
              $tempgroupsession->paid = $payment_amount;
              $tempgroupsession->verificationstatus = 0;

              if($tempgroupsession->save()){
                  $data['success'] = 'temp data successfully saved';
              }
              else{
                  $data['error'] = 'temp data not saved';
              }

            }
            else if($classtype == 'beneplace'){

              $tempbeneplace = new Beneplacetemp();
              $tempbeneplace->sessionid = $sessionid;
              $tempbeneplace->centerid = $centerid;
              $tempbeneplace->confirmationnumber = $transactionid;
              $tempbeneplace->name = $name;
              $tempbeneplace->email = $email;
              $tempbeneplace->phone = $phone;
              $tempbeneplace->day = $day;
              $tempbeneplace->hour = $hour;
              $tempbeneplace->comment = $comment;
              $tempbeneplace->quantity = $qty;
              $tempbeneplace->device = $device;
              $tempbeneplace->deviceos = $deviceos;
              $tempbeneplace->devicebrowser = $devicebrowser;
              $tempbeneplace->datecreated = date('Y-m-d H:i:s');
              $tempbeneplace->centersessionstatus = 0;
              $tempbeneplace->regionsessionstatus = 0;
              $tempbeneplace->adminsessionstatus = 0;
              $tempbeneplace->payment = 'Paypal';
              $tempbeneplace->paid = $payment_amount;
              $tempbeneplace->verificationstatus = 0;
              $tempbeneplace->program = $classtypebeneplace;

              if($tempbeneplace->save()){
                  $data['success'] = 'temp data successfully saved';
              }
              else{
                  $data['error'] = 'temp data not saved';
              }

            }

            echo json_encode($data);
        }
    }

    public function authorizeAction(){
      $request = new \Phalcon\Http\Request();

      if($request->isPost()){

        $authorizeid = $request->getPost("authorizeid");
        $authorizekey = $request->getPost("authorizekey");
        $cardnumber = $request->getPost("cardnumber");
        $expimonth = $request->getPost("expimonth");
        $expiyear = $request->getPost("expiyear");
        $billfirstname = $request->getPost("billfirstname");
        $billlastname = $request->getPost("billlastname");
        $billadd1 = $request->getPost("billadd1");
        $billstate = $request->getPost("billstate");
        $billzip = $request->getPost("billzip");

        $guid = new \Utilities\Guid\Guid();
        $sessionid = $guid->GUID();
        $centerid = $request->getPost("centerid");
        $temptransactionid = $request->getPost("temptransactionid");
        $name = $request->getPost("name");
        $email = $request->getPost("email");
        $phone = $request->getPost("phone");
        $day = $request->getPost("day");
        $hour = $request->getPost("hour");
        $comment = $request->getPost("comment");
        $qty = $request->getPost("qty");
        $device = $request->getPost("device");
        $deviceos = $request->getPost("deviceos");
        $devicebrowser = $request->getPost("devicebrowser");
        $payment_amount = $request->getPost("payment_amount");
        $classtype = $request->getPost("classtype");
        $beneplaceclasstype = $request->getPost("beneplaceclasstype");
        $classsession = $request->getPost("classsession");


        $autorizecheck = CB::authorizeCheck($authorizeid, $authorizekey, $cardnumber, $expimonth, $expiyear, $payment_amount, $classsession, $billfirstname, $billlastname, $billadd1, $billstate, $billzip);

        //autorizecheck if
        if($autorizecheck['response'][0] == 1){

          if($classtype == 'intro'){
              $tempsession = new Introsessiontemp();
              $tempsession->sessionid = $sessionid;
              $tempsession->centerid = $centerid;
              $tempsession->confirmationnumber = $temptransactionid;
              $tempsession->name = $name;
              $tempsession->email = $email;
              $tempsession->phone = $phone;
              $tempsession->day = $day;
              $tempsession->hour = $hour;
              $tempsession->comment = $comment;
              $tempsession->quantity = $qty;
              $tempsession->device = $device;
              $tempsession->deviceos = $deviceos;
              $tempsession->devicebrowser = $devicebrowser;
              $tempsession->datecreated = date('Y-m-d H:i:s');
              $tempsession->centersessionstatus = 0;
              $tempsession->regionsessionstatus = 0;
              $tempsession->adminsessionstatus = 0;
              $tempsession->payment = $autorizecheck['response'][51];
              $tempsession->paid = $payment_amount;
              $tempsession->verificationstatus = 0;

              if($tempsession->save()){

                  $movetempsession =  Paypalctrl::introsessionsaving($temptransactionid, $autorizecheck['response'][6], $autorizecheck['response'][51]);
                  if($movetempsession == 'success'){
                    $data['success'] = 'success';
                    $data['msg'] = $autorizecheck['response'][6];
                  }
                  else if($movetempsession == 'error'){
                    $data['error'] = 'savingerror';
                    $data['msg'] = 'Something went wrong please try again later or contact our admin';
                  }

              }
              else{
                $data['error'] = 'savingerror';
                $data['msg'] = 'Something went wrong please try again later or contact our admin';
              }

            }
            else if($classtype == 'group'){
              $tempgroupsession = new Groupclassintrosessiontemp();
              $tempgroupsession->sessionid = $sessionid;
              $tempgroupsession->centerid = $centerid;
              $tempgroupsession->confirmationnumber = $temptransactionid;
              $tempgroupsession->name = $name;
              $tempgroupsession->email = $email;
              $tempgroupsession->phone = $phone;
              $tempgroupsession->day = $day;
              $tempgroupsession->hour = $hour;
              $tempgroupsession->comment = $comment;
              $tempgroupsession->quantity = $qty;
              $tempgroupsession->device = $device;
              $tempgroupsession->deviceos = $deviceos;
              $tempgroupsession->devicebrowser = $devicebrowser;
              $tempgroupsession->datecreated = date('Y-m-d H:i:s');
              $tempgroupsession->centersessionstatus = 0;
              $tempgroupsession->regionsessionstatus = 0;
              $tempgroupsession->adminsessionstatus = 0;
              $tempgroupsession->payment = $autorizecheck['response'][51];
              $tempgroupsession->paid = $payment_amount;
              $tempgroupsession->verificationstatus = 0;

              if($tempgroupsession->save()){
                  $movetempsession =  Paypalctrl::groupsessionsaving($temptransactionid, $autorizecheck['response'][6], $autorizecheck['response'][51]);
                  if($movetempsession == 'success'){
                    $data['success'] = 'success';
                    $data['msg'] = $autorizecheck['response'][6];
                  }
                  else if($movetempsession == 'error'){
                    $data['error'] = 'savingerror';
                    $data['msg'] = 'Something went wrong please try again later or contact our admin';
                  }
              }
              else{
                $data['error'] = 'savingerror';
                $data['msg'] = 'Something went wrong please try again later or contact our admin';
              }

            }
            else if($classtype == 'beneplace'){
              $tempbeneplace = new Beneplacetemp();
              $tempbeneplace->sessionid = $sessionid;
              $tempbeneplace->centerid = $centerid;
              $tempbeneplace->confirmationnumber = $temptransactionid;
              $tempbeneplace->name = $name;
              $tempbeneplace->email = $email;
              $tempbeneplace->phone = $phone;
              $tempbeneplace->day = $day;
              $tempbeneplace->hour = $hour;
              $tempbeneplace->comment = $comment;
              $tempbeneplace->quantity = $qty;
              $tempbeneplace->device = $device;
              $tempbeneplace->deviceos = $deviceos;
              $tempbeneplace->devicebrowser = $devicebrowser;
              $tempbeneplace->datecreated = date('Y-m-d H:i:s');
              $tempbeneplace->centersessionstatus = 0;
              $tempbeneplace->regionsessionstatus = 0;
              $tempbeneplace->adminsessionstatus = 0;
              $tempbeneplace->payment = $autorizecheck['response'][51];
              $tempbeneplace->paid = $payment_amount;
              $tempbeneplace->verificationstatus = 0;
              $tempbeneplace->program = $beneplaceclasstype;

              if($tempbeneplace->save()){
                  $movetempbeneplace =  Paypalctrl::beneplacesaving($temptransactionid, $autorizecheck['response'][6], $autorizecheck['response'][51]);
                  if($movetempbeneplace == 'success'){
                    $data['success'] = 'success';
                    $data['msg'] = $autorizecheck['response'][6];
                  }
                  else if($movetempsession == 'error'){
                    $data['error'] = 'savingerror';
                    $data['msg'] = 'Something went wrong please try again later or contact our admin';
                  }
              }
              else{
                $data['error'] = 'savingerror';
                $data['msg'] = 'Something went wrong please try again later or contact our admin';
              }

            }

        }
        //end of autorizecheck if

        //autorizecheck else
        else{
          $data['error'] = 'authorizeerror';
          $data['mgs'] = $autorizecheck['response'];
        }
        //end of autorizecheck else

      }

      echo json_encode($data);

    }

  public function allintrosessionunverifiedlistAction($num, $page, $keyword){

    if ($keyword == 'null' || $keyword == 'undefined') {
       $offsetfinal = ($page * 10) - 10;

       $db = \Phalcon\DI::getDefault()->get('db');
       $stmt = $db->prepare("SELECT *,introsessiontemp.datecreated as 'classdatecreated' FROM introsessiontemp LEFT JOIN center ON introsessiontemp.centerid = center.centerid WHERE verificationstatus=0 ORDER BY introsessiontemp.datecreated DESC  LIMIT " . $offsetfinal . ",10");

       $stmt->execute();
       $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


       $db1 = \Phalcon\DI::getDefault()->get('db');
       $stmt1 = $db1->prepare("SELECT introsessiontemp.centerid FROM introsessiontemp LEFT JOIN center ON introsessiontemp.centerid=center.centerid WHERE verificationstatus=0 ORDER BY introsessiontemp.datecreated DESC");

       $stmt1->execute();
       $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

       $totalitem = count($searchresult1);
    } else {

     $offsetfinal = ($page * 10) - 10;

     $db = \Phalcon\DI::getDefault()->get('db');
     $stmt = $db->prepare("SELECT *,introsessiontemp.datecreated as 'classdatecreated' FROM introsessiontemp LEFT JOIN center ON introsessiontemp.centerid=center.centerid  WHERE  (introsessiontemp.name LIKE '%" . $keyword . "%' or  introsessiontemp.email LIKE '%" . $keyword . "%' or  introsessiontemp.email LIKE '%" . $keyword . "%' or  introsessiontemp.quantity LIKE '%" . $keyword . "%' or  introsessiontemp.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  introsessiontemp.payment LIKE '%" . $keyword . "%' or  introsessiontemp.confirmationnumber LIKE '%" . $keyword . "%') AND introsessiontemp.verificationstatus=0 ORDER BY introsessiontemp.datecreated DESC LIMIT " . $offsetfinal . ",10");

     $stmt->execute();
     $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


     $db1 = \Phalcon\DI::getDefault()->get('db');
     $stmt1 = $db1->prepare("SELECT introsessiontemp.centerid FROM introsessiontemp LEFT JOIN center ON introsessiontemp.centerid=center.centerid WHERE  (introsessiontemp.name LIKE '%" . $keyword . "%' or  introsessiontemp.email LIKE '%" . $keyword . "%' or  introsessiontemp.email LIKE '%" . $keyword . "%' or  introsessiontemp.quantity LIKE '%" . $keyword . "%' or  introsessiontemp.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  introsessiontemp.payment LIKE '%" . $keyword . "%' or  introsessiontemp.confirmationnumber LIKE '%" . $keyword . "%') AND introsessiontemp.verificationstatus=0 ORDER BY introsessiontemp.datecreated DESC ");

     $stmt1->execute();
     $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

     $totalitem = count($searchresult1);

    }


    echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

  }

  public function allintrosessioncanceledlistAction($num, $page, $keyword){

    if ($keyword == 'null' || $keyword == 'undefined') {
       $offsetfinal = ($page * 10) - 10;

       $db = \Phalcon\DI::getDefault()->get('db');
       $stmt = $db->prepare("SELECT *,introsessioncanceled.datecreated as 'classdatecreated' FROM introsessioncanceled LEFT JOIN center ON introsessioncanceled.centerid = center.centerid  ORDER BY introsessioncanceled.datecreated DESC  LIMIT " . $offsetfinal . ",10");

       $stmt->execute();
       $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


       $db1 = \Phalcon\DI::getDefault()->get('db');
       $stmt1 = $db1->prepare("SELECT introsessioncanceled.centerid FROM introsessioncanceled LEFT JOIN center ON introsessioncanceled.centerid=center.centerid ORDER BY introsessioncanceled.datecreated DESC");

       $stmt1->execute();
       $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

       $totalitem = count($searchresult1);
    } else {

     $offsetfinal = ($page * 10) - 10;

     $db = \Phalcon\DI::getDefault()->get('db');
     $stmt = $db->prepare("SELECT *,introsessioncanceled.datecreated as 'classdatecreated' FROM introsessioncanceled LEFT JOIN center ON introsessioncanceled.centerid=center.centerid  WHERE  (introsessioncanceled.name LIKE '%" . $keyword . "%' or  introsessioncanceled.email LIKE '%" . $keyword . "%' or  introsessioncanceled.email LIKE '%" . $keyword . "%' or  introsessioncanceled.quantity LIKE '%" . $keyword . "%' or  introsessioncanceled.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  introsessioncanceled.payment LIKE '%" . $keyword . "%' or  introsessioncanceled.confirmationnumber LIKE '%" . $keyword . "%')  ORDER BY introsessioncanceled.datecreated DESC LIMIT " . $offsetfinal . ",10");

     $stmt->execute();
     $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


     $db1 = \Phalcon\DI::getDefault()->get('db');
     $stmt1 = $db1->prepare("SELECT introsessioncanceled.centerid FROM introsessioncanceled LEFT JOIN center ON introsessioncanceled.centerid=center.centerid WHERE  (introsessioncanceled.name LIKE '%" . $keyword . "%' or  introsessioncanceled.email LIKE '%" . $keyword . "%' or  introsessioncanceled.email LIKE '%" . $keyword . "%' or  introsessioncanceled.quantity LIKE '%" . $keyword . "%' or  introsessioncanceled.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  introsessioncanceled.payment LIKE '%" . $keyword . "%' or  introsessioncanceled.confirmationnumber LIKE '%" . $keyword . "%') ORDER BY introsessioncanceled.datecreated DESC ");

     $stmt1->execute();
     $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

     $totalitem = count($searchresult1);

    }


    echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

  }

  public function loadunverifiedsessionAction($sessionid){
      $db = \Phalcon\DI::getDefault()->get('db');
      $stmt = $db->prepare("SELECT * FROM introsessiontemp WHERE sessionid = '".$sessionid."' ");

      $stmt->execute();
      $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
      echo json_encode($searchresult);
  }
  public function loadcanceledsessionAction($sessionid){
      $db = \Phalcon\DI::getDefault()->get('db');
      $stmt = $db->prepare("SELECT * FROM introsessioncanceled WHERE sessionid = '".$sessionid."' ");

      $stmt->execute();
      $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
      echo json_encode($searchresult);
  }


  public function verifysessionAction($id) {
    $request = new \Phalcon\Http\Request();

      if($request->isPost()){

          $sessionid = $request->getPost('sessionid');
          $confirmation = $request->getPost('confirmation');

          $findsession = CB::bnbQueryFirst("SELECT * FROM introsessiontemp WHERE sessionid = '".$sessionid."' and verificationstatus = 0");
          if($findsession){
            $movetempsession=  Paypalctrl::introsessionsaving($findsession['confirmationnumber'], $confirmation, $findsession['payment']);
            if($movetempsession == 'success'){
              $data['success'] = 'success';
              echo json_encode($data);
            }
            else if($movetempsession == 'error'){
              $data['error'] = 'Something went wrong session not verified';
              echo json_encode($data);
            }
          }
          else{
            $data['error'] = 'Something went wrong session not verified';
            echo json_encode($data);
          }



      }
  }

  // public function verifysessionAction($id) {
  //   $request = new \Phalcon\Http\Request();
  //   if($request->isPost()){
  //     try{
  //       $transactionManager = new TransactionManager();
  //       $transaction = $transactionManager->get();
  //
  //       $it = Introsessiontemp::findFirst("sessionid='$id'");
  //       $it->setTransaction($transaction);
  //
  //       $i = new Introsession();
  //       $i->setTransaction($transaction);
  //       $i->assign(array(
  //         'sessionid' => $id,
  //         'centerid' => $it->centerid,
  //         'confirmationnumber' => $request->getPost('conf'),
  //         'name' => $it->name,
  //         'email' => $it->email,
  //         'phone' => $it->phone,
  //         'day' => $it->day,
  //         'hour' => $it->hour,
  //         'comment' => $it->comment,
  //         'quantity' => $it->quantity,
  //         'datecreated' => $it->datecreated,
  //         'centersessionstatus' => 0,
  //         'regionsessionstatus' => 0,
  //         'adminsessionstatus' => 0,
  //         'device' => $it->device,
  //         'deviceos' => $it->deviceos,
  //         'devicebrowser' => $it->devicebrowser,
  //         'payment' => $it->payment,
  //         'paid' => $it->paid ));
  //
  //       if ($i->save() == false) {
  //           $data = [];
  //           foreach ($i->getMessages() as $message) {
  //               $data[] = $message;
  //           }
  //           $transaction->rollback();
  //       }
  //
  //       $it->verificationstatus = 1;
  //       if ($it->save() == false) {
  //           $data = [];
  //           foreach ($it->getMessages() as $message) {
  //               $data[] = $message;
  //           }
  //           $transaction->rollback();
  //       }
  //
  //       $transaction->commit();
  //       echo json_encode(array('success' => 'Session has been successfully verified.' ));
  //     }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
  //         die( json_encode(array('401' => $e->getMessage())) );
  //     }
  //   }
  // }


  public function allgroupclasssessionunverifiedlistAction($num, $page, $keyword){


        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT *,groupclassintrosessiontemp.datecreated as 'classdatecreated' FROM groupclassintrosessiontemp LEFT JOIN center ON groupclassintrosessiontemp.centerid = center.centerid WHERE verificationstatus=0 ORDER BY groupclassintrosessiontemp.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosessiontemp.centerid FROM groupclassintrosessiontemp LEFT JOIN center ON groupclassintrosessiontemp.centerid=center.centerid WHERE verificationstatus=0 ORDER BY groupclassintrosessiontemp.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        }
        else {

           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT *,groupclassintrosessiontemp.datecreated as 'classdatecreated' FROM groupclassintrosessiontemp LEFT JOIN center ON groupclassintrosessiontemp.centerid=center.centerid  WHERE  (groupclassintrosessiontemp.name LIKE '%" . $keyword . "%' or  groupclassintrosessiontemp.email LIKE '%" . $keyword . "%' or  groupclassintrosessiontemp.email LIKE '%" . $keyword . "%' or  groupclassintrosessiontemp.quantity LIKE '%" . $keyword . "%' or  groupclassintrosessiontemp.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  groupclassintrosessiontemp.payment LIKE '%" . $keyword . "%') WHERE verificationstatus=0 ORDER BY groupclassintrosessiontemp.datecreated DESC LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosessiontemp.centerid FROM   groupclassintrosessiontemp LEFT JOIN center ON groupclassintrosessiontemp.centerid=center.centerid WHERE  (groupclassintrosessiontemp.name LIKE '%" . $keyword . "%' or  groupclassintrosessiontemp.email LIKE '%" . $keyword . "%' or  groupclassintrosessiontemp.email LIKE '%" . $keyword . "%' or  groupclassintrosessiontemp.quantity LIKE '%" . $keyword . "%' or  groupclassintrosessiontemp.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  groupclassintrosessiontemp.payment LIKE '%" . $keyword . "%') WHERE verificationstatus=0 ORDER BY groupclassintrosessiontemp.datecreated DESC ");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);

        }

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }

    public function loadunverifiedgroupsessionAction($sessionid){

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM groupclassintrosessiontemp WHERE sessionid = '".$sessionid."' ");

        $stmt->execute();
        $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
        echo json_encode($searchresult);
    }

    // public function verifygroupsessionAction($id) {
    //     $request = new \Phalcon\Http\Request();
    // if($request->isPost()){
    //   try{
    //     $transactionManager = new TransactionManager();
    //     $transaction = $transactionManager->get();
    //
    //     $gt = Groupclassintrosessiontemp::findFirst("sessionid='$id'");
    //     $gt->setTransaction($transaction);
    //
    //     $i = new Groupclassintrosession();
    //     $i->setTransaction($transaction);
    //     $i->assign(array(
    //       'sessionid' => $id,
    //       'centerid' => $gt->centerid,
    //       'confirmationnumber' => $request->getPost('conf'),
    //       'name' => $gt->name,
    //       'email' => $gt->email,
    //       'phone' => $gt->phone,
    //       'day' => $gt->day,
    //       'hour' => $gt->hour,
    //       'comment' => $gt->comment,
    //       'quantity' => $gt->quantity,
    //       'datecreated' => $gt->datecreated,
    //       'centersessionstatus' => 0,
    //       'regionsessionstatus' => 0,
    //       'adminsessionstatus' => 0,
    //       'device' => $gt->device,
    //       'deviceos' => $gt->deviceos,
    //       'devicebrowser' => $gt->devicebrowser,
    //       'payment' => $gt->payment,
    //       'paid' => $gt->paid ));
    //
    //     if ($i->save() == false) {
    //         $data = [];
    //         foreach ($i->getMessages() as $message) {
    //             $data[] = $message;
    //         }
    //         $transaction->rollback();
    //     }
    //
    //     $gt->verificationstatus = 1;
    //     if ($gt->save() == false) {
    //         $data = [];
    //         foreach ($gt->getMessages() as $message) {
    //             $data[] = $message;
    //         }
    //         $transaction->rollback();
    //     }
    //
    //     $transaction->commit();
    //     echo json_encode(array('success' => 'Session has been successfully verified.' ));
    //   }catch (\Phalcon\Mvc\Model\Transaction\Failed $e) {
    //       die( json_encode(array('401' => $e->getMessage())) );
    //   }
    // }
    // }

    public function verifygroupsessionAction() {
      $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $sessionid = $request->getPost('sessionid');
            $confirmation = $request->getPost('confirmation');

            $findsession = CB::bnbQueryFirst("SELECT * FROM groupclassintrosessiontemp WHERE sessionid = '".$sessionid."' and verificationstatus = 0");
            if($findsession){
              $movetempsession=  Paypalctrl::groupsessionsaving($findsession['confirmationnumber'], $confirmation, $findsession['payment']);
              if($movetempsession == 'success'){
                $data['success'] = 'success';
                echo json_encode($data);
              }
              else if($movetempsession == 'error'){
                $data['error'] = 'Something went wrong session not verified';
                echo json_encode($data);
              }
            }
            else{
              $data['error'] = 'Something went wrong session not verified';
              echo json_encode($data);
            }



        }
    }

  public function getsuccesstransactionAction($transactionid, $moduletype){
    if($moduletype == 'intro'){
      $list = CB::bnbQueryFirst("SELECT * FROM introsession LEFT JOIN center ON introsession.centerid=center.centerid LEFT JOIN states ON states.state_code=center.centerstate LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE introsession.confirmationnumber = '".$transactionid."'");
      if($list){
        $list['moduletype'] = '1-on-1 Intro Session';
        echo json_encode($list);
      }
    }
    else if($moduletype == 'group'){
      $list = CB::bnbQueryFirst("SELECT * FROM groupclassintrosession LEFT JOIN center ON groupclassintrosession.centerid=center.centerid LEFT JOIN states ON states.state_code=center.centerstate LEFT JOIN centerlocation ON center.centerid=centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid=centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid=centeremail.centerid WHERE groupclassintrosession.confirmationnumber = '".$transactionid."'");
      if($list){
        $list['moduletype'] = '1 Group Class + 1-on-1 Intro Session';
        echo json_encode($list);
      }
    }
    else if($moduletype == 'beneplace'){
      $list = CB::bnbQueryFirst("SELECT * FROM beneplace LEFT JOIN center ON beneplace.centerid=center.centerid LEFT JOIN states ON states.state_code=center.centerstate LEFT JOIN centerlocation ON center.centerid=centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid=centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid=centeremail.centerid WHERE beneplace.confirmationnumber = '".$transactionid."'");
      if($list){
        $list['moduletype'] = '1 Group Class + 1-on-1 Intro Session';
        echo json_encode($list);
      }
    }
  }

  public function searchzipAction($zip) {
    $cb = new CB;
    $zipfrom = Cities_extended::findFirst('zip = "'.$zip.'"');
    if($zipfrom){
      $searchresult = array();
      $centers = CB::bnbQuery("SELECT * FROM center LEFT JOIN states ON states.state_code=center.centerstate LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.status = 1 ");

      // $getdis = array();
      // $data = array();

      foreach ($centers as $key => $value) {
        $fdistance = $cb->distance($value['lat'], $value['lon'], $zipfrom->latitude, $zipfrom->longitude, 'Miles');
        if( $fdistance <= 50 ) {

          $value['fdistance'] = $fdistance;
          $searchresult[] = $value;

        }
      }

      echo json_encode($searchresult);
      // echo json_encode($centers[0]['fdistance']);
      // $fsearchresult = array();
      // $final = array();
      // foreach ($getdis as $centerid) {
      //
      //   $ssearchresult = CB::bnbQuery("SELECT * FROM center LEFT JOIN states ON states.state_code=center.centerstate LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE center.centerid = '".$centerid."'");
      //
      //   foreach ($ssearchresult as $ssearchresult) {
      //     $ssearchresult["fdistance"] = intval($cb->distance($key['lat'], $key['lon'], $zipfrom->latitude, $zipfrom->longitude, 'Miles'));
      //     $final[]=$ssearchresult;
      //   }
      // }
      // $searchresult =  $final;
      // echo json_encode($searchresult);
    }
  }

  public function getcenterAction($slug) {
    $center = CB::bnbQuery("SELECT * FROM center LEFT JOIN states ON states.state_code=center.centerstate LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid LEFT JOIN centeremail ON center.centerid = centeremail.centerid WHERE (center.centerslugs='".$slug."' OR center.centerslugs2='" . $slug . "' OR center.centerslugs3='" . $slug . "') AND center.status = 1");
    if($center) { $searchresult = $center[0]; }
    else { $searchresult['error'] = 'no result found!'; }
    echo json_encode($searchresult);
  }



  public function allgroupclasssessioncanceledlistAction($num, $page, $keyword){


        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT *,groupclassintrosessioncanceled.datecreated as 'classdatecreated' FROM groupclassintrosessioncanceled LEFT JOIN center ON groupclassintrosessioncanceled.centerid = center.centerid  ORDER BY groupclassintrosessioncanceled.datecreated DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosessioncanceled.centerid FROM groupclassintrosessioncanceled LEFT JOIN center ON groupclassintrosessioncanceled.centerid=center.centerid  ORDER BY groupclassintrosessioncanceled.datecreated DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);
        }
        else {

           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT *,groupclassintrosessioncanceled.datecreated as 'classdatecreated' FROM groupclassintrosessioncanceled LEFT JOIN center ON groupclassintrosessioncanceled.centerid=center.centerid  WHERE  (groupclassintrosessioncanceled.name LIKE '%" . $keyword . "%' or  groupclassintrosessioncanceled.email LIKE '%" . $keyword . "%' or  groupclassintrosessioncanceled.email LIKE '%" . $keyword . "%' or  groupclassintrosessioncanceled.quantity LIKE '%" . $keyword . "%' or  groupclassintrosessioncanceled.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  groupclassintrosessioncanceled.payment LIKE '%" . $keyword . "%')  ORDER BY groupclassintrosessioncanceled.datecreated DESC LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT groupclassintrosessioncanceled.centerid FROM   groupclassintrosessioncanceled LEFT JOIN center ON groupclassintrosessioncanceled.centerid=center.centerid WHERE  (groupclassintrosessioncanceled.name LIKE '%" . $keyword . "%' or  groupclassintrosessioncanceled.email LIKE '%" . $keyword . "%' or  groupclassintrosessioncanceled.email LIKE '%" . $keyword . "%' or  groupclassintrosessioncanceled.quantity LIKE '%" . $keyword . "%' or  groupclassintrosessioncanceled.email LIKE '%" . $keyword . "%' or  center.centertitle LIKE '%" . $keyword . "%' or  groupclassintrosessioncanceled.payment LIKE '%" . $keyword . "%')  ORDER BY groupclassintrosessioncanceled.datecreated DESC ");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalitem = count($searchresult1);

        }

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalitem));

    }

    public function loadcanceledgroupsessionAction($sessionid){

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM groupclassintrosessioncanceled WHERE sessionid = '".$sessionid."' ");

        $stmt->execute();
        $searchresult = $stmt->fetch(\PDO::FETCH_ASSOC);
        echo json_encode($searchresult);
    }

  public function feTryAClassIndexAction() {

    $data['outeroneclassbanner'] = false;
		$outeronebanner = CB::bnbQueryFirst("SELECT image FROM images WHERE page = 'oneclassouter' ");
    if($outeronebanner == true) {
      $data['outeronebanner'] = $outeronebanner;
      $data['outeroneclassbanner'] = true;
    }

    $data['inneroneclassbanner'] = false;
		$inneronebanner = CB::bnbQueryFirst("SELECT image FROM images WHERE page = 'oneclassinner' ");
    if($inneronebanner == true) {
      $data['inneronebanner'] = $inneronebanner;
      $data['inneroneclassbanner'] = true;
    }

    $data['outergroupclassbanner'] = false;
		$outergroupbanner = CB::bnbQueryFirst("SELECT image FROM images WHERE page = 'groupclassouter' ");
    if($outergroupbanner == true) {
      $data['outergroupbanner'] = $outergroupbanner;
      $data['outergroupclassbanner'] = true;
    }

    $data['innergroupclassbanner'] = false;
		$innergroupbanner = CB::bnbQueryFirst("SELECT image FROM images WHERE page = 'groupclassinner' ");
    if($innergroupbanner == true) {
      $data['innergroupbanner'] = $innergroupbanner;
      $data['innergroupclassbanner'] = true;
    }

		// if($oneclassbanners == true) {
		// 		if(count($oneclassbanners) > 2) { $data['oneclassslider'] = true; }
		// 		else { $data['oneclassslider'] = false; }
		// 		$data['oneclassbanners'] = $oneclassbanners;
		// } else {
		// 	$data['oneclassbanner'] = false;
		// }
    //
    // $data['groupclassbanner'] = true;
		// $groupclassbanners = CB::bnbQuery("SELECT url, image FROM images WHERE page = 'groupclass' AND status = 1 ORDER BY sequence ");
		// if($groupclassbanners == true) {
		// 		if(count($oneclassbanners) > 2) { $data['groupclassslider'] = true; }
		// 		else { $data['groupclassslider'] = false; }
		// 		$data['groupclassbanners'] = $groupclassbanners;
		// } else {
		// 	$data['groupclassbanner'] = false;
		// }

    echo json_encode($data);
  }

}
