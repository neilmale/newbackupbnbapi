<?php

namespace Controllers;

use \Models\Users as Users;
use \Models\Pages as Pages;
use \Models\Pageleftbar as Pageleftbar;
use \Models\Pagerightbar as Pagerightbar;
use \Models\Workshop as Workshop;
use \Models\Worshoptitle as Workshoptitle;
use \Models\Workshopvenue as Workshopvenue;
use \Models\Testimonies as Testimonies;
use \Models\Leftsidebaritem as Leftsidebaritem;
use \Models\Pageimage as Pageimage;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class PagesController extends \Phalcon\Mvc\Controller {

    public function createPagesAction() {

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $title = $request->getPost('title');
            $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
            $metatitle = $request->getPost('metatitle');
            $metadesc = $request->getPost('metadesc');
            $slugs = $request->getPost('slugs');
            $body = $request->getPost('body');
            $fbcapc = $request->getPost('fbcapc');

            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $page = new Pages();
            $page->assign(array(
                'pageid' => $id,
                'title' => $title,
                'metatitle' => $metatitle,
                'metatags' => $metatags,
                'metadesc' => $metadesc,
                'pageslugs' => $slugs,
                'body' => $body,
                'status' => 1,
                'fbcapc' => $fbcapc,
                'type' => 'Pages',
                ));

            if (!$page->save()) {
                $errors = array();
                foreach ($page->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Pages",
                    "event" => "Add",
                    "title" => "Add New Page ".$title.""
                    ));

                $data['success'] = "Success";
            }
        }
        echo json_encode($data);
    }

    public function saveimageAction() {

         $filename = $_POST['imgfilename'];

        $picture = new Pageimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }

    }

    public function listimageAction() {

        $getimages = Pageimage::find(array("order" => "id DESC"));
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        echo json_encode($data);

    }


    public function managepagesAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Pages::find();
        } else {
            $conditions = "title LIKE '%" . $keyword . "%' OR pageslugs LIKE '%" . $keyword . "%'";
            $Pages = Pages::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'pageid' => $m->pageid,
                'title' => $m->title,
                'pageslugs' => $m->pageslugs,
                'status' => $m->status
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));
    }


    public function pageUpdatestatusAction($status,$pageid,$keyword) {

        $data = array();
        $page = Pages::findFirst('pageid="' . $pageid . '"');
        $title = $page->title;
        $page->status = $status;
            if (!$page->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
                if($status == 1){
                    $pagestatus = 'Active';
                }
                else{
                    $pagestatus = 'Deactivate';
                }
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Pages",
                    "event" => "Update",
                    "title" => "Update ".$title." status to ".$pagestatus
                    ));

            }

            echo json_encode($data);
    }

    public function pagedeleteAction($pageid) {
        $conditions = 'pageid="' . $pageid . '"';
        $pages = Pages::findFirst(array($conditions));
        $title = $pages->title;
        $data = array('error' => 'Not Found');
        if ($pages) {
            if ($pages->delete()) {
                $data = array('success' => 'Page Deleted');
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Pages",
                    "event" => "Delete",
                    "title" => "Delete page ".$title.""
                    ));
            }
        }
        echo json_encode($data);
    }

    public function pageeditoAction($pageid) {
        $data = array();
        $pages = Pages::findFirst('pageid="' . $pageid . '"');

        if ($pages) {
            $data = array(
                'pageid' => $pages->pageid,
                'title' => $pages->title,
                'metatitle' =>$pages->metatitle,
                'metatags' => $pages->metatags,
                'metadesc' => $pages->metadesc,
                'slugs' => htmlentities($pages->pageslugs),
                'body' => $pages->body,
                'pagebanner' => $pages->pagebanner,
                'layout' => $pages->pagelayout,
                'fbcapc' => $pages->fbcapc
                );
        }
        echo json_encode($data);
    }

    public function pagerightitemAction($pageid) {
        $data = array();


        $rightbar= Pagerightbar::find('pageid="' . $pageid . '"');
         foreach ($rightbar as $rightbar) {;
            $data[] = array(
                'rightbar'=>$rightbar->item
                );
        }

        echo json_encode($data);
    }



    public function saveeditedPagesAction() {

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $pageid = $request->getPost('pageid');
            $title = $request->getPost('title');
            $metatitle = $request->getPost('metatitle');
            $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
            $metadesc = $request->getPost('metadesc');
            $slugs = $request->getPost('slugs');
            $body = $request->getPost('body');
            $layout = $request->getPost('layout');
            $fbcapc = $request->getPost('fbcapc');

            $pages = Pages::findFirst('pageid="' . $pageid . '"');
            $pages->title = $title;
            $pages->metatitle = $metatitle;
            $pages->metatags = $metatags;
            $pages->metadesc = $metadesc;
            $pages->pageslugs = $slugs;
            $pages->body = $body;
            $pages->pagelayout = $layout;
            $pages->fbcapc = $fbcapc;

            if (!$pages->save()) {
                $data['error'] = "Something went wrong saving the data, please try again.";
            }
            else {

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Pages",
                    "event" => "Update",
                    "title" => "Update page ".$title.""
                    ));

                    $data['success'] = "Success";

            }
            echo json_encode($data);
        }
    }

    public function getPageAction($pageslugs) {

        $conditions = "pageslugs LIKE'" . $pageslugs . "'" ;
        $getpage = Pages::findFirst(array($conditions));
        if($getpage){
            $data = $getpage;
        }
        else{
            $data['error'] ='no page found';
        }
        echo json_encode(array(
            "pagedata" => $data
        ));

    }

    public function leftsidebaridAction($pageid) {

        $conditions = 'pageid="' . $pageid . '"' ;
        echo json_encode(Pageleftbar::findFirst(array($conditions))->toArray(), JSON_NUMERIC_CHECK);

    }

    public function listleftsidebarAction($offset,$sidebarid) {

    $leftbar = Leftsidebaritem::find(array("sidebarid =" . $sidebarid . ""));
    $leftsidebar = json_encode($leftbar->toArray(), JSON_NUMERIC_CHECK);
    echo $leftsidebar;

    }

    public function listrightsidebarAction($offset,$pageid) {
        $rightbar = Pagerightbar::find(array('pageid="' . $pageid . '"'));
        $rightbar = json_encode($rightbar->toArray(), JSON_NUMERIC_CHECK);
        echo $rightbar;
    }

    public function pageinitialawakeningAction() {
        $datenow = date('Y-m-d');
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN center ON workshop.center = center.centerid WHERE workshoptitle.titleslugs = 'initial-awakening' AND workshop.datestart >= '$datenow' ORDER BY workshop.datestart LIMIT 1 ");
        $stmt->execute();
        $initialawakening = $stmt->fetch(\PDO::FETCH_ASSOC);

        $date_s = "";
        $date_started = date_create($initialawakening['datestart']);
        $new_date_started = date_format($date_started, "M Y");
        $month_date_started = date_format($date_started, "F");
        $year_date_started = date_format($date_started, "Y");

        $date_ended = date_create($initialawakening['dateend']);
        $new_date_ended = date_format($date_ended, "M Y");
        $year_date_ended = date_format($date_ended, "Y");

            if($year_date_started == $year_date_ended) { //IF YEAR IS THE SAME
                if($new_date_started == $new_date_ended) { //IF YEAR AND MONTH IS THE SAME
                    $new_date_started = date_format($date_started, "M d-");
                    $new_date_ended = date_format($date_ended,"d");
                    $date_s = $new_date_started . $new_date_ended . " ".date_format($date_ended,"Y");
                } else { //IF YEAR IS THE SAME BUT MONTH IS !SAME
                    $date_s = date_format($date_started, "M d-") . date_format($date_ended, "M d")." ".date_format($date_ended,"Y");
                }
            } else { //IF YEAR IS !SAME
                $date_s = date_format($date_started, "M d, Y - "). " " . date_format($date_ended, "M d, Y");
            }

        if($initialawakening == '') {
            $date_s = '';
        }

        echo json_encode(array("initawakening" => $initialawakening, "date_s" => $date_s));
    }

    public function pagerelatedAction($pageslugs) {
        $datenow = date("Y-m-d");
        $metatag = str_replace("-"," ",$pageslugs);

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT testimonies.ssid,testimonies.id, testimonies.subject, testimonies.photo, testimonies.author, testimonies.state, testimonies.metadesc, center.centertitle, center.centercity, center.centerstate FROM testimonies LEFT JOIN center ON testimonies.center = center.centerid WHERE testimonies.status = 1 AND metatags LIKE '%".$metatag."%' ORDER BY RAND() LIMIT 2");
        $stmt->execute();
        $stories = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if($stories == null) {
            $stmt = $db->prepare("SELECT testimonies.ssid,testimonies.id,testimonies.subject, testimonies.photo, testimonies.author, testimonies.state, testimonies.metadesc, center.centertitle, center.centercity, center.centerstate FROM testimonies LEFT JOIN center ON testimonies.center = center.centerid WHERE testimonies.status = 1 ORDER BY RAND() LIMIT 2");
            $stmt->execute();
            $stories = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }

        foreach($stories as $key => $value) {
            $stories[$key]['metadesc'] = preg_replace('/[^A-Za-z0-9\-]/', ' ', $stories[$key]['metadesc']);
        }

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news WHERE date <= '".$datenow."' AND relatedworkshop LIKE '%$pageslugs%' ORDER BY RAND() LIMIT 2");
        $stmt->execute();
        $articles = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode(array("stories" => $stories, "articles" => $articles));
    }

    public function fepageviewAction($pageslugs) {
        $datenow = date("Y-m-d");
        $db = \Phalcon\DI::getDefault()->get('db');

        $condition1 = "pageslugs LIKE'" . $pageslugs . "'" ;
        $page = Pages::findFirst(array($condition1));
        if($page) {
            $pageid = $page->pageid;
        }

        $condition2 = 'pageid="' . $pageid . '"' ;
        $pageleftbar = Pageleftbar::findFirst(array($condition2));
        if($pageleftbar) {
            $sidebarid = $pageleftbar->item;
        }

        $leftsidebar = Leftsidebaritem::find(array("sidebarid =" . $sidebarid . ""));

        $rightsidebar = Pagerightbar::find(array('pageid="' . $pageid . '"'));

        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9  AND  status = 1 ORDER BY news.views DESC LIMIT 0,5");
        $stmt->execute();
        $popularnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($popularnews as $key => $value) {
            $popularnews[$key]['title'] = utf8_encode( $popularnews[$key]['title']);
            $popularnews[$key]["body"] = utf8_encode($popularnews[$key]["body"]);
            $popularnews[$key]['description'] = utf8_encode($popularnews[$key]['description']);
            $popularnews[$key]["newsslugs"] = utf8_encode($popularnews[$key]["newsslugs"]);
        }
        // var_dump($popularnews);
        echo json_encode(array(
            "page" => $page,
            "pageleftbar" => $pageleftbar,
            "leftsidebar" => $leftsidebar->toArray(),
            "rightsidebar" => $rightsidebar->toArray(),
            "popularnews" => $popularnews
        ));
    }

    public function pageviewvalidateAction($dataslugs) {
      $pagevalidate = CB::bnbQueryFirst("SELECT * FROM pages WHERE pageslugs = '".$dataslugs."' AND status = 1");
        if($pagevalidate){
            $data['type'] = 'pages';
            $data['content'] = $pagevalidate;

            $data['pagebanner'] = CB::bnbQuery("SELECT * FROM images WHERE page='" . $pagevalidate['pageslugs'] . "' ORDER BY sequence ASC");
            if(count($data['pagebanner']) > 0){
                $data['nobanner'] = false;
                if(count($data['pagebanner']) > 1){
                    $data['slider'] = true;
                } else {
                    $data['slider'] = false;
                }
            } else {
                $data['nobanner'] = true;
            }
        }
        else {
            $center = CB::bnbQueryFirst("SELECT center.*, centeremail.email, centerphonenumber.phonenumber FROM center
              LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid
              LEFT JOIN centeremail ON center.centerid = centeremail.centerid
              WHERE (centerslugs ='".$dataslugs."' OR centerslugs2 = '" . $dataslugs . "' OR centerslugs3='" . $dataslugs . "') AND center.status = 1 ");

            if($center) {
                $data['type'] = 'center';
                $data['center'] = $center;

                $datenow = date('Y-m-d');

                // $data['centerschedule'] = array();
                // for($o=0; $o<3; $o++) {
                //    $data['centerschedule'][$o]['date'] = date("D m/d", strtotime($o.' days'));
                //    $day = strtolower(substr(date("D", strtotime($o.' days')), 0, 2));
                //    $data['centerschedule'][$o]['agenda'] = CB::bnbQuery("SELECT ".$day." as class, starttime, endtime,
                //                                                 CASE WHEN starttime LIKE '%AM' THEN 1 ELSE 2 END AS timetype
                //                                                 FROM centerschedule
                //                                                 WHERE centerid = '".$center['centerid']."' AND ".$day." != ''
                //                                                 ORDER BY timetype ASC, starttime ASC ");
                // }

                $centerimages = CB::bnbQuery("SELECT centerid, imagename FROM centerimages WHERE centerid = '".$center['centerid']."' ORDER BY imageorder ASC ");
                $data['centerimages'] = $centerimages;

                $centernews = CB::bnbQuery("SELECT newsid, title, author, description, banner, newsslugs FROM centernews WHERE newslocation = '".$center['centerid']."' AND date <= '".$datenow."' AND status = 1 ORDER BY date DESC");
                foreach($centernews as $cnews) {
                  $cnews['title'] = utf8_encode($cnews['title']);
                  $cnews['description']  = utf8_encode($cnews['description']);
                  $cnews['body']  = utf8_encode($cnews['body']);
                }
                $data['centernews'] = $centernews;

                // ============================================================
                // $data['centerevents'] = CB::bnbQuery("SELECT * FROM(
                //     SELECT 'dummy' as ecenterslugs, activityid as eid,'dummy' as evenue,centerid as ecenterid,'dummy' as ecentertitle,activitytitle as etitle,activitydate as edate,description as edesc,activitydate as edatestart,activitytimefrom as estarthour,'dummy' as estartminute,activitytimefromformat as estarttimeformat,activitytimeto as eendhour,'dummy' as eendminute,activitytimetoformat as eendtimeformat, 'act' as type  FROM centercalendar WHERE centerid ='" . $center['centerid'] . "' and activitydate >= '".$datenow."'  and status = 1
                //     UNION
                //     SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.center ='" . $center['centerid'] . "' AND workshop.datestart >= '".$datenow."'
                //     UNION
                //     SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop INNER JOIN workshopassociatedcenter ON workshopassociatedcenter.workshopid=workshop.workshopid LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshopassociatedcenter.centerid ='". $center['centerid'] ."' AND workshop.datestart >= '".$datenow."'
                //     )
                //     AS event ORDER BY event.edatestart LIMIT 2
                //     ");
                // ============================================================

                $data['centermembership'] = CB::bnbQuery("SELECT * FROM centermembership WHERE centerid='" . $center['centerid'] . "' ORDER BY period ASC");
                $testimonials = CB::bnbQuery("SELECT ssid, author, photo, subject, metadesc  FROM testimonies WHERE center='" . $center['centerid'] . "'  AND testimonies.status = 1 ORDER BY date_published DESC LIMIT 10");
                foreach($testimonials as $testimony) {
                  $testimony['metadesc'] = utf8_encode($testimony['metadesc']);
                }
                $data['centertesti'] = $testimonials;

                $data["scllnks"] = CB::bnbQuery("SELECT * FROM centersociallinks WHERE centerid = '".$center['centerid']."' AND status = 1 ORDER BY FIELD(title,'facebook','google','yelp', 'twitter') ");

            } else {
              $member = CB::bnbQueryFirst("SELECT memberid,
                username,
                email,
                password,
                firstname,
                middlename,
                lastname,
                street,
                city,
                country,
                phoneno,
                state,
                zipcode,
                status FROM members WHERE username='" . $dataslugs . "'");

              if($member) {
                $data['type'] = 'account';
                $data['user'] = $member;
              } else {
                $data['type'] = '404';
              }
            }
        }

        echo json_encode($data);

    }

    public function loadbannerAction($pageslug) {
        $page = CB::bnbQuery("SELECT title, pageslugs FROM pages WHERE pageslugs = '" . $pageslug . "' LIMIT 1")[0];
        $images = CB::bnbQuery("SELECT * FROM images WHERE page = '".$pageslug."' ORDER BY sequence ");
        echo json_encode(array('page' => $page, 'banners' =>$images), JSON_NUMERIC_CHECK);
    }
}
