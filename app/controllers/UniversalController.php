<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Models\Images as Images;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class UniversalController extends \Phalcon\Mvc\Controller {

	public function saveImageAction() {
		$request = new Request();
		if($request->isPost()) {
				$page  = $request->getPost("page");
				$image = $request->getPost("image");
				$url   = $request->getPost("url");

				if($page == 'oneclassouter' || $page == 'oneclassinner' || $page == 'groupclassouter' || $page == 'groupclassinner') {
					$newimage = Images::findFirst("page = '".$page."' ");

					if(!$newimage) {
						$newimage              = new Images();
						$newimage->id          = CB::genGUID();
					}

					$sequence = 1;
				} else {
					$newimage              = new Images();
					$newimage->id          = CB::genGUID();
					$sequence = count(CB::bnbQuery("SELECT * FROM images WHERE page = '".$page."'")) + 1;
				}

				$newimage->page        = $page;
				$newimage->image       = $image;
				$newimage->url         = $url;
				$newimage->sequence    = $sequence;
				$newimage->status      = 1;
				$newimage->datecreated = date("Y-m-d H:i:s");

				if(!$newimage->save()) {
					$data['error']    = true;
					$data['errorMsg'] = "Error occured.";
				}
		} else {
			$data['error']    = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function loadImagesAction($page) {
		echo json_encode(CB::bnbQuery("SELECT * FROM images WHERE page = '".$page."' ORDER BY sequence "));
	}

	public function removeImageAction($id) {
		$img = Images::findFirst("id = '".$id."'");

		$page = $img->page;
		$sequence = $img->sequence;

		if($img == true) {
			if($img->delete()) {
				$images = Images::find(" page = '".$page."' AND sequence > '".$sequence."' ");

				foreach($images as $image) {
					$image->sequence = $image->sequence - 1;
					$image->save();
				}

			} else {
				$data['error']    = true;
				$data['errorMsg'] = "There was a problem DELETING this image. Please try again.";
			}
		} else {
			$data['error']    = true;
			$data['errorMsg'] = "There was a problem finding this image from the DB. Please try again.";
		}
		echo json_encode($data);
	}

	public function editImageAction($id) {
		echo json_encode(CB::bnbQueryFirst("SELECT * FROM images WHERE id = '".$id."'"));
	}

	public function updateImageAction() {
		$request = new Request();
		if($request->getPost()) {
			$id = $request->getPost("id");
			$url = $request->getPost("url");

			$image = Images::findFirst("id = '".$id."'");
			$image->url = $url;
			if(!$image->save()) {
				$data['error'] = true;
				$data['errorMsg'] = "Failed to update.";
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "Failed to find this banner from the DB.";
		}
		echo json_encode($data);
	}

	public function toggleStatusImageAction($id) {
		$image = Images::findFirst("id='".$id."'");
		if($image->status == 1) { $image->status = 0; }
		else { $image->status = 1; }
		if($image->save()) { $data['success'] = true; }
		echo json_encode($data);
	}

	public function loadSingleBannerImageAction($page) {
		$image = Images::findFirst("page='".$page."' AND status = 1");
		echo json_encode($image);
	}

	public function updateorderAction() {
		$request = new \Phalcon\Http\Request();

        if($request->isPost()){
            try {
                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();

                $id = $request->getPost('id');
                $page = $request->getPost("page");
                $orderimg = $request->getPost("sequence");

                $order = Images::findFirst("id='$id'");
                if($order){
                    $orderimg3 = $order->sequence;
                }

                $order2 = Images::findFirst("page='$page' AND sequence='$orderimg'");
                if($order2){
                    $order2->setTransaction($transaction);
                    $orderimg2 = $order2->sequence;
                    $order2->sequence = $orderimg3;

                    if ($order2->save() == false) {
                        $data = [];
                        foreach ($order2->getMessages() as $message) {
                          $data[] = $message;
                        }
                        $transaction->rollback();
                    }
                } else {
                    $orderimg2 = $orderimg;
                }

                if($order) {
                    $order->setTransaction($transaction);
                    $order->sequence = $orderimg2;
                    if ($order->save() == false) {
                        $data = [];
                        foreach ($order->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }
                }else {
                    die(json_encode(array('error' => 'error' )));
                }

                $transaction->commit();
                echo json_encode(array('success' => 'success' ));
            } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
        }
    }

}
