<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Models\Presspage as Presspage;
use \Models\Presslogo as Presslogo;
use \Models\Pressbanner as Pressbanner;
use \Models\Pressthumbnail as Pressthumbnail;
use \Controllers\ControllerBase as CB;

class PresspageController extends \Phalcon\Mvc\Controller {
    public function createNewsAction() {

      $request = new \Phalcon\Http\Request();
      $escaper = new \Phalcon\Escaper();
      $filter = new \Phalcon\Filter();

      $escaper->setEncoding('utf-8');
      if($request->isPost()) {

        $title          = $request->getPost('title');
        $metatags       = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost("metatags"));
        $metatitle      = $request->getPost("metatitle");
        $slugs          = $request->getPost("slugs");
        $author         = $request->getPost("author");
        $date           = $request->getPost("date");
        $body           = $request->getPost("body");
        $status         = $request->getPost("status");
        $featurednews   = $request->getPost("featurednews");
        $banner         = $request->getPost("banner");
        $logo           = $request->getPost("logo");
        $thumbnail      = $request->getPost("thumbnail");
        $category       = $request->getPost("category");
        $description    = $request->getPost("newsdescription");
        $relatedworkshop = $request->getPost("relatedworkshop");

            // DATE FORMATTING
            /*if user used the default date and didn't change the date in frontend
            Already ('Y-m-d') format if unchange */
            if(strlen($date) == 10){
               $date_published = $date;
           }
            /* If date was changed
             The format is like this "Thu Sep 17 2015 08:00:00 GMT+0800 (Taipei Standard Time)"
             and need to convert to ('Y-m-d') before saving */
             else {
                $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                $dates = explode(" ", $date);
                $date_published = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
            }
            //DATE FORMATTING ends

            //UPDATE THE FEATURED NEWS

            $guid = new \Utilities\Guid\Guid();
            $newsid = $guid->GUID();

            $_saveData = new Presspage();
            $_saveData->newsid = $newsid;
            $_saveData->title = $title;
            $_saveData->metatags = $metatags;
            $_saveData->author = ucwords(strtolower($author));
            $_saveData->description = $description;
            $_saveData->body = $body;
            $_saveData->category = $category;
            $_saveData->status = $status;
            $_saveData->date = $date_published;
            $_saveData->views = 1;
            $_saveData->type = 'News';
            $_saveData->relatedworkshop = $relatedworkshop;
            $_saveData->datecreated =  date('Y-m-d');
            $_saveData->dateedited = date('Y-m-d');
            $_saveData->metatitle = $metatitle;
            $_saveData->banner = $banner;
            $_saveData->imglogo = $logo;
            $_saveData->thumbnail = $thumbnail;
            $_saveData->newsslugs = $slugs;


            if ($_saveData->save()) {
                $data['success'] = "Success";
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"News",
                    "event" => "Add",
                    "title" => "Add News : ".$title
                    ));
            }
            else {

               $errors = array();
               foreach ($_saveData->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }
            echo json_encode(array('error' => $errors));
        }

    }
    echo json_encode($data = $logo);

}


public function manageNewsAction($num, $page, $keyword) {
    if ($keyword == 'null' || $keyword == 'undefined') {
     $offsetfinal = ($page * $num) - $num;

     $db = \Phalcon\DI::getDefault()->get('db');
     $stmt = $db->prepare("SELECT *, presspage.datecreated as datecreated, presspage.status as status FROM presspage  ORDER BY presspage.date DESC LIMIT " . $offsetfinal . ",$num");
     $stmt->execute();
     $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
     foreach($searchresult as $key => $value) {
        $searchresult[$key]['title'] = utf8_encode( $searchresult[$key]['title']);
        $searchresult[$key]["body"] = utf8_encode($searchresult[$key]["body"]);
        $searchresult[$key]['description'] = utf8_encode($searchresult[$key]['description']);
        $searchresult[$key]["newsslugs"] = utf8_encode($searchresult[$key]["newsslugs"]);
    }
    $db1 = \Phalcon\DI::getDefault()->get('db');


    $stmt1 = $db1->prepare("SELECT *, presspage.datecreated as datecreated, presspage.status as status FROM presspage ORDER BY presspage.date DESC");
    $stmt1->execute();
    $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

    $totalreportdirty = count($searchresult1);
} else {

   $offsetfinal = ($page * 6) - 6;

   $db = \Phalcon\DI::getDefault()->get('db');

   $stmt = $db->prepare("SELECT *, presspage.datecreated as datecreated, presspage.status as status FROM presspage  WHERE  presspage.title LIKE '%" . $keyword . "%' or presspage.newsslugs LIKE '%" . $keyword . "%' or presspage.author LIKE '%" . $keyword . "%' ORDER BY presspage.date DESC LIMIT " . $offsetfinal . ",$num");

   $stmt->execute();
   $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

   foreach($searchresult as $key => $value) {
    $searchresult[$key]['title'] = utf8_encode( $searchresult[$key]['title']);
    $searchresult[$key]["body"] = utf8_encode($searchresult[$key]["body"]);
    $searchresult[$key]['description'] = utf8_encode($searchresult[$key]['description']);
    $searchresult[$key]["newsslugs"] = utf8_encode($searchresult[$key]["newsslugs"]);
}
$db1 = \Phalcon\DI::getDefault()->get('db');

$stmt1 = $db1->prepare("SELECT *, presspage.datecreated as datecreated, presspage.status as status FROM presspage  WHERE  presspage.title LIKE '%" . $keyword . "%' or presspage.newsslugs LIKE '%" . $keyword . "%' or presspage.author LIKE '%" . $keyword . "%' ORDER BY presspage.datecreated DESC");
$stmt1->execute();
$searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
$totalreportdirty = count($searchresult1);
}
echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty));
}



public function newseditoAction($newsid) {
    $data = array();
    $leftbaritem = array();
    $rightbaritem = array();

    $news = Presspage::findFirst('newsid="' . $newsid . '"');
    if ($news) {
        $data = array(
            'newsid' => utf8_encode($news->newsid),
            'title' => utf8_encode($news->title),
            'metatags' => utf8_encode($news->metatags),
            'metatitle' => utf8_encode($news->metatitle),
            'slugs' => utf8_encode($news->newsslugs),
            'author' => utf8_encode($news->author),
            'newsdescription' => utf8_encode($news->description),
            'body' => utf8_encode($news->body),
            'banner' => utf8_encode($news->banner),
            'thumbnail' => utf8_encode($news->thumbnail),
            'logo' => utf8_encode($news->imglogo),
            'category' => utf8_encode($news->category),
            'status' => utf8_encode($news->status),
            'featurednews' => utf8_encode($news->featurednews),
            'date' => utf8_encode($news->date),
            'datecreated' => utf8_encode($news->datecreated)
            );
    }
    echo json_encode($data);
}


public function editNewsAction() {
    $filter = new \Phalcon\Filter();
    $request = new \Phalcon\Http\Request();
    if($request->isPost()){
        $newsid = $request->getPost('newsid');
        $title = $request->getPost('title');
        $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
        $metatitle = $request->getPost('metatitle');
        $slugs = $request->getPost('slugs');
        $author = $request->getPost('author');
        $newsdate = $request->getPost('date');
        $description = $request->getPost('newsdescription');
        $body = $request->getPost('body');
        $status = $request->getPost('status');
        $banner = $request->getPost('banner');
        $logo         = $request->getPost("logo");
        $thumbnail         = $request->getPost("thumbnail");
        $newslocation = $request->getPost('newslocation');
        $category = $request->getPost('category');
        $featurednews = $request->getPost('featurednews');
        $datecreated = $request->getPost('datecreated');
        $relatedworkshop = $request->getPost('relatedworkshop');


        $conditions = 'newsid="' . $newsid . '"';
        $deletenews = Presspage::findFirst(array($conditions));
        $data = array('error' => 'Not Found');
        if ($deletenews) {
            if ($deletenews->delete()) {
                $data = array('success' => 'News Deleted');
            }
        }



        if(strlen($newsdate) > 10) {
           $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
           $dates = explode(" ", $newsdate);
           $newsdate = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
       }


       $_saveData = new Presspage();
       $_saveData->newsid = $newsid;
       $_saveData->title = $title;
       $_saveData->metatags = $metatags;
       $_saveData->author = ucwords(strtolower($author));
       $_saveData->description = $description;
       $_saveData->body = $body;
       $_saveData->category = $category;
       $_saveData->status = $status;
       $_saveData->date = $newsdate;
       $_saveData->views = 1;
       $_saveData->type = 'News';
       $_saveData->relatedworkshop = $relatedworkshop;
       $_saveData->datecreated =  date('Y-m-d');
       $_saveData->dateedited = date('Y-m-d');
       $_saveData->metatitle = $metatitle;
       $_saveData->banner = $banner;
       $_saveData->imglogo = $logo;
       $_saveData->thumbnail = $thumbnail;
       $_saveData->newsslugs = $slugs;


       if (!$_saveData->save()) {
        $errors = array();
        foreach ($_saveData->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }
        echo json_encode(array('error' => $errors));
    }else {
        $data['success'] = "Success";
        $audit = new CB();
        $audit = new CB();
        $audit->auditlog(array(
            "module" =>"News",
            "event" => "Update",
            "title" => "Update News : ".$title
            ));
    }

}

echo json_encode($data );
}


public function readPressAction($slug) {
    $data = array();
    $leftbaritem = array();
    $rightbaritem = array();

    $news = Presspage::findFirst('newsslugs="' . $slug . '" AND status = 1');
    if ($news) {
        $data = array(
            'newsid' => utf8_encode($news->newsid),
            'title' => utf8_encode($news->title),
            'metatags' => utf8_encode($news->metatags),
            'metatitle' => utf8_encode($news->metatitle),
            'slugs' => utf8_encode($news->newsslugs),
            'author' => utf8_encode($news->author),
            'newsdescription' => utf8_encode($news->description),
            'body' => utf8_encode($news->body),
            'banner' => utf8_encode($news->banner),
            'logo' => utf8_encode($news->imglogo),
            'category' => utf8_encode($news->category),
            'status' => utf8_encode($news->status),
            'featurednews' => utf8_encode($news->featurednews),
            'date' => date("F j, Y", strtotime($news->date)),
            'datecreated' => utf8_encode($news->datecreated),
            'error' => 'false'
            );
    } else {
      $data['error'] = true;
    }
    echo json_encode($data);
}

   public function newscenterdeleteAction($newsid){

        $news = Presspage::findFirst('newsid="' . $newsid . '"');
        if($news){
            if ($news->delete()) {
                $data = array('success' => 'News Deleted');
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"News",
                    "event" => "Delete",
                    "title" => "Delete Center News ".$center->centertitle. " "
                ));
            }
        }else{
          $data = array('error' => 'Not Found');
        }
    }

     public function saveBannerAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Pressbanner();
        $picture->assign(array(
            'filename' => $filename,
            'videourl' => NULL,
            'type' => 'image'
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
        echo json_encode($data);
    }

     public function saveLogoAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Presslogo();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
    }


     public function listBannerAction() {
        $getimages = Pressbanner::find(array("order" => "id DESC"));
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename,
                'url' => $getimages->videourl,
                'type' => $getimages->type
                );
        }
        echo json_encode($data);
    }

    public function listLogoAction() {
        $getimages = Presslogo::find(array("order" => "id DESC"));
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        echo json_encode($data);
    }

    public function deleteBannerAction($id){
        $newsimg = Pressbanner::findFirst("id=$id");
        if ($newsimg){
            if ($newsimg->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else{
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }
    public function deleteLogoAction($id){
        $newsimg = Presslogo::findFirst("id=$id");
        if ($newsimg){
            if ($newsimg->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else{
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function saveVidBannerAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $p = new Pressbanner();
            $p->assign(array(
                'videourl' => $request->getPost('vid'),
                'type' => 'video' ));
            if($p->save()){
                echo json_encode(array('success' => 'success' ));
            }
        }
    }

    public function loadpressAction($page) {
        $app = new CB();
        $page = ($page * 9) - 9;
        $p = $app->bnbQuery("SELECT presspage.title,
            presspage.date,
            presspage.banner,
            presspage.imglogo,
            presspage.thumbnail,
            presspage.newsslugs FROM presspage WHERE status = 1 ORDER BY presspage.date DESC LIMIT $page, 9");

        $count = count($app->bnbQuery("SELECT presspage.title,
            presspage.date,
            presspage.banner,
            presspage.imglogo,
            presspage.thumbnail,
            presspage.newsslugs FROM presspage WHERE status = 1 ORDER BY presspage.date DESC"));
        echo json_encode(array('presspage' => $p, 'total' => $count));
    }

    public function changestatusAction() {
      $request = new Request();
      if($request->isPost()) {
        $pressid = $request->getPost('pressid');
        $data['press'] = $pressid;
        $thispress = Presspage::findFirst("newsid ='".$pressid."'");
        if($thispress) {
          $currentStatus = $thispress->status;

          if($currentStatus == 1) { $thispress->status = 0; }
          else { $thispress->status = 1; }
          if($thispress->save()) {
            $data['success'] = "SAVED!";
          } else {
            $data['error'] = true;
            $data['errorMsg'] = "NOT SAVE";
          }
        } else {
          $data['error'] = true;
          $data['errorMsg'] = "PRESS CAN't FIND";
        }
      } else {
          $data['error'] = true;
          $data['errorMsg'] = "NO POST DATA";
      }
      echo json_encode($data);
    }

    public function listthumbnailAction() {
        $getimages = Pressthumbnail::find();
        if(count($getimages)){
            foreach ($getimages as $getimages)
            {
                $data[] = array(
                    'id'=>$getimages->id,
                    'filename'=>$getimages->filename,
                    'url' => $getimages->videourl,
                    'type' => $getimages->type
                    );
            }
        } else {
            $data = $getimages;
        }
        echo json_encode($data);
    }

     public function savethumbnailAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Pressthumbnail();
        $picture->assign(array(
            'filename' => $filename,
            'videourl' => NULL,
            'type' => 'image'
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
        echo json_encode($data);
    }

    public function deletethumbnailAction($id){
        $newsimg = Pressthumbnail::findFirst("id=$id");
        if ($newsimg){
            if ($newsimg->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else{
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function savethumbnailvidAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $p = new Pressthumbnail();
            $p->assign(array(
                'videourl' => $request->getPost('vid'),
                'type' => 'video' ));
            if($p->save()){
                echo json_encode(array('success' => 'success' ));
            }
        }
    }
}
