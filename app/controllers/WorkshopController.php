<?php

namespace Controllers;
use \Phalcon\Http\Request;
use \Models\Workshop as Workshop;
use \Models\Workshopassociatedcenter as Workshopassociatedcenter;
use \Models\Workshopvenue as Workshopvenue;
use \Models\Notification as Notification;
use \Models\Workshoptitle as Workshoptitle;
use \Models\Workshopregistrant as Workshopregistrant;
use \Models\Workshopregistrant_temp as Workshopregistrant_temp;
use \Models\Center as Center;
use \Models\States as States;
use \Models\Cities as Cities;
use \Models\Cities_extended as Cities_extended;
use \Models\Testimonies as Testimonies;
use \Models\News as News;
use \Models\Centernews as Centernews;
use \Models\Images as Images;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class WorkshopController extends \Phalcon\Mvc\Controller {

	public function BEworkshopCreatevenueAction() {
		$request = new Request();
		$data['error'] = false;
		$errors = array();

		if($request->isPost()) {
			$venuename = $request->getPost('venuename');
			$venueslugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $venuename);
			$doesExist = CB::bnbQueryFirst("SELECT workshopvenueid FROM workshopvenue  WHERE venueslugs ='".$venueslugs."'");

			if(!$doesExist) {
				$wsvenue = new Workshopvenue();
				$wsvenue->workshopvenueid = CB::genGUID();
				$wsvenue->venuename = $venuename;
				$wsvenue->venueslugs = $venueslugs;
				$wsvenue->city = $request->getPost('city');
				$wsvenue->SPR = $request->getPost('SPR');
				$wsvenue->zipcode = $request->getPost('zipcode');
				$wsvenue->country = $request->getPost('country');
				$wsvenue->website = $request->getPost('website');
				$wsvenue->address1 = $request->getPost('address1');
				$wsvenue->address2 = $request->getPost('address2');
				$wsvenue->contact = $request->getPost('contact');
				$wsvenue->latitude = $request->getPost('latitude');
				$wsvenue->longtitude = $request->getPost('longtitude');
				$wsvenue->vauthorizeid = $request->getPost('authorizeid');
				$wsvenue->vauthorizekey = $request->getPost('authorizekey');
				$wsvenue->date_updated = date('Y-m-d H:i:s');
				$wsvenue->date_created = date('Y-m-d');
				if(!$wsvenue->create()) {
						foreach($wsvenue->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						$data['error'] = true;
						$data['errorMsg'] = $errors[0];
				}
			} else {
				$data['error'] = true;
				$data['errorMsg'] = "Venue Name already exist!";
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function BEworkshopListofvenuesAction($num,$page,$keyword) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$offsetfinal = ($page * 10) - 10;
		if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {

			$stmt = $db->prepare("SELECT * FROM workshopvenue LIMIT $offsetfinal, $num");
			$stmt->execute();
			$venues = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$venuescount = Workshopvenue::find();

			$totalNumberOfVenues = count($venuescount);

		} else {
			$stmt = $db->prepare("SELECT * FROM workshopvenue WHERE venuename LIKE '%$keyword%' LIMIT $offsetfinal, $num");
			$stmt->execute();
			$venues = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$stmt = $db->prepare("SELECT * FROM workshopvenue WHERE venuename LIKE '%$keyword%' ");
			$stmt->execute();
			$venuescount = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$totalNumberOfVenues = count($venuescount);
		}

		echo json_encode(array('data' => $venues, 'index' => $page, 'total_items' => $totalNumberOfVenues));

	}
	// //dati to, pwd na siguro idelete soon (12-28-15)
	// public function BEworkshopCheckvenuenameAction($venuename) {
	// 	$slugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $venuename);
	// 	$venue= Workshopvenue::findFirst("venueslugs = '$slugs' ");
	// 	if($venue) {
	// 		echo json_encode(array("exists" => true));
	// 	} else {
	// 		echo json_encode(array("exists" => false));
	// 	}
	// }

	public function BEworkshopDeleteworkshopvenueAction($venueid) {
		$workshopvenue = Workshopvenue::findFirst("workshopvenueid = '$venueid' ");
		if($workshopvenue) {
			if($workshopvenue->delete()) {
				echo json_encode(array("result" => "Successfully Deleted!"));
			} else {
				echo json_encode(array("result" => "There was an error!"));
			}
		} else {
			echo json_encode(array("result" => "There was an error!"));
		}
	}

	public function BEworkshopEditworkshopvenueAction($venueid) {
		$workshopvenue = Workshopvenue::findFirst("workshopvenueid = '$venueid' ");
		echo json_encode($workshopvenue);
	}

	// //dati to, pwd na siguro idelete soon (12-29-15)
	// public function BEworkshopCheckvenuenamewexceptionAction($venuename,$venueid) {
	// 	$slugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $venuename);
	// 	$venue= Workshopvenue::findFirst("venueslugs = '$slugs' AND workshopvenueid != '$venueid' ");
	// 	if($venue) {
	// 		echo json_encode(array("exists" => true));
	// 	} else {
	// 		echo json_encode(array("exists" => false));
	// 	}
	// }

	public function BEworkshopUpdatevenueAction($venueid) {
		$request = new Request();
		$data['error'] = false;
		if($request->isPost()) {
			$venue = $request->getPost('venue');
			$workshopvenueid = $request->getPost('workshopvenueid');
			$venueslugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $venue['venuename']);
			$doesExist = CB::bnbQueryFirst("SELECT workshopvenueid FROM workshopvenue  WHERE workshopvenueid != '".$workshopvenueid."' AND venueslugs ='".$venueslugs."' ");
			if(!$doesExist) {
					$updatevenue = Workshopvenue::findFirst("workshopvenueid = '".$workshopvenueid."'");
					$updatevenue->venuename = $venue['venuename'];
					$updatevenue->venueslugs = $venueslugs;
					$updatevenue->city = $venue['city'];
					$updatevenue->SPR = $venue['SPR'];
					$updatevenue->zipcode = $venue['zipcode'];
					$updatevenue->country = $venue['country'];
					$updatevenue->website = $venue['website'];
					$updatevenue->address1 = $venue['address1'];
					$updatevenue->address2 = $venue['address2'];
					$updatevenue->latitude = $venue['latitude'];
					$updatevenue->longtitude = $venue['longtitude'];
					$updatevenue->contact = $venue['contact'];
					$updatevenue->vauthorizeid = $venue['vauthorizeid'];
					$updatevenue->vauthorizekey = $venue['vauthorizekey'];
					$updatevenue->date_updated = date('Y-m-d H:i:s');
					if(!$updatevenue->save()) {
						$errors = array();
						foreach($updatevenue->getMessages() as $message) {
							$errors[] = $message->getMessage();
						}
						$data['error'] = true;
						$data['errorMsg'] = $errors[0];
					}
			} else {
					$data['error'] = true;
					$data['errorMsg'] ="Update failed. Venue Name already exist!";
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function BEworkshopCreatetitleAction() {
		$request = new Request();
		if($request->isPost()) {
			$createtitle = new Workshoptitle();
			$createtitle->workshoptitleid = CB::genGUID();
			$createtitle->title = $_POST['title'];
			$createtitle->titleslugs = $_POST['titleslugs'];
			$createtitle->description = $_POST['description'];
			$createtitle->brochure = $_POST['brochure'];
			$createtitle->date_created = date('Y-m-d');
			$createtitle->date_updated =  date('Y-m-d H:i:s');

			if($createtitle->create()) {
				$data['error'] = false;
			} else {
				$errors = array();
				foreach($createtitle->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				$data['error'] = true;
				$data['errorMsg'] = $errors[0];
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function BEworkshopListoftitlesAction($num,$page,$keyword) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$offsetfinal = ($page * 10) - 10;
		if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {

			$stmt = $db->prepare("SELECT * FROM workshoptitle ORDER BY date_updated DESC LIMIT $offsetfinal, $num");
			$stmt->execute();
			$titles = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$titlescount = Workshoptitle::find();
			$totalNumberOfTitles = count($titlescount);

		} else {
			$stmt = $db->prepare("SELECT * FROM workshoptitle WHERE title LIKE '%$keyword%' ORDER BY date_updated DESC LIMIT $offsetfinal, $num");
			$stmt->execute();
			$titles = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$stmt = $db->prepare("SELECT * FROM workshoptitle WHERE title LIKE '%$keyword%' ORDER BY date_updated DESC ");
			$stmt->execute();
			$titlescount = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$totalNumberOfTitles = count($titlescount);
		}

		echo json_encode(array('data' => $titles, 'index' => $page, 'total_items' => $totalNumberOfTitles));

	}

	public function BEworkshopremovetitleAction($titleid) {
		$title = Workshoptitle::findFirst("workshoptitleid = '$titleid' ");
		if($title) {
			if($title->delete()) {
				echo json_encode(array("result"=>"Successful!"));
			} else {
				echo json_encode(array("result"=>"Error!"));
			}
		} else {
			echo json_encode(array("result"=>"Error!"));
		}
	}

	public function BEworkshopChecktitlenameAction($titleslugs) {

		// $titleslugs = strtolower(preg_replace('/[^\w#& ]/', '', $titlename));
		// $titleslugs = preg_replace('/\s+/', ' ', $titleslugs);
		// $titleslugs = preg_replace('/[^A-Za-z0-9\-]/', '-', $titleslugs);

		$titlename = Workshoptitle::findFirst(" titleslugs = '$titleslugs' ");
		if($titlename) {
			echo json_encode(array("exists" => true));
		} else {
			echo json_encode(array("exists" => false));
		}
	}

	public function workshopTitlesAction() {
		echo json_encode(Workshoptitle::find(array("order"=>"title ASC"))->toArray());
	}

	public function centersNVenuesAction() {
		$venues = CB::bnbQuery("SELECT * FROM
										(
											SELECT 	centerid as venueid, centertitle as venuename, centerstate as venuestate FROM center WHERE status = 1
											UNION
											SELECT 	workshopvenueid as venueid, venuename, SPR as venuestate FROM workshopvenue
										) as venues
										ORDER BY venues.venuestate ASC, venues.venuename ASC
								");
		echo json_encode($venues);
	}

	public function BEworkshopListAction() {
		$db = \Phalcon\DI::getDefault()->get('db');

		$titles = Workshoptitle::find();
		$venues = Workshopvenue::find();

        $stmt = $db->prepare("SELECT * FROM center ORDER BY centerstate, centertitle ");
        $stmt->execute();
        $centers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        // FRONTEND USE
        $states = States::find();

        $stmt = $db->prepare(" SELECT * FROM center centercity ORDER BY centercity ");
        $stmt->execute();
        $cities = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid ORDER BY workshop.datestart");
        $stmt->execute();
        $workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $stmt = $db->prepare("SELECT * FROM workshop WHERE datestart >= '$datenow'  ORDER BY datestart");
        $stmt->execute();
        $TOTALWORKSHOPS = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $month = ''; $year='';
        $month_year = array();
        foreach ($TOTALWORKSHOPS as $date) {
        	$date_started = date_create($date['datestart']);
        	$month_date_started = date_format($date_started, "F");
        	$year_date_started = date_format($date_started, "Y");
	        	//DATE DROPDOWN LIST
        	if($month != $month_date_started && $year != $year_date_started) {
        		$month_year[] = array(
        			"datelike" => date_format($date_started, "Y-m"),
        			"month_year" => date_format($date_started, "F, Y")
        			);
        		$month = $month_date_started;
        	}
        }

		echo json_encode(array(
			"venues" => $venues->toArray(),
			"titles" => $titles->toArray(),
			"centers" => $centers,
			"states" => $states->toArray(),
			"cities" => $cities,
			"workshops" => $workshops,
			"date_choices" => $month_year
		));
	}

	public function BEworkshopCreateworkshopAction() {
		$request = new Request();
		$data['error'] = false;
		$data['warning'] = false;
		if($request->isPost()) {
			$title = $request->getPost('title');
			$venue = $request->getPost('venue');
			$center = $request->getPost('center');
			$associatedcenter = $request->getPost('associatedcenter');
			$tuition = $request->getPost('tuition');
			$datestart = $request->getPost ('datestart');

			if(strlen($datestart) > 10) {
				$mont0  = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
				$dates  = explode(" ", $datestart);
				$datestart   = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
      }

			$starthour = sprintf("%02d",$request->getPost('starthour'));
			$startminute = sprintf("%02d",$request->getPost('startminute'));
			$starttimeformat = $request->getPost('starttimeformat');
			$dateend = $request->getPost('dateend');
			if(strlen($dateend) > 10) {
				$mont0  = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
				$dates = explode(" ", $dateend);
				$dateend = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
			}
			$endhour = sprintf("%02d",$request->getPost('endhour'));
			$endminute = sprintf("%02d",$request->getPost('endminute'));
			$endtimeformat = $request->getPost('endtimeformat');
			$about = $request->getPost('about');
			$note = $request->getPost('note');
			$accomodation = $request->getPost('accomodation');
			$miscellaneous = $request->getPost('miscellaneous');
			$metatitle = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatitle'));
			$metadesc = $request->getPost('metadesc');

			$workshopid = CB::genGUID();

			$workshop = new Workshop();
			$workshop->workshopid = $workshopid;
			$workshop->title = $title;
			$workshop->venue = $venue;
			$workshop->center = $center;
			$workshop->tuition = $tuition;
			$workshop->datestart = $datestart;
			$workshop->starthour = $starthour;
			$workshop->startminute = $startminute;
			$workshop->starttimeformat = $starttimeformat;
			$workshop->dateend = $dateend;
			$workshop->endhour = $endhour;
			$workshop->endminute = $endminute;
			$workshop->endtimeformat = $endtimeformat;
			$workshop->about = $about;
			$workshop->note = $note;
			$workshop->accomodation = $accomodation;
			$workshop->miscellaneous = $miscellaneous;
			$workshop->metatitle = $metatitle;
			$workshop->metadesc = $metadesc;
			$workshop->date_created = date('Y-m-d');
			$workshop->date_updated = date('Y-m-d H:i:s');

			if($workshop->create()) {
				foreach($associatedcenter as $centerid){
					$workshopassociatedcenter = new Workshopassociatedcenter();
					$workshopassociatedcenter->workshopid = $workshopid;
					$workshopassociatedcenter->centerid = $centerid;
					$workshopassociatedcenter->create();
				}
			} else {
				$errors = array();
				foreach($workshop->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				$data['error'] = true;
				$data['errorMsg'] = $errors[0];
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function BEworkshopListofworkshopAction($num,$page,$keyword) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$offsetfinal = ($page * 10) - 10;
		if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {

			$stmt = $db->prepare("SELECT center.centertitle, workshop.*, workshopvenue.venuename, workshoptitle.title as titlename FROM workshop LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN center ON workshop.center = center.centerid  LIMIT $offsetfinal, $num");
			$stmt->execute();
			$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$workshopscount = Workshop::find();
			$totalNumberOfWorkshops = count($workshopscount);

		} else {
			$stmt = $db->prepare("SELECT center.centertitle,workshop.*, workshopvenue.venuename, workshoptitle.title as titlename FROM workshop LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN center ON workshop.center = center.centerid WHERE workshoptitle.title LIKE '%$keyword%' LIMIT $offsetfinal, $num");
			$stmt->execute();
			$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$stmt = $db->prepare("SELECT * FROM workshop WHERE title LIKE '%$keyword%' ");
			$stmt->execute();
			$workshopscount = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$totalNumberOfWorkshops = count($workshopscount);
		}
		echo json_encode(array('data' => $workshops, 'index' => $page, 'total_items' => $totalNumberOfWorkshops, "counter" => $offsetfinal));
	}

	public function BEworkshopRemoveworkshopAction($workshopid) {
		$data['error'] = false;
		$workshopX = Workshop::findFirst(" workshopid = '".$workshopid."' ");
		if($workshopX) {
			if($workshopX->delete()) {
				$remove = Workshopassociatedcenter::find("workshopid = '".$workshopid."' ");
				$remove->delete();
			} else {
				$data['error'] = true;
				$data['errorMsg'] = "Deleting workshop failed. Please try again.";
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "Can't find this workshop from the database";
		}
		echo json_encode($data);
	}

	public function BEworkshopEditworkshopAction($workshopid) {

		$workshopI = Workshop::findFirst("workshopid = '".$workshopid."' ");
		$workshopA = Workshopassociatedcenter::find("workshopid = '".$workshopid."' ");
		$newdata = array();
		foreach ($workshopA as $workshopA) {
			// $newdata = $workshopA->centerid;
			array_push($newdata,$workshopA->centerid);
		}
		$data = array(
			"workshopid" => $workshopI->workshopid,
			"title"=> $workshopI->title,
			"venue"=> $workshopI->venue,
			"center"=> $workshopI->center,
			"tuition"=> $workshopI->tuition,
			"sale"=> $workshopI->sale,
			"datestart"=> $workshopI->datestart,
			"starthour"=> $workshopI->starthour,
			"startminute"=> $workshopI->startminute,
			"starttimeformat"=> $workshopI->starttimeformat,
			"dateend"=> $workshopI->dateend,
			"endhour"=> $workshopI->endhour,
			"endminute"=> $workshopI->endminute,
			"endtimeformat"=> $workshopI->endtimeformat,
			"about"=> $workshopI->about,
			"note"=> $workshopI->note,
			"accomodation"=> $workshopI->accomodation,
			"miscellaneous"=> $workshopI->miscellaneous,
			"metatitle"=> $workshopI->metatitle,
			"metadesc"=> $workshopI->metadesc,
			"date_created"=>$workshopI->date_created,
			"date_updated"=> $workshopI->date_updated,
			"id"=> $workshopI->workshopid,
			"centerid"=> $workshopI->workshopid,
			"associatedcenter"=> $newdata
		 );
		echo json_encode(array(
			"workshopprop" => $data
		));
	}

	public function BEworkshopUpdateworkshopAction() {
		$request = new Request();
		$data['error'] = false;

		if($request->isPost()){
			$workshopid = $request->getPost('workshopid');
			$workshop = $request->getPost('workshop');

			$datestart = $workshop['datestart'];
			if(strlen($datestart) > 10) {
                 $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                 $dates = explode(" ", $datestart);
                 $datestart = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
             }

			$dateend = $workshop['dateend'];
			if(strlen($dateend) > 10) {
                 $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                 $dates = explode(" ", $dateend);
                 $dateend = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
             }

			$upworkshop = Workshop::findFirst(" workshopid = '".$workshopid."' ");
			$upworkshop->title = $workshop['title'];
			$upworkshop->venue = $workshop['venue'];
			$upworkshop->center = $workshop['center'];
			$upworkshop->tuition = $workshop['tuition'];
			$upworkshop->sale = $workshop['sale'];
			$upworkshop->datestart = $datestart;
			$upworkshop->starthour = sprintf("%02d",$workshop['starthour']);
			$upworkshop->startminute = sprintf("%02d",$workshop['startminute']);
			$upworkshop->starttimeformat = $workshop['starttimeformat'];
			$upworkshop->dateend = $dateend;
			$upworkshop->endhour = sprintf("%02d",$workshop['endhour']);
			$upworkshop->endminute = sprintf("%02d",$workshop['endminute']);
			$upworkshop->endtimeformat = $workshop['endtimeformat'];
			$upworkshop->about = $workshop['about'];
			$upworkshop->note = $workshop['note'];
			$upworkshop->accomodation = $workshop['accomodation'];
			$upworkshop->miscellaneous = $workshop['miscellaneous'];
			$upworkshop->metatitle = preg_replace('/^([^a-zA-Z0-9])*/', '', $workshop['metatitle']);
			$upworkshop->metadesc = $workshop['metadesc'];
			$upworkshop->date_updated = date('Y-m-d H:i:s');
			if($upworkshop->save()) {
				$remove = Workshopassociatedcenter::find("workshopid = '".$workshopid."' ");
				$remove->delete();

				foreach($workshop['associatedcenter'] as $centerid) {
					$workshopassociatedcenter = new Workshopassociatedcenter();
					$workshopassociatedcenter->workshopid = $workshopid;
					$workshopassociatedcenter->centerid = $centerid;
					$workshopassociatedcenter->create();
				}
			} else {
				$errors = array();
				foreach($upworkshop->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				$data['error'] = true;
				$data['errorMsg'] = $errors[0];
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function BEworkshopUpdatetitleAction() {
		$request = new Request();
		if($request->isPost()) {
			$checktitle = Workshoptitle::findFirst(" workshoptitleid != '".$_POST['workshoptitleid']."' AND titleslugs = '". $_POST['titleslugs'] ."' ");
			if($checktitle == true) {
				$data['error'] = true;
				$data['errorMsg'] = "Workshop Title already exist. Please try a new one.";
			} else {
				$uptitle = Workshoptitle::findFirst(" workshoptitleid = '".$_POST['workshoptitleid']."' ");
				$uptitle->title = $_POST['title'];
				$uptitle->titleslugs = $_POST['titleslugs'];
				$uptitle->description = $_POST['description'];
				$uptitle->brochure = $_POST['brochure'];
				$uptitle->date_updated = date('Y-m-d H:i:s');

				if($uptitle->save()) {
					$data['error'] = false;
				} else {
					$errors = array();
					foreach($uptitle->getMessages() as $message) {
						$errors[] = $message->getMessage();
					}
					$data['error'] = true;
					$data['errorMsg'] = $errors[0];
				}
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function loadcityViastatecodeAction($statecode) {
		if($statecode == 'All' || $statecode == '') {
			$db = \Phalcon\DI::getDefault()->get('db');
			$stmt = $db->prepare(" SELECT * FROM center GROUP BY centercity ORDER BY centercity ");
			$stmt->execute();
			$centercities = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		} else {
			$db = \Phalcon\DI::getDefault()->get('db');
			$stmt = $db->prepare(" SELECT * FROM center WHERE centerstate = '$statecode' GROUP BY centercity ORDER BY centercity ");
			$stmt->execute();
			$centercities = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		}

		echo json_encode(array("cities" => $centercities));
	}

	public function workshopschedulePaginationAction($titleslugs, $offset) {
			$db = \Phalcon\DI::getDefault()->get('db');
			$finaloffset = ($offset * 10) - 10;

			$request = new \Phalcon\Http\Request();
			if($request->isPost()) {
				$state = $request->getPost('state');
			}

		if($titleslugs == 'all') {
	        $stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid ORDER BY workshop.datestart LIMIT $finaloffset, 10");
        	$stmt->execute();
        	$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        	$totalWorkshops = Workshop::find();
		} else {
			$stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid WHERE workshoptitle.titleslugs = '$titleslugs' ORDER BY workshop.datestart LIMIT $finaloffset, 10");
        	$stmt->execute();
        	$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        	$stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid WHERE workshoptitle.titleslugs = '$titleslugs' ORDER BY workshop.datestart");
        	$stmt->execute();
        	$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		}

		echo json_encode(array(
			"workshops" => $workshops,
			"totalworkshops" => count($totalWorkshops),
			"finaloffset" => $finaloffset,
			"state" => $state
		));
	}

	public function workshopschedulepaginationangularAction() {
		$db = \Phalcon\DI::getDefault()->get('db');
		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$titleslugs = $request->getPost('titleslugs');
			$title = $request->getPost('title');
			$offset = $request->getPost('offset');

			$finaloffset = ($offset * 10) - 10;

			if($titleslugs == 'all') {
				$stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid ORDER BY workshop.datestart LIMIT $finaloffset, 10");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$totalWorkshops = Workshop::find();
			} else {
				$stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid WHERE workshoptitle.titleslugs = '$titleslugs' ORDER BY workshop.datestart LIMIT $finaloffset, 10");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid WHERE workshoptitle.titleslugs = '$titleslugs' ORDER BY workshop.datestart");
				$stmt->execute();
				$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			}

			echo json_encode(array(
				"titleslugs" => $titleslugs,
				"offset" => $offset,
				"workshops" => $workshops,
				"totalworkshops" => count($totalWorkshops)
			));
		}
	}

	public function workshopSchedulelistAction($titleslugs, $offset) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$datenow = date('Y-m-d');
		$finaloffset = ($offset * 10) - 10;

		$request = new \Phalcon\Http\Request();
		if($request->isPost()) {
			$title = $request->getPost('title');
			if($title == 'all') { $title = ''; }

			$state = $request->getPost('state');
			if($state == 'All') { $state = ''; }

			$city = $request->getPost('city');
			if($city == false) { $city = ''; }

			$datechoice = $request->getPost('datechoice');
		}

		if($titleslugs == 'all') {
			if(($title == null || $title == '') && ($state == '' || $state == null ) && ($city == null || $city == '') && ($datechoice == null || $datechoice == '')) {
				$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshop.datestart >= '$datenow'  ORDER BY workshop.datestart LIMIT $finaloffset, 10");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshop.datestart >= '$datenow'  ORDER BY workshop.datestart");
				$stmt->execute();
				$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$wew = "1";
			} else {
				$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND center.centerstate LIKE '%$state%' AND center.centercity LIKE '%$city%' AND workshop.datestart LIKE '$datechoice%')  ORDER BY workshop.datestart LIMIT $finaloffset, 10");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND center.centerstate LIKE '%$state%' AND center.centercity LIKE '%$city%' AND workshop.datestart LIKE '$datechoice%') ORDER BY workshop.datestart");
				$stmt->execute();
				$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$wew = "2";
			}

		} else {
			if(($title === null) && ($state == '' || $state == null ) && ($city == null || $city == 'false') && $datechoice == null) {
				$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshoptitle.titleslugs = '$titleslugs' AND workshop.datestart >= '$datenow' ORDER BY workshop.datestart LIMIT $finaloffset, 10");
				$stmt->execute();
				$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				$stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid  WHERE workshoptitle.titleslugs = '$titleslugs' AND workshop.datestart >= '$datenow' ORDER BY workshop.datestart");
				$stmt->execute();
				$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
				$wew = "3";
			}
			else {
				if($title == '') {
					$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshoptitle.titleslugs LIKE '%$title%' AND workshop.datestart >= '$datenow' AND (center.centerstate LIKE '%$state%' AND center.centercity LIKE '$city%' AND workshop.datestart LIKE '$datechoice%') ORDER BY workshop.datestart LIMIT $finaloffset, 10");
					$stmt->execute();
					$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					$stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN center ON workshop.center = center.centerid WHERE workshoptitle.titleslugs LIKE '%$title%' AND workshop.datestart >= '$datenow' AND (center.centerstate LIKE '%$state%' AND center.centercity LIKE '%$city%' AND workshop.datestart LIKE '$datechoice%') ORDER BY workshop.datestart");
					$stmt->execute();
					$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
					$wew = "4";
				} else {
					$stmt = $db->prepare("SELECT workshop.*,centerphonenumber.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid WHERE workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND center.centerstate LIKE '%$state%' AND center.centercity LIKE '$city%' AND workshop.datestart LIKE '$datechoice%') or workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND workshopvenue.SPR LIKE '%$state%' AND workshopvenue.city LIKE '$city%' AND workshop.datestart LIKE '$datechoice%') ORDER BY workshop.datestart LIMIT $finaloffset, 10");
					$stmt->execute();
					$workshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					$stmt = $db->prepare("SELECT workshop.workshopid FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centermanagerphone ON center.centerid = centermanagerphone.centerid WHERE workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND center.centerstate LIKE '%$state%' AND center.centercity LIKE '$city%' AND workshop.datestart LIKE '$datechoice%') or workshop.datestart >= '$datenow' AND (workshoptitle.titleslugs LIKE '%$title%' AND workshopvenue.SPR LIKE '%$state%' AND workshopvenue.city LIKE '$city%' AND workshop.datestart LIKE '$datechoice%') ORDER BY workshop.datestart");
					$stmt->execute();
					$totalWorkshops = $stmt->fetchAll(\PDO::FETCH_ASSOC);
					$wew = "5";
				}

			}
		}

		$date_s = array();
		foreach($workshops as $date) {
			$date_started = date_create($date['datestart']);
			$new_date_started = date_format($date_started, "M Y");
			$month_date_started = date_format($date_started, "F");
			$year_date_started = date_format($date_started, "Y");

			$date_ended = date_create($date['dateend']);
			$new_date_ended = date_format($date_ended, "M Y");
			$year_date_ended = date_format($date_ended, "Y");

	        	if($year_date_started == $year_date_ended) { //IF YEAR IS THE SAME
		        	if($new_date_started == $new_date_ended) { //IF YEAR AND MONTH IS THE SAME
		        		$new_date_started = date_format($date_started, "M d-");
		        		$new_date_ended = date_format($date_ended,"d, Y");
		        		$date_s[] = $new_date_started . $new_date_ended;
		        	} else { //IF YEAR IS THE SAME BUT MONTH IS !SAME
		        		$date_s[] = date_format($date_started, "M d-") . date_format($date_ended, "M d, Y");
		        	}
	        	} else { //IF YEAR IS !SAME
	        		$date_s[] = date_format($date_started, "M d, Y - ") . date_format($date_ended, "M d, Y");
	        	}
	        }

	        //pwede na itong tanggalin pag okay na :D
	        $filter = array("title"=>$title, "state" => $state, "city" => $city, "datechoice" => $datechoice, "sql" => $wew);

	        $lastnumber = $finaloffset + count($workshops);

	        echo json_encode(array(
	        	"filter" => $filter,
	        	"titleslugs" => $titleslugs,
	        	"index" => $offset,
	        	"lastnumber" => $lastnumber,
	        	"workshops" => $workshops,
	        	"total_items" => count($totalWorkshops),
	        	"dates_of_schedule" => $date_s,
	        	"counter" => $finaloffset,
	        ));
	}

	public function workshopdetailAction($workshopid) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$stmt = $db->prepare("SELECT workshop.*, center.*,centerphonenumber.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid LEFT JOIN centerphonenumber on centerphonenumber.centerid=center.centerid WHERE workshop.workshopid = '$workshopid' ");
		$stmt->execute();
		$workshop = $stmt->fetch(\PDO::FETCH_ASSOC);

		echo json_encode(array(
			"workshopprop" => $workshop
		));
	}
	public function workshoplocationAction($workshopid) {
		$db = \Phalcon\DI::getDefault()->get('db');
		$stmt = $db->prepare("SELECT * FROM workshop LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN centerlocation ON workshop.center = centerlocation.centerid  WHERE workshop.workshopid = '$workshopid' ");
		$stmt->execute();
		$workshoplocation = $stmt->fetch(\PDO::FETCH_ASSOC);
		echo json_encode(array(
			"workshoplocation" => $workshoplocation
		));
	}

	public function registrantslistAction() {
		$request = new Request();
		$data['error'] = false;
		if($request->isPost()) {
			$num = $request->getPost('num');
			$page = $request->getPost('page');
			$title = $request->getPost('filter')['titlename'];
			$venue = $request->getPost('filter')['venue'];
			$offsetfinal = ($page * $num) - $num;

			$registrants = CB::bnbQuery("SELECT * FROM
																		(
																			SELECT 	workshopregistrant.*,
																							workshop.tuition,
																							workshop.datestart,
																							workshop.dateend,
																							workshop.starthour,
																							workshop.startminute,
																							workshop.starttimeformat,
																							workshop.endhour,
																							workshop.endminute,
																							workshop.endtimeformat,
																							center.centerid as venueid,
																							center.centertitle as venuename,
																							workshoptitle.workshoptitleid,
																							workshoptitle.title as workshoptitle,
																							'bnbcenter' as venue
																			FROM 		workshopregistrant
																			LEFT JOIN workshop ON workshopregistrant.workshopid = workshop.workshopid
																			LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid
																			LEFT JOIN center ON workshop.center = center.centerid
																			WHERE workshop.venue = 'bnbcenter'
																			UNION
																			SELECT 	workshopregistrant.*,
																							workshop.tuition,
																							workshop.datestart,
																							workshop.dateend,
																							workshop.starthour,
																							workshop.startminute,
																							workshop.starttimeformat,
																							workshop.endhour,
																							workshop.endminute,
																							workshop.endtimeformat,
																							workshopvenue.workshopvenueid as venueid,
																							workshopvenue.venuename as venuename,
																							workshoptitle.workshoptitleid,
																							workshoptitle.title as workshoptitle,
																							'bnbvenue' as venue
																			FROM 		workshopregistrant
																			LEFT JOIN workshop ON workshopregistrant.workshopid = workshop.workshopid
																			LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid
																			LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid
																			WHERE workshop.venue != 'bnbcenter'
																		) AS reg
																			WHERE reg.workshoptitleid LIKE '%".$title."%'
																			AND reg.venueid LIKE '%".$venue."%'
																			ORDER BY reg.date_created DESC
																	");

			$data['TotalItems'] = count($registrants);
			$data['registrants'] = array_slice($registrants, $offsetfinal, $num);
			$data['CurrentPage'] = $page;

		} else {
			$data['error'] = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function removeRegistrantAction($registrantid) {
		$remove = Workshopregistrant::findFirst("workshopregistrantid = '$registrantid' ");
		if($remove) {
			if($remove->delete()) {
				echo json_encode(array("result" => "Successful!"));
			} else {
				echo json_encode(array("result" => "Error!"));
			}
		}
	}

	public function overviewAction() {
		$db = \Phalcon\DI::getDefault()->get('db');
		$datenow = date('Y-m-d');
		$stmt = $db->prepare("SELECT workshop.*, center.*, workshopvenue.*, workshoptitle.title as 'titlename' FROM workshop LEFT JOIN center ON workshop.center = center.centerid LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid WHERE workshop.datestart >= '$datenow'  ORDER BY workshop.datestart LIMIT 3");
		$stmt->execute();
		$upcomingschedule = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		$date_s = array();
		foreach($upcomingschedule as $date) {
			$date_started = date_create($date['datestart']);
			$new_date_started = date_format($date_started, "M Y");
			$month_date_started = date_format($date_started, "F");
			$year_date_started = date_format($date_started, "Y");

			$date_ended = date_create($date['dateend']);
			$new_date_ended = date_format($date_ended, "M Y");
			$year_date_ended = date_format($date_ended, "Y");

        	if($year_date_started == $year_date_ended) { //IF YEAR IS THE SAME
	        	if($new_date_started == $new_date_ended) { //IF YEAR AND MONTH IS THE SAME
	        		$new_date_started = date_format($date_started, "M d-");
	        		$new_date_ended = date_format($date_ended,"d");
	        		$date_s[] = $new_date_started . $new_date_ended . " ".date_format($date_ended,"Y");
	        	} else { //IF YEAR IS THE SAME BUT MONTH IS !SAME
	        		$date_s[] = date_format($date_started, "M d-") . date_format($date_ended, "M d")." ".date_format($date_ended,"Y");
	        	}
        	} else { //IF YEAR IS !SAME
        		$date_s[] = date_format($date_started, "M d, Y - "). " " . date_format($date_ended, "M d, Y");
        	}
	    }
	    echo json_encode(array('upcomingschedule' => $upcomingschedule, 'schedule' => $date_s));
	}

	public function relatedAction() {
		$db = \Phalcon\DI::getDefault()->get('db');

		$titles = Workshoptitle::find();

		$relatedstories = array();
		$relatedmainarticles = array();
		$relatedcenterarticles = array();
		foreach($titles as $title) {

			$stmt = $db->prepare("SELECT * FROM testimonies WHERE relatedworkshop LIKE '%".$title->titleslugs."%' ");
			$stmt->execute();
			$related = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$relatedstories[] = $related;

			$stmt = $db->prepare("SELECT * FROM news WHERE relatedworkshop LIKE '%".$title->titleslugs."%' ");
			$stmt->execute();
			$relatednews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
			$relatedmainarticles[] = $relatednews;

			$stmt = $db->prepare("SELECT * FROM centernews WHERE relatedworkshop LIKE '%".$title->titleslugs."%' ");
			$stmt->execute();
			$relatedcenternews = $stmt->fetchAll(\PDO::FETCH_ASSOC);

			$relatedcenterarticles[] = $relatedcenternews;
		}

		echo json_encode(array(
			"titles" => $titles->toArray(),
			"relatedstories" => $relatedstories,
			"relatedmainarticles" => $relatedmainarticles,
			"relatedcenterarticles" => $relatedcenterarticles
		));
	}

	public function _workshopsIndexAction() {

		$data['nobanner'] = false;
		$banners = CB::bnbQuery("SELECT url, image FROM images WHERE page = 'workshop' AND status = 1 ORDER BY sequence ");
		if($banners == true) {
				if(count($banners) > 1) { $data['slider'] = true; }
				else { $data['slider'] = false; }
				$data['workshopbanners'] = $banners;
		} else {
			$data['nobanner'] = true;
		}

		$workshoptitles = CB::bnbQuery("SELECT * FROM workshoptitle ORDER BY date_updated DESC");
		if(count($workshoptitles) > 0 ) {
			$data['notitles'] = false;
			$data['workshoptitles'] = $workshoptitles;
		} else {
			$data['notitle'] = true;
		}

		echo json_encode($data);
	}

	public function _workshopsTitleCheckAction($title) {
		$checktitle = CB::bnbQueryFirst("SELECT title FROM workshoptitle WHERE titleslugs = '".$title."'");
		if($checktitle == true) {
			$data['title'] = $checktitle['title'];
			$data['titleExist'] = true;
		}
		else { $data['titleExist'] = false; }
		echo json_encode($data);
	}

	public function _workshopsFindWorkshopAction() {
		$request = new Request();
		if($request->isPost()) {
			$datenow = date("Y-m-d");
			$zip = $request->getPost('zip');
			$data['zip'] = $zip;
			$city = strtolower($request->getPost('city'));
			$data['city'] = $city;
			$state = $request->getPost('state');
			$data['state'] = $state;
			$workshoptitleslugs = $request->getPost('workshoptitleslugs');
			$data['workshoptitleslugs'] = $workshoptitleslugs;

			$find = CB::bnbQuery("SELECT * FROM
														(
															SELECT 	* FROM (
																			SELECT
																			workshop.workshopid as workshopid,
																			workshop.datestart as datestart,
																			workshoptitle.titleslugs as titleslugs,
																			center.centercity as workshopcity,
																			center.centerstate as workshopstatecode,
																			states.state as workshopstate,
																			center.centerzip as centerzip,
																			center.centertitle as centertitle,
																			center.centeraddress as centeraddress,
																			centerlocation.lat as lat,
																			centerlocation.lon as lon,
																			centerphonenumber.phonenumber as contact,
																			'bnbcenter' as venue
																			FROM workshop
																			LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid
																			LEFT JOIN center ON workshop.center = center.centerid
																			LEFT JOIN states ON center.centerstate = states.state_code
																			LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid
																			LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid
																			WHERE venue = 'bnbcenter'
																			UNION
																			SELECT
																			workshop.workshopid as workshopid,
																			workshop.datestart as datestart,
																			workshoptitle.titleslugs as titleslugs,
																			center.centercity as workshopcity,
																			center.centerstate as workshopstatecode,
																			states.state as workshopstate,
																			center.centerzip as centerzip,
																			center.centertitle as centertitle,
																			center.centeraddress as centeraddress,
																			centerlocation.lat as lat,
																			centerlocation.lon as lon,
																			centerphonenumber.phonenumber as contact,
																			'bnbcenter' as venue
																			FROM workshop
																			LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid
																			LEFT JOIN workshopassociatedcenter ON workshop.workshopid = workshopassociatedcenter.workshopid
																			LEFT JOIN center ON center.centerid = workshopassociatedcenter.centerid
																			LEFT JOIN states ON center.centerstate = states.state_code
																			LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid
																			LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid
																			WHERE venue = 'bnbcenter'
															) as wsbnbcenter
															UNION
															SELECT 	workshop.workshopid as workshopid,
																			workshop.datestart as datestart,
																			workshoptitle.titleslugs as titleslugs,
																			workshopvenue.city as workshopcity,
																			workshopvenue.SPR as workshopstatecode,
																			states.state as workshopstate,
																			workshopvenue.zipcode as centerzip,
																			workshopvenue.venuename as centertitle,
																			workshopvenue.address1 as centeraddress,
																			workshopvenue.latitude as lat,
																			workshopvenue.longtitude as lon,
																			workshopvenue.contact as contact,
																			'bnbvenue' as venue
															FROM workshop
															LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid
															LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid
															LEFT JOIN states ON workshopvenue.SPR = states.state_code
															WHERE venue != 'bnbcenter'
														) as ws
														WHERE ws.titleslugs = '".$workshoptitleslugs."'
														AND ws.datestart >= '".$datenow."'
													");
			$data['find'] = $find;
			$data['workshopResult'] = array();
			foreach($find as $workshop) {
				if($state != undefined && $state != "") {
					if($workshop['workshopstatecode'] == $state ) {
						array_push($data['workshopResult'], $workshop);
						$data['workshopState'] = $workshop['workshopstate'];
					}
				} elseif ($city != undefined && $city != "" ) {
					$exploded = explode(",",$city);
					$expldCity = strtolower($exploded[0]);
					$expldState = strtoupper(trim($exploded[1]));
					if(strtolower($workshop['workshopcity']) == $expldCity && $workshop['workshopstatecode'] == $expldState ) {
						array_push($data['workshopResult'], $workshop);
						$data['workshopState'] = $workshop['workshopstate'];
					}
				} elseif ($zip != undefined && $zip != "") {
					if($workshop['centerzip'] == $zip ) {
						array_push($data['workshopResult'], $workshop);
						$data['workshopState'] = $workshop['workshopstate'];
					}
				}
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function _workshopsThisWorkshopAction() {
		$request = new Request();
		if($request->isPost()){
			$workshoptitleslugs = $request->getPost('workshoptitleslugs');
			$workshopid = $request->getPost('workshopid');
			$statecode = $request->getPost('statecode');
			$checkvenue = CB::bnbQueryFirst("SELECT venue FROM workshop WHERE workshopid ='".$workshopid."' AND datestart >= '".date('Y-m-d')."' ");
			if($checkvenue) {
				if($checkvenue['venue'] == 'bnbcenter') {
					$data['workshop'] = CB::bnbQuery("SELECT 	workshop.workshopid,
																						workshoptitle.title as workshoptitle,
																						workshop.tuition,
																						workshop.sale,
																						states.state as workshopstate,
																						center.centercity as workshopcity,
																						center.centerzip as centerzip,
																						center.centertitle as centertitle,
																						center.centeraddress as centeraddress,
																						centerlocation.lat as lat,
																						centerlocation.lon as lon,
																						centerphonenumber.phonenumber as contact,
																						'https://www.bodynbrain.com' as website,
																						center.authorizeid as authid,
																						center.authorizekey as authkey,
																						'bnbcenter' as venue
																		FROM workshop
																		LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid
																		LEFT JOIN center ON workshop.center = center.centerid
																		LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid
																		LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid
																		LEFT JOIN states ON center.centerstate = states.state_code
																		WHERE workshop.workshopid ='".$workshopid."'
																		AND workshoptitle.titleslugs = '".$workshoptitleslugs."'
																		AND states.state_code = '".$statecode."'
																		LIMIT 1");
				} else {
					$data['workshop'] = CB::bnbQuery("SELECT 	workshop.workshopid,
																						workshoptitle.title as workshoptitle,
																						workshop.tuition,
																						workshop.sale,
																						states.state as workshopstate,
																						workshopvenue.city as workshopcity,
																						workshopvenue.zipcode as centerzip,
																						workshopvenue.venuename as centertitle,
																						workshopvenue.address1 as centeraddress,
																						workshopvenue.latitude as lat,
																						workshopvenue.longtitude as lon,
																						workshopvenue.contact as contact,
																						workshopvenue.website as website,
																						workshopvenue.vauthorizeid as authid,
																						workshopvenue.vauthorizekey as authkey,
																						'bnbvenue' as venue
																		FROM workshop
																		LEFT JOIN workshoptitle ON workshop.title = workshoptitle.workshoptitleid
																		LEFT JOIN workshopvenue ON workshop.venue = workshopvenue.workshopvenueid
																		LEFT JOIN states ON workshopvenue.SPR = states.state_code
																		WHERE workshop.workshopid ='".$workshopid."'
																		AND workshoptitle.titleslugs = '".$workshoptitleslugs."'
																		AND states.state_code = '".$statecode."'
																		LIMIT 1");
				}
				if(count($data['workshop']) == 1) {
						$data['error'] = false;
				} else {
						$data['error'] = true;
						$data['errorMsg'] = "Something went wrong, please try again";
				}
			} else {
				$data['error'] = true;
				$data['errorMsg'] = "WORKSHOP NOT FOUND";
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function _workshopsSubmitPaymentAction() {
		$request = new Request();
		$errors = array();
		$data['error'] = false;
		$data['warning'] = false;
		if($request->isPost()) {
			$sessionkey = $request->getPost('sessionkey');
			$device = $request->getPost('device');
			$workshop = $request->getPost('workshop');
			$registrant = $request->getPost('registrant');
			$paymentinfo = $request->getPost('paymentinfo');
			$billinginfo = $request->getPost('billinginfo');

			$registrantid = $sessionkey;

			if($workshop['sale'] == 0 || $workshop['sale'] == null) {  $x_amount = $workshop['tuition'] * $billinginfo['qty']; }
			else { $x_amount = $workshop['sale'] * $billinginfo['qty']; }

			$temp = new Workshopregistrant_temp();
			$temp->id = $registrantid;
			$temp->workshopid = $workshop['workshopid'];
			$temp->name = ucwords(strtolower($registrant['name']));
			$temp->address = $registrant['address'];
			$temp->email = $registrant['email'];
			$temp->contact = $registrant['contact'];
			$temp->qty = $billinginfo['qty'];
			$temp->cash = $x_amount;
			$temp->device = $device['device'];
			$temp->deviceos = $device['deviceos'];
			$temp->devicebrowser = $device['devicebrowser'];
			$temp->status = 'not verified';
			$temp->notice = 'waiting for authorized response';
			$temp->date_created = date('Y-m-d H:i:s');
			$temp->date_updated = date('Y-m-d H:i:s');

			if($temp->save()) {
				$wsauth = array(
					"x_login"           => $workshop['authid'],
					"x_tran_key"        => $workshop['authkey'],
					"x_card_num"        => $paymentinfo['ccNumber'],
					"x_exp_date"        => sprintf("%02d", $paymentinfo['ccExpiMonth']).$paymentinfo['ccExpiYear'],
					"x_amount"          => $x_amount,
					"x_description"     => $workshop['workshoptitle'] ." in ". $workshop['centertitle'],
					"x_first_name"      => ucwords(strtolower($paymentinfo['fname'])),
					"x_last_name"       => ucwords(strtolower($paymentinfo['lname'])),
					"x_address"         => $registrant['address']
				);
				$auth_response = CB::workshopAuthorized($wsauth);
				$data['auth_response'] = $auth_response;

				if($auth_response[0] == 1) {
						$datetime = date('Y-m-d H:i:s');
						$wsreg = new Workshopregistrant();
						$wsreg->id = $registrantid;
						$wsreg->workshopid = $workshop['workshopid'];
						$wsreg->name = ucwords(strtolower($registrant['name']));
						$wsreg->address = $registrant['address'];
						$wsreg->email = $registrant['email'];
						$wsreg->contact = $registrant['contact'];
						$wsreg->qty = $billinginfo['qty'];
						$wsreg->cash = $auth_response[9];
						$wsreg->paymenttype = $auth_response[51];
						$wsreg->confirmation = $auth_response[6];
						$wsreg->device = $device['device'];
						$wsreg->deviceos = $device['deviceos'];
						$wsreg->devicebrowser = $device['devicebrowser'];
						$wsreg->status = 'new';
						$wsreg->method = 'auto';
						$wsreg->date_created = $datetime;
						$wsreg->date_updated = $datetime;

						if($wsreg->create()) {
								if($workshop['venue'] == 'bnbcenter' ) {
									$venue = $workshop['centertitle'] . " center";
								} else {
									$venue = $workshop['centertitle'];
								}

								$data['voucher'] = array(
										'registrantname' => ucwords(strtolower($registrant['name'])),
										'workshop' => $workshop['workshoptitle'],
										'centertitle' => $workshop['centertitle'],
										'venue' => $venue,
										'contact' => $workshop['contact']
								);

								$confirm = Workshopregistrant_temp::findFirst(" id = '".$registrantid."' ");
								$confirm->status = 'verified';
								$confirm->notice = 'success authorized response';
								$confirm->save();

								$notification = new Notification();
                $notification->notificationid = CB::genGUID();
                $notification->centerid = '';
                $notification->itemid = $registrantid;
                $notification->name = ucwords(strtolower($registrant['name']));
                $notification->datecreated = date('Y-m-d H:i:s');
                $notification->adminstatus = 0;
                $notification->regionstatus = 0;
                $notification->centerstatus = 0;
                $notification->type = 'workshop';
								$notification->create();

								$emailToRegistrant = array(
										'lat' => $workshop['lat'],
										'lon' => $workshop['lon'],
										'registrant_name' => ucwords(strtolower($registrant['name'])),
										'registrant_address' => $registrant['address'],
										'workshoptitle' => $workshop['workshoptitle'],
										'centertitle' => ucwords($venue),
										'venue_contact' => $workshop['contact'],
										'venue_address' => $workshop['centeraddress'],
										'venue_web' => $workshop['website'],
										'confirmation' => $auth_response[6],
										'ccnumber' => $paymentinfo['ccNumber'],
										'cash' => $auth_response[9],
										'cctype' => $auth_response[51]
								);

								$emailToBnb = array(
										'centertitle' => ucwords($venue),
										'registrant_name' => ucwords(strtolower($registrant['name'])),
										'registrant_contact' => $registrant['contact'],
										'registrant_address' => $registrant['address'],
										'registrant_email' => $registrant['email'],
										'payment' => "$".$auth_response[9]." via ".$auth_response[51],
										'confirmation' => $auth_response[6]
								);

								CB::sendMail($registrant['email'],'BodynBrain: Workshop Confirmation Email',CB::workshopEmailToRegistrant($emailToRegistrant));

								$emailToBnbSubject = "BodynBrain: ".$workshop['workshoptitle']." Sign-Up for ".ucwords($venue);
								CB::sendMail("phil@mailinator.com",$emailToBnbSubject,CB::workshopEmailToBnb($emailToBnb));
								CB::sendMail("customerservice@mailinator.com",$emailToBnbSubject,CB::workshopEmailToBnb($emailToBnb));

						} else {
							$wstemp = Workshopregistrant_temp::findFirst(" id = '".$registrantid."' ");
							$wstemp->status = 'verified';
							$wstemp->notice = 'success authorized response but failed to save in Workshopregistrant table';
							$wstemp->save();

							foreach($wsreg->getMessages() as $message) {
								$errors[] = $message->getMessage();
							}
							$data['warning'] = true;
							$data['warningMsg'] = $errors[0] . " (dbbnb failed). Your registration has finished but our system failed to save your record. If you see this warning please report immediately. Help us to figure what's wrong by taking a screenshot of this. Sorry for the inconvenient.";
						}

				} else {
						$wstemp = Workshopregistrant_temp::findFirst(" id = '".$registrantid."' ");
						$wstemp->notice = $auth_response[3];
						$wstemp->save();

						$data['error'] = true;
						if($auth_response[3] != undefined || $auth_response[3] != "") {
							$data['errorMsg'] = $auth_response[3];
						} else {
							$data['errorMsg'] = "Server error, please try submitting again.";
						}
				}
			} else {
				foreach($temp->getMessages() as $message) {
					$errors[] = $message->getMessage();
				}
				$data['error'] = true;
				$data['errorMsg'] = $errors;
			}
		} else {
			$data['error'] = true;
			$data['errorMsg'] = "NO POST DATA";
		}
		echo json_encode($data);
	}

	public function newwstitleAction() {
		$request = new Request();
		if($request->isPost()) {
				$title       = $request->getPost("title");
				$titleslugs  = $request->getPost("titleslugs");
				$description = $request->getPost("description");
				$body        = $request->getPost("body");
				$brochure    = $request->getPost("brochure");
				$metatitle   = $request->getPost("metatitle");
				$metadesc    = $request->getPost("metadesc");

				$new = new Workshoptitle();
				$new->workshoptitleid = CB::genGUID();
				$new->title           = $title;
				$new->titleslugs      = $titleslugs;
				$new->description     = $description;
				$new->body            = $body;
				$new->brochure        = $brochure;
				$new->metatitle       = $metatitle;
				$new->metadesc        = $metadesc;
				$new->date_created    = date("Y-m-d");
				$new->date_updated    = date("Y-m-d H:i:s");
				if($new->create()) {
					$data["suc"] = "New workshop title has been successfully created.";
				} else {
					$data['err'] = "API: " . $new->getMessages()[0]->getMessage();
				}
		} else {
			$data["err"] = "API: NO POST DATA.";
		}
		echo json_encode($data);
	}

}
