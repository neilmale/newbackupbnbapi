<?php

namespace Controllers;

use \Models\States as States;
use \Models\Center as Center;
use \Controllers\ControllerBase as CB;

class LocationsController extends \Phalcon\Mvc\Controller {
	public function locationStatesAction() {
		$states = States::find(array("order" => "state ASC"));
		$data = array();
		$centerarray = array();
		foreach($states as $state) {

			$st_code = $state->state_code;
			$centers = Center::find("centerstate = '$st_code' AND status = 1 ORDER BY centercity ASC");
			foreach($centers as $center) {
				$centerarray = array(
					'centertitle' =>$center->centertitle,
					'centerslugs' =>$center->centerslugs,
					'centertype' => $center->centertype,
					'centercity' => $center->centercity
					);

				$data[] = array(
	                'state' => $state->state,
	                'center' => $centerarray
                	);

			}

		}
		echo json_encode(array('data' => $data));
		// echo $states;
	}

	public function feLocationsIndexAction() {
		$data['nobanner'] = false;
		$banners = CB::bnbQuery("SELECT url, image FROM images WHERE page = 'locations' AND status = 1 ORDER BY sequence ");
		if($banners == true) {
				if(count($banners) > 1) { $data['slider'] = true; }
				else { $data['slider'] = false; }
				$data['locationsbanners'] = $banners;
		} else {
			$data['nobanner'] = true;
		}
		echo json_encode($data);
	}

}
