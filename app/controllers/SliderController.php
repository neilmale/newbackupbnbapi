<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Models\Slider as Slider;
use \Models\Images as Images;
use \Models\Sociallinks as Sociallinks;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class SliderController extends \Phalcon\Mvc\Controller {

    public function saveSlideAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $page = $request->getPost('page');
            $url      = $request->getPost('url');
            $image      = $request->getPost('image');
            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $s = Slider::findFirst(array("page='$page'", "order" => "orderimg DESC"));
            if($s){
                $order = $s->orderimg + 1;
            }else {
                $order = 1;
            }

            $slider = new Slider();
            $slider->assign(array(
                'id' => $id,
                'url' => $url,
                'page' => $page,
                'image' => $image,
                'orderimg' => $order
            ));
            if (!$slider->save()) {
                $errors = array();
                foreach ($slider->getMessages() as $message) {
                  $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $audit = new CB();
                $audit->auditlog(array("module" =>"Slider", "event" => "Add", "title" => "Add New Photo"));
                $data['success'] = "Success";
            }
        }
        echo json_encode($data);
    }

    public function imglistAction() {
        $getimages = Slider::find(array("order" => "orderimg ASC"));
        if(count($getimages) == 0){
            $data['error']=array('NOIMAGE');
        }else{
            foreach ($getimages as $getimages) {
                $data[] = array(
                    'id'=>$getimages->id,
                    'filename'=>$getimages->image,
                    'url'=>$getimages->url,
                    'page'=>$getimages->page,
                    'orderimg'=>$getimages->orderimg
                );
            }
        }
        echo json_encode($data, JSON_NUMERIC_CHECK);
    }

    public function deleteAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){
            $id = $request->getPost('id');
            $s = Slider::findFirst("id='" . $id . "'");
            $page = $s->page;
            if($s){
                if($s->delete()){
                    $s = Slider::find(array("page='$page'", "order" => "orderimg asc"));

                    foreach ($s->toArray() as $key => $value) {
                        $data = array('success' => 'Image has been successfully deleted.' );
                        $order = $key + 1;
                        if($order != $value['orderimg']) {
                            $sl = Slider::findFirst("id='" . $value['id'] . "'");
                            $sl->setTransaction($transaction);
                            $sl->orderimg = $order;
                            if ($sl->save() == false) {
                                $data = [];
                                foreach ($sl->getMessages() as $message) {
                                    $data[] = $message;
                                }
                            } else {
                                $data = array('success' => 'Image has been successfully deleted.' );
                            }
                        }
                    }
                }else {
                    $data = [];
                    foreach ($s->getMessages() as $message) {
                        $data[] = $message;
                    }
                }

                die(json_encode($data));
            } else {
                echo json_encode(array('error' => 'An error occurred please try again later' ));
            }
        }
    }

    public function updateorderAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            try {
                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();

                $id = $request->getPost('id');
                $page = $request->getPost("page");
                $orderimg = $request->getPost("orderimg");

                $order = Slider::findFirst("id='$id'");
                if($order){
                    $orderimg3 = $order->orderimg;
                }

                $order2 = Slider::findFirst("page='$page' AND orderimg='$orderimg'");
                if($order2){
                    $order2->setTransaction($transaction);
                    $orderimg2 = $order2->orderimg;
                    $order2->orderimg = $orderimg3;

                    if ($order2->save() == false) {
                        $data = [];
                        foreach ($order2->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }
                } else {
                    $orderimg2 = $orderimg;
                }

                if($order){
                    $order->setTransaction($transaction);
                    $order->orderimg = $orderimg2;
                    if ($order->save() == false) {
                        $data = [];
                        foreach ($order->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }
                }else {
                    die(json_encode(array('error' => 'error' )));
                }

                $transaction->commit();
                echo json_encode(array('success' => 'success' ));
            } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
        }
    }

    public function updateurlAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $id = $request->getPost('id');
            $url = $request->getPost('url');

            $s = Slider::findFirst("id='$id'");
            if($s){
                $s->url = $url;
                if($s->save() == false){
                    $data = [];
                    foreach ($s->getMessages() as $message) {
                        $data[] = $message;
                    }
                    die(json_encode($data));
                }
            }

            echo json_encode(array('success' => 'success' ));
        }
    }

    public function getshopsliderAction() {
        echo json_encode(Images::find(array("page='shop' AND status = 1", 'order' => 'sequence asc'))->toArray());
    }

    public function gethomesliderAction() {
      $data['hasImage'] = true;
        $data['images'] = Images::find(array("page='home' AND status = 1", "order" => "sequence asc"))->toArray();
        if(!$data['images']) {
          $data['hasImage'] = false;
        }
        $data['sociallinks'] = Sociallinks::findFirst();
        echo json_encode($data);
    }
}
