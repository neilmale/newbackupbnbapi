<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Models\Center as Center;
use \Models\News as News;
use \Models\Centernews as Centernews;
use \Models\Testimonies as Testimonies;
use \Models\Newsimage as Newsimage;
use \Models\Newscategory as Newscategory;
use \Models\Workshoptitle as Workshoptitle;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class NewsController extends \Phalcon\Mvc\Controller {

    public function Validatenewstitlesction($centername){
        $validate = Centernews::find(array("title='".$centername."'"));
        $validate1 = News::find(array("title='".$centername."'"));

        $count = count($validate) + count($validate1);
        echo json_encode($count);
    }

    // Added like the validation for the title action validation
    public function ValidateNewssSlugsAction($slugs,$centerid){
        // $validate = Centernews::find(array("newsslugs='".$slugs."'"));
        $validate = Centernews::find("newsslugs='".$slugs."' and newslocation ='".$centerid ."'");
        // $validate1 = News::find(array("newsslugs='".$slugs."'"));
        $validate1 = News::find("newsslugs='".$slugs."' and newslocation ='".$centerid ."'");


        $count = count($validate) + count($validate1);
        echo json_encode($count);
    }

    public function ValidateNewsSlugsAction($slugs){
        $validate = Centernews::find("newsslugs='".$slugs."'");
        $validate1 = News::find("newsslugs='".$slugs."'");

        $count = count($validate) + count($validate1);
        echo json_encode($count);
    }

    public function createNewsAction() {

      $request = new Request();
      $escaper = new \Phalcon\Escaper();
      $escaper->setEncoding('utf-8');
      $filter = new \Phalcon\Filter();

      if($request->isPost()) {

          $title = $request->getPost('title');
          $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost("metatags"));
          $metatitle = $request->getPost("metatitle");
          $slugs = $request->getPost("slugs");
          $author = $request->getPost("author");
          $date = $request->getPost("date");
          $body = $request->getPost("body");
          $status = $request->getPost("status");
          $featurednews = $request->getPost("featurednews");
          $banner = $request->getPost("banner");
          $thumbnail = $request->getPost("thumbnail");
          $newslocation = $request->getPost("newslocation");
          $category = $request->getPost("category");
          $description = $request->getPost("description");
          $relatedworkshop = $request->getPost("relatedworkshop");

          // DATE FORMATTING
          /*if user used the default date and didn't change the date in frontend
          Already ('Y-m-d') format if unchange */
          if(strlen($date) == 10) {
               $date_published = $date;
          }
          /* If date was changed
           The format is like this "Thu Sep 17 2015 08:00:00 GMT+0800 (Taipei Standard Time)"
          and need to convert to ('Y-m-d') before saving */
          else {
              $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
              $dates = explode(" ", $date);
              $date_published = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
          }
          //DATE FORMATTING ends

          //UPDATE THE FEATURED NEWS
          if($featurednews == 1) {
              $getFeatNews = News::find("featurednews = 1");
              foreach($getFeatNews as $news) {
                  $news->featurednews = 0;
                  $news->save();
              }
          }

          $newsid = CB::genGUID();

          $savenews = new News();
          $savenews->newsid = $newsid;
          $savenews->title = $title;
          $savenews->metatags = $metatags;
          $savenews->metatitle = $metatitle;
          $savenews->newsslugs = $slugs;
          $savenews->author = ucwords(strtolower($author));
          $savenews->description = $description;
          $savenews->body = $body;
          $savenews->banner = $banner;
          $savenews->thumbnail = $thumbnail;
          $savenews->newslocation = $newslocation;
          $savenews->category = $category;
          $savenews->date = $date_published;
          $savenews->status = $status;
          $savenews->views = 1;
          $savenews->type = 'News';
          $savenews->featurednews = $featurednews;
          $savenews->relatedworkshop = $relatedworkshop;
          $savenews->datecreated =  date('Y-m-d');
          $savenews->dateedited = date('Y-m-d');

          if ($savenews->save()) {
              $data["suc"] = "News has been successfully created.";
              $audit = new CB();
              $audit->auditlog(array(
                  "module" =>"News",
                  "event" => "Add",
                  "title" => "Add News : ".$title
                  ));
          } else {
            $data["err"] = $savenews->getMessages()[0]->getMessage();
          }
      }
      echo json_encode($data);
    }

    public function createNewsCenterAction() {
        $request = new Request();
        $escaper = new \Phalcon\Escaper();
        $filter = new \Phalcon\Filter();

        if($request->isPost()) {

            $title =$request->getPost('title');
            $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
            $metatitle = $request->getPost('metatitle');
            $slugs = $request->getPost('slugs');
            $author = $request->getPost('author');
            $date = $request->getPost('date'); // MAINSIDE
            // $date = $request->getPost('datevalid'); // CENTERSIDE?
            $description = $request->getPost('description');
            $body = $request->getPost('body');
            $status = $request->getPost('status');
            $banner = $request->getPost('banner');
            $thumbnail = $request->getPost('thumbnail');
            $newslocation = $request->getPost('newslocation');
            $relatedworkshop = $request->getPost('relatedworkshop');
            $centers = $request->getPost('centers');

            // DATE FORMATTING
            /*if user used the default date and didn't change the date in frontend
            Already ('Y-m-d') format if unchange */
            if(strlen($date) == 10) {
                 $date_published = $date;
            }
            /* If date was changed
             The format is like this "Thu Sep 17 2015 08:00:00 GMT+0800 (Taipei Standard Time)"
            and need to convert to ('Y-m-d') before saving */
            else {
                $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                $dates = explode(" ", $date);
                $date_published = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
            }
            //DATE FORMATTING ends

            foreach ($centers as $keycenters) {

                $center = Center::findFirst('centerid="'.$keycenters['centerid'].'"');
                $newsid = CB::genGUID();

                $savenews = new Centernews();
                $savenews->newsid = $newsid;
                $savenews->title = $title;
                $savenews->metatags = $metatags;
                $savenews->metatitle = $metatitle;
                $savenews->newsslugs = $slugs;
                $savenews->author = ucwords(strtolower($author));
                $savenews->description = $description;
                $savenews->body = $body;
                $savenews->banner = $banner;
                $savenews->thumbnail = $thumbnail;
                $savenews->newslocation = $keycenters['centerid'];
                $savenews->date = $date_published;
                $savenews->status = $status;
                $savenews->views = 1;
                $savenews->type = 'News';
                $savenews->relatedworkshop = $relatedworkshop;
                $savenews->datecreated =  date('Y-m-d');
                $savenews->dateedited = date('Y-m-d');
                if ($savenews->create()) {
                  $data['suc'] = "Center news has been successfully created in all selected centers.";
                  $audit = new CB();
                  $audit->auditlog(array(
                      "module" =>"News",
                      "event" => "Add",
                      "title" => "Add Center News ".$title." in ".$center->centertitle." center"
                      ));
                } else {
                  $data["err"] = "API: Something went wrong. (wala to)";
                }
            }
        } else {
          $data["err"] = "API: NO POST DATA";
        }
        echo json_encode($data);
    }

    public function saveimageAction() {
        $filename = $_POST['imgfilename'];
        $picture = new Newsimage();
        $picture->assign(array(
            'filename' => $filename
            ));

        if (!$picture->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else {
            $data['success'] = "Success";
        }
    }



    public function listimageAction() {

        $getimages = Newsimage::find(array("order" => "id DESC"));
        foreach ($getimages as $getimages)
        {
            $data[] = array(
                'id'=>$getimages->id,
                'filename'=>$getimages->filename
                );
        }
        echo json_encode($data);

    }

    public function listcategoryAction()
    {

        $getcategory= Newscategory::find(array("order" => "categoryid ASC"));
        foreach ($getcategory as $getcategory) {
            $data[] = array(
                'categoryid'=>$getcategory->categoryid,
                'categoryname'=>$getcategory->categoryname
                );
        }
        echo json_encode($data);

    }


    public function manageNewsAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           // $stmt = $db->prepare("
           //  select * from
           //  (
           //      select date as date, newsid as newsid, author as author, title as title, newsslugs as newsslugs, newslocation as newslocation, status as statuss, datecreated as datecreatedd, dateedited as dateedited from centernews
           //      UNION
           //      select date as date, newsid as newsid, author as author, title as title, newsslugs as newsslugs, newslocation as newslocation, status as statuss, datecreated as datecreatedd, dateedited as dateedited  from news
           //  )
           // as news LEFT JOIN center ON news.newslocation = center.centerid ORDER BY datecreated DESC  LIMIT " . $offsetfinal . ",10");
           $stmt = $db->prepare("SELECT *, news.datecreated as datecreated, news.status as status FROM news LEFT JOIN center ON news.newslocation = center.centerid ORDER BY news.datecreated DESC LIMIT " . $offsetfinal . ",10");
           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            foreach($searchresult as $key => $value) {
                $searchresult[$key]['title'] = utf8_encode( $searchresult[$key]['title']);
                $searchresult[$key]["body"] = utf8_encode($searchresult[$key]["body"]);
                $searchresult[$key]['description'] = utf8_encode($searchresult[$key]['description']);
                $searchresult[$key]["newsslugs"] = utf8_encode($searchresult[$key]["newsslugs"]);
            }

           $db1 = \Phalcon\DI::getDefault()->get('db');
           // $stmt1 = $db1->prepare("
           //  select * from
           //  (
           //      select date as date, newsid as newsid, author as author, title as title, newsslugs as newsslugs, newslocation as newslocation, status as statuss, datecreated as datecreatedd, dateedited as dateedited from centernews
           //      UNION
           //      select date as date, newsid as newsid, author as author, title as title, newsslugs as newsslugs, newslocation as newslocation, status as statuss, datecreated as datecreatedd, dateedited as dateedited from news
           //      )
           // as news LEFT JOIN center ON news.newslocation = center.centerid");

           $stmt1 = $db1->prepare("SELECT *, news.datecreated as datecreated, news.status as status FROM news LEFT JOIN center ON news.newslocation = center.centerid ORDER BY news.datecreated DESC");
           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalreportdirty = count($searchresult1);
        } else {

         $offsetfinal = ($page * 10) - 10;

         $db = \Phalcon\DI::getDefault()->get('db');
         // $stmt = $db->prepare("
         //    select * from
         //    (
         //        select date as date, newsid as newsid, author as author,  title as title, newsslugs as newsslugs,newslocation as newslocation,status as statuss, datecreated as datecreatedd, dateedited as dateedited from centernews
         //        UNION
         //        select date as date, newsid as newsid, author as author,  title as title, newsslugs as newsslugs,newslocation as newslocation,status as statuss, datecreated as datecreatedd, dateedited as dateedited from news
         //    )
         // as news LEFT JOIN center ON news.newslocation = center.centerid Where news.title LIKE '%" . $keyword . "%' or news.newsslugs LIKE '%" . $keyword . "%' or news.newslocation LIKE '%" . $keyword . "%' or center.centertitle LIKE '%" . $keyword . "%' ORDER BY datecreated DESC LIMIT " . $offsetfinal . ",10");
         $stmt = $db->prepare("SELECT *, news.datecreated as datecreated, news.status as status FROM news LEFT JOIN center ON news.newslocation = center.centerid WHERE  news.title LIKE '%" . $keyword . "%' or news.newsslugs LIKE '%" . $keyword . "%' or news.author LIKE '%" . $keyword . "%' ORDER BY news.datecreated DESC LIMIT " . $offsetfinal . ",10");

         $stmt->execute();
         $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        foreach($searchresult as $key => $value) {
            $searchresult[$key]['title'] = utf8_encode( $searchresult[$key]['title']);
            $searchresult[$key]["body"] = utf8_encode($searchresult[$key]["body"]);
            $searchresult[$key]['description'] = utf8_encode($searchresult[$key]['description']);
            $searchresult[$key]["newsslugs"] = utf8_encode($searchresult[$key]["newsslugs"]);
        }


         $db1 = \Phalcon\DI::getDefault()->get('db');
         // $stmt1 = $db1->prepare("
         //    select * from
         //    (
         //        select date as date, newsid as newsid, title as title, author as author, newsslugs as newsslugs,newslocation as newslocation,status as statuss, datecreated as datecreatedd, dateedited as dateedited from centernews
         //        UNION
         //        select date as date, newsid as newsid, title as title, author as author, newsslugs as newsslugs,newslocation as newslocation,status as statuss, datecreated as datecreatedd, dateedited as dateedited from news
         //    )
         // as news LEFT JOIN center ON news.newslocation = center.centerid Where news.title LIKE '%" . $keyword . "%' or news.newsslugs LIKE '%" . $keyword . "%' or news.newslocation LIKE '%" . $keyword . "%' or center.centertitle LIKE '%" . $keyword . "%' ");
         $stmt1 = $db1->prepare("SELECT *, news.datecreated as datecreated, news.status as status FROM news LEFT JOIN center ON news.newslocation = center.centerid WHERE  news.title LIKE '%" . $keyword . "%' or news.newsslugs LIKE '%" . $keyword . "%' or news.author LIKE '%" . $keyword . "%' ORDER BY news.datecreated DESC");
         $stmt1->execute();
         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

         $totalreportdirty = count($searchresult1);

        }
        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty));
    }


    public function newsUpdatestatusAction($status,$newsid,$keyword) {

        $data = array();
        $news = News::findFirst('newsid="' . $newsid . '"');
        $news->status = $status;
            if (!$news->save()) {
                $news['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
                if($status == 1){
                    $newsstatus = 'Active';
                }
                else{
                    $newsstatus = 'Deactivated';
                }
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"News",
                    "event" => "Update",
                    "title" => "Update News ".$news->title." status to ".$newsstatus
                    ));
            }

            echo json_encode($data);
    }

    public function newsCenterUpdatestatusAction($status,$newsid,$keyword) {

        $data = array();
        $news = Centernews::findFirst('newsid="' . $newsid . '"');
        $newstitle = $news->title;
        $center = Center::findFirst('centerid="'.$news->newslocation.'"');
        $news->status = $status;
            if (!$news->save()) {
                $news['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
                if($status == 1){
                    $newsstatus = 'Active';
                }
                else{
                    $newsstatus = 'Deactivated';
                }
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"News",
                    "event" => "Update",
                    "title" => "Update Center News ".$newstitle." status to ".$newsstatus. " in ".$center->centertitle. " center"
                    ));
            }

            echo json_encode($data);
    }

    public function newsdeleteAction($newsid) {
        $conditions = 'newsid="' . $newsid . '"';
        $news = News::findFirst(array($conditions));
        $newstitle = $news->title;
        $data = array('error' => 'Not Found');
        if ($news) {
            if ($news->delete()) {
                $data = array('success' => 'News Deleted');
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"News",
                    "event" => "Delete",
                    "title" => "Delete News ".$newstitle.""
                    ));
            }
        }
        echo json_encode($data);
    }

    public function newscenterdeleteAction($newsid) {
        $conditions = 'newsid="' . $newsid . '"';
        $news = Centernews::findFirst(array($conditions));
        $newstitle = $news->title;
        $center = Center::findFirst('centerid="'.$news->newslocation.'"');
        $data = array('error' => 'Not Found');
        if ($news) {
            if ($news->delete()) {
                $data = array('success' => 'News Deleted');
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"News",
                    "event" => "Delete",
                    "title" => "Delete Center News ".$newstitle." in ".$center->centertitle. " center"
                ));
            }
        }
        echo json_encode($data);
    }

    public function newseditoAction($newsid) {
        $data = array();
        $leftbaritem = array();
        $rightbaritem = array();

        $news = News::findFirst('newsid="' . $newsid . '"');
        if ($news) {
            $data = array(
                'newsid' => utf8_encode($news->newsid),
                'title' => utf8_encode($news->title),
                'metatags' => utf8_encode($news->metatags),
                'metatitle' => utf8_encode($news->metatitle),
                'slugs' => utf8_encode($news->newsslugs),
                'author' => utf8_encode($news->author),
                'description' => utf8_encode($news->description),
                'body' => utf8_encode($news->body),
                'banner' => utf8_encode($news->banner),
                'thumbnail' => utf8_encode($news->thumbnail),
                'newslocation' => utf8_encode($news->newslocation),
                'category' => utf8_encode($news->category),
                'status' => utf8_encode($news->status),
                'featurednews' => utf8_encode($news->featurednews),
                'date' => utf8_encode($news->date),
                'datecreated' => utf8_encode($news->datecreated)
                );
        }
        echo json_encode($data);
    }

    public function newscentereditoAction($newsid){
        $data = array();
        $leftbaritem = array();
        $rightbaritem = array();
        $news = Centernews::findFirst('newsid="' . $newsid . '"');
        if($news){
            $data = array(
                'newsid' => $news->newsid,
                'title' => $news->title,
                'metatags' => $news->metatags,
                'metatitle' => $news->metatitle,
                'slugs' => $news->newsslugs,
                'author' => $news->author,
                'description' => $news->description,
                'body' => $news->body,
                'banner' => $news->banner,
                'thumbnail' => $news->thumbnail,
                'newslocation' => $news->newslocation,
                'status' => $news->status,
                'featurednews' => 0,
                'date' => $news->date,
                'datecreated' => $news->datecreated
            );
        }
        echo json_encode($data);
    }

    public function editNewsAction() {
        $filter = new \Phalcon\Filter();
        $request = new Request();

        if($request->isPost()){
            $newsid = $request->getPost('newsid');
            $title = $request->getPost('title');
            $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
            $metatitle = $request->getPost('metatitle');
            $slugs = $request->getPost('slugs');
            $author = $request->getPost('author');
            $newsdate = $request->getPost('date');
            $description = $request->getPost('description');
            $body = $request->getPost('body');
            $status = $request->getPost('status');
            $banner = $request->getPost('banner');
            $thumbnail = $request->getPost('thumbnail');
            $newslocation = $request->getPost('newslocation');
            $category = $request->getPost('category');
            $featurednews = $request->getPost('featurednews');
            $datecreated = $request->getPost('datecreated');
            $relatedworkshop = $request->getPost('relatedworkshop');

            $conditions = 'newsid="' . $newsid . '"';
            $deletenews = News::findFirst(array($conditions));
            $data = array('error' => 'Not Found');
            if ($deletenews) {
                if ($deletenews->delete()) {
                    $data = array('success' => 'News Deleted');
                }
            }

            $conditions = 'newsid="' . $newsid . '"';
            $deletenewscenter = Centernews::findFirst(array($conditions));
            $data = array('error' => 'Not Found');
            if ($deletenewscenter) {
                if ($deletenewscenter->delete()) {
                    $data = array('success' => 'News Deleted');
                }
            }

            if(strlen($newsdate) > 10) {
                 $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                 $dates = explode(" ", $newsdate);
                 $newsdate = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
             }

            //UPDATE THE FEATURED NEWS
            if($featurednews == 1) {
                $getFeatNews = News::find('featurednews = 1');
                foreach($getFeatNews as $news) {
                    $news->assign(array(
                    'featurednews' => 0
                    ));
                    if (!$news->save()) {
                    $errors = array();
                    foreach ($news->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                    } else {
                        $data['success'] = "Success";
                    }
                }
            }

            $upnews = new News();
            $upnews->newsid          = $newsid;
            $upnews->title           = $title;
            $upnews->metatags        = $metatags;
            $upnews->newsslugs       = $slugs;
            $upnews->author          = ucwords(strtolower($author));
            $upnews->metatitle       = $metatitle;
            $upnews->description     = $description;
            $upnews->body            = $body;
            $upnews->banner          = $banner;
            $upnews->thumbnail       = $thumbnail;
            $upnews->newslocation    = $newslocation;
            $upnews->category        = $category;
            $upnews->date            = $newsdate;
            $upnews->status          = $status;
            $upnews->featurednews    = $featurednews;
            $upnews->views           = 1;
            $upnews->type            = 'News';
            $upnews->relatedworkshop = $relatedworkshop;
            $upnews->datecreated     = $datecreated;
            $upnews->dateedited      = date('Y-m-d');
            if($upnews->save()) {
                $data['suc'] = "News changes has been successfully saved.";
                $audit = new CB();
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"News",
                    "event" => "Update",
                    "title" => "Update News : ".$title
                    ));
            } else {
                $data['err'] = $upnews->getMessages()[0]->getMessage();
            }
        }
        echo json_encode($data);
    }

    public function editNewsCenterAction() {
        $data = array();
        $filter = new \Phalcon\Filter();
        $request = new Request();

        if($request->isPost()) {
            $newsid = $request->getPost('newsid');
            $title = $request->getPost('title');
            $metatags = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatags'));
            $metatitle = $request->getPost('metatitle');
            $slugs = $request->getPost('slugs');
            $author = $request->getPost('author');
            $description = $request->getPost('description');
            $newsdate = $request->getPost('date');
            $body = $request->getPost('body');
            $status = $request->getPost('status');
            $featurednews = $request->getPost('featurednews');
            $banner = $request->getPost('banner');
            $thumbnail = $request->getPost('thumbnail');
            $newslocation = $request->getPost('newslocation');
            $category = $request->getPost('category');
            $datecreated = $request->getPost('datecreated');
            $relatedworkshop = $request->getPost('relatedworkshop');

            // $center = Center::findFirst('centerid="'.$newslocation.'"');
            // $conditions = 'newsid="' . $newsid . '"';
            // $deletenews = News::findFirst(array($conditions));
            // $data['error'] = "Not Found";
            // if ($deletenews) {
            //     if ($deletenews->delete()) {
            //         $data['success'] = "News Deleted";
            //     }
            // }
            //
            // $conditions = 'newsid="' . $newsid . '"';
            // $deletenewscenter = Centernews::findFirst(array($conditions));
            // $data['error'] = "Not Found";
            // if ($deletenewscenter) {
            //     if ($deletenewscenter->delete()) {
            //         $data['success'] = "News Deleted";
            //     }
            // }

           if(strlen($newsdate) > 10) {
                 $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                 $dates = explode(" ", $newsdate);
                 $newsdate = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
             }

            // $page = new Centernews();
            $ctr_news               = Centernews::findFirst('newsid="'.$newsid.'"');
            $ctr_news->newsid       = $newsid;
            $ctr_news->title        = $title;
            $ctr_news->metatags     = $metatags;
            $ctr_news->metatitle    = $metatitle;
            $ctr_news->newsslugs    = $slugs;
            $ctr_news->author       = ucwords(strtolower($author));
            $ctr_news->description  = $description;
            $ctr_news->body         = $body;
            $ctr_news->banner       = $banner;
            $ctr_news->thumbnail    = $thumbnail;
            $ctr_news->newslocation = $newslocation;
            $ctr_news->date         = $newsdate;
            $ctr_news->status       = $status;
            $ctr_news->dateedited   = date('Y-m-d');

            if($ctr_news->save()) {
              $data['suc'] = "Changes has been successfully saved.";
              $audit = new CB();
              $audit->auditlog(array(
                  "module" =>"News",
                  "event" => "Update",
                  "title" => "Update Center News ".$title." in ".$center->centertitle. " center"
              ));
            } else {
              $data['err'] = $ctr_news->getMessages()[0]->getMessage();
            }
        }
        echo json_encode($data);
    }

    public function managecategoryAction($num, $page, $keyword) {

        if ($keyword == 'null' || $keyword == 'undefined') {
            $Pages = Newscategory::find();
        } else {
            $conditions = "categoryname LIKE '%" . $keyword . "%'";
            $Pages = Newscategory::find(array($conditions));
        }

        $currentPage = (int) ($page);

        // Create a Model paginator, show 10 rows by page starting from $currentPage
        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $Pages,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'categoryid' => $m->categoryid,
                'categoryname' => $m->categoryname,
                );
        }
        $p = array();
        for ($x = 1; $x <= $page->total_pages; $x++) {
            $p[] = array('num' => $x, 'link' => 'page');
        }
        echo json_encode(array('data' => $data, 'pages' => $p, 'index' => $page->current, 'before' => $page->before, 'next' => $page->next, 'last' => $page->last, 'total_items' => $page->total_items));

    }

    public function validatecategAction($categ){
        $validate = Newscategory::find(array("categoryname='".$categ."'"));

        echo json_encode(count($validate));
    }

    public function createcategoryAction()
    {

       $data = array();

       $catnames = new Newscategory();
       $catnames->assign(array(
        'categoryname' => $_POST['catnames'],
        'categoryslugs' => $_POST['slugs']
        ));
        if (!$catnames->save()){
        $data['error'] = "Something went wrong saving the data, please try again.";
        }
        else{
         $data['success'] = "Success";
         $audit = new CB();
         $audit->auditlog(array(
            "module" =>"News",
            "event" => "Create",
            "title" => "Create News Category".$_POST['catnames'].""
            ));
        }
        echo json_encode($data);
    }

    public function updatecategoryAction()
    {

        $data = array();
        $news = Newscategory::findFirst('categoryid=' . $_POST['memid'] . ' ');
        $news->categoryname = $_POST['data'];
        $news->categoryslugs = $_POST['slugs'];

        if (!$news->save())
        {
            $data['error'] = "Something went wrong saving the data, please try again.";
        }
        else
        {
            $data['success'] = "Success";
            $audit = new CB();
            $audit->auditlog(array(
                "module" =>"News",
                "event" => "Update",
                "title" => "Update News Category".$_POST['data'].""
                ));
        }
        echo json_encode($data);
    }

    public function categorydeleteAction($id) {
        $conditions = "categoryid=" . $id;
        $category = Newscategory::findFirst(array($conditions));
        $categoryname = $category->categoryname;
        $data = array('error' => 'Not Found');
        if ($category) {
            if ($category->delete()) {
                $data = array('success' => 'Category Deleted');
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"News",
                    "event" => "Delete",
                    "title" => "Delete News Category".$categoryname.""
                    ));
            }
        }
        echo json_encode($data);
    }

    public function listcategoryfrontendAction() {
        $newscategory = Newscategory::find();
        $newscategory = json_encode($newscategory->toArray(), JSON_NUMERIC_CHECK);
        echo $newscategory;
    }

    public function featurednewsfrontendAction() {
        $datenow = date("Y-m-d");
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 1 AND news.date <= '".$datenow."' AND status = 1 LIMIT 0, 1");
        $stmt->execute();
        $featurednews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($featurednews);
    }

    public function latestnewsfrontendAction() {
        $datenow = date("Y-m-d");
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 7 AND news.date <= '".$datenow."' AND  status = 1 ORDER BY date DESC LIMIT 0, 10");
        $stmt->execute();
        $latestnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($latestnews);
    }

    public function founderswisdomfrontendAction() {
        $datenow = date("Y-m-d");
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE categoryid = 7 AND news.date <= '".$datenow."' AND status = 1 ORDER BY date DESC LIMIT 0, 1");

        $stmt->execute();
        $founderswisdome = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode($founderswisdome);

    }

    public function showcategorynameAction($categoryslugs) {

    $newscategory = Newscategory::find(array('categoryslugs="' . $categoryslugs . '"'));
    $newscategory = json_encode($newscategory->toArray(), JSON_NUMERIC_CHECK);
    echo $newscategory;

    }

    public function listnewsbycategoryAction($categoryslugs, $offset) {
        $datenow = date("Y-m-d");
        $finaloffset = ($offset * 10) - 10;
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE newscategory.categoryslugs ='" . $categoryslugs . "' AND news.date <= '".$datenow."' AND status = 1 ORDER BY date DESC LIMIT $finaloffset, 10");
        $stmt->execute();
        $listnewsbycategory = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($listnewsbycategory);
    }

    public function showtotalnewscategoryitemAction($categoryslugs) {
        $datenow = date("Y-m-d");
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE newscategory.categoryslugs ='" . $categoryslugs . "'  AND news.date <= '".$datenow."' AND status = 1 ORDER BY date DESC");
        $stmt->execute();
        $totalpage = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode(count($totalpage));
    }

    public function showFullNewsAction($newsslugs) {
        $datenow = date("Y-m-d");
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE news.newsslugs ='" . $newsslugs . "' AND news.date <= '".$datenow."' AND status = 1 ");
        $stmt->execute();
        $fullnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($fullnews);
    }

    public function showSidebarMostViewedAction() {
        $datenow = date("Y-m-d");
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND news.date <= '".$datenow."' AND  status = 1 ORDER BY news.views DESC LIMIT 0, 3");
        $stmt->execute();
        $latestnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($latestnews);
    }

    public function showSidebarLatestNewsAction() {
        $datenow = date("Y-m-d");
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND news.date <= '".$datenow."' AND  status = 1 ORDER BY date DESC LIMIT 0, 3");
        $stmt->execute();
        $latestnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($latestnews);
    }

    public function newsViewsCountUpdateAction($newsslugs) {
        $data = array();
        $news = News::findFirst('newsslugs="' . $newsslugs . '"');
        $news->views = $news->views + 1;
        if (!$news->save()) {
            $data['error'] = "Something went wrong saving the data, please try again.";
        } else
        {
            $data['success'] = "Success";
        }
        echo json_encode($data);
    }


    public function deletenewsimgAction($id)
    {
        $newsimg = Newsimage::findFirst("id=$id");
        if ($newsimg) {
            if ($newsimg->delete()) {
                $data = array('success' => 'Image Deleted');
            }
            else
            {
                $data = array('error' => 'Image Not Deleted');
            }
        }
        echo json_encode($data);
    }

    public function displayPopularNewsAction() {
        $datenow = date("Y-m-d");
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND news.date <= '".$datenow."' AND   status = 1 ORDER BY news.views DESC LIMIT 0,5");
        $stmt->execute();
        $latestnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($latestnews);
    }

    public function newrssAction() {
        $news = News::find(array("order"=>"date DESC", "limit" => array("number" => 10, "offset" => $offset)));
        $postinfo = json_encode($news->toArray(), JSON_NUMERIC_CHECK);
        echo $postinfo;
    }

    public function socialUpdatestatusAction() { // [. .]socialUpdatestatus talaga?
        $center = Center::find(array("order"=>"centertitle"));
        $center = json_encode($center->toArray(), JSON_NUMERIC_CHECK);
        echo $center;
    }

    public function viewAction() {
        $stories = Testimonies::find("status = '1' ORDER BY date_published DESC LIMIT 5  ");
        echo json_encode(array("stories"=> $stories->toArray()));
    }

    public function randomcategoryfrontendAction() {
        $datenow = date("Y-m-d");
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE news.date <= '".$datenow."' AND news.status = 1 and news.featurednews != 1 ORDER BY RAND() DESC LIMIT 0,1");

        $stmt->execute();
        $randomenews = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode(array('randomenews'=>$randomenews));
    }

    public function workshoptitlesAction() {
        $workshoptitles = Workshoptitle::find();
        $titles = array();
        foreach($workshoptitles as $title) {
            $titles[] = $title->titleslugs;
        }
        echo json_encode(array("titles" => $titles));
    }

    public function mainnewsworkshoprelatedAction($newsid) {
        $workshoptitles = Workshoptitle::find();
        $titles = array();
        foreach($workshoptitles as $title) {
            $titles[] = $title->titleslugs;
        }

        $news = News::findFirst(" newsid = '$newsid' ");
        $related = $news->relatedworkshop;

        echo json_encode(array(
            "titles" => $titles,
            "related" => $related
        ));
    }

    public function mainnewsaddrelatedAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $newsid = $request->getPost('newsid');
            $title = $request->getPost('title');

            $find = News::findFirst(" newsid = '$newsid' ");
            $find->relatedworkshop = $find->relatedworkshop .",".$title;
            if($find->save()) {
                echo json_encode(array("result" => "Successful!"));
            } else {
                echo json_encode(array("result" => "Error!"));
            }
        }
    }

    public function mainnewsremoverelatedAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $newsid = $request->getPost('newsid');
            $related = $request->getPost('related');

            $find = News::findFirst(" newsid = '$newsid' ");
            if($find) {
                $find->relatedworkshop = $related;
                if($find->save()) {
                    echo json_encode(array("result" => "Successful!"));
                } else {
                    echo json_encode(array("result" => "Error!"));
                }
            }
        }
    }

    //  public function centernewsworkshoprelatedAction($newsid) {
    //     $workshoptitles = Workshoptitle::find();
    //     $titles = array();
    //     foreach($workshoptitles as $title) {
    //         $titles[] = $title->titleslugs;
    //     }

    //     $news = Centernews::findFirst(" newsid = '$newsid' ");
    //     $related = $news->relatedworkshop;

    //     echo json_encode(array(
    //         "titles" => $titles,
    //         "related" => $related
    //     ));
    // }

    // public function centernewsaddrelatedAction() {
    //     $request = new \Phalcon\Http\Request();
    //     if($request->isPost()) {
    //         $newsid = $request->getPost('newsid');
    //         $title = $request->getPost('title');

    //         $find = Centernews::findFirst(" newsid = '$newsid' ");
    //         $find->relatedworkshop = $find->relatedworkshop .",".$title;
    //         if($find->save()) {
    //             echo json_encode(array("result" => "Successful!"));
    //         } else {
    //             echo json_encode(array("result" => "Error!"));
    //         }
    //     }
    // }

    // public function centernewsremoverelatedAction() {
    //     $request = new \Phalcon\Http\Request();
    //     if($request->isPost()) {
    //         $newsid = $request->getPost('newsid');
    //         $related = $request->getPost('related');

    //         $find = Centernews::findFirst(" newsid = '$newsid' ");
    //         if($find) {
    //             $find->relatedworkshop = $related;
    //             if($find->save()) {
    //                 echo json_encode(array("result" => "Successful!"));
    //             } else {
    //                 echo json_encode(array("result" => "Error!"));
    //             }
    //         }
    //     }
    // }

    public function febnbbuzzindexAction() {
        $datenow = date('Y-m-d');
        $db = \Phalcon\DI::getDefault()->get('db');

        $newscategory = Newscategory::find();

        // $stmt1 = $db->prepare("SELECT * FROM  news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE news.category = 1 AND news.date <= '".$datenow."' AND  news.status = 1 AND news.featurednews != 1 ORDER BY RAND() LIMIT 1");
        $stmt1 = $db->prepare("SELECT * FROM  news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE news.category = 1  ORDER BY news.date DESC  LIMIT 0, 1");

        $stmt1->execute();
        $randomenews = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
        foreach($randomenews as $key => $value) {
            $randomenews[$key]['title'] = utf8_encode( $randomenews[$key]['title']);
            $randomenews[$key]["body"] = utf8_encode($randomenews[$key]["body"]);
            $randomenews[$key]['description'] = utf8_encode($randomenews[$key]['description']);
            $randomenews[$key]["newsslugs"] = utf8_encode($randomenews[$key]["newsslugs"]);
        }
        $dups = $randomenews[0]['newsid'];

        $stmt2 = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 1 AND news.date <= '".$datenow."' AND  status = 1 LIMIT 0, 1");
        $stmt2->execute();
        $featurednews = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
        foreach($featurednews as $key => $value) {
            $featurednews[$key]['title'] = utf8_encode( $featurednews[$key]['title']);
            $featurednews[$key]["body"] = utf8_encode($featurednews[$key]["body"]);
            $featurednews[$key]['description'] = utf8_encode($featurednews[$key]['description']);
            $featurednews[$key]["newsslugs"] = utf8_encode($featurednews[$key]["newsslugs"]);
        }

        $stmt3 = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 7 AND news.date <= '".$datenow."' AND   status = 1 ORDER BY date DESC LIMIT 0, 10");
        $stmt3->execute();
        $latestnews = $stmt3->fetchAll(\PDO::FETCH_ASSOC);
        foreach($latestnews as $key => $value) {
            $latestnews[$key]['title'] = utf8_encode( $latestnews[$key]['title']);
            $latestnews[$key]["body"] = utf8_encode($latestnews[$key]["body"]);
            $latestnews[$key]['description'] = utf8_encode($latestnews[$key]['description']);
            $latestnews[$key]["newsslugs"] = utf8_encode($latestnews[$key]["newsslugs"]);
        }

        $stmt4 = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE categoryid = 7 AND news.date <= '".$datenow."' AND  status = 1 ORDER BY date DESC LIMIT 0, 1");
        $stmt4->execute();
        $founderwisdom = $stmt4->fetchAll(\PDO::FETCH_ASSOC);

        foreach($founderwisdom as $key => $value) {
            $founderwisdom[$key]['title'] = utf8_encode( $founderwisdom[$key]['title']);
            $founderwisdom[$key]["body"] = utf8_encode($founderwisdom[$key]["body"]);
            $founderwisdom[$key]['description'] = utf8_encode($founderwisdom[$key]['description']);
            $founderwisdom[$key]["newsslugs"] = utf8_encode($founderwisdom[$key]["newsslugs"]);
        }

        // var_dump($latestnews);

        echo json_encode(array(
            "dups" => $dups,
            "newscategory" => $newscategory->toArray(),
            "randomenews"=> $randomenews,
            "featurednews" => $featurednews,
            "latestnews" => $latestnews,
            "founderwisdom" => $founderwisdom
        ));

    }

    public function febnbbuzzviewAction($newsslugs) {
        $datenow = date('Y-m-d');
        $db = \Phalcon\DI::getDefault()->get('db');

        $newscategory = Newscategory::find(array());

        // $stmt1 = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE news.newsslugs ='" . $newsslugs . "'  AND news.date <= '".$datenow."' AND status = 1 ");
        // $stmt1->execute();
        // $fullnews = $stmt1->fetch(\PDO::FETCH_ASSOC);
        $query = "SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE news.newsslugs ='" . $newsslugs . "'  AND news.date <= '".$datenow."' AND status = 1 ";
        $fullnews = CB::bnbQueryFirst($query);

            $fullnews['title'] = utf8_encode( $fullnews['title']);
            $fullnews["body"] = utf8_encode($fullnews["body"]);
            $fullnews['description'] = utf8_encode($fullnews['description']);
            $fullnews["newsslugs"] = utf8_encode($fullnews["newsslugs"]);
        $fullnews_catid = $fullnews['category'];

        $query = "SELECT * FROM news LEFT JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid = ".$fullnews_catid." AND news.date <= '".$datenow."' AND status = 1 ORDER BY date DESC";
        $latestnews = CB::bnbQuery($query);

        foreach($latestnews as $key => $value) {
            $latestnews[$key]['title'] = utf8_encode( $latestnews[$key]['title']);
            $latestnews[$key]["body"] = utf8_encode($latestnews[$key]["body"]);
            $latestnews[$key]['description'] = utf8_encode($latestnews[$key]['description']);
            $latestnews[$key]["newsslugs"] = utf8_encode($latestnews[$key]["newsslugs"]);
        }

        // $stmt3 = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND news.date <= '".$datenow."' AND status = 1 ORDER BY news.views DESC LIMIT 0, 3");
        // $stmt3->execute();
        // $mostviewed = $stmt3->fetchAll(\PDO::FETCH_ASSOC);
        $query = "SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND news.date <= '".$datenow."' AND status = 1 ORDER BY news.views DESC LIMIT 0, 3";
        $mostviewed = CB::bnbQuery($query);

        foreach($mostviewed as $key => $value) {
            $mostviewed[$key]['title'] = utf8_encode( $mostviewed[$key]['title']);
            $mostviewed[$key]["body"] = utf8_encode($mostviewed[$key]["body"]);
            $mostviewed[$key]['description'] = utf8_encode($mostviewed[$key]['description']);
            $mostviewed[$key]["newsslugs"] = utf8_encode($mostviewed[$key]["newsslugs"]);
        }

        // $stmt4 = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND news.date <= '".$datenow."' AND status = 1 ORDER BY date DESC LIMIT 0, 3");
        // $stmt4->execute();
        // $sidebarlatestnews = $stmt4->fetchAll(\PDO::FETCH_ASSOC);
        $query = "SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND news.date <= '".$datenow."' AND status = 1 ORDER BY date DESC LIMIT 0, 3";
        $sidebarlatestnews = CB::bnbQuery($query);

        foreach($sidebarlatestnews as $key => $value) {
            $sidebarlatestnews[$key]['title'] = utf8_encode( $sidebarlatestnews[$key]['title']);
            $sidebarlatestnews[$key]["body"] = utf8_encode($sidebarlatestnews[$key]["body"]);
            $sidebarlatestnews[$key]['description'] = utf8_encode($sidebarlatestnews[$key]['description']);
            $sidebarlatestnews[$key]["newsslugs"] = utf8_encode($sidebarlatestnews[$key]["newsslugs"]);
        }

        $stories = Testimonies::find("status = '1' ORDER BY date_published DESC LIMIT 5 ");

        // var_dump($fullnews);

        echo json_encode(array(
            "totalcategorynews" => $totalcategorynews,
            "newscategory" => $newscategory->toArray(),
            "fullnews" => $fullnews,
            "latestnews" => $latestnews,
            "mostviewed" => $mostviewed,
            "sidebarlatestnews" => $sidebarlatestnews,
            "stories" => $stories->toArray()
        ));
    }

    public function febnbbuzzcategoryAction($categoryslugs, $offset) {
        $datenow = date('Y-m-d');
        $db = \Phalcon\DI::getDefault()->get('db');
        $newscategory = Newscategory::findFirst('categoryslugs="' . $categoryslugs . '"');

        $categorylist = Newscategory::find();

        $finaloffset = ($offset * 10) - 10;
        $stmt1 = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE newscategory.categoryslugs ='" . $categoryslugs . "' AND news.date <= '".$datenow."' AND  status = 1 ORDER BY date DESC LIMIT $finaloffset, 10");
        $stmt1->execute();
        $listnewsbycategory = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

        foreach($listnewsbycategory as $key => $value) {
            $listnewsbycategory[$key]['title'] = utf8_encode( $listnewsbycategory[$key]['title']);
            $listnewsbycategory[$key]["body"] = utf8_encode($listnewsbycategory[$key]["body"]);
            $listnewsbycategory[$key]['description'] = utf8_encode($listnewsbycategory[$key]['description']);
            $listnewsbycategory[$key]["newsslugs"] = utf8_encode($listnewsbycategory[$key]["newsslugs"]);
        }

        $stmt2 = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE newscategory.categoryslugs ='" . $categoryslugs . "' AND news.date <= '".$datenow."' AND  status = 1 ORDER BY date DESC");
        $stmt2->execute();
        $totalpage = $stmt2->fetchAll(\PDO::FETCH_ASSOC);

         foreach($totalpage as $key => $value) {
            $totalpage[$key]['title'] = utf8_encode( $totalpage[$key]['title']);
            $totalpage[$key]["body"] = utf8_encode($totalpage[$key]["body"]);
            $totalpage[$key]['description'] = utf8_encode($totalpage[$key]['description']);
            $totalpage[$key]["newsslugs"] = utf8_encode($totalpage[$key]["newsslugs"]);
        }

        $stmt3 = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND news.date <= '".$datenow."' AND status = 1 ORDER BY news.views DESC LIMIT 0, 3");
        $stmt3->execute();
        $mostviewed = $stmt3->fetchAll(\PDO::FETCH_ASSOC);

         foreach($mostviewed as $key => $value) {
            $mostviewed[$key]['title'] = utf8_encode( $mostviewed[$key]['title']);
            $mostviewed[$key]["body"] = utf8_encode($mostviewed[$key]["body"]);
            $mostviewed[$key]['description'] = utf8_encode($mostviewed[$key]['description']);
            $mostviewed[$key]["newsslugs"] = utf8_encode($mostviewed[$key]["newsslugs"]);
        }

        $stmt4 = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND news.date <= '".$datenow."' AND status = 1 ORDER BY date DESC LIMIT 0, 3");
        $stmt4->execute();
        $sidebarlatestnews = $stmt4->fetchAll(\PDO::FETCH_ASSOC);

        foreach($sidebarlatestnews as $key => $value) {
            $sidebarlatestnews[$key]['title'] = utf8_encode( $sidebarlatestnews[$key]['title']);
            $sidebarlatestnews[$key]["body"] = utf8_encode($sidebarlatestnews[$key]["body"]);
            $sidebarlatestnews[$key]['description'] = utf8_encode($sidebarlatestnews[$key]['description']);
            $sidebarlatestnews[$key]["newsslugs"] = utf8_encode($sidebarlatestnews[$key]["newsslugs"]);
        }

        echo json_encode(array(
            "newscategory" => $newscategory,
            "categorylist" => $categorylist->toArray(),
            "listnewsbycategory" => $listnewsbycategory,
            "totalpage" => count($totalpage),
            "mostviewed" => $mostviewed,
            "sidebarlatestnews" => $sidebarlatestnews
        ));

    }

    public function bnbbuzzindexAction($category, $page){

        $datenow = date('Y-m-d');
        $offsetfinal = ($page * 9) - 9;

        $newscategory = Newscategory::find();
        if($newscategory){
            $data['newscategory'] = $newscategory->toArray();
        }

        if ($category == 'all') {
            $data['currentcategoryname'] = 'All';
            $newsdata = CB::bnbQuery("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE news.date <= '".$datenow."' AND   status = 1 ORDER BY date DESC LIMIT " . $offsetfinal . ",9");
            $newsdatacount = CB::bnbQuery("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE news.date <= '".$datenow."' AND   status = 1 ORDER BY date DESC");
            if($newsdata){
                foreach($newsdata as $key => $value) {
                    $newsdata[$key]['title'] = utf8_encode( $newsdata[$key]['title']);
                    $newsdata[$key]["body"] = utf8_encode($newsdata[$key]["body"]);
                    $newsdata[$key]['description'] = utf8_encode($newsdata[$key]['description']);
                    $newsdata[$key]["newsslugs"] = utf8_encode($newsdata[$key]["newsslugs"]);
                }
                $data['newslist'] = $newsdata;
                $data['index'] = $page;
                $data['totalnews'] = count($newsdatacount);
            }
       }
       else{
            $newsdata = CB::bnbQuery("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE newscategory.categoryslugs = '". $category ."' AND news.date <= '".$datenow."' AND   status = 1 ORDER BY date DESC LIMIT " . $offsetfinal . ",9");
            $newsdatacount = CB::bnbQuery("SELECT newsid FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE newscategory.categoryslugs = '". $category ."' AND news.date <= '".$datenow."' AND   status = 1");
            if($newsdata){
                foreach($newsdata as $key => $value) {
                    $newsdata[$key]['title'] = utf8_encode( $newsdata[$key]['title']);
                    $newsdata[$key]["body"] = utf8_encode($newsdata[$key]["body"]);
                    $newsdata[$key]['description'] = utf8_encode($newsdata[$key]['description']);
                    $newsdata[$key]["newsslugs"] = utf8_encode($newsdata[$key]["newsslugs"]);
                }
                $data['newslist'] = $newsdata;
                $data['index'] = $page;
                $data['totalnews'] = count($newsdatacount);
                $data['currentcategoryname'] = $newsdata[0]['categoryname'];
            }
       }
       echo json_encode($data);

    }

    public function getnewscategoriesAction(){
        $newscategory = Newscategory::find();
        if($newscategory){
            $data['newscategory'] = $newscategory->toArray();
        }
        echo json_encode($data);
    }

    public function febnbbuzznewsviewAction($newsslugs){
        $newsdata = CB::bnbQueryFirst("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE news.newsslugs = '". $newsslugs ."' AND news.status = 1 ");
        if($newsdata) {
            $newsdata['title'] = utf8_encode( $newsdata['title']);
            $newsdata["body"] = utf8_encode($newsdata["body"]);
            $newsdata['description'] = utf8_encode($newsdata['description']);
            $newsdata["newsslugs"] = utf8_encode($newsdata["newsslugs"]);

            $data['newsdata'] = $newsdata;
        }
        echo json_encode($data);
    }


    public function setThumbnailAction(){
        $newsdata = CB::bnbQuery("SELECT * FROM news");
        foreach ($newsdata as $news) {
            $newsupdate = CB::bnbQueryupdate("UPDATE news set thumbnail = '". $news['banner'] ."' WHERE newsid = '".$news['newsid']."'");
        }
    }

}
