<?php

namespace Controllers;

use \Models\Subscribers as Subscribers;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class SubscribersController extends \Phalcon\Mvc\Controller {

    public function addsubscriberAction(){

        $request = new \Phalcon\Http\Request();
        
        if($request->isPost()){

            $name = $request->getPost('name');
            $email = $request->getPost('email');

            $guid = new \Utilities\Guid\Guid();
            $id = $guid->GUID();

            $subscribers = new Subscribers();
            $subscribers->assign(array(
                'id' => $id,
                'sname' => $name,
                'semail' => $email,
                "datecreated" => date('Y-m-d'),
                "dateupdated" => date('Y-m-d H:i:s')
                ));

            if (!$subscribers->save()) {
                $errors = array();
                foreach ($subscribers->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
               $data['success'] = "Success";
               echo json_encode($data);
           }

       }
   }
   public function checkemailAction($email){
        $email = Subscribers::find("semail = '$email' ");
        echo json_encode(count($email));
   }

   public function subscriberslistAction($num,$page,$keyword) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $offsetfinal = ($page * 10) - 10;
        if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {

            $stmt = $db->prepare("SELECT * FROM subscribers LIMIT $offsetfinal, $num");
            $stmt->execute();
            $subscribers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            $subscriberscount = Subscribers::find();

            $totalNumberOfsubscribers = count($subscriberscount);

        } else {
            $stmt = $db->prepare("SELECT * FROM subscribers WHERE sname LIKE '%$keyword%' or semail LIKE '%$keyword%'  LIMIT $offsetfinal, $num");
            $stmt->execute();
            $subscribers = $stmt->fetchAll(\PDO::FETCH_ASSOC);

            $stmt = $db->prepare("SELECT * FROM subscribers WHERE sname LIKE '%$keyword%' or semail LIKE '%$keyword%' ");
            $stmt->execute();
            $subscriberscount = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            $totalNumberOfsubscribers = count($subscriberscount);
        }

        echo json_encode(array('data' => $subscribers, 'index' => $page, 'total_items' => $totalNumberOfsubscribers));

    }
    public function deletesubscriberAction($id) {
        $subscriber = Subscribers::findFirst("id = '$id' ");
        if($subscriber) {
            if($subscriber->delete()) {
                echo json_encode(array("result" => "Successfully Deleted!"));
            } else {
                echo json_encode(array("result" => "There was an error!"));
            }
        } else {
            echo json_encode(array("result" => "There was an error!"));
        }
    }

}
