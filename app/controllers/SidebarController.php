<?php

namespace Controllers;

use \Models\Testimonies as Testimonies;

class SidebarController extends \Phalcon\Mvc\Controller {
	public function contentsAction() {
		// $db = \Phalcon\DI::getDefault()->get('db');
        // $stmt1 = $db->prepare("SELECT * FROM testimonies ORDER BY ");
        // $stmt1->execute();
        // $story5 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

        $story5 = Testimonies::find("status = '1' ORDER BY date_published DESC LIMIT 5  ");

        echo json_encode(array(
        	"story5" => $story5->toArray()
        ));
	}
}

