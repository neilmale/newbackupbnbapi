<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Models\Timezonebyzipcode as Timezonebyzipcode;
use \Models\News as News;
use \Models\Centercalendar as Centercalendar;
use \Models\Centerschedule as Centerschedule;
use \Models\Centerlocation as Centerlocation;
use \Models\Beneplacepricing as Beneplacepricing;
use \Models\Centermanagerphone as Centermanagerphone;
use \Models\Centeremail as Centeremail;
use \Models\Classpricing as Classpricing;
use \Models\Centernews as Centernews;
use \Models\Centerdistrict as Centerdistrict;
use \Models\Centerregion as Centerregion;
use \Models\Centerpricingregfee as Centerpricingregfee;
use \Models\Centerphonenumber as Centerphonenumber;
use \Models\Centermembership as Centermembership;
use \Models\Centerpricingregclass as Centerpricingregclass;
use \Models\Centerpricingsessionfee as Centerpricingsessionfee;
use \Models\Centerhours as Centerhours;
use \Models\Centerprivatesession as Centerprivatesession;
use \Models\Centersociallinks as Centersociallinks;
use \Models\Center as Center;
use \Models\Centerimages as Centerimages;
use \Models\Centeroffer as Centeroffer;
use \Models\Users as Users;
use \Models\States as States;
use \Models\Centersession1 as Centersession1;
use \Models\Centersession2 as Centersession2;
use \Models\Centersession3 as Centersession3;
use \Models\Centersession4 as Centersession4;
use \Models\Cities as Cities;
use \Models\Cities_extended as Cities_extended;
use \Models\Testimonies as Testimonies;
use \Models\Classes as Classes;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class CenterController extends \Phalcon\Mvc\Controller {


  public function savecenterAction() {
      $request = new Request();

      if($request->isPost()) {
          $centertitle      = $request->getPost('centertitle');
          $slugs            = $request->getPost('slugs')[0]["slug"];
          $slugs2           = $request->getPost('slugs')[1]["slug"];
          $slugs3           = $request->getPost('slugs')[2]["slug"];
          $centerdesc       = $request->getPost('centerdesc');
          $centeraddress    = $request->getPost('centeraddress');
          $centerregion     = $request->getPost('centerregion');
          $centerstate      = $request->getPost('centerstate');
          $centercity       = $request->getPost('centercity');
          $centerzip        = $request->getPost('centerzip');
          $metatitle        = $request->getPost('metatitle');
          $metadesc         = $request->getPost('metadesc');
          $paypalid         = $request->getPost('paypalid');
          $authorizekey     = $request->getPost('authorizekey');
          $authorizeid      = $request->getPost('authorizeid');
          $phonenumber      = $request->getPost("phonenumber");
          $centeremail      = $request->getPost("email");
          $fbcapc           = $request->getPost('fbcapc');
          $centertype       = $request->getPost('centertype');
          $status           = $request->getPost('status');

          $origsessionprice = $request->getPost('origsessionprice');
          $dissessionprice  = $request->getPost('dissessionprice');
          $origgroupprice   = $request->getPost('origgroupprice');
          $disgroupprice    = $request->getPost('disgroupprice');

          $orig1monthprice  = $request->getPost('orig1monthprice');
          $dis1monthprice   = $request->getPost('dis1monthprice');
          $orig3monthprice  = $request->getPost('orig3monthprice');
          $dis3monthprice   = $request->getPost('dis3monthprice');

          // $centerdistrict = $request->getPost('centerdistrict');
          // $thisdistrict = Centerdistrict::findFirst("districtid = '$centerdistrict'");

          $openingdate = $request->getPost('openingdate');
          $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
          $dates = explode(" ", $openingdate);
          $openingdate = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

          $centerid   = CB::genGUID();
          $locationid = CB::genGUID();

          $center = new Center();
          $center->centerid        = $centerid;
          $center->centertitle     = $centertitle;
          $center->centerslugs     = $slugs;
          $center->centerslugs2    = $slugs2;
          $center->centerslugs3    = $slugs3;
          $center->centerdesc      = $centerdesc;
          $center->openingdate     = $openingdate;
          $center->centerregion    = $centerregion;
          $center->centerdistrict  = 'null';
          $center->centeraddress   = $centeraddress;
          $center->centerstate     = $centerstate;
          $center->centercity      = $centercity;
          $center->centerzip       = $centerzip;
          $center->metatitle       = $metatitle;
          $center->metadesc        = $metadesc;
          $center->centermanager   = 0;
          $center->centertype      = $centertype;
          $center->status          = $status;
          $center->fbcapc          = $fbcapc;
          // $center->centertelnumber = '(123) 456-7894', this column has been deleted in TABLE center [-null]
          $center->authorizeid     = $authorizeid;
          $center->authorizekey    = $authorizekey;
          $center->paypalid        = $paypalid;
          $center->orig1monthprice = $orig1monthprice;
          $center->dis1monthprice  = $dis1monthprice;
          $center->orig3monthprice = $orig3monthprice;
          $center->dis3monthprice  = $dis3monthprice;
          $center->spnl            = 0;
          $center->datecreated     = date('Y-m-d');
          $center->dateedited      = date('Y-m-d');

          if (!$center->save()) {

              $errors = array();
              foreach ($center->getMessages() as $message) {
                  $errors[] = $message->getMessage();
              }
              $data['err'] = $center->getMessages()[0]->getMessage();

          } else {
              $data['scs'] = "New Center has been successfully created";
              $data['wrnMsg'] = []; //warnings array

              $audit = new CB();
              $audit->auditlog(array(
                  "module" =>"Center",
                  "event" => "Create",
                  "title" => "Create Center : ". $centertitle .""
              ));

              $ctr_loc             = new Centerlocation();
              $ctr_loc->locationid = $locationid;
              $ctr_loc->centerid   = $centerid;
              $ctr_loc->lat        = "39.8422862102958";
              $ctr_loc->lon        = "-104.95239295312501";
              if(!$ctr_loc->create()){
                $data['wrn'] = true;
                foreach ($ctr_loc->getMessages() as $message) {
                    $data['wrnMsg'][] = $message->getMessage();
                }
              };

              $prcregfee           = new Centerpricingregfee();
              $prcregfee->centerid = $centerid;
              $prcregfee->fee      = "15";
              $prcregfee->status   = '1';
              if(!$prcregfee->create()){
                $data['wrn'] = true;
                foreach ($prcregfee->getMessages() as $message) {
                    $data['wrnMsg'][] = $message->getMessage();
                }
              };

              $prcregclass           = new Centerpricingregclass();
              $prcregclass->centerid = $centerid;
              $prcregclass->fee      = "15";
              $prcregclass->status   = '1';
              if(!$prcregclass->create()){
                $data['wrn'] = true;
                foreach ($prcregclass->getMessages() as $message) {
                    $data['wrnMsg'][] = $message->getMessage();
                }
              };

              $prcssnfee           = new Centerpricingsessionfee();
              $prcssnfee->centerid = $centerid;
              $prcssnfee->fee      = "15";
              $prcssnfee->status   = '1';
              if(!$prcssnfee->create()){
                $data['wrn'] = true;
                foreach ($prcssnfee->getMessages() as $message) {
                    $data['wrnMsg'][] = $message->getMessage();
                }
              };

              $openhourid          = CB ::genGUID();
              $hrs                 = new Centerhours();
              $hrs->openhourid     = $openhourid;
              $hrs->centerid       = $centerid;

              $hrs->montimestart   = "07:00";
              $hrs->monformatstart = "AM";
              $hrs->montimeend     = "09:00";
              $hrs->monformatend   = "PM";

              $hrs->tuetimestart   = "07:00";
              $hrs->tueformatstart = "AM";
              $hrs->tuetimeend     = "09:00";
              $hrs->tueformatend   = "PM";

              $hrs->wedtimestart   = "07:00";
              $hrs->wedformatstart = "AM";
              $hrs->wedtimeend     = "09:00";
              $hrs->wedformatend   = "PM";

              $hrs->thutimestart   = "07:00";
              $hrs->thuformatstart = "AM";
              $hrs->thutimeend     = "09:00";
              $hrs->thuformatend   = "PM";

              $hrs->fritimestart   = "07:00";
              $hrs->friformatstart = "AM";
              $hrs->fritimeend     = "09:00";
              $hrs->friformatend   = "PM";

              $hrs->sattimestart   = "07:00";
              $hrs->satformatstart = "AM";
              $hrs->sattimeend     = "09:00";
              $hrs->satformatend   = "PM";
              $hrs->satstatus      = 0;

              $hrs->suntimestart   = "07:00";
              $hrs->sunformatstart = "AM";
              $hrs->suntimeend     = "09:00";
              $hrs->sunformatend   ="PM";
              $hrs->sunstatus      = 0;
              if(!$hrs->create()){
                $data['wrn'] = true;
                foreach ($hrs->getMessages() as $message) {
                    $data['wrnMsg'][] = $message->getMessage();
                }
              };

              $ctrphone              = new Centerphonenumber();
              $ctrphone->centerid    = $centerid;
              $ctrphone->phonenumber = $phonenumber;
              if(!$ctrphone->create()){
                $data['wrn'] = true;
                foreach ($ctrphone->getMessages() as $message) {
                    $data['wrnMsg'][] = $message->getMessage();
                }
              };

              $newemail           = new Centeremail();
              $newemail->centerid = $centerid;
              $newemail->email    = $centeremail;
              if(!$newemail->create()){
                $data['wrn'] = true;
                foreach ($newemail->getMessages() as $message) {
                    $data['wrnMsg'][] = $message->getMessage();
                }
              };
          }
      } else {
        $data['err'] = "API: NO POST DATA!";
      }
      echo json_encode($data);
  }

    public function listregionmanagerAction()
    {
       $regionmanager= Users::find(array('task = "Region Manager"'));
        foreach ($regionmanager as $regionmanager) {
            $data[] = array(
                'id'=>$regionmanager->id,
                'name'=>$regionmanager->first_name .' '. $regionmanager->last_name
                );
        }
        echo json_encode($data);
    }

    public function listregionAction() {
      echo json_encode(Centerregion::find("regionid != 'this is dummy' ORDER BY regionname ")->toArray());
    }

    public function listcentermanagerAction()
    {
       $centermanager= Users::find(array('task = "Center Manager"'));
        foreach ($centermanager as $centermanager) {
            $data[] = array(
                'id'=>$centermanager->id,
                'name'=>$centermanager->first_name .' '. $centermanager->last_name
                );
        }
        echo json_encode($data);
    }

    public function liststateAction()
    {
       $getstate= States::find(array("order" => "state_code asc"));
        foreach ($getstate as $getstate) {
            $data[] = array(
                'state_code'=>$getstate->state_code,
                'state'=>$getstate->state
                );
        }
        echo json_encode($data);
    }

    public function FEloadstateAction() {
        $getstate = States::find(array("ORDER" => "state_code"));
        echo json_encode($getstate->toArray());
    }

    public function listcityAction($statecode)
    {
      echo json_encode(CB::bnbQUery("SELECT * FROM cities WHERE state_code='".$statecode."' ORDER BY city"));
      //  $getcity= Cities::find("state_code='" . $statecode . "' ORDER BY city ");
      //   foreach ($getcity as $gcity) {
      //       $data[] = array(
      //           'state_code'=>$gcity->state_code,
      //           'city'=>$gcity->city
      //           );
      //   }
      //   echo json_encode($data);
    }

    public function getzipAction($cityname)
    {
      echo json_encode(CB::bnbQUery("SELECT * FROM cities_extended WHERE city='".$cityname."' ORDER BY zip"));
      //  $getcity= Cities_extended::find(array('city="' . $cityname . '" ORDER BY city'));
      //   foreach ($getcity as $getcity) {
      //       $data[] = array(
      //           'zip'=>$getcity->zip,
      //           'city'=>$getcity->city
      //           );
      //   }
      //   echo json_encode($data);
    }

     public function manageCenterAction($num, $page, $keyword, $userid) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $getuser = $db->prepare("SELECT * FROM users  WHERE users.id = '".$userid."'");
        $getuser->execute();
        $userresult = $getuser->fetch(\PDO::FETCH_ASSOC);

        if($userresult['task'] =='Administrator'){

            if ($keyword == 'null' || $keyword == 'undefined') {
               $offsetfinal = ($page * 10) - 10;

               $db = \Phalcon\DI::getDefault()->get('db');
               $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerdistrict ON center.centerdistrict = centerdistrict.districtid LEFT JOIN centerregion ON center.centerregion = centerregion.regionid ORDER BY centerstate, centertitle DESC  LIMIT " . $offsetfinal . ",10");

               $stmt->execute();
               $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


               $db1 = \Phalcon\DI::getDefault()->get('db');
               $stmt1 = $db1->prepare("SELECT * FROM center LEFT JOIN centerdistrict ON center.centerdistrict = centerdistrict.districtid LEFT JOIN centerregion ON center.centerregion = centerregion.regionid ORDER BY centerstate, centertitle DESC");

               $stmt1->execute();
               $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

               $totalreportdirty = count($searchresult1);
            }
            else {

                 $offsetfinal = ($page * 10) - 10;

                 $db = \Phalcon\DI::getDefault()->get('db');
                 $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerdistrict ON center.centerdistrict = centerdistrict.districtid LEFT JOIN centerregion ON center.centerregion = centerregion.regionid  WHERE centertitle LIKE '%" . $keyword . "%' or centerregion LIKE '%" . $keyword . "%' or centerdistrict LIKE '%" . $keyword . "%' or centerstate LIKE '%" . $keyword . "%' or centercity LIKE '%" . $keyword . "%' or centerzip LIKE '%" . $keyword . "%' or centertype LIKE '%" . $keyword . "%' ORDER BY centerstate, centertitle DESC LIMIT " . $offsetfinal . ",10");

                 $stmt->execute();
                 $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


                 $db1 = \Phalcon\DI::getDefault()->get('db');
                 $stmt1 = $db1->prepare("SELECT * FROM center LEFT JOIN centerdistrict ON center.centerdistrict = centerdistrict.districtid LEFT JOIN centerregion ON center.centerregion = centerregion.regionid WHERE centertitle LIKE '%" . $keyword . "%' or centerregion LIKE '%" . $keyword . "%' or centerdistrict LIKE '%" . $keyword . "%' or centerstate LIKE '%" . $keyword . "%' or centercity LIKE '%" . $keyword . "%' or centerzip LIKE '%" . $keyword . "%' or centertype LIKE '%" . $keyword . "%' ORDER BY centerstate, centertitle DESC LIMIT " . $offsetfinal . ",10");

                 $stmt1->execute();
                 $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

                 $totalreportdirty = count($searchresult1);

            }
        }
        elseif($userresult['task'] =='Region Manager'){

            $db = \Phalcon\DI::getDefault()->get('db');
            $getdistrictid = $db->prepare("SELECT users.*,centerregion.* FROM users LEFT JOIN centerregion ON users.id = centerregion.regionmanager WHERE users.id = '".$userid."'");

            $getdistrictid->execute();
            $idresult = $getdistrictid->fetch(\PDO::FETCH_ASSOC);
            // var_dump($idresult);
            if ($keyword == 'null' || $keyword == 'undefined') {
               $offsetfinal = ($page * 10) - 10;

               $db = \Phalcon\DI::getDefault()->get('db');
               $stmt = $db->prepare("SELECT * FROM center  LEFT JOIN centerregion ON center.centerregion = centerregion.regionid WHERE centerregion.regionid ='".$idresult['regionid']."' ORDER BY centerstate, centertitle DESC  LIMIT " . $offsetfinal . ",10");

               $stmt->execute();
               $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

               $db1 = \Phalcon\DI::getDefault()->get('db');
               $stmt1 = $db1->prepare("SELECT * FROM center  LEFT JOIN centerregion ON center.centerregion = centerregion.regionid WHERE centerregion.regionid ='".$idresult['regionid']."' ORDER BY centerstate, centertitle DESC");
               $stmt1->execute();
               $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

               $totalreportdirty = count($searchresult1);
            }
            else {

                 $offsetfinal = ($page * 10) - 10;

                 $db = \Phalcon\DI::getDefault()->get('db');
                 $stmt = $db->prepare("SELECT * FROM center  LEFT JOIN centerregion ON center.centerregion = centerregion.regionid WHERE centerregion.regionid ='".$idresult['regionid']."' AND centerregion.regionname LIKE '%" . $keyword . "%' or centerregion.regionid ='".$idresult['regionid']."' AND  center.centertitle LIKE '%" . $keyword . "%' or centerregion.regionid ='".$idresult['regionid']."' AND center.centerstate LIKE '%" . $keyword . "%' or centerregion.regionid ='".$idresult['regionid']."' AND center.centercity LIKE '%" . $keyword . "%' or centerregion.regionid ='".$idresult['regionid']."' AND center.centerzip LIKE '%" . $keyword . "%' or centerregion.regionid ='".$idresult['regionid']."' AND center.centertype LIKE '%" . $keyword . "%' ORDER BY center.centerstate, center.centertitle DESC LIMIT " . $offsetfinal . ",10");

                 $stmt->execute();
                 $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


                 $db1 = \Phalcon\DI::getDefault()->get('db');
                 $stmt1 = $db1->prepare("SELECT * FROM center  LEFT JOIN centerregion ON center.centerregion = centerregion.regionid WHERE centerregion.regionid ='".$idresult['regionid']."' AND centerregion.regionname LIKE '%" . $keyword . "%' or centerregion.regionid ='".$idresult['regionid']."' AND  center.centertitle LIKE '%" . $keyword . "%' or centerregion.regionid ='".$idresult['regionid']."' AND center.centerstate LIKE '%" . $keyword . "%' or centerregion.regionid ='".$idresult['regionid']."' AND center.centercity LIKE '%" . $keyword . "%' or centerregion.regionid ='".$idresult['regionid']."' AND center.centerzip LIKE '%" . $keyword . "%' or centerregion.regionid ='".$idresult['regionid']."' AND center.centertype LIKE '%" . $keyword . "%' ORDER BY center.centerstate, center.centertitle DESC LIMIT " . $offsetfinal . ",10");

                 $stmt1->execute();
                 $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

                 $totalreportdirty = count($searchresult1);

            }

        }

        elseif($userresult['task'] =='District Manager'){


            $db = \Phalcon\DI::getDefault()->get('db');
            $getdistrictid = $db->prepare("SELECT users.*,centerregion.*,centerdistrict.* FROM users LEFT JOIN centerregion ON users.id = centerregion.regionmanager LEFT JOIN centerdistrict ON users.id = centerdistrict.districtmanager WHERE users.id = '".$userid."'");

            $getdistrictid->execute();
            $idresult = $getdistrictid->fetch(\PDO::FETCH_ASSOC);
            // var_dump($idresult);
            if ($keyword == 'null' || $keyword == 'undefined') {
               $offsetfinal = ($page * 10) - 10;

               $db = \Phalcon\DI::getDefault()->get('db');
               $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerdistrict ON center.centerdistrict = centerdistrict.districtid LEFT JOIN centerregion ON centerdistrict.regionid = centerregion.regionid WHERE  center.centerdistrict ='".$idresult['districtid']."' ORDER BY centerstate, centertitle DESC  LIMIT " . $offsetfinal . ",10");

               $stmt->execute();
               $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


               $db1 = \Phalcon\DI::getDefault()->get('db');
               $stmt1 = $db1->prepare("SELECT * FROM center LEFT JOIN centerdistrict ON center.centerdistrict = centerdistrict.districtid LEFT JOIN centerregion ON centerdistrict.regionid = centerregion.regionid WHERE center.centerdistrict ='".$idresult['districtid']."' ORDER BY centerstate, centertitle DESC");

               $stmt1->execute();
               $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

               $totalreportdirty = count($searchresult1);
            }
            else {

                 $offsetfinal = ($page * 10) - 10;

                 $db = \Phalcon\DI::getDefault()->get('db');
                 $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerdistrict ON center.centerdistrict = centerdistrict.districtid LEFT JOIN centerregion ON centerdistrict.regionid = centerregion.regionid WHERE  center.centerdistrict ='".$idresult['districtid']."' AND centerregion.regionname LIKE '%" . $keyword . "%' or center.centerdistrict ='".$idresult['districtid']."' AND centerdistrict.districtname LIKE '%" . $keyword . "%'  or center.centerdistrict ='".$idresult['districtid']."' AND  center.centertitle LIKE '%" . $keyword . "%' or center.centerdistrict ='".$idresult['districtid']."' AND center.centerstate LIKE '%" . $keyword . "%' or center.centerdistrict ='".$idresult['districtid']."' AND center.centercity LIKE '%" . $keyword . "%' or center.centerdistrict ='".$idresult['districtid']."' AND center.centerzip LIKE '%" . $keyword . "%' or center.centerdistrict ='".$idresult['districtid']."' AND center.centertype LIKE '%" . $keyword . "%' ORDER BY center.centerstate, center.centertitle DESC LIMIT " . $offsetfinal . ",10");

                 $stmt->execute();
                 $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


                 $db1 = \Phalcon\DI::getDefault()->get('db');
                 $stmt1 = $db1->prepare("SELECT * FROM center LEFT JOIN centerdistrict ON center.centerdistrict = centerdistrict.districtid LEFT JOIN centerregion ON centerdistrict.regionid = centerregion.regionid WHERE  center.centerdistrict ='".$idresult['districtid']."' AND centerregion.regionname LIKE '%" . $keyword . "%' or center.centerdistrict ='".$idresult['districtid']."' AND centerdistrict.districtname LIKE '%" . $keyword . "%'  or center.centerdistrict ='".$idresult['districtid']."' AND  center.centertitle LIKE '%" . $keyword . "%' or center.centerdistrict ='".$idresult['districtid']."' AND center.centerstate LIKE '%" . $keyword . "%' or center.centerdistrict ='".$idresult['districtid']."' AND center.centercity LIKE '%" . $keyword . "%' or center.centerdistrict ='".$idresult['districtid']."' AND center.centerzip LIKE '%" . $keyword . "%' or center.centerdistrict ='".$idresult['districtid']."' AND center.centertype LIKE '%" . $keyword . "%' ORDER BY center.centerstate, center.centertitle DESC LIMIT " . $offsetfinal . ",10");

                 $stmt1->execute();
                 $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

                 $totalreportdirty = count($searchresult1);

            }

        }

        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty));
    }

    public function dropdownmanageCenterAction($num, $page, $keyword, $userid) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $getuser = $db->prepare("SELECT * FROM users  WHERE users.id = '".$userid."'");
        $getuser->execute();
        $userresult = $getuser->fetch(\PDO::FETCH_ASSOC);

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM center LEFT JOIN centerdistrict ON center.centerdistrict = centerdistrict.districtid LEFT JOIN centerregion ON center.centerregion = centerregion.regionid ORDER BY centertitle ASC");
           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        echo json_encode(array('data' => $searchresult, 'index' =>$page));
    }

    public function centerUpdatestatusAction($status,$centerid,$keyword) {

        $data = array();
        $center = Center::findFirst('centerid="' . $centerid . '"');
        $center->status = $status;
            if (!$center->save()) {
                $center['error'] = "Something went wrong saving page status, please try again.";
            } else {
                $data['success'] = "Success";
                if($status == 1){
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Update",
                        "title" => "Update Center Status : ". $center->centertitle ." to Active"
                    ));
                }
                else{
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Update",
                        "title" => "Update Center Status : ". $center->centertitle ." to Deactivated"
                    ));
                }

            }

            echo json_encode($data);
    }

    public function centerdeleteAction($centerid) {
        $center = Center::findFirst("centerid = '".$centerid."'");
        $centertitle = $center->centertitle;
        if ($center == true) {
            if ($center->delete()) {
                $data['scs'] = true;
                // $data = array('success' => 'Center Deleted');
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Delete",
                    "title" => "Delete Center: ". $centertitle .""
                ));

                $rm_ctrloc = Centerlocation::findFirst("centerid = '".$centerid."'");
                $rm_ctrloc->delete();

                $rm_ctrregfee = Centerpricingregfee::findFirst("centerid = '".$centerid."'");
                $rm_ctrregfee->delete();

                $rm_ctrregcls = Centerpricingregclass::findFirst("centerid = '".$centerid."'");
                $rm_ctrregcls->delete();

                $rm_ctrsesfee = Centerpricingsessionfee::findFirst("centerid = '".$centerid."'");
                $rm_ctrsesfee->delete();

                $rm_ctrhrs = Centerhours::findFirst("centerid = '".$centerid."'");
                $rm_ctrhrs->delete();

                $rm_ctrphn = Centerphonenumber::findFirst("centerid = '".$centerid."'");
                $rm_ctrphn->delete();

                $rm_ctreml = Centeremail::findFirst("centerid = '".$centerid."'");
                $rm_ctreml->delete();

            } else {
                $data['err'] = "Something went wrong, please try to hit the delete button again.";
            }
        } else {
          $data['err'] = "Can't find center, please try again.";
        }
        echo json_encode($data);
    }

    public function loadCenterAction($centerid) {
        $loadcenter = Center::findfirst(" centerid= '$centerid' ");

        $centerdistrict = Centerdistrict::findFirst("districtid = '$loadcenter->centerdistrict' ");

        echo json_encode(array(
            "center" => $loadcenter,
            "centerdistrict" => $centerdistrict
        ));

    }

    public function saveeditedAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $centerslugs = $request->getPost('centerslugs')[0]["slug"];
            $centerslugs2 = $request->getPost('centerslugs')[1]["slug"];
            $centerslugs3 = $request->getPost('centerslugs')[2]["slug"];

            $centertitle = $request->getPost('centertitle');
            $centerdesc = $request->getPost('centerdesc');
            $centeraddress = $request->getPost('centeraddress');
            $centerregion = $request->getPost('centerregion');
            $centerstate = $request->getPost('centerstate');
            $centercity = $request->getPost('centercity');
            $centerzip = $request->getPost('centerzip');
            $metatitle = $request->getPost('metatitle');
            $metadesc = $request->getPost('metadesc');
            $centerdistrict = $request->getPost('centerdistrict');
            // $regionmanager = $request->getPost('regionmanager');
            // $districtmanager = $request->getPost('districtmanager');
            // $centermanager = $request->getPost('centermanager');
            $centertype = $request->getPost('centertype');
            $status = $request->getPost('status');
            $paypalid = $request->getPost('paypalid');
            $authorizekey = $request->getPost('authorizekey');
            $authorizeid = $request->getPost('authorizeid');

            $origsessionprice = $request->getPost('origsessionprice');
            $dissessionprice = $request->getPost('dissessionprice');
            $origgroupprice = $request->getPost('origgroupprice');
            $disgroupprice = $request->getPost('disgroupprice');
            $fbcapc = $request->getPost('fbcapc');

            $orig1monthprice = $request->getPost('orig1monthprice');
            $dis1monthprice = $request->getPost('dis1monthprice');
            $orig3monthprice = $request->getPost('orig3monthprice');
            $dis3monthprice = $request->getPost('dis3monthprice');

            // $district = Centerdistrict::findFirst("districtid = '$centerdistrict' ");

            $openingdate = $request->getPost('openingdate');
            $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
            $dates = explode(" ", $openingdate);
            $openingdate = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

            $savecenter = Center::findFirst('centerid="' . $centerid . '"');
            if($savecenter) {
                $savecenter->centertitle         = $centertitle;
                $savecenter->centerslugs         = $centerslugs;
                $savecenter->centerslugs2        = $centerslugs2;
                $savecenter->centerslugs3        = $centerslugs3;
                $savecenter->centerdesc          = $centerdesc;
                $savecenter->centeraddress       = $centeraddress;
                $savecenter->centerstate         = $centerstate;
                $savecenter->centercity          = $centercity;
                $savecenter->centerzip           = $centerzip;
                $savecenter->metatitle           = $metatitle;
                $savecenter->metadesc            = $metadesc;
                // $savecenter->centerdistrict   = $centerdistrict;
                $savecenter->openingdate         = $openingdate;
                $savecenter->centerregion        = $centerregion;
                $savecenter->centertype          = $centertype;
                $savecenter->status              = $status;
                $savecenter->fbcapc              = $fbcapc;
                $savecenter->authorizeid         = $authorizeid;
                $savecenter->authorizekey        = $authorizekey;
                $savecenter->paypalid            = $paypalid;
                // $savecenter->origsessionprice = $origsessionprice;
                // $savecenter->dissessionprice  = $dissessionprice;
                // $savecenter->origgroupprice   = $origgroupprice;
                // $savecenter->disgroupprice    = $disgroupprice;
                $savecenter->orig1monthprice     = $orig1monthprice;
                $savecenter->dis1monthprice      = $dis1monthprice;
                $savecenter->orig3monthprice     = $orig3monthprice;
                $savecenter->dis3monthprice      = $dis3monthprice;
                $savecenter->dateedited          = date("Y-m-d");

                if (!$savecenter->save()) {
                    $data['err'] = "API: ".$savecenter->getMessages()[0]->getMessage();
                } else {
                    $data['scs'] = "Center changes has been successfully updated!";

                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Update",
                        "title" => "Update Center: ". $centertitle .""
                    ));
                }
            }
        } else {
          $data['err'] = "API: NO POST DATA.";
        }

        echo json_encode($data);
    }

    public function saveEventAction(){

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $activitytitle = $request->getPost('calendartitle');
            $activitydate = $request->getPost('calendardate');
            $activitytimefrom = $request->getPost('timefrom');
            $activitytimeto = $request->getPost('timeto');
            $activitytimefromformat = $request->getPost('timefromformat');
            $activitytimetoformat = $request->getPost('timetoformat');
            $description = $request->getPost('description');
            $status = $request->getPost('status');

            $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
            $dates = explode(" ", $activitydate);
            $d = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];

            $center = Center::findFirst('centerid="'.$centerid.'"');


            $guid = new \Utilities\Guid\Guid();
            $activityid = $guid->GUID();

            $calendar = new Centercalendar();
            $calendar->assign(array(
                'activityid' => $activityid,
                'centerid' => $centerid,
                'activitytitle' => $activitytitle,
                'activitydate' => $d,
                'activitytimefrom' => $activitytimefrom,
                'activitytimefromformat' => $activitytimefromformat,
                'activitytimeto' => $activitytimeto,
                'activitytimetoformat' => $activitytimetoformat,
                'description' => $description,
                'status' => $status,
                'datecreated' => date('Y-m-d'),
                'dateedited' =>date('Y-m-d')
                ));


            if (!$calendar->save()) {
                $data['msg'] = "Something went wrong saving the data, please try again.";
                $data['type'] = 'danger';
            }
            else {

                $data['msg'] = "Activity successfully Added!";
                $data['type'] = 'success';

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Add",
                    "title" => "Add ".$activitytitle." Event to: ".  $center->centertitle ." center"
                ));

            }

        }

        echo json_encode($data);

    }

    public function manageEventAction($num, $page, $keyword,$centerid){

        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM centercalendar WHERE centerid = '" .$centerid. "' ORDER BY activitydate DESC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM centercalendar WHERE centerid = '" .$centerid. "' ORDER BY activitydate DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalreportdirty = count($searchresult1);
        } else {

         $offsetfinal = ($page * 10) - 10;

         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT * FROM centercalendar WHERE centerid = '" .$centerid. "' AND activitytitle LIKE '%" . $keyword . "%' ORDER BY activitydate DESC LIMIT " . $offsetfinal . ",10");

         $stmt->execute();
         $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


         $db1 = \Phalcon\DI::getDefault()->get('db');
         $stmt1 = $db1->prepare("SELECT * FROM centercalendar WHERE centerid = '" .$centerid. "' AND activitytitle LIKE '%" . $keyword . "%' ORDER BY activitydate DESC LIMIT " . $offsetfinal . ",10");

         $stmt1->execute();
         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

         $totalreportdirty = count($searchresult1);

        }


        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty));

    }

    public function eventUpdatestatusAction($status,$activityid,$keyword) {

        $data = array();
        $event = centercalendar::findFirst('activityid="' . $activityid . '"');
        $center = Center::findFirst('centerid="'.$event->centerid.'"');
        $event->status = $status;
            if (!$event->save()) {
                $data['error'] = "Something went wrong saving page status, please try again.";
            }
            else {
                $data['success'] = "Success";
                if($status == 1){
                    $eventstatus = 'Active';
                }
                else{
                    $eventstatus = 'Deactivated';
                }
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Add",
                    "title" => "Update ".$event->activitytitle." Event Status to ".$eventstatus." in ".$center->centertitle ." center"
                ));
            }

            echo json_encode($data);
    }

    public function eventdeleteAction($activityid) {
        $conditions = 'activityid="' . $activityid . '"';
        $event = centercalendar::findFirst(array($conditions));
        $center = Center::findFirst('centerid="'.$event->centerid.'"');
        $data = array('error' => 'Not Found');
        if ($event) {
            if ($event->delete()) {
                $data = array('success' => 'event Deleted');
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Delete",
                    "title" => "Delete ".$event->activitytitle." Event in ".$center->centertitle ." center"
                ));
            }
        }
        echo json_encode($data);
    }

    public function loadEventAction($activityid){
        $loadevent = centercalendar::findfirst(array('activityid="' . $activityid . '"'));
        $loadevent = json_encode($loadevent->toArray(), JSON_NUMERIC_CHECK);
        echo $loadevent;
    }

    public function eventupdateAction(){

         $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $activityid = $request->getPost('activityid');
            $activitytitle = $request->getPost('activitytitle');
            $activitydate = $request->getPost('activitydate');
            $activitytimefrom = $request->getPost('activitytimefrom');
            $activitytimefromformat = $request->getPost('activitytimefromformat');
            $activitytimeto = $request->getPost('activitytimeto');
            $activitytimetoformat = $request->getPost('activitytimetoformat');
            $description = $request->getPost('description');
            $status = $request->getPost('status');

            if(strlen($activitydate) > 10) {
                 $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                 $dates = explode(" ", $activitydate);
                 $activitydate = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
             }



            $saveactivity = centercalendar::findFirst('activityid="' . $activityid . '"');
            $center = Center::findFirst('centerid="'.$saveactivity->centerid.'"');
            $saveactivity->activitytitle = $activitytitle;
            $saveactivity->activitydate = $activitydate;
            $saveactivity->activitytimefrom = $activitytimefrom;
            $saveactivity->activitytimefromformat = $activitytimefromformat;
            $saveactivity->activitytimeto = $activitytimeto;
            $saveactivity->activitytimetoformat = $activitytimetoformat;
            $saveactivity->description = $description;
            $saveactivity->status = $status;
            $saveactivity->dateedited = date('Y-m-d');


            if (!$saveactivity->save()) {
                $data['msg'] = "Something went wrong saving the data, please try again.";
                $data['type'] = "danger";

            }
            else {

                $data['msg'] = "Activity successfully updated!";
                $data['type'] = "success";

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Update",
                    "title" => "Update ".$saveactivity->activitytitle." Event in ".$center->centertitle ." center"
                ));

            }

        }

        echo json_encode($data);

    }

    public function savelocationAction(){

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $lat = $request->getPost('lat');
            $lon = $request->getPost('lon');

            $conditions = 'centerid="' . $centerid . '"';
            $deletecenterlocation = centerlocation::findFirst(array($conditions));
            $data = array('error' => 'Not Found');
            if ($deletecenterlocation) {
                if ($deletecenterlocation->delete()) {
                    $data = array('success' => 'News Deleted');
                }
            }

            $center = Center::findFirst('centerid="'.$centerid.'"');
            $guid = new \Utilities\Guid\Guid();
            $locationid = $guid->GUID();

            $location = new centerlocation();
            $location->assign(array(
                'locationid' => $locationid,
                'centerid' => $centerid,
                'lat' => $lat,
                'lon' => $lon
                ));

            if (!$location->save()) {
                $errors = array();
                foreach ($location->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['msg'] = "Location successfully saved!";
                $data['type'] = "success";

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Update",
                    "title" => "Update Map Location in ".$center->centertitle ." center"
                ));

            }

        }

        echo json_encode($data);

    }

    public function loadLocationAction($centerid){

        $loadlocation = centerlocation::findfirst(array('centerid="' . $centerid . '"'));
        if($loadlocation){
            $loadlocations = json_encode($loadlocation);
        echo $loadlocations;
        }


    }

    public function saveopenhourAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $centerid = $request->getPost('centerid');

            $montimestart = $request->getPost('montimestart');
            $monformatstart = $request->getPost('monformatstart');
            $montimeend = $request->getPost('montimeend');
            $monformatend = $request->getPost('monformatend');

            $tuetimestart = $request->getPost('tuetimestart');
            $tueformatstart = $request->getPost('tueformatstart');
            $tuetimeend = $request->getPost('tuetimeend');
            $tueformatend = $request->getPost('tueformatend');

            $wedtimestart = $request->getPost('wedtimestart');
            $wedformatstart = $request->getPost('wedformatstart');
            $wedtimeend = $request->getPost('wedtimeend');
            $wedformatend = $request->getPost('wedformatend');

            $thutimestart = $request->getPost('thutimestart');
            $thuformatstart = $request->getPost('thuformatstart');
            $thutimeend = $request->getPost('thutimeend');
            $thuformatend = $request->getPost('thuformatend');

            $fritimestart = $request->getPost('fritimestart');
            $friformatstart = $request->getPost('friformatstart');
            $fritimeend = $request->getPost('fritimeend');
            $friformatend = $request->getPost('friformatend');

            $sattimestart = $request->getPost('sattimestart');
            $satformatstart = $request->getPost('satformatstart');
            $sattimeend = $request->getPost('sattimeend');
            $satformatend = $request->getPost('satformatend');
            $suntimestart = $request->getPost('suntimestart');
            $sunformatstart = $request->getPost('sunformatstart');
            $suntimeend = $request->getPost('suntimeend');
            $sunformatend = $request->getPost('sunformatend');
            $satstatus = $request->getPost('satstatus');
            $sunstatus = $request->getPost('sunstatus');

            $center = Center::findFirst('centerid="'.$centerid.'"');

            $findcenter = Centerhours::findFirst("centerid='".$centerid."'");
            if($findcenter){
                $findcenter->montimestart = $montimestart;
                $findcenter->monformatstart = $monformatstart;
                $findcenter->montimeend = $montimeend;
                $findcenter->monformatend = $monformatend;
                $findcenter->tuetimestart = $tuetimestart;
                $findcenter->tueformatstart = $tueformatstart;
                $findcenter->tuetimeend = $tuetimeend;
                $findcenter->tueformatend = $tueformatend;
                $findcenter->wedtimestart = $wedtimestart;
                $findcenter->wedformatstart = $wedformatstart;
                $findcenter->wedtimeend = $wedtimeend;
                $findcenter->wedformatend = $wedformatend;
                $findcenter->thutimestart = $thutimestart;
                $findcenter->thuformatstart = $thuformatstart;
                $findcenter->thutimeend = $thutimeend;
                $findcenter->thuformatend = $thuformatend;
                $findcenter->fritimestart = $fritimestart;
                $findcenter->friformatstart = $friformatstart;
                $findcenter->fritimeend = $fritimeend;
                $findcenter->friformatend = $friformatend;
                $findcenter->sattimestart = $sattimestart;
                $findcenter->satformatstart = $satformatstart;
                $findcenter->sattimeend = $sattimeend;
                $findcenter->satformatend = $satformatend;
                $findcenter->satstatus = $satstatus;
                $findcenter->suntimestart = $suntimestart;
                $findcenter->sunformatstart = $sunformatstart;
                $findcenter->suntimeend = $suntimeend;
                $findcenter->sunformatend = $sunformatend;
                $findcenter->sunstatus = $sunstatus;
                if (!$findcenter->save()) {
                     $data['msg'] = "Something went wrong please try again later!";
                    $data['type'] = "danger";
                } else {
                    $data['msg'] = "Opening ang Closing time successfully saved!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Update",
                        "title" => "Update Opening and Closing Time in ".$center->centertitle ." center"
                    ));

                }
            }
            else{

                $guid = new \Utilities\Guid\Guid();
                $openhourid = $guid->GUID();

                $hours = new Centerhours();
                $hours->assign(array(
                    'openhourid' => $openhourid,
                    'centerid' => $centerid,
                    'montimestart' => $montimestart,
                    'monformatstart' => $monformatstart,
                    'montimeend' => $montimeend,
                    'monformatend' => $monformatend,
                    'tuetimestart' => $tuetimestart,
                    'tueformatstart' => $tueformatstart,
                    'tuetimeend' => $tuetimeend,
                    'tueformatend' => $tueformatend,
                    'wedtimestart' => $wedtimestart,
                    'wedformatstart' => $wedformatstart,
                    'wedtimeend' => $wedtimeend,
                    'wedformatend' => $wedformatend,
                    'thutimestart' => $thutimestart,
                    'thuformatstart' => $thuformatstart,
                    'thutimeend' => $thutimeend,
                    'thuformatend' => $thuformatend,
                    'fritimestart' => $fritimestart,
                    'friformatstart' => $friformatstart,
                    'fritimeend' => $fritimeend,
                    'friformatend' => $friformatend,
                    'sattimestart' => $sattimestart,
                    'satformatstart' => $satformatstart,
                    'sattimeend' => $sattimeend,
                    'satformatend' => $satformatend,
                    'satstatus' => $satstatus,
                    'suntimestart' => $suntimestart,
                    'sunformatstart' => $sunformatstart,
                    'suntimeend' => $suntimeend,
                    'sunformatend' => $sunformatend,
                    'sunstatus' => $sunstatus
                    ));

                if (!$hours->save()) {
                    $data['msg'] = "Something went wrong please try again later!";
                    $data['type'] = "danger";
                } else {
                    $data['msg'] = "Opening ang Closing time successfully saved!";
                    $data['type'] = "success";

                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Update",
                        "title" => "Update Opening and Closing Time in ".$center->centertitle ." center"
                    ));


                }
            }

        }

        echo json_encode($data);
    }

    public function loadopenhourAction($centerid){

        $loadtime = Centerhours::findfirst(array('centerid="' . $centerid . '"'));
        if ($loadtime) {
            // $data = array(
            //     'weekdaystimestart' => $loadtime->weekdaystimestart,
            //     'weekdaysformatstart' => $loadtime->weekdaysformatstart,
            //     'weekdaystimeend' => $loadtime->weekdaystimeend,
            //     'weekdaysformatend' => $loadtime->weekdaysformatend,
            //     'sattimestart' => $loadtime->sattimestart,
            //     'satformatstart' => $loadtime->satformatstart,
            //     'sattimeend' => $loadtime->sattimeend,
            //     'satformatend' => $loadtime->satformatend,
            //     'satstatus' => $loadtime->satstatus,
            //     'suntimestart' => $loadtime->suntimestart,
            //     'sunformatstart' => $loadtime->sunformatstart,
            //     'suntimeend' => $loadtime->suntimeend,
            //     'sunformatend' => $loadtime->sunformatend,
            //     'sunstatus' => $loadtime->sunstatus
            //     );
             echo json_encode(array("hours" => $loadtime));
        }

    }

    public function listOfCentersAction() {
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM center");
        $stmt->execute();
        $centers = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($centers);
    }

    public function centerListViaStateAction($state_code) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db->prepare("SELECT * FROM center where centerstate = '$state_code' AND status = 1 ORDER BY centertitle ");
        $stmt->execute();
        $centers = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        echo json_encode($centers);
    }

    public function savesociallinkAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $title = $request->getPost('title');
            $linkurl = $request->getPost('linkurl');
            $imgfilename = $request->getPost('imgfilename');
            $status = $request->getPost('status');

            $center = Center::findFirst('centerid="'.$centerid.'"');

            $guid = new \Utilities\Guid\Guid();
            $linkid = $guid->GUID();

            $sociallinks = new Centersociallinks();
            $sociallinks->assign(array(
                'linkid' => $linkid,
                'centerid' => $centerid,
                'title' => $title,
                'linkurl' => $linkurl,
                'image' => $imgfilename,
                'status' => $status
                ));

            if (!$sociallinks->save()) {
                $data['msg'] = "Something went wrong please try again later!";
                $data['type'] = "danger";
            } else {
                $data['msg'] = "Social Link successfully saved!";
                $data['type'] = "success";

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Add",
                    "title" => "Add Social link ".$title." in ".$center->centertitle ." center"
                ));

            }

        }

        echo json_encode($data);

    }


    public function loadsocialAction($centerid){
        $sociallinks = Centersociallinks::find(array('centerid="' . $centerid . '"'));
        foreach ($sociallinks as $sociallink) {
            if($sociallink->title == 'facebook') {
                $data['facebook'] = array(
                    'linkid' => $sociallink->linkid,
                    'title' => $sociallink->title,
                    'linkurl' => $sociallink->linkurl,
                    'image' => $sociallink->image,
                    'status' => $sociallink->status
                );
            } elseif ($sociallink->title == 'google') {
                $data['google'] = array(
                    'linkid' => $sociallink->linkid,
                    'title' => $sociallink->title,
                    'linkurl' => $sociallink->linkurl,
                    'image' => $sociallink->image,
                    'status' => $sociallink->status
                );
            } elseif ($sociallink->title == 'twitter') {
                $data['twitter'] = array(
                    'linkid' => $sociallink->linkid,
                    'title' => $sociallink->title,
                    'linkurl' => $sociallink->linkurl,
                    'image' => $sociallink->image,
                    'status' => $sociallink->status
                );
            } elseif ($sociallink->title == 'yelp') {
                $data['yelp'] = array(
                    'linkid' => $sociallink->linkid,
                    'title' => $sociallink->title,
                    'linkurl' => $sociallink->linkurl,
                    'image' => $sociallink->image,
                    'status' => $sociallink->status
                );
            }
        }
        echo json_encode($data);
    }

    public function deletesociallinkAction($linkid){
            $conditions = 'linkid="' . $linkid . '"';
            $deletecenterlinks = Centersociallinks::findFirst(array($conditions));
            $center = Center::findFirst('centerid="'.$deletecenterlinks->centerid.'"');
            $data = array('error' => 'Not Found');
            if ($deletecenterlinks) {
                if ($deletecenterlinks->delete()) {
                    $data = array('success' => 'Social link Deleted');
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Delete",
                        "title" => "Delete Social link ".$deletecenterlinks->title." in ".$center->centertitle ." center"
                    ));
                }
            }
        echo json_encode($data);
    }

    public function socialUpdatestatusAction($status,$linkid,$keyword) {

        $data = array();
        $socialstatus = Centersociallinks::findFirst('linkid="' . $linkid . '"');
        $center = Center::findFirst('centerid="'.$socialstatus->centerid.'"');
        $social = Centersociallinks::findFirst('linkid="' . $linkid . '"');
        $socialstatus->status = $status;
        if (!$socialstatus->save()) {
            $center['error'] = "Something went wrong saving page status, please try again.";
        } else {
            $data['success'] = "Success";
            if($status == 1){
                $socialstatus = 'show';
            }
            else{
                $socialstatus = 'hidden';
            }
            $audit = new CB();
            $audit->auditlog(array(
                "module" =>"Center",
                "event" => "Update",
                "title" => "Update Social link ".$social->title." status to ".$socialstatus." in ".$center->centertitle ." center"
                ));
        }

        echo json_encode($data);
    }

    public function centerviewloadcenterAction($centerid){
            $loadcenterview = Center::findfirst(array('centerid="' . $centerid . '"'));
            echo json_encode($loadcenterview);
    }

    public function loadlinksbyidAction($linkid){

        $loadlinkbyid = Centersociallinks::findfirst(array('linkid="' . $linkid . '"'));
        echo json_encode($loadlinkbyid);

    }

    public function updatelinksbyidAction(){

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $linkid = $request->getPost('linkid');
            $title = $request->getPost('title');
            $linkurl = $request->getPost('linkurl');
            $imgfilename = $request->getPost('imgfilename');
            $status = $request->getPost('status');

            $center = Center::findFirst('centerid="'.$centerid.'"');

            $sociallinks = Centersociallinks::findfirst(array('linkid="' . $linkid . '"'));
            $sociallinks->title = $title;
            $sociallinks->linkurl = $linkurl;
            $sociallinks->image = $imgfilename;
            $sociallinks->activitytimeto = $activitytimeto;
            $sociallinks->status = $status;


            if (!$sociallinks->save()) {
                $data['msg'] = "Something went wrong please try again later!";
                $data['type'] = "danger";
            } else {
                $data['msg'] = "Social Link successfully updated!";
                $data['type'] = "success";

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Update",
                    "title" => "Update Social link ".$title." in ".$center->centertitle ." center"
                    ));

            }

        }

        echo json_encode($data);

    }

    public function saveofferAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $image = $request->getPost('image');
            $status = $request->getPost('status');
            $title = $request->getPost('title');
            $description = $request->getPost('description');
            $comments = $request->getPost('comments');
            $expimonth = $request->getPost('expimonth');
            $expiday = $request->getPost('expiday');
            $expiyear = $request->getPost('expiyear');
            $expiration = $expiyear."-".$expimonth."-".$expiday;

            $center = Center::findFirst('centerid="'.$centerid.'"');

            $guid = new \Utilities\Guid\Guid();
            $offerid = $guid->GUID();

            $saveoffer = new Centeroffer();
            $saveoffer->assign(array(
                'offerid' => $offerid,
                'centerid' => $centerid,
                'image' => $image,
                'title' => $title,
                'description' => $description,
                'comments' => $comments,
                'expiration' => $expiration,
                'date_created' => date("Y-m-d"),
                'date_updated' => date("Y-m-d H:i:s"),
                'status' => $status
                ));

            if (!$saveoffer->save()) {
                $data['msg'] = "Something went wrong please try again later!";
                $data['type'] = "danger";
            } else {
                $data['msg'] = "Offer successfully saved!";
                $data['type'] = "success";

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Add",
                    "title" => "Add Offer ".$title." in ".$center->centertitle ." center"
                    ));

            }

        }
        echo json_encode($data);
    }

    public function updateofferAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $offerid = $request->getPost('offerid');
            $status = $request->getPost('status');
            $title = $request->getPost('title');
            $description = $request->getPost('description');
            $comments = $request->getPost('comments');
            $expimonth = $request->getPost('expimonth');
            $expiday = $request->getPost('expiday');
            $expiyear = $request->getPost('expiyear');
            $expiration = $expiyear."-".$expimonth."-".$expiday;

            $updateoffer = Centeroffer::findFirst("offerid = '$offerid' ");
            $center = Center::findFirst('centerid="'.$updateoffer->centerid.'"');
            $updateoffer->assign(array(
                'title' => $title,
                'description' => $description,
                'comments' => $comments,
                'expiration' => $expiration,
                'date_updated' => date("Y-m-d H:i:s"),
                'status' => $status
                ));

            if (!$updateoffer->save()) {
                $data['msg'] = "Something went wrong please try again later!";
                $data['type'] = "danger";
            } else {
                $data['msg'] = "Offer successfully updated!";
                $data['type'] = "success";
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Update",
                    "title" => "Update Offer ".$title." in ".$center->centertitle ." center"
                    ));
            }
        }
        echo json_encode(array("message" => $data, "offerid" => $offerid));
    }
    public function updateofferWithUploadAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $offerid = $request->getPost('offerid');
            $image = $request->getPost('image');
            $status = $request->getPost('status');
            $title = $request->getPost('title');
            $description = $request->getPost('description');
            $comments = $request->getPost('comments');
            $expimonth = $request->getPost('expimonth');
            $expiday = $request->getPost('expiday');
            $expiyear = $request->getPost('expiyear');
            $expiration = $expiyear."-".$expimonth."-".$expiday;

            $updateoffer = Centeroffer::findFirst("offerid = '$offerid' ");
            $center = Center::findFirst('centerid="'.$updateoffer->centerid.'"');
            $updateoffer->assign(array(
                'image' => $image,
                'title' => $title,
                'description' => $description,
                'comments' => $comments,
                'expiration' => $expiration,
                'date_updated' => date("Y-m-d H:i:s"),
                'status' => $status
                ));

            if (!$updateoffer->save()) {
                $data['msg'] = "Something went wrong please try again later!";
                $data['type'] = "danger";
            } else {
                $data['msg'] = "Offer successfully updated!";
                $data['type'] = "success";
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Update",
                    "title" => "Update Offer ".$title." in ".$center->centertitle ." center"
                    ));
            }
        }
        echo json_encode(array("message" => $data, "offerid" => $offerid));
    }

    public function loadofferAction($centerid){
        $loadoffer = Centeroffer::find('centerid="' . $centerid . '"');
        $loadoffers = json_encode($loadoffer->toArray(), JSON_NUMERIC_CHECK);
        echo  $loadoffers;
    }

    public function deleteofferAction($offerid){

        $conditions = 'offerid="' . $offerid . '"';
            $deleteoffer = Centeroffer::findFirst(array($conditions));
            $center = Center::findFirst('centerid="'.$deleteoffer->centerid.'"');
            $data = array('error' => 'Not Found');
            if ($deleteoffer) {
                if ($deleteoffer->delete()) {
                    $data = array('success' => 'Offer Deleted');
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Delete",
                        "title" => "Delete Offer ".$deleteoffer->title." in ".$center->centertitle ." center"
                    ));
                }
            }
        echo json_encode($data);
    }

    public function editofferAction($offerid) {
        $thisOffer = Centeroffer::findFirst(" offerid = '$offerid' ");

        echo json_encode(array("offerprop" => $thisOffer));
    }

    public function offerUpdatestatusAction($status,$offerid) {

        $data = array();
        $offer = Centeroffer::findFirst('offerid="' . $offerid . '"');
        $center = Center::findFirst('centerid="'.$offer->centerid.'"');
        $offer->status = $status;
            if (!$offer->save()) {
                $data['error'] = "Something went wrong saving offer status, please try again.";
            } else {
                $data['success'] = "Success";
                if($status == 1){
                    $offerstatus = 'show';
                }
                else{
                    $offerstatus = 'hidden';
                }
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Update",
                    "title" => "Update Offer ".$offer->title." status to ".$offerstatus." in ".$center->centertitle ." center"
                ));
            }

            echo json_encode($data);
    }


    public function loadfeeAction($centerid){

         $regfee = Centerpricingregfee::findFirst('centerid="' . $centerid . '"');
         $regclass = Centerpricingregclass::findFirst('centerid="' . $centerid . '"');
         $sessionfee = Centerpricingsessionfee::findFirst('centerid="' . $centerid . '"');
         if(regfee && regclass && sessionfee){
            echo json_encode(array('regfee' => $regfee, 'regclass' =>$regclass, 'sessionfee' => $sessionfee));
         }
         else{

         }

    }

    public function savefeeAction(){

        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $regfee = $request->getPost('regfee');
            $regfeestatus = $request->getPost('regfeestatus');
            $regclass = $request->getPost('regclass');
            $regclassstatus = $request->getPost('regclassstatus');
            $sessionfee = $request->getPost('sessionfee');
            $sessionfeestatus = $request->getPost('sessionfeestatus');

            $conditions = 'centerid="' . $centerid . '"';
            $deleteCenterpricingregfee = Centerpricingregfee::findFirst(array($conditions));
            $data = array('error' => 'Not Found');
            if ($deleteCenterpricingregfee) {
                if ($deleteCenterpricingregfee->delete()) {

                }
            }

            $conditions = 'centerid="' . $centerid . '"';
            $deleteCenterpricingregclass = Centerpricingregclass::findFirst(array($conditions));
            $data = array('error' => 'Not Found');
            if ($deleteCenterpricingregclass) {
                if ($deleteCenterpricingregclass->delete()) {

                }
            }

            $conditions = 'centerid="' . $centerid . '"';
            $deleteCenterpricingsessionfee = Centerpricingsessionfee::findFirst(array($conditions));
            $data = array('error' => 'Not Found');
            if ($deleteCenterpricingsessionfee) {
                if ($deleteCenterpricingsessionfee->delete()) {

                }
            }


            $regfeesave = new Centerpricingregfee();
            $regfeesave->centerid = $centerid;
            $regfeesave->fee = $regfee;
            $regfeesave->status = $regfeestatus;
            if (!$regfeesave->save()) {
               $data['msg'] = "Something went wrong, please try again.";
                $data['type'] = "danger";
            }
            else {
                $data['msg'] = "Price successfully Updated!";
                $data['type'] = "success";
            }

            $regclasssave = new Centerpricingregclass();
            $regclasssave->centerid = $centerid;
            $regclasssave->fee = $regclass;
            $regclasssave->status = $regclassstatus;
            if (!$regclasssave->save()) {
               $data['msg'] = "Something went wrong, please try again.";
                $data['type'] = "danger";
            }
            else {
                $data['msg'] = "Price successfully Updated!";
                $data['type'] = "success";
            }

            $sessionfeesave = new Centerpricingsessionfee();
            $sessionfeesave->centerid = $centerid;
            $sessionfeesave->fee = $sessionfee;
            $sessionfeesave->status = $sessionfeestatus;
            if (!$sessionfeesave->save()) {
                $data['msg'] = "Something went wrong, please try again.";
                $data['type'] = "danger";
            }
            else {
                $data['msg'] = "Price successfully Updated!";
                $data['type'] = "success";
            }


        }

        echo json_encode($data);
    }

    public function statusregfeeAction($centerid,$status) {

        $data = array();
        $setstatus = Centerpricingregfee::findFirst('centerid="' . $centerid . '"');
        $setstatus->status = $status;
            if (!$setstatus->save()) {
                $data['error'] = "Something went wrong, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
    }

    public function statusregclassAction($centerid,$status) {

        $data = array();
        $setstatus = Centerpricingregclass::findFirst('centerid="' . $centerid . '"');
        $setstatus->status = $status;
            if (!$setstatus->save()) {
                $data['error'] = "Something went wrong, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
    }

    public function statussessionfeeAction($centerid,$status) {

        $data = array();
        $setstatus = Centerpricingsessionfee::findFirst('centerid="' . $centerid . '"');
        $setstatus->status = $status;
            if (!$setstatus->save()) {
                $data['error'] = "Something went wrong, please try again.";
            } else {
                $data['success'] = "Success";
            }

            echo json_encode($data);
    }

    public function savemembershipAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $period = $request->getPost('period');
            $frequent = $request->getPost('frequent');
            $price = $request->getPost('price');
            $description = $request->getPost('description');

            $center = Center::findFirst('centerid="'.$centerid.'"');

            $guid = new \Utilities\Guid\Guid();
            $membershipid = $guid->GUID();

            $membershipsave = new Centermembership();
            $membershipsave->assign(array(
                'membershipid' => $membershipid,
                'centerid' => $centerid,
                'period' => $period,
                'frequent' => $frequent,
                'price' => $price,
                'description' => $description
                ));
            if (!$membershipsave->save()) {
                $errors = array();
                foreach ($membershipsave->getMessages() as $message) {
                    $errors[] = $message->getMessage();
                }
                echo json_encode(array('error' => $errors));
            } else {
                $data['msg'] = "Membership successfully saved!";
                $data['type'] = "success";
                if($period == 1){
                    $periodtxt = '1 Month';
                }
                else if($period == 3){
                    $periodtxt = '3 Months';
                }
                else if($period == 6){
                    $periodtxt = '6 Month';
                }
                else if($period == 12){
                    $periodtxt = '1 Year';
                }
                else if($period == 24){
                    $periodtxt = '2 Year';
                }
                else if($period == 36){
                    $periodtxt = '3 Year';
                }
                else if($period == 110){
                    $periodtxt = '10 Classes';
                }
                else if($period == 120){
                    $periodtxt = '20 Classes';
                }

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Add",
                    "title" => "Add Membership price ".$periodtxt." in ".$center->centertitle ." center"
                ));
            }
        }

        echo json_encode($data);
    }

    public function loadmembershipAction($centerid){

        $loadmembership = Centermembership::find('centerid="'.$centerid.'"');
        $loadmemberships = json_encode($loadmembership->toArray(), JSON_NUMERIC_CHECK);
        echo  $loadmemberships;

    }

    public function loadmembershipbyidAction($membershipid){

        $loadmembership = Centermembership::findFirst('membershipid = "'.$membershipid.'"');
        echo json_encode($loadmembership);

    }

    public function savemembershipbyidAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $membershipid = $request->getPost('membershipid');
            $period = $request->getPost('period');
            $frequent = $request->getPost('frequent');
            $price = $request->getPost('price');
            $description = $request->getPost('description');


            $updatemembership = Centermembership::findFirst('membershipid = "'.$membershipid.'"');
            $center = Center::findFirst('centerid="'.$updatemembership->centerid.'"');

            $updatemembership->period = $period;
            $updatemembership->frequent = $frequent;
            $updatemembership->price = $price;
            $updatemembership->description = $description;

            if (!$updatemembership->save()) {
                $data['msg'] = "something went wrong please try again!";
                $data['type'] = "danger";
            }
            else {
                $data['msg'] = "Membership successfully updated!";
                $data['type'] = "success";

                if($period == 1){
                    $periodtxt = '1 Month';
                }
                else if($period == 3){
                    $periodtxt = '3 Months';
                }
                else if($period == 6){
                    $periodtxt = '6 Month';
                }
                else if($period == 12){
                    $periodtxt = '1 Year';
                }
                else if($period == 24){
                    $periodtxt = '2 Years';
                }
                else if($period == 36){
                    $periodtxt = '3 Years';
                }
                else if($period == 110){
                    $periodtxt = '10 Classes';
                }
                else if($period == 120){
                    $periodtxt = '20 Classes';
                }

                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Update",
                    "title" => "Update Membership price ".$periodtxt." in ".$center->centertitle ." center"
                ));
            }



        }

        echo json_encode($data);

    }

    public function deletemembershipbyidAction($membershipid){
            $conditions = 'membershipid="' . $membershipid . '"';
            $deletemembership = Centermembership::findFirst(array($conditions));
            $center = Center::findFirst('centerid="'.$deletemembership->centerid.'"');
            $data = array('error' => 'Not Found');
            if ($deletemembership) {
                if ($deletemembership->delete()) {
                    $data = array('success' => 'Membership Deleted');
                    if($deletemembership->period == 1){
                        $periodtxt = '1 Month';
                    }
                    else if($deletemembership->period == 3){
                        $periodtxt = '3 Months';
                    }
                    else if($deletemembership->period == 6){
                        $periodtxt = '6 Month';
                    }
                    else if($deletemembership->period == 12){
                        $periodtxt = '1 Year';
                    }
                    else if($deletemembership->period == 24){
                        $periodtxt = '2 Years';
                    }
                    else if($deletemembership->period == 36){
                        $periodtxt = '3 Years';
                    }
                    else if($deletemembership->period == 110){
                        $periodtxt = '10 Classes';
                    }
                    else if($deletemembership->period == 120){
                        $periodtxt = '20 Classes';
                    }
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Delete",
                        "title" => "Delete Membership price ".$periodtxt." in ".$center->centertitle ." center"
                    ));
                }
            }

            echo json_encode($data);

    }


    public function centerSaveImagesForSliderAction() {
        $filename = $_POST['imgfilename'];
        $centerid = $_POST['centerid'];

        $centerexist = Centerimages::findFirst("centerid = '$centerid' ORDER BY imageorder DESC ");
        if($centerexist) {
            $last = $centerexist->imageorder;

            $imageorder = $last + 1;
            $saveImage = new Centerimages();
            $saveImage->assign(array(
                "centerid" => $centerid,
                "imagename" => $filename,
                "imageorder" => $imageorder
            ));

            if($saveImage->save()) {
                echo json_encode(array("result" => "Image Saved!"));
            } else {
                echo json_encode(array("result" => "Something went wrong saving center image!"));
            }
        } else {
            $saveImage = new Centerimages();
            $saveImage->assign(array(
                "centerid" => $centerid,
                "imagename" => $filename,
                "imageorder" => 1
            ));

            if($saveImage->save()) {
                echo json_encode(array("result" => "Image Saved!"));
            } else {
                echo json_encode(array("result" => "Something went wrong saving center image!"));
            }
        }

    }
    //IMAGE DELETE
    public function deleteimageAction($imgid) {
        $img = Centerimages::findFirst('id="'. $imgid.'"');
        $centerid = $img->centerid;
        $filename = $img->filename;

        if ($img) {
            if ($img->delete()) {
                $data[]=array('success' => "");
                $reorder = Centerimages::find("centerid = '$centerid' ORDER BY imageorder");
                if($reorder) {
                    $x = 1;
                    foreach($reorder as $order) {
                        $order->imageorder = $x;
                        if($order->save()){}
                        $x++;
                    }
                }
            }else{
                $data[]=array('error' => '');
            }
        }else{
            $data[]=array('error' => '');
        }
        echo json_encode($data);
    }

    public function loadCenterImagesAction($centerid) {
        $centerimages = Centerimages::find("centerid = '$centerid' ORDER BY imageorder ");
        echo json_encode(array("centerimages" => $centerimages->toArray() ));
    }

    public function loadscheduleAction($centerid) {
        $schedule = Centerschedule::find("centerid = '".$centerid."' ORDER BY STR_TO_DATE(starttime, '%l:%i %p')");
        if($schedule){
         echo json_encode($schedule->toArray());

        }
    }

    public function savescheduleAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $starttime = $request->getPost('starthourtime').':'.$request->getPost('startmintime').' '.$request->getPost('starttimeformat');
            $endtime = $request->getPost('endhourtime').':'.$request->getPost('endmintime').' '.$request->getPost('endtimeformat');
            $class = $request->getPost('class');
            $mo = $request->getPost('mo');
            $tu = $request->getPost('tu');
            $we = $request->getPost('we');
            $th = $request->getPost('th');
            $fr = $request->getPost('fr');
            $sa = $request->getPost('sa');
            $su = $request->getPost('su');

            $center = Center::findFirst('centerid="'.$centerid.'"');

            $guid = new \Utilities\Guid\Guid();
            $classid = $guid->GUID();


            $centerschedule = new Centerschedule();
            $centerschedule->classid = $classid;
            $centerschedule->centerid = $centerid;
            $centerschedule->starttime = $starttime;
            // $centerschedule->starttimeformat = $starttimeformat;
            $centerschedule->endtime = $endtime;
            // $centerschedule->endtimeformat = $endtimeformat;
            // $centerschedule->classtype = $class;
            // $centerschedule->classday = $day;
            $centerschedule->mo = $mo;
            $centerschedule->tu = $tu;
            $centerschedule->we = $we;
            $centerschedule->th = $th;
            $centerschedule->fr = $fr;
            $centerschedule->sa = $sa;
            $centerschedule->su = $su;
            if (!$centerschedule->save()) {
                // $data['msg'] = "Something went wrong please try again!";
                // $data['type'] = "danger";
                $errors = array();
                foreach ($centerschedule->getMessages() as $message) {
                    $data[] = $message->getMessage();
                }
                // echo json_encode(array('error' => $errors));
            } else {
                $data['msg'] = "Schedule successfully saved!";
                $data['type'] = "success";
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Add",
                    "title" => "Add Schedule ".$class." in ".$center->centertitle ." center"
                    ));
            }

        }

        echo json_encode($data);

    }

    public function loadschedulebyidAction($sessionid) {
        $schedule = Centerschedule::findFirst("classid = '".$sessionid."' ");
        if($schedule){
         echo json_encode($schedule);
        }
    }


    public function updatescheduleAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $classid = $request->getPost('classid');
            $centerid = $request->getPost('centerid');
            $starttime = $request->getPost('starthourtime').':'.$request->getPost('startmintime').' '.$request->getPost('starttimeformat');
            $endtime = $request->getPost('endhourtime').':'.$request->getPost('endmintime').' '.$request->getPost('endtimeformat');
            $class = $request->getPost('class');
            $mo = $request->getPost('mo');
            $tu = $request->getPost('tu');
            $we = $request->getPost('we');
            $th = $request->getPost('th');
            $fr = $request->getPost('fr');
            $sa = $request->getPost('sa');
            $su = $request->getPost('su');

            $centerschedule = Centerschedule::findFirst('classid="'.$classid.'"');
            $center = Center::findFirst('centerid="'.$centerschedule->centerid.'"');
            if($centerschedule){
                $centerschedule->starttime = $starttime;
                $centerschedule->endtime = $endtime;
                $centerschedule->mo = $mo;
                $centerschedule->tu = $tu;
                $centerschedule->we = $we;
                $centerschedule->th = $th;
                $centerschedule->fr = $fr;
                $centerschedule->sa = $sa;
                $centerschedule->su = $su;
                if (!$centerschedule->save()) {
                    $data['msg'] = "Something went wrong please try again!";
                    $data['type'] = "danger";
                } else {
                    $data['msg'] = "Schedule successfully updated!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Update",
                        "title" => "Update Schedule ".$class." in ".$center->centertitle ." center"
                        ));
                }
            }

        }

        echo json_encode($data);

    }

    public function deletescheduleAction($sessionid) {
        $schedule = Centerschedule::findFirst("classid = '".$sessionid."' ");
        $center = Center::findFirst('centerid="'.$schedule->centerid.'"');
        $schedules = $schedule->classtype;
        if($schedule){
           if ($schedule->delete()) {
                $data['msg'] = "Schedule successfully Deleted!";
                $data['type'] = "success";
                $audit = new CB();
                $audit->auditlog(array(
                    "module" =>"Center",
                    "event" => "Delete",
                    "title" => "Delete Schedule ".$schedules." in ".$center->centertitle ." center"
                    ));
           }
           else{
            $data['msg'] = "Something went wrong please try again!";
            $data['type'] = "danger";
           }
        }
        echo json_encode($data);
    }

    public function newslistAction($num, $page, $keyword,$centerid){
        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT newsid, title, author, centernews.date, datecreated, status  FROM centernews WHERE newslocation = '" .$centerid. "' ORDER BY dateedited DESC  LIMIT " . $offsetfinal . ",10");
           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
           foreach($searchresult as $key => $value) {
                $searchresult[$key]->title = utf8_encode($searchresult[$key]->title);
            }

           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM centernews WHERE newslocation = '" .$centerid. "' ORDER BY dateedited DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalreportdirty = count($searchresult1);
        } else {

         $offsetfinal = ($page * 10) - 10;

         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT * FROM centernews WHERE newslocation = '" .$centerid. "' and title LIKE '%" . $keyword . "%' or newslocation = '" .$centerid. "' and  newsslugs LIKE '%" . $keyword . "%' ORDER BY dateedited DESC LIMIT " . $offsetfinal . ",10");

         $stmt->execute();
         $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


         $db1 = \Phalcon\DI::getDefault()->get('db');
         $stmt1 = $db1->prepare("SELECT * FROM   centernews WHERE newslocation = '" .$centerid. "' and title LIKE '%" . $keyword . "%' or newslocation = '" .$centerid. "' and newsslugs LIKE '%" . $keyword . "%' ORDER BY dateedited DESC ");

         $stmt1->execute();
         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

         $totalreportdirty = count($searchresult1);

        }


        echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty));

    }

    public function getcenterphonenumberAction($centerid){

        $phone = Centerphonenumber::findFirst("centerid = '".$centerid."'");
        if($phone){
            echo json_encode($phone);
        }

    }


    public function savecenterphonenumberAction(){
        $data = array();
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $phonenumber = $request->getPost('phonenumber');
            $center = Center::findFirst('centerid="'.$centerid.'"');

            $conditions = 'centerid="' . $centerid . '"';
            $deletephone = Centerphonenumber::findFirst(array($conditions));
            $data = array('error' => 'Not Found');
            if ($deletephone) {
                if ($deletephone->delete()) {

                }
            }

            $phone = new Centerphonenumber();
                $phone->assign(array(
                    'centerid' => $centerid,
                    'phonenumber' => $phonenumber
                    ));
                if (!$phone->save()) {
                    $data['msg'] = "Something went wrong please try again!";
                    $data['type'] = "danger";
                } else {

                    $data['msg'] = "Phone number successfully updated!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Update",
                        "title" => "Update Phonenumber in ".$center->centertitle ." center"
                        ));
                }



        }

        echo json_encode($data);

    }

    public function loadCenterPageStoryAction($centerslugs, $storyslugs, $ssid) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $center = Center::findFirst(" centerslugs = '$centerslugs' ");
        $centerid = $center->centerid;
        $centerzip = $center->centerzip;

        $timezone = Timezonebyzipcode::findFirst("zip = ".$centerzip."");
        $TZ = date_default_timezone_set($timezone->timezone);
        $datenow = date('Y-m-d');

        $story = $db->prepare("SELECT * FROM testimonies WHERE ssid = '".$ssid."'");
        $story->execute();
        $storyfinal = $story->fetchAll(\PDO::FETCH_ASSOC);

            if($storyfinal[0]['relatedworkshop'] != '' || $storyfinal[0]['relatedworkshop'] != null || $storyfinal[0]['metatags'] != '' || $storyfinal[0]['metatags'] != null) {
            if($storyfinal[0]['metatags'] == null || $storyfinal[0]['metatags'] == '') {
                $room = 1;
                $related = preg_replace('/[\']/', '', $storyfinal[0]['relatedworkshop']);
                $related = preg_replace('/[,]/', '|', $related);
                $related = preg_replace('/^([^a-zA-Z0-9])*/', '', $related).',';
                $stmt2 = $db->prepare("SELECT * FROM testimonies WHERE ssid != ".$ssid." AND center = '".$centerid."' AND id != '".$storyid."' AND relatedworkshop RLIKE '".$related."' AND status = 1 ORDER BY date_published DESC LIMIT 5");
            } elseif ($storyfinal[0]['relatedworkshop'] == null || $storyfinal[0]['relatedworkshop'] == '') {
                $room = 2;
                $relatedtags = preg_replace('/[\']/', '', $storyfinal[0]['metatags']);
                $relatedtags = preg_replace('/[,]/', '|', $relatedtags);
                $relatedtags = preg_replace('/^([^a-zA-Z0-9])*/', '', $relatedtags).',';
                $stmt2 = $db->prepare("SELECT * FROM testimonies WHERE ssid != ".$ssid." AND center = '".$centerid."' AND id != '".$storyid."' AND metatags RLIKE '".$relatedtags."' AND status = 1 ORDER BY date_published DESC LIMIT 5");
            } else {
                $room = 3;
                $related = preg_replace('/[\']/', '', $storyfinal[0]['relatedworkshop']);
                $related = preg_replace('/[,]/', '|', $related);
                $related = preg_replace('/^([^a-zA-Z0-9])*/', '', $related).',';

                // $relatedtags = str_replace(' ', '', $story['metatags']);
                $relatedtags = preg_replace('/[\']/', '', $storyfinal[0]['metatags']);
                $relatedtags = preg_replace('/[,]/', '|', $relatedtags);
                $relatedtags = preg_replace('/^([^a-zA-Z0-9])*/', '', $relatedtags).',';
                $stmt2 = $db->prepare("SELECT * FROM testimonies WHERE ssid != ".$ssid." AND center = '".$centerid."' AND id != '".$storyid."'  AND (relatedworkshop RLIKE '".$related."' OR metatags RLIKE '".$relatedtags."') AND status = 1 ORDER BY date_published DESC LIMIT 5");
            }

            $stmt2->execute();
            $relatedstory = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
            foreach($relatedstory as $key => $value) {
                $relatedstory[$key]['subject'] = utf8_encode($relatedstory[$key]['subject']);
                $relatedstory[$key]['metadesc'] = utf8_encode($relatedstory[$key]['metadesc']);
            }
        } else {
            $relatedstory = array();
        }

        foreach($storyfinal as $key => $value) {
            $storyfinal[$key]['subject'] = utf8_encode($storyfinal[$key]['subject']);
            $storyfinal[$key]['metadesc'] = utf8_encode($storyfinal[$key]['metadesc']);
        }

        //SIDEBAR success stories
        $conditions = "center = '$centerid' AND status = 1 ORDER BY RAND() DESC  LIMIT 2";
        $columns = "ssid,id, author, photo, subject, metadesc";
        $success = Testimonies::find(array("conditions" => $conditions, "columns" => $columns));
            foreach($success as $key => $value) {
                $success[$key]->subject = utf8_encode($success[$key]->subject);
                $success[$key]->metadesc = utf8_encode($success[$key]->metadesc);
            }

        //SIDEBAR mainnews
        $conditions = "date <= '".$datenow."' AND status = 1 ORDER BY views DESC LIMIT 5";
        $columns = "banner, description, newsslugs";
        $mainnews = News::find(array("conditions"=>$conditions, "columns"=>$columns));
            foreach($mainnews as $key => $value) {
                $mainnews[$key]->description = utf8_encode($mainnews[$key]->description);
            }

        echo json_encode(array(
            "related" => $storyfinal[0]['relatedworkshop'],
            "relatedstory" => $relatedstory,
            "storyprop" => $storyfinal,
            "centerprop" => $center,
            "success" => $success->toArray(),
            "mainnews" => $mainnews->toArray(),
        ));
    }

    public function loadCenterPageInfoAction($centerslugs) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $center = Center::findFirst(" centerslugs = '$centerslugs' ");
        $centerid = $center->centerid;
        $centerzip = $center->centerzip;

        $timezone = Timezonebyzipcode::findFirst("zip = ".$centerzip."");
        $TZ = date_default_timezone_set($timezone->timezone);
        $datenow = date('Y-m-d');

        $centerphonenumber = Centerphonenumber::findFirst("centerid = '$centerid' ");
        $centerhours = Centerhours::findFirst("centerid = '$centerid' ");
        // $mainnews = News::find(" date <= '".$datenow."' AND status = 1 ORDER BY views DESC LIMIT 5 ");
            // foreach($mainnews as $key => $value) {
            //     $mainnews[$key]->title = utf8_encode($mainnews[$key]->title);
            //     $mainnews[$key]->description = utf8_encode($mainnews[$key]->description);
            //     $mainnews[$key]->body = utf8_encode($mainnews[$key]->body);
            // }

        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND  status = 1 ORDER BY news.views DESC LIMIT 0,5");
        $stmt->execute();
        $popularnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($popularnews as $key => $value) {
            $popularnews[$key]['title'] = utf8_encode( $popularnews[$key]['title']);
            $popularnews[$key]["body"] = utf8_encode($popularnews[$key]["body"]);
            $popularnews[$key]['description'] = utf8_encode($popularnews[$key]['description']);
            $popularnews[$key]["newsslugs"] = utf8_encode($popularnews[$key]["newsslugs"]);
        }

        echo json_encode(array(
            "timezone" => $timezone->timezone,
            "centerprop" => $center,
            "centerphonenumber" => $centerphonenumber,
            "centerhours" => $centerhours,
            "mainnews" => $popularnews
        ));
    }

    public function listnewsbycenterAction($centerslugs, $offset) {
        $center = Center::findFirst(" centerslugs = '$centerslugs' ");
        $centerid = $center->centerid;
        $centerzip = $center->centerzip;

        // $timezone = Timezonebyzipcode::findFirst("zip = ".$centerzip."");
        // $TZ = date_default_timezone_set($timezone->timezone);
        $datenow = date('Y-m-d');

        $finaloffset = ($offset * 10) - 10;

        $db = \Phalcon\DI::getDefault()->get('db');
        $getnewsdata = $db->prepare("SELECT * FROM centernews LEFT JOIN center ON centernews.newslocation = center.centerid WHERE center.centerslugs ='" . $centerslugs . "'  AND centernews.date <= '".$datenow."' AND centernews.status = 1 ORDER BY date DESC LIMIT $finaloffset, 10");
        $getnewsdata->execute();
        $listnewsbycategory = $getnewsdata->fetchAll(\PDO::FETCH_ASSOC);
            foreach($listnewsbycategory as $key => $value) {
                $listnewsbycategory[$key]['title'] = utf8_encode($listnewsbycategory[$key]['title']);
                $listnewsbycategory[$key]['description'] = utf8_encode($listnewsbycategory[$key]['description']);
                $listnewsbycategory[$key]['body'] = utf8_encode($listnewsbycategory[$key]['body']);
            }

        $getnewstotal = $db->prepare("SELECT * FROM centernews LEFT JOIN center ON centernews.newslocation = center.centerid WHERE center.centerslugs ='" . $centerslugs . "'  AND centernews.date <= '".$datenow."' AND centernews.status = 1 ");
        $getnewstotal->execute();
        $totalpage = $getnewstotal->fetchAll(\PDO::FETCH_ASSOC);

        //SIDEBAR success stories
        $conditions = "center = '$centerid' AND status = 1 ORDER BY RAND() DESC  LIMIT 2";
        $columns = "ssid,id, author, photo, subject, metadesc";
        $success = Testimonies::find(array("conditions" => $conditions, "columns" => $columns));
            foreach($success as $key => $value) {
                $success[$key]->subject = utf8_encode($success[$key]->subject);
                $success[$key]->metadesc = utf8_encode($success[$key]->metadesc);
            }

        //SIDEBAR mainnews
        $conditions = "date <= '".$datenow."' AND status = 1 ORDER BY views DESC LIMIT 5";
        $columns = "banner, description, newsslugs";
        $mainnews = News::find(array("conditions"=>$conditions, "columns"=>$columns));
            foreach($mainnews as $key => $value) {
                $mainnews[$key]->description = utf8_encode($mainnews[$key]->description);
            }

        echo json_encode(array(
            'newslist'=>$listnewsbycategory,
            'totalnews' => count($totalpage),
            'centerprop' => $center,
            "success" => $success->toArray(),
            "mainnews" => $mainnews->toArray(),
        ));
    }

    public function fullnewsbycenterAction($centerslugs,$newsslugs) {
        $db = \Phalcon\DI::getDefault()->get('db');

        $center = Center::findFirst(" centerslugs = '".$centerslugs."' ");
        $centerid = $center->centerid;
        $centerzip = $center->centerzip;

        $timezone = Timezonebyzipcode::findFirst("zip = ".$centerzip."");
        $TZ = date_default_timezone_set($timezone->timezone);
        $datenow = date('Y-m-d');

        $query = "SELECT centernews.*, centernews.metatitle as centernewsmetatitle
                    FROM centernews
                    LEFT JOIN center ON centernews.newslocation = center.centerid
                    WHERE centernews.newslocation = '".$centerid."'
                          AND centernews.newsslugs ='".$newsslugs."'
                          AND centernews.date <= '".$datenow."'
                          AND centernews.status = 1";
        // $query =" SELECT * from centernews";
        $fullnews = CB::bnbQueryFirst($query);

        $fullnews['title'] = utf8_encode($fullnews['title']);
        $fullnews['description'] = utf8_encode($fullnews['description']);
        $fullnews['body'] = utf8_encode($fullnews['body']);

        //SIDEBAR success stories
        $conditions = "center = '$centerid' AND status = 1 ORDER BY RAND() DESC  LIMIT 2";
        $columns = "id, author, photo, subject, metadesc";
        $success = Testimonies::find(array("conditions" => $conditions, "columns" => $columns));
            foreach($success as $key => $value) {
                $success[$key]->subject = utf8_encode($success[$key]->subject);
                $success[$key]->metadesc = utf8_encode($success[$key]->metadesc);
            }

        //SIDEBAR mainnews
        $conditions = "date <= '".$datenow."' AND status = 1 ORDER BY views DESC LIMIT 5";
        $columns = "banner, description, newsslugs";
        $mainnews = News::find(array("conditions"=>$conditions, "columns"=>$columns));
            foreach($mainnews as $key => $value) {
                $mainnews[$key]->description = utf8_encode($mainnews[$key]->description);
            }

        echo json_encode(array(
            "fullnews" => $fullnews,
            "centerprop" => $center,
            "success" => $success->toArray(),
            "mainnews" => $mainnews->toArray()
        ));

    }

    public function loadCenterPageClassScheduleAction($centerslugs) {
        $center = Center::findFirst(" centerslugs = '$centerslugs' ");
        $centerid = $center->centerid;
        $centerzip = $center->centerzip;

        $timezone = Timezonebyzipcode::findFirst("zip = ".$centerzip."");
        $TZ = date_default_timezone_set($timezone->timezone);
        $datenow = date('Y-m-d');

        $schedule = Centerschedule::find("centerid = '".$centerid."' ORDER BY starttime");

        //SIDEBAR success stories
        $conditions = "center = '$centerid' AND status = 1 ORDER BY RAND() DESC  LIMIT 2";
        $columns = "id, author, photo, subject, metadesc";
        $success = Testimonies::find(array("conditions" => $conditions, "columns" => $columns));
            foreach($success as $key => $value) {
                $success[$key]->subject = utf8_encode($success[$key]->subject);
                $success[$key]->metadesc = utf8_encode($success[$key]->metadesc);
            }

        //SIDEBAR mainnews
        $conditions = "date <= '".$datenow."' AND status = 1 ORDER BY views DESC LIMIT 5";
        $columns = "banner, description, newsslugs";
        $mainnews = News::find(array("conditions"=>$conditions, "columns"=>$columns));
            foreach($mainnews as $key => $value) {
                $mainnews[$key]->description = utf8_encode($mainnews[$key]->description);
            }

        echo json_encode(array(
            "centerprop" => $center,
            "schedule" => $schedule->toArray(),
            "success" => $success->toArray(),
            "mainnews" => $mainnews->toArray()
        ));
    }


    public function listsuccesssotriesbycenterAction($centerslugs, $offset) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $finaloffset = ($offset * 10) - 10;

        $center = Center::findFirst(" centerslugs = '$centerslugs' ");
        $centerid = $center->centerid;
        $centerzip = $center->centerzip;

        $timezone = Timezonebyzipcode::findFirst("zip = ".$centerzip."");
        $TZ = date_default_timezone_set($timezone->timezone);
        $datenow = date('Y-m-d');

        $getstorydata = $db->prepare("SELECT  testimonies.ssid, testimonies.subject, testimonies.author, testimonies.photo, testimonies.metadesc FROM testimonies WHERE center ='" . $centerid . "'  and status = 1 ORDER BY testimonies.date_published DESC LIMIT ".$finaloffset.", 10");
        $getstorydata->execute();
        $listsuccesssotriesbycenter = $getstorydata->fetchAll(\PDO::FETCH_ASSOC);
         $cb = new CB();

         // var_dump($listsuccesssotriesbycenter);
            foreach($listsuccesssotriesbycenter as $key => $value) {
                $listsuccesssotriesbycenter[$key]['subject'] = utf8_encode($listsuccesssotriesbycenter[$key]['subject']);
                $listsuccesssotriesbycenter[$key]['metadesc'] = utf8_encode($listsuccesssotriesbycenter[$key]['metadesc']);
                // $listsuccesssotriesbycenter[$key]['details'] = utf8_encode($listsuccesssotriesbycenter[$key]['details']);
            }

        $getnewstotal = $db->prepare("SELECT testimonies.id FROM testimonies WHERE center ='" . $centerid . "'  and status = 1");
        $getnewstotal->execute();
        $totalpage = $getnewstotal->fetchAll(\PDO::FETCH_ASSOC);

        //SIDEBAR success stories
        $conditions = "center = '$centerid' AND status = 1 ORDER BY RAND() DESC  LIMIT 2";
        $columns = "ssid ,id, author, photo, subject, metadesc";
        $success = Testimonies::find(array("conditions" => $conditions, "columns" => $columns));
            foreach($success as $key => $value) {
                $success[$key]->subject = utf8_encode($success[$key]->subject);
                $success[$key]->metadesc = utf8_encode($success[$key]->metadesc);
            }

        //SIDEBAR mainnews
        $conditions = "date <= '".$datenow."' AND status = 1 ORDER BY views DESC LIMIT 5";
        $columns = "banner, description, newsslugs";
        $mainnews = News::find(array("conditions"=>$conditions, "columns"=>$columns));
            foreach($mainnews as $key => $value) {
                $mainnews[$key]->description = utf8_encode($mainnews[$key]->description);
            }

        echo json_encode(array(
            'totalstory' => count($totalpage),
            'storylist'=>$listsuccesssotriesbycenter,
            'centerprop' => $center,
            "success" => $success->toArray(),
            "mainnews" => $mainnews->toArray()
            ));

    }

    public function listeventsbycentercenterAction($centerslugs, $offset) {

        $center = Center::findFirst(" centerslugs = '".$centerslugs."' ");
        $centerid = $center->centerid;
        $centerzip = $center->centerzip;

        $timezone = Timezonebyzipcode::findFirst("zip = ".$centerzip."");
        $TZ = date_default_timezone_set($timezone->timezone);
        $datenow = date('Y-m-d');

        $finaloffset = ($offset * 10) - 10;
        $db = \Phalcon\DI::getDefault()->get('db');

        $geteventsdata = $db->prepare("SELECT * FROM centercalendar WHERE centerid ='" . $centerid . "'  and status = 1 AND centercalendar.activitydate >= '".$datenow."' ORDER BY centercalendar.activitydate ASC LIMIT $finaloffset, 10");
        $geteventsdata->execute();
        $listeventsbycentercenter = $geteventsdata->fetchAll(\PDO::FETCH_ASSOC);

        $getnewstotal = $db->prepare("SELECT * FROM centercalendar WHERE centerid ='" . $centerid . "'  and status = 1 AND centercalendar.activitydate >= '".$datenow."' ORDER BY centercalendar.activitydate ASC");
        $getnewstotal->execute();
        $totalpage = $getnewstotal->fetchAll(\PDO::FETCH_ASSOC);

        // $mainnews = News::find("date <= '".$datenow."' AND status = 1 ORDER BY views DESC LIMIT 5 ");

        //SIDEBAR mainnews
        $conditions = "date <= '".$datenow."' AND status = 1 ORDER BY views DESC LIMIT 5";
        $columns = "banner, description, newsslugs";
        $mainnews = News::find(array("conditions"=>$conditions, "columns"=>$columns));
            foreach($mainnews as $key => $value) {
                $mainnews[$key]->description = utf8_encode($mainnews[$key]->description);
            }

        echo json_encode(array(
            'TZ' => $timezone->timezone,
            'centerprop' => $center,
            'eventslist' => $listeventsbycentercenter,
            'totalevents' => count($totalpage),
            "mainnews" => $mainnews->toArray()
            ));

    }

    public function saveregionAction(){
         $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $guid = new \Utilities\Guid\Guid();
            $regionid = $guid->GUID();

            $regionname = $request->getPost('regionname');

            $saveregion = new Centerregion();
            $saveregion->assign(array(
                    'regionid' => $regionid,
                    'regionname' => $regionname,
                    'regionmanager' => 0
                    ));

                if (!$saveregion->save()) {
                    $errors = array();
                    foreach ($saveregion->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }
                else {
                    $data['msg'] = "Region successfully Saved!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Add",
                        "title" => "Add Region ".$regionname.""
                        ));
                }


        }


    }

    public function loadregionAction($num, $page, $keyword){

        if ($keyword == 'null' || $keyword == 'undefined') {
             $loadregion = Centerregion::find();
        } else {
            $conditions = "regionname LIKE '%" . $keyword . "%'";
            $loadregion = Centerregion::find(array($conditions));
        }

        $currentPage = (int) ($page);

        $paginator = new \Phalcon\Paginator\Adapter\Model(
            array(
                "data" => $loadregion,
                "limit" => 10,
                "page" => $currentPage
                )
            );

        // Get the paginated results
        $page = $paginator->getPaginate();

        $data = array();
        foreach ($page->items as $m) {
            $data[] = array(
                'regionid' => $m->regionid,
                'regionname' => $m->regionname
                );
        }

         echo json_encode(array('data' => $data,'index' => $page->current,'total_items' => $page->total_items));
    }

    public function deleteregionAction(){
        $data = array();
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $regionid = $request->getPost('regionid');

            $deleteregion = Centerregion::findFirst("regionid='".$regionid."'");
            if($deleteregion){
                 if ($deleteregion->delete()) {
                    $data['msg'] = "Region successfully Deleted!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Delete",
                        "title" => "Delete Region ".$deleteregion->regionname.""
                        ));
                }
                else
                {
                    $data['msg'] = "something went wrong please try again!";
                    $data['type'] = "danger";
                }
            }
        }
        echo json_encode($data);
    }

    public function updateregionAction(){
        $data = array();
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $regionid = $request->getPost('regionid');
            $regionname = $request->getPost('regionname');

            $updateregion = Centerregion::findFirst("regionid='".$regionid."'");
            if($updateregion){
                $updateregion->regionname = $regionname;
                if($updateregion->save()){
                    $data['msg'] = "Region successfully Updated!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Update",
                        "title" => "Update Region ".$regionname.""
                        ));
                }
                else{
                    $data['msg'] = "something went wrong please try again!";
                    $data['type'] = "danger";
                }
            }
        }

        echo json_encode($data);

    }

    public function savedistrictAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $guid = new \Utilities\Guid\Guid();
            $districtid = $guid->GUID();

            $regionid = $request->getPost('regionid');
            $districtname = $request->getPost('districtname');

            $savedistrict = new Centerdistrict();
            $savedistrict->assign(array(
                    'districtid' => $districtid,
                    'regionid' => $regionid,
                    'districtname' => $districtname,
                    'districtmanager' => 0
                    ));

                if (!$savedistrict->save()) {
                    $errors = array();
                    foreach ($savedistrict->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }
                else {
                    $data['msg'] = "District successfully Save!";
                    $data['type'] = "success";

                }


        }
    }

    public function loadallregionAction(){
        $loadregion = Centerregion::find();
        if($loadregion){
            echo json_encode($loadregion->toArray());
        }
    }


    public function loaddistrictAction($num, $page, $keyword){
        if ($keyword == 'null' || $keyword == 'undefined') {
           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM centerdistrict LEFT JOIN centerregion ON centerdistrict.regionid = centerregion.regionid ORDER BY centerdistrict.districtname ASC  LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $districtlist = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM centerdistrict LEFT JOIN centerregion ON centerdistrict.regionid = centerregion.regionid ORDER BY centerdistrict.districtname ASC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totaldistrictitem = count($searchresult1);
        }
        else {

         $offsetfinal = ($page * 10) - 10;

         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT * FROM centerdistrict LEFT JOIN centerregion ON centerdistrict.regionid = centerregion.regionid WHERE centerdistrict.districtname LIKE '%" . $keyword . "%' ORDER BY centerdistrict.districtname ASC LIMIT " . $offsetfinal . ",10");

         $stmt->execute();
         $districtlist = $stmt->fetchAll(\PDO::FETCH_ASSOC);


         $db1 = \Phalcon\DI::getDefault()->get('db');
         $stmt1 = $db1->prepare("SELECT * FROM centerdistrict LEFT JOIN centerregion ON centerdistrict.regionid = centerregion.regionid WHERE centerdistrict.districtname LIKE '%" . $keyword . "%' ORDER BY centerdistrict.districtname ASC");

         $stmt1->execute();
         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

         $totaldistrictitem = count($searchresult1);

        }


        echo json_encode(array('data' => $districtlist, 'index' =>$page, 'total_items' => $totaldistrictitem));
    }

    public function deletedistrictAction(){
        $data = array();
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $districtid = $request->getPost('districtid');

            $deletedistrict = Centerdistrict::findFirst("districtid='".$districtid."'");
            if($deletedistrict){
                 if ($deletedistrict->delete()) {
                    $data['msg'] = "District successfully Deleted!";
                    $data['type'] = "success";
                }
                else
                {
                    $data['msg'] = "something went wrong please try again!";
                    $data['type'] = "danger";
                }
            }
        }
        echo json_encode($data);
    }


    public function loadCenterPagePricingAction($centerslugs) {
        $center = Center::findFirst(" centerslugs = '$centerslugs' ");
        $centerid = $center->centerid;

        $centerregfee = Centerpricingregfee::findFirst(" centerid = '$centerid' AND status = 1 ");
        $centerregclass = Centerpricingregclass::findFirst(" centerid = '$centerid' AND status = 1 ");
        $center1on1intro = Centerpricingsessionfee::findFirst(" centerid = '$centerid' AND status = 1 ");
        $centermembership = Centermembership::find(" centerid = '$centerid' AND (period != 110 AND period != 120) ORDER BY period ");
        $classpackages = Centermembership::find(" centerid = '$centerid' AND (period = 110 OR period = 120) ORDER BY period ");
        $privatesession = Centerprivatesession::findFirst(" centerid = '".$centerid."' AND status = 1 ");

        $mainnews = News::find(" status = 1 ORDER BY views DESC LIMIT 5 ");

        echo json_encode(array(
            "mainnews" => $mainnews->toArray(),
            "regfee" => $centerregfee,
            "regclass" => $centerregclass,
            "reg1on1intro" => $center1on1intro,
            "centermembership" => $centermembership->toArray(),
            "classpackages" => $classpackages->toArray(),
            "privatesession" => $privatesession,
            "centerprop" => $center
        ));
    }

    public function loaddistrictbyidAction(){
         $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $districtid = $request->getPost('districtid');

            $loaddistrictdata = Centerdistrict::findFirst("districtid='".$districtid."'");

            if($loaddistrictdata){
                echo json_encode($loaddistrictdata);
            }
        }
    }

    public function updateistrictAction(){
        $data = array();
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $districtid = $request->getPost('districtid');
            $regionid = $request->getPost('regionid');
            $districtname = $request->getPost('districtname');

            $savedistrict = Centerdistrict::findFirst("districtid='".$districtid."'");
            $savedistrict->regionid = $regionid;
            $savedistrict->districtname = $districtname;

                if (!$savedistrict->save()) {
                    $errors = array();
                    foreach ($savedistrict->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array('error' => $errors));
                }
                else {
                    $data['msg'] = "District successfully updated!";
                    $data['type'] = "success";

                }
        }
        echo json_encode($data);
    }

    public function getcenteremailAction($centerid){
        $getemail = Centeremail::findFirst("centerid='" .$centerid. "'");
        if($getemail){
            echo json_encode($getemail);
        }
    }

    public function saveemailAction(){
        $data = array();
        $request = new \Phalcon\Http\Request();

            if($request->isPost()){
            $centerid = $request->getPost('centerid');
            $email = $request->getPost('email');
            $center = Center::findFirst('centerid="'.$centerid.'"');
            $getemail = Centeremail::findFirst("centerid='" .$centerid. "'");
            if($getemail){
                $getemail->email = $email;

                if($getemail->save()){
                    $data['msg'] = "Email successfully updated!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Update",
                        "title" => "Update Email in".$center->centertitle.""
                        ));
                }
                else{
                    $data['msg'] = "Something went wrong please try again!";
                    $data['type'] = "danger";
                }
            }
            else{

                $saveemail = new Centeremail();
                $saveemail->assign(array(
                    'centerid' => $centerid,
                    'email' => $email
                    ));

                if($saveemail->save()){
                    $data['msg'] = "Email successfully updated!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Center",
                        "event" => "Update",
                        "title" => "Update Email in".$center->centertitle.""
                        ));
                }
                else{
                    $data['msg'] = "Something went wrong please try again!";
                    $data['type'] = "danger";
                }

            }
        }

        echo json_encode($data);

    }

    public function loadpriceAction(){
        $pricing = Classpricing::findFirst('priceid = 0');
        if($pricing){
           echo json_encode($pricing);
        }
    }

    public function loadbeneplacepriceAction(){
        $pricing = Beneplacepricing::findFirst('priceid = 0');
        if($pricing){
           echo json_encode($pricing);
        }
    }

    public function saveintroAction(){
        $data = array();
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $introofflinerate = $request->getPost('introofflinerate');
            $introonlinerate = $request->getPost('introonlinerate');
            $groupofflinerate = $request->getPost('groupofflinerate');
            $grouponlinerate = $request->getPost('grouponlinerate');

            $saveintro = Classpricing::findFirst('priceid = 0');
            if($saveintro){
                $saveintro->introofflinerate = $introofflinerate;
                $saveintro->introonlinerate = $introonlinerate;
                $saveintro->groupofflinerate = $groupofflinerate;
                $saveintro->grouponlinerate = $grouponlinerate;
                if($saveintro->save()){
                    $data['msg'] = "Session price successfully updated!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Starter Package",
                        "event" => "Update",
                        "title" => "Update Starter Package prices"
                        ));
                }
                else{
                    $data['msg'] = "Something went wrong please try again!";
                    $data['type'] = "danger";
                }
            }

        }

        echo json_encode($data);

    }

    public function beneplacesaveintroAction(){
        $data = array();
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $introofflinerate = $request->getPost('introofflinerate');
            $introonlinerate = $request->getPost('introonlinerate');
            $groupofflinerate = $request->getPost('groupofflinerate');
            $grouponlinerate = $request->getPost('grouponlinerate');

            $saveintro = Beneplacepricing::findFirst('priceid = 0');
            if($saveintro){
                $saveintro->introofflinerate = $introofflinerate;
                $saveintro->introonlinerate = $introonlinerate;
                $saveintro->groupofflinerate = $groupofflinerate;
                $saveintro->grouponlinerate = $grouponlinerate;
                if($saveintro->save()){
                    $data['msg'] = "Session price successfully updated!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Starter Package",
                        "event" => "Update",
                        "title" => "Update Starter Package prices"
                        ));
                }
                else{
                    $data['msg'] = "Something went wrong please try again!";
                    $data['type'] = "danger";
                }
            }

        }

        echo json_encode($data);

    }

    public function savecontactAction(){
         $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $centerid = $request->getPost('centerid');
            $phone1 = $request->getPost('phone1');
            $phone2 = $request->getPost('phone2');
            $phone3 = $request->getPost('phone3');
            $carrier = $request->getPost('carrier');
            $status1 = $request->getPost('status1');
            $status2 = $request->getPost('status2');
            $status3 = $request->getPost('status3');
            $center = Center::findFirst('centerid="'.$centerid.'"');

            $savephone = Centermanagerphone::findFirst('centerid = "'.$center.'"');
            if($savephone){
                $savephone->phone1 = $phone1;
                $savephone->phone2 = $phone2;
                $savephone->phone3 = $phone3;
                $savephone->carrier = $carrier;
                $savephone->status1 = $status1;
                $savephone->status2 = $status2;
                $savephone->status3 = $status3;
                if($savephone->save()){
                    $data['msg'] = "Cellphone successfully updated!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Cellphone set up",
                        "event" => "Update",
                        "title" => "Update cell phone setup in".$center->centertitle." center"
                        ));
                }
                else{
                    // $data['msg'] = "Something went wrong please try again!";
                    // $data['type'] = "danger";
                     $errors = array();
                    foreach($savephone->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array("errors"=>$errors));
                }
            }
            else{

                $savephone = new Centermanagerphone();
                $savephone->assign(array(
                    'centerid' => $centerid,
                    'phone1' => $phone1,
                    'phone2' => $phone2,
                    'phone3' => $phone3,
                    'carrier' => $carrier,
                    'status1' => $status1,
                    'status2' => $status2,
                    'status3' => $status3
                    ));

                if($savephone->save()){
                    $data['msg'] = "Cellphone successfully Saved!";
                    $data['type'] = "success";
                    $audit = new CB();
                    $audit->auditlog(array(
                        "module" =>"Cellphone set up",
                        "event" => "Update",
                        "title" => "Update cell phone setup in ".$center->centertitle." center"
                        ));
                }
                else{
                    // $data['msg'] = "Something went wrong please try again!";
                    // $data['type'] = "danger";
                    $errors = array();
                    foreach($savephone->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array("errors"=>$errors));
                }

            }

        }

        echo json_encode($data);
    }

    public function loadcontacttAction($centerid){
        $loadphone = Centermanagerphone::findFirst('centerid="'.$centerid.'"');
        if($loadphone){
            echo json_encode($loadphone);
        }
    }

    public function testsmsmessageAction(){
        $dc = new CB();
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){

            $phone1 = $request->getPost('phone1');
            $phone2 = $request->getPost('phone2');
            $phone3 = $request->getPost('phone3');
            $carrier = $request->getPost('carrier');

            $emailcarrier = '';
            if($carrier == '1'){
                $emailcarrier = '@vtext.com';
            }
            else if($carrier == '2'){
                $emailcarrier = '@txt.att.net';
            }
            else if($carrier == '3'){
                $emailcarrier = '@messaging.sprintpcs.com';
            }
            else if($carrier == '4'){
                $emailcarrier = '@tmomail.net';
            }
            else if($carrier == '5'){
                $emailcarrier = '@vmobl.com';
            }
            else if($carrier == '6'){
                $emailcarrier = '@mymetropcs.com';
            }

            $emailnumber = $phone1.$phone2. $phone3.$emailcarrier;
            // $emailnumber = 'llarenasjanrainier@gmail.com';
            $textmsg = 'TEST MESSAGE @BODYANDBRAIN/';

            $sendsmsemail = $dc->sendMail($emailnumber,'BodyandBrain :Appointment Confirmation Email',$textmsg );

            $data['msg'] = "Message successfully Sent!";
            $data['type'] = "success";

        }

        echo json_encode($data);
    }

    public function detailsAction($centerid) {
        $details = Center::findFirst("centerid = '$centerid' ");
        echo json_encode(array(
            'details' => $details
        ));

    }

    public function savedescriptionAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $centerid = $request->getPost('centerid');
            $centerdesc = $request->getPost('centerdesc');
            $id = $centerid;

            $save = Center::findFirst("centerid = '$centerid'");
            if($save) {
                $save->assign(array(
                        'centerdesc' => $centerdesc
                    ));

                if($save->save()) {
                    echo json_encode(array("result"=>"[. .]Successful!"));
                } else {
                    $errors = array();
                    foreach($save->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array("errors"=>$errors));
                }
            }

        }

    }

    public function saveorderingAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $imageid = $request->getPost('imageid');
            $ordering = $request->getPost('ordering');

            $x = 0;
            foreach($imageid as $image) {
                $findimage = Centerimages::findFirst("id = $image");
                $findimage->imageorder = $ordering[$x];
                if($findimage->save()){}
                $x++;
            }
            echo json_encode(array("imageid" => $imageid, "ordering" => $ordering));
        }
    }

    public function saveprivatesessionAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $centerid = $request->getPost('centerid');
            $status = $request->getPost('status');
            $details = $request->getPost('details');

            $find = Centerprivatesession::findFirst("centerid = '".$centerid."' ");
            if($find) {
                $find->assign(array(
                    'details' => $details,
                    'status' => $status,
                    'dateupdated' => date('Y-m-d H:i:s')
                ));
                if($find->save()) {
                    echo json_encode(array("stat"=>'success',"statusmsg"=>'Private Session Successfully Saved!'));
                } else {
                    $errors = array();
                    foreach($find->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array("stat"=>'danger',"statusmsg"=>$errors[0]));
                }
            } else {
                $guid = new \Utilities\Guid\Guid();
                $newid = $guid->GUID();

                $add = new Centerprivatesession();
                $add->assign(array(
                    'id' => $newid,
                    'centerid' => $centerid,
                    'details' => $details,
                    'status' => $status,
                    'datecreated' => date('Y-m-d'),
                    'dateupdated' => date('Y-m-d H:i:s')
                ));
                if($add->create()) {
                    echo json_encode(array("stat"=>'success',"statusmsg"=>'Private Session Successfully Added!'));
                } else {
                    $errors = array();
                    foreach($add->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    echo json_encode(array("stat"=>'danger',"statusmsg"=>$errors[0]));
                }
            }
        } else {
            echo json_encode(array("stat"=>'danger',"statusmsg"=>'Error Request!'));
        }
    }

    public function loadprivatesessionAction($centerid) {
        $find = Centerprivatesession::findFirst("centerid = '".$centerid."' ");
        if($find) {
            echo json_encode(array("privatesession"=>$find));
        } else {
            echo json_encode(array("privatesession"=>null));
        }

    }

    public function getofferAction(){
       $request = new \Phalcon\Http\Request();
       if($request->isPost()) {

            $offerid = $request->getPost('offerid');
            $centerslugs = $request->getPost('centerslugs');
            $offer = Centeroffer::findFirst("offerid = '$offerid'");

            $center = Center::findFirst("centerslugs = '$centerslugs' ");
            $centerid = $center->centerid;

            $centerphonenumber = Centerphonenumber::findFirst("centerid = '$centerid' ");



            if($offer){
                echo json_encode(array('offer'=>$offer,'center'=>$center,'phone'=>$centerphonenumber));
            }
        }
    }

    public function sociallinksAction($centerid) {
        $findfb = Centersociallinks::findFirst("centerid = '".$centerid."' AND title = 'facebook' ");
        $findgoogle = Centersociallinks::findFirst("centerid = '".$centerid."' AND title = 'google' ");
        $findtwitter = Centersociallinks::findFirst("centerid = '".$centerid."' AND title = 'twitter' ");
        $findyelp = Centersociallinks::findFirst("centerid = '".$centerid."' AND title = 'yelp' ");

        echo json_encode(array(
            'facebook' => $findfb,
            'google' => $findgoogle,
            'twitter' => $findtwitter,
            'yelp' => $findyelp
        ));

    }

    public function updatesociallinkAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $centerid = $request->getPost('centerid');
            $linkurl = $request->getPost('link');
            $title = $request->getPost('type');

            $find = Centersociallinks::findFirst("centerid ='".$centerid."' AND title = '".$title."' ");
            if($find) {
                $find->assign(array(
                    'linkurl' => $linkurl
                ));
                if($find->save()) {
                    $data['status'] = "success";
                    $data['msg'] = ucwords($title). " URL Successfully Updated";
                } else {
                    $errors = array();
                    foreach($find->getMessages as $message) {
                        $errors[] = $message->getMessage();
                    }
                    $data['status'] = "danger";
                    $data['msg'] = $errors[0];
                }
            } else {
                $guid = new \Utilities\Guid\Guid();
                $linkid = $guid->GUID();
                $add = new Centersociallinks();
                $add->assign(array(
                    'linkid' => $linkid,
                    'centerid' => $centerid,
                    'title' => $title,
                    'linkurl' => $linkurl,
                    'status' => 1
                ));
                if($add->create()) {
                    $data['status'] = "success";
                    $data['msg'] = ucwords($title). " URL Successfully Added";
                } else {
                    $errors = array();
                    foreach($add->getMessages as $message) {
                        $errors[] = $message->getMessage();
                    }
                    $data['status'] = "danger";
                    $data['msg'] = $errors[0];
                }
            }
            echo json_encode($data);
        }
    }

    public function updatesocialurlstatusAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $centerid = $request->getPost('centerid');
            $status = $request->getPost('status');
            $title = $request->getPost('type');

            $find = Centersociallinks::findFirst("centerid ='".$centerid."' AND title = '".$title."' ");
            if($find) {
                $find->assign(array(
                    'status' => $status
                ));
                if($find->save()) {
                    if($status == 1) {
                        $data['status'] = "success";
                        $data['msg'] = ucwords($title). " URL status was turned on";
                    } else {
                        $data['status'] = "warning";
                        $data['msg'] = ucwords($title). " URL status was turned off";
                    }
                } else {
                    $errors = array();
                    foreach($find->getMessages as $message) {
                        $errors[] = $message->getMessage();
                    }
                    $data['status'] = "danger";
                    $data['msg'] = $errors[0];
                }
            } else {
                $data['status'] = "danger";
                $data['msg'] = ucwords($title). " URL has no record";
            }
            echo json_encode($data);
        }
    }

    public function spnlAction($centerid){
        $getspnl = Center::findFirst('centerid = "'.$centerid.'"');
        if($getspnl){
            echo json_encode(array('spnl' => $getspnl->spnl));
        }
    }

    public function setspnlAction($centerid,$spnlstatus){
        $setspnl = Center::findFirst('centerid = "'.$centerid.'"');
        if($setspnl){
            $setspnl->spnl = $spnlstatus;
            if($setspnl->save()){
                echo json_encode($data['success'] = 'Success');
            }
        }
    }

    public function centervalidateAction($centerslugs) {
      $valid = CB::bnbQueryFirst("SELECT centerslugs FROM center WHERE centerslugs = '".$centerslugs."'");
      echo json_encode($valid);
    }

    public function fe_centermainpropAction() {
      $request = new Request();
      if($request->isPost()) {
        $datenow = date("Y-m-d");
        $centerslugs = $request->getPost("centerslugs");

        $center = CB::bnbQueryFirst("SELECT center.centerid, centerlocation.lat, centerlocation.lon FROM center
          LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid
          WHERE centerslugs ='".$centerslugs."'");
        $data['center'] = $center;

        $data['centerevents'] = CB::bnbQuery("SELECT * FROM(
            SELECT 'dummy' as ecenterslugs, activityid as eid,'dummy' as evenue,centerid as ecenterid,'dummy' as ecentertitle,activitytitle as etitle,activitydate as edate,description as edesc,activitydate as edatestart,activitytimefrom as estarthour,'dummy' as estartminute,activitytimefromformat as estarttimeformat,activitytimeto as eendhour,'dummy' as eendminute,activitytimetoformat as eendtimeformat, 'act' as type  FROM centercalendar WHERE centerid ='" . $center['centerid'] . "' and activitydate >= '".$datenow."'  and status = 1
            UNION
            SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.center ='" . $center['centerid'] . "' and workshop.datestart >= '".$datenow."'
            UNION
            SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop INNER JOIN workshopassociatedcenter ON workshopassociatedcenter.workshopid=workshop.workshopid LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshopassociatedcenter.centerid ='". $center['centerid'] ."'
            )
            AS event ORDER BY event.edatestart
            ");

        echo json_encode($data);
      }
    }

    public function loadstatewithcentersAction() {
        $state = CB::bnbQuery("SELECT states.* FROM states right join center ON states.state_code=center.centerstate GROUP BY states.state_code ORDER BY states.state_code ASC");
        echo json_encode($state);
    }

    public function getstarterloadscheduleAction() {
       $request = new Request();
       if($request->isPost()) {

            $centerid = $request->getPost("centerid");
            $date = $request->getPost("date");
            $dd = date("D", strtotime($date));
            if($dd == 'Sun'){
                $day = 'su';
            }
            else if($dd == 'Mon'){
                $day = 'mo';
            }
            else if($dd == 'Tue'){
                $day = 'tu';
            }
            else if($dd == 'Wed'){
                $day = 'we';
            }
            else if($dd == 'Thu'){
                $day = 'th';
            }
            else if($dd == 'Fri'){
                $day = 'fr';
            }
            else if($dd == 'Sat'){
                $day = 'sa';
            }
            $schedule = CB::bnbQuery("SELECT starttime, endtime, ".$day." as classandday FROM centerschedule where centerid = '".$centerid."' and  ".$day." != '' ORDER BY STR_TO_DATE(starttime, '%l:%i %p')");
            if($schedule){
                $data['schedule'] = $schedule;
                $data['day'] = $day;
           }

           echo json_encode($data);
       }

    }

    public function getScheduleAction(){
        $request = new Request();
       if($request->isPost()) {

            $centerslugs = $request->getPost("centerslugs");
            $centerid = CB::bnbQueryFirst("SELECT centerid FROM center WHERE centerslugs = '".$centerslugs."' OR centerslugs2='" . $centerslugs . "' OR centerslugs3='" . $centerslugs . "'");
            $date = $request->getPost("date");
            $dd = date("D", strtotime($date));
            $days = array();
            if($dd == 'Sun'){
                $days = ['su','mo','tu'];
            }
            else if($dd == 'Mon'){
                $days = ['mo','tu','we'];
            }
            else if($dd == 'Tue'){
                $days = ['tu','we','th'];
            }
            else if($dd == 'Wed'){
                $days = ['we','th','fr'];
            }
            else if($dd == 'Thu'){
                $days = ['th','fr','sa'];
            }
            else if($dd == 'Fri'){
                $days = ['fr','sa','su'];
            }
            else if($dd == 'Sat'){
                $days = ['sa','su','mo'];
            }

            $schedule = array();
            $dateadd = array();
            $counter = 0;
            $counter2 = 0;

            foreach ($days as $key => $day) {
                $d = $key;
                $activitydate = date('Y-m-d',strtotime($date . "+".$d." days"));

                $schedulelist = CB::bnbQuery("SELECT * FROM (
                  SELECT '" . $centerslugs . "' as ecenterslugs, classid as eid, 'dummy' as evenue, centerid as ecenterid, 'dummy' as ecentertitle, ".$day." as etitle, '$activitydate' as edate, 'dummy' as edesc, 'dummy' as edatestart, starttime as estarthour,'dummy' as estartminute, endtime as eendhour,'dummy' as eendminute, 'class' as type FROM centerschedule where centerid = '".$centerid['centerid']."' and  ".$day." != ''
                  UNION
                  SELECT '" . $centerslugs . "' as ecenterslugs, activityid as eid,'dummy' as evenue,centerid as ecenterid,'dummy' as ecentertitle,activitytitle as etitle,activitydate as edate,description as edesc,activitydate as edatestart, CONCAT(activitytimefrom,' ', activitytimefromformat) as estarthour,'dummy' as estartminute , CONCAT(activitytimeto, ' ', activitytimetoformat) as eendhour,'dummy' as eendminute, 'act' as type  FROM centercalendar WHERE centerid ='" . $centerid['centerid'] . "' and activitydate = '".$activitydate."'  and status = 1
                  UNION
                  SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart, CONCAT(workshop.starthour, ' ', workshop.starttimeformat) as estarthour,workshop.startminute as estartminute, CONCAT(workshop.endhour, ' ', workshop.endtimeformat) as eendhour,workshop.endminute as eendminute, 'wrk' as type FROM workshop LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.center ='" . $centerid['centerid'] . "' AND workshop.datestart = '".$activitydate."'
                  UNION
                  SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart, CONCAT(workshop.starthour, ' ',workshop.starttimeformat) as estarthour,workshop.startminute as estartminute,CONCAT(workshop.endhour, ' ', workshop.endtimeformat) as eendhour,workshop.endminute as eendminute, 'wrk' as type FROM workshop INNER JOIN workshopassociatedcenter ON workshopassociatedcenter.workshopid=workshop.workshopid LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshopassociatedcenter.centerid ='". $centerid['centerid'] ."' AND workshop.datestart ='".$activitydate."'
                  ) AS event ORDER BY STR_TO_DATE(event.estarthour, '%l:%i %p')");

                if($schedulelist){
                    $counter++;
                    $schedule[] = $schedulelist;
                    $date = $date;
                    $date1 = str_replace('-', '/', $date);
                    $dateadd[] = date('Y-m-d',strtotime($date1 . "+".$counter2++." days"));
                }
                else{
                    break;
                }

            }

           $data['schedule'] = $schedule;
           $data['days'] = $dateadd;

           echo json_encode($data);
       }
    }

    public function viewschedAction() {
      $request = new Request();
      $id = $request->getPost('id');
      $centerslugs = $request->getPost('centerslugs');
      $start = $request->getPost('start');
      $data = "error";

      $date = $request->getPost("date");
      $dd = date("D", strtotime($date));
      
      $days = array();
      if($dd == 'Sun'){
          $day = 'su';
      }
      else if($dd == 'Mon'){
          $day = 'mo';
      }
      else if($dd == 'Tue'){
          $day = 'tu';
      }
      else if($dd == 'Wed'){
          $day = 'we';
      }
      else if($dd == 'Thu'){
          $day = 'th';
      }
      else if($dd == 'Fri'){
          $day = 'fr';
      }
      else if($dd == 'Sat'){
          $day = 'sa';
      }

      $schedule = CB::bnbQuery("SELECT '" . $centerslugs . "' as ecenterslugs, classid as eid, 'dummy' as evenue, centerid as ecenterid, 'dummy' as ecentertitle, ".$day." as etitle, 'dummy' as edate, description as edesc, 'dummy' as edatestart, TIME_FORMAT(starttime, '%h:%i') as estarthour,'dummy' as estartminute, starttime as estarttimeformat, TIME_FORMAT(endtime, '%h:%i') as eendhour,'dummy' as eendminute, endtime as eendtimeformat, 'class' as type FROM centerschedule
      INNER JOIN classes ON ".$day."=classes.class where classid='$id' AND starttime='$start'");
      if($schedule){
          $ar = explode(" ", $schedule[0]['estarttimeformat']);
          if(count($ar) >= 2){
            $k = count($ar) - 1;
            $schedule[0]['estarttimeformat'] = $ar[$k];
          }

          $ar2 = explode(" ", $schedule[0]['eendtimeformat']);
          if(count($ar2) >= 2){
            $k = count($ar2) - 1;
            $schedule[0]['eendtimeformat'] = $ar2[$k];
          }

          $schedule[0]['datesss'] = date('Y-m-d',strtotime($date));
          echo json_encode($schedule);
      } else {
        $schedule = CB::bnbQuery("SELECT * FROM (
          SELECT '" . $centerslugs . "' as ecenterslugs, activityid as eid,'dummy' as evenue,centerid as ecenterid,'dummy' as ecentertitle,activitytitle as etitle,activitydate as edate,description as edesc,activitydate as edatestart,activitytimefrom as estarthour,'dummy' as estartminute,activitytimefromformat as estarttimeformat,activitytimeto as eendhour,'dummy' as eendminute,activitytimetoformat as eendtimeformat, 'act' as type  FROM centercalendar WHERE activityid='$id'
          UNION
          SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.workshopid='$id'
          UNION
          SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop INNER JOIN workshopassociatedcenter ON workshopassociatedcenter.workshopid=workshop.workshopid LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.workshopid='$id'
          ) AS event ORDER BY event.estarthour");

          if($schedule){
            $schedule[0]['datesss'] = date('Y-m-d',strtotime($date));
            echo json_encode($schedule);
          } else {
            echo json_encode(array('error' => 'error' ));
          }
      }
    }

    public function centermanageMycenterAction() {
      $request = new Request();
      if($request->isPost()) {
        $ctrid = $request->getPost("ctrid");
        $ctrmgrid = $request->getPost("ctrmgrid");

         $mycenter = CB::bnbQueryFirst("SELECT * FROM center WHERE centerid = '".$ctrid."' AND centermanager = '".$ctrmgrid."'");
        if($mycenter == true) {
          $data["mycenter"] = $mycenter;
          $data['regions'] = CB::bnbQuery("SELECT * FROM centerregion WHERE regionid != 'this is dummy' ORDER BY regionname");
          $data['states']  = CB::bnbQuery("SELECT * FROM states ORDER BY state_code");
          $data["cities"]  = CB::bnbQuery("SELECT * FROM cities WHERE state_code = '".$mycenter['centerstate']."' ORDER BY city");
          $data["zips"]    = CB::bnbQuery("SELECT city, zip FROM cities_extended WHERE city ='".$mycenter['centercity']."' ORDER BY zip");
        } else {
          $data["err"] = "API: CENTER NOT FOUND!";
        }
      } else {
        $data["err"] = "API: NO POST DATA!";
      }
      echo json_encode($data);

    }

    public function validateClassAction($id, $class) {
      if($id == "undefined"){
        $result = Classes::findFirst("class='" . $class . "'");
      } else {
        $result = Classes::findFirst("class='" . $class . "' AND id!='" . $id . "'");
      }
      if($result){
        echo json_encode(array('exist' => true ));
      } else {
        echo json_encode(array('exist' => false ));
      }
    }

    public function saveclassAction() {
      $request = new Request();
      if($request->isPost()) {
        $id   = CB::genGUID();
        $class = $request->getPost('class');
        $description = $request->getPost('description');

        $c = new Classes();
        $c->assign(array(
          'id' => $id,
          'class' => $class,
          'description' => $description,
          'status' => 1,
          'created_at' => date("Y-m-d H:i:s"),
          'updated_at' => date("Y-m-d H:i:s"))
        );

        if($c->save()) {
          echo json_encode(array('success' => 'Class has been added successfully' ));
        } else {
          // foreach ($center->getMessages() as $message) {
          //     $errors[] = $message->getMessage();
          // }
          // die(var_dump($errors));
          echo json_encode(array('err' => 'Something wen wrong, Please try again later.' ));
        }
      }
    }

    public function getclassesAction($num, $page, $keyword) {
      if ($keyword == 'null' || $keyword == 'undefined') {
         $offsetfinal = ($page * 10) - 10;

         $db = \Phalcon\DI::getDefault()->get('db');
         $stmt = $db->prepare("SELECT * FROM classes ORDER BY updated_at DESC LIMIT " . $offsetfinal . ",10");

         $stmt->execute();
         $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


         $db1 = \Phalcon\DI::getDefault()->get('db');
         $stmt1 = $db1->prepare("SELECT * FROM classes ORDER BY updated_at DESC");

         $stmt1->execute();
         $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

         $totalreportdirty = count($searchresult1);
      }
      else {

           $offsetfinal = ($page * 10) - 10;

           $db = \Phalcon\DI::getDefault()->get('db');
           $stmt = $db->prepare("SELECT * FROM classes WHERE class LIKE '%" . $keyword . "%' ORDER BY updated_at DESC LIMIT " . $offsetfinal . ",10");

           $stmt->execute();
           $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);


           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT * FROM classes WHERE class LIKE '%" . $keyword . "%' ORDER BY updated_at DESC");

           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalreportdirty = count($searchresult1);
      }

      echo json_encode(array('data' => $searchresult, 'index' =>$page, 'total_items' => $totalreportdirty));
    }

    public function updateclassAction() {
      $request = new Request();
      if($request->isPost()) {
        $id = $request->getPost('id');
        $class = $request->getPost('class');
        $description = $request->getPost('description');

        $c = Classes::findFirst("id='" . $id . "'");
        if($c){
          $c->class = $class;
          $c->description = $description;
          $c->updated_at = date("Y-m-d H:i:s");

          if($c->save()) {
            echo json_encode(array('success' => 'Class has been updated successfully' ));
          } else {
            echo json_encode(array('err' => 'Something wen wrong, Please try again later.' ));
          }
        } else {
          echo json_encode(array('err' => 'Something wen wrong, Please try again later.' ));
        }
      }
    }

    public function deleteclassAction() {
      $request = new Request();
      if($request->isPost()) {
        $id = $request->getPost('id');

        $c = Classes::findFirst("id='" . $id . "'");
        $class = addslashes($c->class);

        if($c){
          if($c->delete()){
            $c = CB::bnbQuery("SELECT * FROM centerschedule WHERE mo='" . $class . "' OR tu='" . $class . "' OR we='" . $class . "' OR th='" . $class . "' OR fr='" . $class . "' OR sa='" . $class . "' OR su='" . $class . "'");
            if($c){
              foreach ($c as $key => $value) {
                  $cl = Centerschedule::findFirst("classid='" . $value['classid']. "'");
                  if($cl->mo == $class){
                    $cl->mo = null;
                  }
                  if($cl->tu == $class){
                    $cl->tu = null;
                  }
                  if($cl->we == $class){
                    $cl->we = null;
                  }
                  if($cl->th == $class){
                    $cl->th = null;
                  }
                  if($cl->fr == $class){
                    $cl->fr = null;
                  }
                  if($cl->sa == $class){
                    $cl->sa = null;
                  }
                  if($cl->su == $class){
                    $cl->su = null;
                  }

                  if(!$cl->save()){
                    $errors = array();
                    foreach ($cl->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    die(json_encode($errors));
                  }
              }
            }

            echo json_encode(array('success' => 'Class has been deleted successfully' ));
          } else {
            echo json_encode(array('err' => 'Something wen wrong, Please try again later.' ));
          }
        } else {
          echo json_encode(array('err' => 'Something wen wrong, Please try again later.' ));
        }
      }
    }

    public function getcenterclassesAction() {
      $c = CB::bnbQuery("SELECT class as classlist FROM classes ORDER BY class ASC");
      if($c){
        echo json_encode($c);
      }
    }
}
