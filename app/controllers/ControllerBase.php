<?php

namespace Controllers;
use \Models\Center as Center;
use \Models\Auditlog as Auditlog;
use Utilities\Guid\Guid;
use \Models\Producttags as Producttags;
use \Models\Prodtags as Prodtags;
use \Models\Producttype as Producttype;
use \Models\Prodtype2 as Prodtype2;
use \Models\Productcategory as Productcategory;
use \Models\Prodcat as Prodcat;
use \Models\Productsize as Productsize;
use \Models\Prodsubcat2 as Prodsubcat2;
use \Models\Productsubcategory as Productsubcategory;
use \Models\Prodsize as Prodsize;
use PHPMailer as PHPMailer;

class ControllerBase extends \Phalcon\Mvc\Controller{
    protected function initialize()
    {
      $this->view->amazonlink = $this->config->application->amazonlink;
    }
    public function conf() {
      $DI = \Phalcon\DI::getDefault();
      return $DI->get('application')->config;
    }
    public function genGUID(){
        $guid = new Guid();
        return $guid->GUID();
    }
    public function modelsManager($phql) {
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $result = $app->modelsManager->executeQuery($phql);
    }

    public function getConfig() {
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        return $app->config;
    }

    public function sendMail($email, $subject,$content) {
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        $json = json_encode(array(
            'From' => $app->config->postmark->signature,
            'To' => $email,
            'Name' => 'BodynBrain',
            'Subject' => $subject,
            'HtmlBody' => $content
            ));
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $app->config->postmark->url);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'X-Postmark-Server-Token: '.$app->config->postmark->token
            ));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        $response = json_decode(curl_exec($ch2), true);
        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
    }

    public function sendSms($email, $subject,$content){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        $json = json_encode(array(
            'From' => $app->config->postmark->signature,
            'To' => $email,
            'Name' => 'BodynBrain',
            'HtmlBody' => $content
            ));

        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $app->config->postmark->url);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'X-Postmark-Server-Token: '.$app->config->postmark->token
            ));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        $response = json_decode(curl_exec($ch2), true);
        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
    }

    public function sendMail2($email, $subject,$content,$replyto){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        $json = json_encode(array(
            'From' => $app->config->postmark->signature,
            'To' => $email,
            'Subject' => $subject,
            'HtmlBody' => $content,
            'Name' => 'BodynBrain',
            'ReplyTo' =>$replyto
            ));

        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $app->config->postmark->url);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'X-Postmark-Server-Token: '.$app->config->postmark->token
            ));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        $response = json_decode(curl_exec($ch2), true);
        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
    }

    public function sendEmail($name, $to, $subject,$body){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        $json = json_encode(array(
            'From'     => $app->config->postmark->signature,
            'To'       => $to,
            'Subject'  => $subject,
            'HtmlBody' => $body,
            'Name'     => $name
            ));
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $app->config->postmark->url);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'X-Postmark-Server-Token: '.$app->config->postmark->token
            ));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        $response = json_decode(curl_exec($ch2), true);
        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);
        return $response;
    }

    public  function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return $miles;
    }

public function time_elapsed_string($datetime, $full = false) {
    // $time = time() - $datetime; // to get the time since that moment

    // $tokens = array (
    //     31536000 => 'year',
    //     2592000 => 'month',
    //     604800 => 'week',
    //     86400 => 'day',
    //     3600 => 'hour',
    //     60 => 'minute',
    //     1 => 'second'
    // );

    // foreach ($tokens as $unit => $text) {
    //     if ($time < $unit) continue;
    //     $numberOfUnits = floor($time / $unit);
    //     return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    // }
    $now =  new \DateTime;
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
        );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

    public function userid(){

      $request = new \Phalcon\Http\Request();
      return $request->getheader("AUTHORIZATION");
    }
    public function auditlog($data) {
        $num = 0;
        foreach ($data as $key => $value) {
           $datacont[]=$key;
           $dataval[]=$value;
       }
       $auditid = new \Utilities\Guid\Guid();


       $savelog = new Auditlog();
       $savelog->logid = $auditid->GUID();
       $savelog->datetime = date("Y-m-d H:i:s");
       $savelog->userid = $this->userid();
       $savelog->module = $dataval[0];
       $savelog->event = $dataval[1];
       $savelog->title = $dataval[2];

       if($savelog->save()){

       }
    }

    public function bnbQuery($phql) {
      $dbbnb = \Phalcon\DI::getDefault()->get('db');
      $stmt = $dbbnb->prepare($phql);
      $stmt->execute();
      $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      return $result;
    }

    public function bnbQueryFirst($phql) {
      $dbbnb = \Phalcon\DI::getDefault()->get('db');
      $stmt = $dbbnb->prepare($phql);
      $stmt->execute();
      $result = $stmt->fetch(\PDO::FETCH_ASSOC);
      return $result;
    }

    public function mssqlQuery($phql) {
      // $dbbnb = \Phalcon\DI::getDefault()->get('dbMaster');
      $con = new \PDO('odbc:Driver=FreeTDS; Servername=bnbmssql2; Database=mnet2; UID=bnbadmin; PWD=5n5@dm1in2015;');
      $stmt = $con->prepare($phql);
      $stmt->execute();
      $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      return $result;
    }

    public function mssqlQueryFirst($phql) {
      $con = new \PDO('odbc:Driver=FreeTDS; Servername=bnbmssql2; Database=mnet2; UID=bnbadmin; PWD=5n5@dm1in2015;');
      $stmt = $con->prepare($phql);
      $stmt->execute();
      $result = $stmt->fetch(\PDO::FETCH_ASSOC);
      return $result;
    }

    public function bnbQuerydelete($phql) {
      $dbbnb = \Phalcon\DI::getDefault()->get('db');
      $stmt = $dbbnb->prepare($phql);
      $stmt->execute();
      return $stmt->execute();
    }

    public function bnbQueryupdate($phql) {
      $dbbnb = \Phalcon\DI::getDefault()->get('db');
      $stmt = $dbbnb->prepare($phql);
      $stmt->execute();
      return $stmt->execute();
    }

    public function insertproducttags($tags, $productid) {
        $prodtags = Prodtags::find("productid='" . $productid . "'");

        $db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db1->prepare("SELECT producttags.tags FROM producttags INNER JOIN prodtags ON producttags.id=prodtags.tagid WHERE prodtags.productid='$productid'");
        $stmt->execute();
        $ptags = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($ptags as $key => $value) {
            $data[] = $value['tags'];
        }

        if($prodtags->delete()){
            foreach ($tags as $tag) {
                $pt = Producttags::findFirst("tags='$tag'");
                if(!$pt){
                    $pt = new Producttags();
                    $pt->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();
                    $pt->assign(array(
                        'id' => $guid->GUID(),
                        'tags' => $tag,
                        'tagslugs' =>str_replace("-", " ", $tag),
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s") ));
                    if (!$pt->save()){
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        $transaction->rollback();
                        echo json_encode(array('error' => $errors));
                    }
                }

                $prodtags = new Prodtags();
                $prodtags->setTransaction($transaction);
                $guid = new \Utilities\Guid\Guid();
                $prodtags->assign(array(
                    'id' => $guid->GUID(),
                    "tagid" => $pt->id,
                    "productid" => $productid ));
                if (!$prodtags->save()){
                    $errors = array();
                    foreach ($page->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    $transaction->rollback();
                    echo json_encode(array('error' => $errors));
                }
            }
        }
        if(count(array_diff($data, $tags)) == 0 && count(array_diff($tags, $data)) == 0){
            return '';
        }else {
            return addslashes("<li>Updated the item tags</li>");
        }
    }

    public function insertproducttype($types, $productid){
        $prodtype2 = Prodtype2::find("productid='" . $productid . "'");

        $db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db1->prepare("SELECT producttype.type FROM producttype INNER JOIN prodtype2 ON producttype.id=prodtype2.typeid WHERE prodtype2.productid='$productid'");
        $stmt->execute();
        $ptype = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($ptype as $key => $value) {
            $data[] = $value['type'];
        }

        if($prodtype2->delete()){
            foreach ($types as $type) {
                $pt = Producttype::findFirst("type='$type'");
                if(!$pt){
                    $pt = new Producttype();
                    $pt->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();
                    $pt->assign(array(
                        'id' => $guid->GUID(),
                        'type' => $type,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s") ));
                    if (!$pt->save()){
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        $transaction->rollback();
                        echo json_encode(array('error' => $errors));
                    }
                }

                $prodtype2 = new Prodtype2();
                $prodtype2->setTransaction($transaction);
                $guid = new \Utilities\Guid\Guid();
                $prodtype2->assign(array(
                    'id' => $guid->GUID(),
                    "typeid" => $pt->id,
                    "productid" => $productid ));
                if (!$prodtype2->save()){
                    $errors = array();
                    foreach ($prodtype2->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    $transaction->rollback();
                    echo json_encode(array('error' => $errors));
                }
            }
        }

        if(count(array_diff($data, $types)) == 0 && count(array_diff($types, $data)) == 0){
            return '';
        }else {
            return addslashes("<li>Updated the item type</li>");
        }
    }

    public function insertproductcat($categories, $productid){
        $prodcat = Prodcat::find("productid='" . $productid . "'");

        $db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db1->prepare("SELECT productcategory.category FROM productcategory INNER JOIN prodcat ON productcategory.id=prodcat.categoryid WHERE prodcat.productid='$productid'");
        $stmt->execute();
        $pc = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($pc as $key => $value) {
            $data[] = $value['category'];
        }

        if($prodcat->delete()){
            foreach ($categories as $cat) {
                $productcat = Productcategory::findFirst("category='$cat'");
                if(!$productcat){
                    $productcat = new Productcategory();
                    $productcat->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();
                    $productcat->assign(array(
                        'id' => $guid->GUID(),
                        'category' => $cat,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s") ));
                    if (!$productcat->save()){
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        $transaction->rollback();
                        echo json_encode(array('error' => $errors));
                    }
                }

                $prodcat = new Prodcat();
                $prodcat->setTransaction($transaction);
                $guid = new \Utilities\Guid\Guid();
                $prodcat->assign(array(
                    'id' => $guid->GUID(),
                    "categoryid" => $productcat->id,
                    "productid" => $productid ));
                if (!$prodcat->save()){
                    $errors = array();
                    foreach ($prodcat->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    $transaction->rollback();
                    echo json_encode(array('error' => $errors));
                }
            }
        }

        if(count(array_diff($data, $categories)) == 0 && count(array_diff($categories, $data)) == 0){
            return '';
        }else {
            return addslashes("<li>Updated the item category</li>");
        }
    }

    public function insertproductsubcat($subcategories, $productid) {
        $prodcat = Prodsubcat2::find("productid='" . $productid . "'");
        $db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db1->prepare("SELECT productcategory.category FROM productcategory INNER JOIN prodcat ON productcategory.id=prodcat.categoryid WHERE prodcat.productid='$productid'");
        $stmt->execute();
        $pc = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($pc as $key => $value) {
            $data[] = $value['category'];
        }

        if($prodcat->delete()){
            foreach ($subcategories as $cat) {
                $productcat = Productsubcategory::findFirst("subcategory='$cat'");
                if(!$productcat){
                    $productcat = new Productsubcategory();
                    $productcat->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();
                    $productcat->assign(array(
                        'id' => $guid->GUID(),
                        'subcategory' => $cat,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s") ));
                    if (!$productcat->save()){
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        $transaction->rollback();
                        echo json_encode(array('error' => $errors));
                    }
                }

                $prodcat = new Prodsubcat2();
                $prodcat->setTransaction($transaction);
                $guid = new \Utilities\Guid\Guid();
                $prodcat->assign(array(
                    'id' => $guid->GUID(),
                    "subcategoryid" => $productcat->id,
                    "productid" => $productid ));
                if (!$prodcat->save()){
                    $errors = array();
                    foreach ($prodcat->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    die(print_r($errors));
                    // $transaction->rollback();
                    echo json_encode(array('error' => $errors));
                }
            }
        }

        if(count(array_diff($data, $subcategories)) == 0 && count(array_diff($subcategories, $data)) == 0){
            return '';
        }else {
            return addslashes("<li>Updated the item sub category</li>");
        }
    }

    public function insertproductsize($sizes, $productid){
        $prodsize = Prodsize::find("productid='" . $productid . "'");

        $db1 = \Phalcon\DI::getDefault()->get('db');
        $stmt = $db1->prepare("SELECT productsize.size FROM productsize INNER JOIN prodsize ON productsize.id=prodsize.sizeid WHERE prodsize.productid='$productid'");
        $stmt->execute();
        $pc = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($pc as $key => $value) {
            $data[] = $value['size'];
        }

        if($prodsize->delete()){
            foreach ($sizes as $size) {
                $productsize = Productsize::findFirst("size='$size'");
                if(!$productsize){
                    $productsize = new Productsize();
                    $productsize->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();
                    $productsize->assign(array(
                        'id' => $guid->GUID(),
                        'size' => $size ));
                    if (!$productsize->save()){
                        $errors = array();
                        foreach ($page->getMessages() as $message) {
                            $errors[] = $message->getMessage();
                        }
                        $transaction->rollback();
                        echo json_encode(array('error' => $errors));
                    }
                }

                $prodsize = new Prodsize();
                $prodsize->setTransaction($transaction);
                $guid = new \Utilities\Guid\Guid();
                $prodsize->assign(array(
                    'id' => $guid->GUID(),
                    "sizeid" => $productsize->id,
                    "productid" => $productid ));
                if (!$prodsize->save()){
                    $errors = array();
                    foreach ($prodsize->getMessages() as $message) {
                        $errors[] = $message->getMessage();
                    }
                    $transaction->rollback();
                    echo json_encode(array('error' => $errors));
                }
            }
        }

        if(count(array_diff($data, $sizes)) == 0 && count(array_diff($sizes, $data)) == 0){
            return '';
        }else {
            return addslashes("<li>Updated the item size</li>");
        }
    }

    public function genLogs($db, $val) {
        $action = "<ul>";
        if($db->name != $val['name']){
            $action .= '<li>Updated the Item name "' . $db->name . '" to "' . $val['name'] . '"</li>';
        }
        if($db->shortdesc != $val['shortdesc']){
            $action .= '<li>Updated the short desctiption "' . $db->shortdesc . '" to "' . $val['shortdesc'] . '"</li>';
        }
        if($db->description != $val['description']){
            $action .= '<li>Updated the desctiption</li>';
        }
        if($db->quantity != $val['quantity']){
            $action .= '<li>Updated the quantity ' . $db->quantity . ' to ' . $val['quantity'] . '</li>';
        }
        if($db->price != $val['price']){
            $action .= '<li>Updated the price $' . $db->price . ' to $' . $val['price'] . '</li>';
        }
        if($db->discount != $val['discount']){
            $action .= '<li>Updated the discount ' . $db->discount . '% to ' . $val['discount'] . '%</li>';
        }
        if($db->discount_from != $val['discount_from'] || $db->discount_to != $val['discount_to']){
            $action .= '<li>Updated the discount date ' . $db->discount_from . " - " . $db->discount_to . ' to ' . $val['discount_from'] . " - " . $val['discount_to'] . '</li>';
        }
        if($db->belowquantity != $val['belowquantity']){
            $action .= '<li>Updated the notify for quantity below ' . $db->belowquantity . ' to ' . $val['belowquantity'] .'</li>';
        }
        if($db->minquantity != $val['minquantity']){
            $action .= '<li>Updated the minimum quanity allowed in shopping cart ' . $db->minquantity . ' to ' . $val['minquantity'] .'</li>';
        }
        if($db->maxquantity != $val['maxquantity']){
            $action .= '<li>Updated the maximum quanity allowed in shopping cart ' . $db->maxquantity . ' to ' . $val['maxquantity'] .'</li>';
        }
        $status = $db->status == 1 ? 'Active' : 'Deactive';
        $status2 = $val['status'] == true ? 'Active' : 'Deactive';
        if($status != $status2){
            $action .= '<li>Updated the status "' . $status . '" to "' . $status2 .'"</li>';
        }
        if($db->color != $val['color']){
            $action .= '<li>Updated the color ' . $db->color . ' to ' . $val['color'] .'</li>';
        }
        if($db->slugs != $val['slugs']){
            $action .= '<li>Updated the Item slug "' . $db->slugs . '" to "' . $val['slugs'] .'"</li>';
        }
        return $action;
    }

    public function storeRedis($id,$token){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        try {
            // Cache data for 2 days
            $frontCache = new \Phalcon\Cache\Frontend\Data(array(
                "lifetime" => $app->config->redis->dataexpiration
            ));

            //Create the Cache setting redis connection options
            $cache = new \Phalcon\Cache\Backend\Redis($frontCache, array(
                'host' => $app->config->redis->host,
                'port' => $app->config->redis->port,
                'persistent' => $app->config->redis->persistent
            ));

            //Cache arbitrary data
            return $cache->save($app->config->redis->sessionkey . $id, $token);

        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function removeRedis($id){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        try {
            // Cache data for 2 days
            $frontCache = new \Phalcon\Cache\Frontend\Data(array(
                "lifetime" => $app->config->redis->dataexpiration
            ));

            //Create the Cache setting redis connection options
            $cache = new \Phalcon\Cache\Backend\Redis($frontCache, array(
                'host' => $app->config->redis->host,
                'port' => $app->config->redis->port,
                'persistent' => $app->config->redis->persistent
            ));

            //Cache arbitrary data
            return $cache->delete($app->config->redis->sessionkey . $id);
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function creditcardPayment($billinginfo, $payment, $paymenttype, $shippingfee, $tax, $totalamount, $invoiceno, $centerid, $description){
        $DI = \Phalcon\DI::getDefault();
        $dc = $DI->get('application');

        $donatorfName = $billinginfo['firstname'];
        $donatorlName = $billinginfo['lastname'];

        $post_url = "https://test.authorize.net/gateway/transact.dll";

        if($cred['shippinginfo']['firstname'] != ''){
            $totalamount = $totalamount + $shippingfee + $tax;
        }

        $post_values='';

        if($centerid != 'null'){
            // $c = Center::findFirst("centerid='$centerid'");
            // $loginname = $c->authorizeid;
            // $transactionkey = $c->authorizekey;
            $loginname = "87r3d7AsfaKH";
            $transactionkey = "59q76Pes524nCnFp";
        } else {
            $loginname = "7mx4FQ9Q";
            $transactionkey = "88wT7Z4rMUT38q34";
        }

        if($paymenttype=='card'){
            $post_values = array(

                // the API Login ID and Transaction Key must be replaced with valid values
                "x_login"           => $loginname,
                "x_tran_key"        => $transactionkey,
                "x_first_name" => $donatorfName,
                "x_last_name" => $donatorlName,

                "x_version"         => "3.1",
                "x_delim_data"      => "TRUE",
                "x_delim_char"      => "|",
                "x_relay_response"  => "FALSE",
                "x_invoice_num"     => $invoiceno,
                "x_type"            => "AUTH_CAPTURE",
                "x_method"          => "CC",
                "x_card_num"        => $payment['ccn'],
                "x_exp_date"        => sprintf("%02d", $payment['expiremonth']).substr( $payment['expireyear'], -2 ),
                "x_amount"          => $totalamount,
                "x_description"     => $donatorfName . ' ' . $donatorlName .' - '.$description,

                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net
            );

            $post_values["x_address"] = $billinginfo['address'];
            $post_values["x_city"] = $billinginfo['city'];
            $post_values["x_state"] = $billinginfo['state'];
            $post_values["x_zip"] = $billinginfo['zip'];
            $post_values["x_country"] = $billinginfo['country'];
        }else{
            $post_values = array(

                // the API Login ID and Transaction Key must be replaced with valid values
                "x_login"           => $loginname,
                "x_tran_key"        => $transactionkey,
                "x_first_name" => $donatorfName,
                "x_last_name" => $donatorlName,

                "x_version"         => "3.1",
                "x_delim_data"      => "TRUE",
                "x_delim_char"      => "|",
                "x_relay_response"  => "FALSE",
                "x_invoice_num"     => $invoiceno,

                "x_method"          => "ECHECK",
                "x_bank_aba_code"   => $payment['bankrouting'],
                "x_bank_acct_num"   => $payment['bankaccountnumber'],
                "x_bank_acct_type"  => $payment['at'],
                "x_bank_name"       => $payment['bankname'],
                "x_bank_acct_name"  => $payment['accountname'],


                "x_amount"          => $totalamount,
                "x_description"     => $donatorfName . ' ' . $donatorlName .' - '.$description,

                // Additional fields can be added here as outlined in the AIM integration
                // guide at: http://developer.authorize.net
            );

            if($cred['billinginfo']['at'] == 'BUSINESSCHECKING'){
                $post_values["x_echeck_type"] = 'CCD';
            }else{
                $post_values["x_echeck_type"] = 'WEB';
            }

            $post_values["x_address"] = $billinginfo['address'];
            $post_values["x_city"] = $billinginfo['city'];
            $post_values["x_state"] = $billinginfo['state'];
            $post_values["x_zip"] = $cbillinginfo['zip'];
            $post_values["x_country"] = $billinginfo['country'];

        }


        // This section takes the input fields and converts them to the proper format
        // for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
        $post_string = "";
        foreach( $post_values as $key => $value )
        { $post_string .= "$key=" . urlencode( $value ) . "&"; }
        $post_string = rtrim( $post_string, "& " );

        // The following section provides an example of how to add line item details to
        // the post string.  Because line items may consist of multiple values with the
        // same key/name, they cannot be simply added into the above array.
        //
        // This section is commented out by default.

//        $line_items = $items;
//
//        foreach( $line_items as $value )
//            { $post_string .= "&x_line_item=" . urlencode( $value ); }
//

        // This sample code uses the CURL library for php to establish a connection,
        // submit the post, and record the response.
        // If you receive an error, you may want to ensure that you have the curl
        // library enabled in your php configuration
        $request = curl_init($post_url); // initiate curl object
        curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
        $post_response = curl_exec($request); // execute curl post and store results in $post_response
        // additional options may be required depending upon your server configuration
        // you can find documentation on curl options at http://www.php.net/curl_setopt
        curl_close ($request); // close curl object

        // This line takes the response and breaks it into an array using the specified delimiting character
        $response_array = explode($post_values["x_delim_char"],$post_response);

        // The results are output to the screen in the form of an html numbered list.
        if($response_array[0]==1){
            return array('success'=>'Success.', 'code' => $response_array);
        }else{
            return array(
                'error'=>'Something went wrong in processing your transaction. Please check you Credit Card or verify your provider.',
                'code' => $response_array
            );
        }
    }

    public function mailTemplate($heading, $body){
        $content = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
        <html lang=en> <head><meta http-equiv=Content-Type content="text/html; charset=UTF-8"><meta name=viewport content="width=device-width, initial-scale=1">
        <!-- So that mobile will display zoomed in --><meta http-equiv=X-UA-Compatible content=IE=edge><!-- enable media queries for windows phone 8 -->
        <meta name=format-detection content=telephone=no><!-- disable auto telephone linking in iOS --><title>Body and Brain</title><style type=text/css>
        body{margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;}table{border-spacing: 0;}table td{border-collapse: collapse;}
        .ExternalClass{width: 100%;}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div {line-height: 100%;}
        .ReadMsgBody {width: 100%;background-color: #ebebeb;}table {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}img {-ms-interpolation-mode: bicubic;}
        .yshortcuts a {border-bottom: none !important;}@media screen and (max-width: 599px) {.force-row,.container {width: 100% !important;max-width: 100% !important;}}
        @media screen and (max-width: 400px) {.container-padding {padding-left: 12px !important;padding-right: 12px !important;}}.ios-footer a {color: #aaaaaa !important;
        text-decoration: underline;}</style></head> <body style="margin:0; padding:0;" bgcolor=#F0F0F0 leftmargin=0 topmargin=0 marginwidth=0 marginheight=0>
        <!-- 100% background wrapper (grey background) --> <table border=0 width=100% height=100% cellpadding=0 cellspacing=0 bgcolor=#F0F0F0> <tr>
        <td align=center valign=top bgcolor=#F0F0F0 style="background-color: #F0F0F0;"> <br> <!-- 600px container (white background) -->
        <table border=0 width=600 cellpadding=0 cellspacing=0 class=container style=width:600px;max-width:600px> <tr>
        <td class="container-padding header" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:24px;font-weight:bold;padding-bottom:12px;color:#49AFCD;padding-left:24px;padding-right:24px"> Body and Brain </td>
        </tr> <tr> <td class="container-padding content" align=left style=padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff> <br>
        <div class=title style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#49AFCD">'.$header.'</div> <br>

        <div class=body-text style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">'.$body.'<br><br></div>

        </td> </tr> <tr> <td class="container-padding footer-text" align=left style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
        <br><br> Copyright: © Body and Brain <br><br> <strong>Body and Brain</strong><br>
        <a href="http://www.bodynbrain.com" style=color:#aaaaaa>www.bodynbrain.com</a><br> <br><br> </td> </tr> </table> <!--/600px container --> </td> </tr> </table> <!--/100% background wrapper--> </body> </html>';
        return $content;
    }

    public function emailCarrier($carrier){
        if($carrier == '1'){
            return '@vtext.com';
        }
        else if($carrier == '2'){
            return '@txt.att.net';
        }
        else if($carrier == '3'){
            return '@messaging.sprintpcs.com';
        }
        else if($carrier == '4'){
            return '@tmomail.net';
        }
        else if($carrier == '5'){
            return '@vmobl.com';
        }
        else if($carrier == '6'){
            return '@mymetropcs.com';
        }
    }

    public function authorizeCheck($authorizeid, $authorizekey, $cardnumber, $expimonth, $expiyear, $totalprice, $sessionclass, $firstname, $lastname, $billadd1, $billstate, $billzip){
        $DI = \Phalcon\DI::getDefault();
        $app = $DI->get('application');
        $post_url = $app->config->authorizeposturl;

        $post_values = array(
          "x_login"           => $authorizeid,
          "x_tran_key"        => $authorizekey,
          "x_version"         => "3.1",
          "x_delim_data"      => "TRUE",
          "x_delim_char"      => "|",
          "x_relay_response"  => "FALSE",
          "x_type"            => "AUTH_CAPTURE",
          "x_method"          => "CC",
          "x_card_num"        => $cardnumber,
          "x_exp_date"        => $expimonth.$expiyear,
          "x_amount"          => $totalprice,
          "x_description"     => $sessionclass,
          "x_first_name"      => $firstname,
          "x_last_name"       => $lastname,
          "x_address"         => $billadd1,
          "x_state"           => $billstate,
          "x_zip"             => $billzip
          );

        $post_string = "";

        foreach( $post_values as $key => $value ){
          $post_string .= "$key=" . urlencode( $value ) . "&";
        }

        $post_string = rtrim( $post_string, "& " );

        $request = curl_init($post_url);
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
        $post_response = curl_exec($request);
        curl_close ($request);

        $response_array = explode($post_values["x_delim_char"],$post_response);
        $successpresponse = $response_array[0];

        if($successpresponse == 1){
            return array(
                'status'=>'success',
                'response' => $response_array
            );
        }
        else {
            return array(
                'status'=>'error',
                'response' => $response_array
            );
        }
    }

    public function workshopAuthorized($wsauth) {
      $DI = \Phalcon\DI::getDefault();
      $app = $DI->get('application');

      $post_url = $app->config->authorizeposturl;
      // $post_url = "https://test.authorize.net/gateway/transact.dll";
			// $post_url = "https://secure.authorize.net/gateway/transact.dll";
      $post_values = array(
          "x_login"           => $wsauth['x_login'],
          "x_tran_key"        => $wsauth['x_tran_key'],
          "x_version"         => "3.1",
          "x_delim_data"      => "TRUE",
          "x_delim_char"      => "|",
          "x_relay_response"  => "FALSE",
          "x_type"            => "AUTH_CAPTURE",
          "x_method"          => "CC",
          "x_card_num"        => $wsauth['x_card_num'],
          "x_exp_date"        => $wsauth['x_exp_date'],
          "x_amount"          => $wsauth['x_amount'],
          "x_description"     => $wsauth['x_description'],
          "x_first_name"      => $wsauth['x_first_name'],
          "x_last_name"       => $wsauth['x_last_name'],
          "x_address"         => $wsauth['x_address']
          // Additional fields can be added here as outlined in the AIM integration
          // guide at: http://developer.authorize.net
      );

			$post_string = "";
      foreach( $post_values as $key => $value ) {
        $post_string .= "$key=" . urlencode( $value ) . "&";
      }
      $post_string = rtrim( $post_string, "& " );

      $request = curl_init($post_url); // initiate curl object
          curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
          curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
          curl_setopt($request, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
          curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
          curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
          $post_response = curl_exec($request); // execute curl post and store results in $post_response
          // additional options may be required depending upon your server configuration
          // you can find documentation on curl options at http://www.php.net/curl_setopt
      curl_close ($request); // close curl object

      return explode($post_values["x_delim_char"],$post_response);
    }

    public function workshopEmailToRegistrant($arr) {
      $map = "http://maps.googleapis.com/maps/api/staticmap?center=".$arr['lat'].", ".$arr['lon']."&zoom=9&size=600x300&maptype=roadmap&markers=color:red%7Clabel:C%7C".$arr['lat'].", ".$arr['lon'];
      $body = '
      <!DOCTYPE html>
      <html>
      <head>
      <title></title>
      <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
      <style>
      @media screen and (max-width:720px) {
        .logo {
          float:none;
          display:block;
          width:100%;
          height:auto;
        }
      }
      </style>
      </head>
      <body style="font-family: "Droid Sans">
      <div style="display:block; width:90%; margin:auto;">
      <div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
      <center><img src="http://bnb.gotitgenius.com/img/frontend/logo.gif"> <br>
      Phone : 1-877-477-YOGA <br>
      E-mail : customerservice@bodynbrain.com
      </center>
      <hr style="border: 0; height: 1px; background: #333; background-image: linear-gradient(to right, #ccc, #333, #ccc);">
      <h1 style="color:#f15d22" align=center>Workshop Registrant Confirmation</h1>

      <div style="width:50%; border:1px solid #999; margin:auto; padding:2%;" class="letter">
      Dear '.$arr['registrant_name'].', <br>
      <br>
      Congratulations! You have made and appointment to try
      <b>"'.$arr['workshoptitle'].'"</b> Workshop. <br>
      Get Ready for less stress and more energy! <br>
      We will contact you soon to confirm your appointment. <br>
      Please be sure to arrive 15 minutes before the workshop. <br>
      <br>
      Please print this page for your records.
      If you have any questions,please contact us at:
      Phone: '.$arr['venue_contact'].' or Visit our website: '.$arr['venue_web'].'
      Your confirmation number is '.$arr['confirmation'].' <br>
      <br>
      We look forward seeing you. Thank you!<br>
      <br><br>
      Truly yours,<br>
      <br>
      <b>The BodynBrain.com Team</b>
      </div>

      <h2 style="background:#00adbb; padding:.5%; color:white">Registrant Information</h2>
      <h3 style="padding-left:20px;">Name</h3>
      <span style="padding-left:30px">'.$arr['registrant_name'].'</span>

      <h3 style="padding-left:20px;">Address</h3>
      <span style="padding-left:30px">'.$arr['registrant_address'].'</span>

      <h2 style="background:#00adbb; padding:.5%; color:white">Payment Information</h2>
      <h3 style="padding-left:20px;">Creditcard number</h3>
      <span style="padding-left:30px">'.$arr['ccnumber'].'</span>

      <h3 style="padding-left:20px;">Confirmation number</h3>
      <span style="padding-left:30px">'.$arr['confirmation'].'</span>

      <h3 style="padding-left:20px;">Payment</h3>
      <span style="padding-left:30px">Paid $'.$arr['cash'].' via '.$arr['cctype'].'</span>

      <h2 style="background:#00adbb; padding:.5%; color:white">Workshop Information</h2>
      <h3 style="padding-left:20px;">What</h3>
      <span style="padding-left:30px">'.$arr['workshoptitle'].'</span>

      <h3 style="padding-left:20px;">Venue</h3>
      <span style="padding-left:30px">'.$arr['centertitle'].'</span>

      <h3 style="padding-left:20px;">Address</h3>
      <span style="padding-left:30px">'.$arr['venue_address'].'</span>

      <h3 style="padding-left:20px;">Phone</h3>
      <span style="padding-left:30px">'.$arr['venue_contact'].'</span>

      <h3 style="padding-left:20px;">Map</h3>
      <br>
      <img src="'.$map.'" width="100%" height="auto">
      </div>
      </div>
      </body>
      </html>';

      return $body;
    }

    public function workshopEmailToBnb($arr) {
      // 4. Workshop Schedule: '.$arr['workshop_schedule'].' <br>

      $body = '
      <!DOCTYPE html>
      <html>
      <head>
      <title></title>
      <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
      <style>
      @media screen and (max-width:720px){
        .logo {
          float:none;
          display:block;
          width:100%;
          height:auto;
        }
      }
      </style>
      </head>
      <body style="font-family: "Droid Sans">
      <div style="display:block; width:90%; margin:auto;">
      <div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
      <center>
      <img src="http://bnb.gotitgenius.com/img/frontend/logo.gif">
      </center>
      </div>
      <div style="width:90%; border:1px solid #999; margin:auto; padding:2%;">
      Dear '.$arr["centertitle"].' Manager, <br>
      <br>
      Congratulations. You\'ve just got a/an <b><i>'.$arr['workshoptitle'].'</i></b> Sign-Up through BodynBrain.com. <br>
      <br>
      Please find the details below : <br>
      1. Name: '.$arr['registrant_name'].' <br>
      2. Address: '.$arr['registrant_address'].' <br>
      3. Email: '.$arr['registrant_email'].' <br>
      4. Phone: '.$arr['registrant_contact'].' <br>
      5. Payment: Paid( '.$arr['payment'].' ) <br>
      6. Confirmation Number: '.$arr['confirmation'].'
      <br>
      Please contact this person to confirm the appointment as soon as possible.  <br>
      <br>
      Thank you <br>
      <br>
      <b>BodynBrain.com</b><br>
      </div>
      </div>
      </body>
      </html>';

      return $body;
    }

    // public function GET_esign($url) {
    //   $curl = curl_init($url);
    //   $headers = array(
    //     'Content-Type:application/json',
    //     'Authorization: Basic ek5OaWJIdVpaRHc0OmxRbG5VbnNtRk5NUQ=='
    //   );
    //   curl_setopt($curl, CURLOPT_HTTPHEADER,$headers);
    //   // curl_setopt($curl, CURLOPT_POST,       true);
    //   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //   $curl_response = curl_exec($curl);
    //   if ($curl_response === false) {
    //       $info = curl_getinfo($curl);
    //       curl_close($curl);
    //       die('error occured during curl exec. Additional info: ' . var_export($info));
    //   }
    //   curl_close($curl);
    //   return json_encode(json_decode($curl_response));
    // }
    //
    // public function POST_esign($url) {
    //   $curl = curl_init($url);
    //   $headers = array(
    //     'Content-Type:application/json',
    //     'Authorization: Basic ek5OaWJIdVpaRHc0OmxRbG5VbnNtRk5NUQ=='
    //   );
    //   curl_setopt($curl, CURLOPT_HTTPHEADER,$headers);
    //   curl_setopt($curl, CURLOPT_POST,       true);
    //   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //   $curl_response = curl_exec($curl);
    //   if ($curl_response === false) {
    //       $info = curl_getinfo($curl);
    //       curl_close($curl);
    //       die('error occured during curl exec. Additional info: ' . var_export($info));
    //   }
    //   curl_close($curl);
    //   return json_decode($curl_response);
    // }
}
