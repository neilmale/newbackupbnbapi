<?php

namespace Controllers;
use \Phalcon\Http\Request;
use \Models\Center as Center;
use \Models\States as States;
use \Models\Testimonies as Testimonies;
use \Models\Workshoptitle as Workshoptitle;
use \Models\News as News;
use \Models\Notification as Notification;
use \Controllers\ControllerBase as CB;

class TestimoniesController extends \Phalcon\Mvc\Controller {

    public function submitStoryAction() {
  		$request = new Request();
      $errors = array();
      $warnings = array();
      $warnings[0] = "NO WARNING";
      $data['error'] = false;
      $data['warning'] = false;

  		if($request->isPost()) {
  			$testimonyID = CB::genGUID();
  			$fullname 	 = ucwords(strtolower($request->getPost('firstname') . " " . $request->getPost('lastname')));

  			$details 	 = $request->getPost('message');
  			$photo 	   = $request->getPost('photo');
        $email 	   = $request->getPost('email');
        $subject	 = $request->getPost('subject');
        $center    = $request->getPost('center')["centerid"];

  			$publish = new Testimonies();
        $publish->id = $testimonyID;
        $publish->author = $fullname;
        $publish->email = $email;
        $publish->subject = $subject;
        $publish->details = $details;
        $publish->photo = $photo;
        $publish->spotlight = 0;
        $publish->status = 0;
        $publish->center = $center;
        $publish->center_status = 0;
        $publish->date_submitted = date('Y-m-d');
        $publish->date_updated = date('Y-m-d');

  			if (!$publish->save()) {
  				foreach ($publish->getMessages() as $message) {
  					$errors[] = $message->getMessage();
  				}
          $data['error'] = true;
  			} else {
            $data['msg'] = "Success! Testimony has been saved.";

            $notificationid = CB::genGUID();
            $notification = new Notification();
            $notification->notificationid = $notificationid;
            $notification->centerid = $center;
            $notification->itemid = $testimonyID;
            $notification->name = $fullname;
            $notification->datecreated = date('Y-m-d H:i:s');
            $notification->adminstatus = 0;
            $notification->regionstatus = 0;
            $notification->centerstatus = 0;
            $notification->type = 'story';

            if (!$notification->save()) {
              foreach ($notification->getMessages() as $message) {
                $warnings[] = $message->getMessage();
              }
              $data['warning'] = true;
            }

            $body0 = '
            <html>
            <head>
            <title></title>
            <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
            <style>
            @media screen and (max-width:720px){
                .logo {
                    float:none;
                    display:block;
                    width:100%;
                    height:auto;
                }
            }
            </style>
            </head>
            <body style="font-family: "Droid Sans">
            <div style="display:block; width:80%; margin:auto;">
                <div style="display:inline-block; width:100%;">
                <img src="' . CB::getConfig()['application']['baseURL'] .'/img/frontend/logo.gif" style="float:right; margin-bottom:20px">
                </div>
                <div style="display:inline-block; width:100%; padding:2%; border:1px solid #ccc;">
                Dear '.$fullname.', <br>
                <br>
                This is a confirmation email that Body & Brain has received your Success Story submission. <br>
                Your story will be posted online at www.BodynBrain.com, under "Success Stories" in about 3 business days. <br>
                <br>
                We truly appreciate and recognized your courage to share your story. It will be of great inspiration to not only practitioners but also prospective members. Thank you so much for sharing your experience with us! <br>
                <br><br>
                Sincerely, <br>
                <br>
                <b>The BodynBrain.com Team</b>
                </div>
            </div>
            </body>
            </html>
            ';
            $send = CB::sendMail($email,'BodynBrain: Success Story Confirmation Email',$body0);

            if($center) {
              $c = Center::findFirst("centerid='$center'");
              $center = ' at center: ' . $c->centertitle . ', ' . $c->centerstate;
            } else {
              $center = ' at center: no center';
            }
            $body1 = '
            <html>
            <head>
            <title></title>
            <link href="http://fonts.googleapis.com/css?family=Droid+Sans|Droid+Sans+Mono|Droid+Serif" rel="stylesheet" type="text/css">
            <style>
            @media screen and (max-width:720px){
                .logo {
                    float:none;
                    display:block;
                    width:100%;
                    height:auto;
                }
            }
            </style>
            </head>
            <body style="font-family: "Droid Sans">
            <div style="display:block; width:80%; margin:auto;">
                <div style="display:inline-block; width:100%;">
                    <img src="' . CB::getConfig()['application']['baseURL'] . '/img/frontend/logo.gif" style="float:right; margin-bottom:20px">
                </div>
                <div style="width:100%; border:1px solid #999; margin:auto; padding:2%;">
                Dear Manager, <br>
                <br>
                Congratulations! <br>
                One of your members, <b><u>'.$fullname.'</u></b> has submitted a Success Story to BodynBrain.com' . $center . '. <br>
                <br>
                Please take a few minutes to review the content to make sure that it is clear, factual and non-controversial. Headquarters also performs its own review process, so it may take several days to see the Success Story online. After about 5 business days, you can check the published Success Story on www.bodynbrain.com under "SUCCESS STORIES." <br>
                <br>
                Thank you for your high-quality member care that encouraged this testimonial! <br>
                <br><Br>
                Sincerely, <br>
                <br>
                <b>The BodynBrain.com Team</b>
                </div>
            </div>
            </body>
            </html>
            ';

            $subject =  "BodynBrain: New Success Story submitted from NEW DESIGN";
            $DI = \Phalcon\DI::getDefault();
            $app = $DI->get('application');
            foreach($app->config->email as $bnbemail) {
                $send = CB::sendMail($bnbemail,$subject,$body1);
            }
  			}

        $data['warningMsg'] = $warnings;
        $data['errorMsg'] = $errors;
  		} else {
        $data['error'] = true;
  			$data['errorMsg'] = "NO POST DATA";
  		}
      echo json_encode($data);
  	}

	public function manageStoriesAction($num, $page, $keyword) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $offsetfinal = ($page * 10) - 10;
        if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {

            $stmt = $db->prepare("SELECT testimonies.ssid, testimonies.id, testimonies.spotlight, testimonies.author, testimonies.status, testimonies.email, testimonies.state, testimonies.subject, testimonies.date_submitted, testimonies.date_published, center.centertitle, center.centerstate FROM testimonies LEFT JOIN center on testimonies.center = center.centerid ORDER BY testimonies.date_submitted DESC LIMIT " . $offsetfinal . ",10");
            $stmt->execute();
            $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            foreach($searchresult as $key => $value) {
                $searchresult[$key]['subject'] = utf8_encode($searchresult[$key]['subject']);
            }

           // var_dump($searchresult);
           $stmt1 = $db->prepare("SELECT testimonies.ssid, testimonies.id, testimonies.details, testimonies.status, testimonies.spotlight, testimonies.author, testimonies.status, testimonies.email, testimonies.state, testimonies.subject, testimonies.date_submitted, testimonies.date_published, center.centertitle, center.centerstate FROM testimonies LEFT JOIN center on testimonies.center = center.centerid");
           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalNumberOfStories = count($searchresult1);
        } else {

            $db = \Phalcon\DI::getDefault()->get('db');
            $stmt = $db->prepare("SELECT testimonies.ssid, testimonies.id, testimonies.status, testimonies.spotlight, testimonies.author, testimonies.status, testimonies.email, testimonies.state, testimonies.subject, testimonies.date_submitted, testimonies.date_published, center.centertitle, center.centerstate FROM testimonies LEFT JOIN center on testimonies.center = center.centerid  WHERE (subject LIKE '%$keyword%' OR center.centertitle LIKE '%$keyword%' OR author LIKE '%$keyword%' OR ssid LIKE '%$keyword%' ) ORDER BY testimonies.date_submitted DESC LIMIT " . $offsetfinal . ",10");
            $stmt->execute();
            $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            foreach($searchresult as $key => $value) {
                $searchresult[$key]['subject'] = utf8_encode($searchresult[$key]['subject']);
            }

            $db1 = \Phalcon\DI::getDefault()->get('db');
            $stmt1 = $db1->prepare("SELECT testimonies.ssid, testimonies.id, center.centertitle FROM testimonies LEFT JOIN center on testimonies.center = center.centerid WHERE (subject LIKE '%".$keyword."%' OR center.centertitle LIKE '%".$keyword."%' OR author LIKE '%".$keyword."%' OR ssid LIKE '%$keyword%') ");
            $stmt1->execute();
            $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

            $totalNumberOfStories = count($searchresult1);
        }
        echo json_encode(array('total_items' => $totalNumberOfStories, 'data' => $searchresult, 'index' => $page));
    }

    public function manageStoriesPerCenterAction($centerid, $num, $page, $keyword) {
        if ($keyword == 'null' || $keyword == 'undefined' || $keyword == '') {
            $offsetfinal = ($page * 10) - 10;
            $db = \Phalcon\DI::getDefault()->get('db');
            $stmt = $db->prepare("SELECT testimonies.id, testimonies.status, testimonies.spotlight, testimonies.author, testimonies.status, testimonies.email, testimonies.state, testimonies.subject, testimonies.date_submitted, testimonies.date_published, center.centertitle, center.centerstate FROM testimonies LEFT JOIN center on testimonies.center = center.centerid WHERE center = '$centerid' ORDER BY testimonies.date_submitted DESC LIMIT $offsetfinal,10");
            $stmt->execute();
            $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            foreach($searchresult as $key => $value) {
                $searchresult[$key]['subject'] = utf8_encode($searchresult[$key]['subject']);
            }

           $db1 = \Phalcon\DI::getDefault()->get('db');
           $stmt1 = $db1->prepare("SELECT testimonies.id, center.centertitle FROM testimonies LEFT JOIN center on testimonies.center = center.centerid WHERE center = '$centerid' ");
           $stmt1->execute();
           $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

           $totalNumberOfStories = count($searchresult1);
        } else {
            $offsetfinal = ($page * 10) - 10;

            $db = \Phalcon\DI::getDefault()->get('db');
            $stmt = $db->prepare("SELECT testimonies.id, testimonies.status, testimonies.spotlight, testimonies.author, testimonies.status, testimonies.email, testimonies.state, testimonies.subject, testimonies.date_submitted, testimonies.date_published, center.centertitle, center.centerstate FROM testimonies LEFT JOIN center on testimonies.center = center.centerid  WHERE (subject LIKE '%$keyword%' OR author LIKE '%$keyword%' ) AND center = '$centerid' ORDER BY testimonies.date_submitted DESC LIMIT $offsetfinal,10");
            $stmt->execute();
            $searchresult = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            foreach($searchresult as $key => $value) {
                $searchresult[$key]['subject'] = utf8_encode($searchresult[$key]['subject']);
            }

            $db1 = \Phalcon\DI::getDefault()->get('db');
            $stmt1 = $db1->prepare("SELECT testimonies.id, testimonies.subject, center.centertitle FROM testimonies LEFT JOIN center on testimonies.center = center.centerid WHERE center = '$centerid'");

            $stmt1->execute();
            $searchresult1 = $stmt1->fetchAll(\PDO::FETCH_ASSOC);

            $totalNumberOfStories = count($searchresult1);
        }

        echo json_encode(array('data' => $searchresult, 'index' => $page, 'total_items' => $totalNumberOfStories));

    }

    public function getStoryAction($storyid) {

    	$storyReviewed = Testimonies::findFirst("id = '$storyid' ");
    	if($storyReviewed->status == 0) {
    		$storyReviewed->assign(array(
    			"status" => 2
    			));

    		if(!$storyReviewed->save()) {
    			$errors[] = array();
    			foreach($storyReviewed->getMessages as $message) {
    				$errors[] = $message->getMessages();
    				echo json_encode(array('result' => $errors));
    			}
    		}
    	}

          $query = "SELECT testimonies.*, center.centertitle, states.state as statename FROM testimonies LEFT JOIN center on testimonies.center = center.centerid LEFT JOIN states ON testimonies.state = states.state_code WHERE testimonies.id = '".$storyid."' ";
    	    $getStory = CB::bnbQueryFirst($query);

          $getStory['details'] = utf8_encode( $getStory['details']);
          $getStory["metadesc"] = utf8_encode($getStory["metadesc"]);
          $getStory["metatags"] = utf8_encode($getStory["metatags"]);
          $getStory['metatitle'] = utf8_encode($getStory['metatitle']);
          $getStory["subject"] = utf8_encode($getStory["subject"]);

          $data['story'] = $getStory;

          $centers = Center::find(array("order"=>"centertitle","columns" => "centerid, centertitle, centerslugs"));
          $data['centers'] = $centers->toArray();

    	    echo json_encode($data);

    }

    public function saveReviewedStoryAction() {
      $db = \Phalcon\DI::getDefault()->get('db');
    	$request = new \Phalcon\Http\Request();
    	if($request->isPost()) {

            $id = $request->getPost('id');

            $photo = $request->getPost('photo');
    		    $status	= $request->getPost('status');
            $author = $request->getPost('author');
            $email = $request->getPost('email');
            // $age = $request->getPost('age');
            $center = $request->getPost('center');
                $find_center = Center::findFirst("centerid = '".$center."'");
                if($find_center) {
                    $state = $find_center->centerstate;
                }
            $subject = $request->getPost('subject');
            $details = $request->getPost('details');
            // $center_status = $request->getPost('center_status');
            // $spotlight = $request->getPost('spotlight');
            $metatitle = $request->getPost('metatitle');
            $metadesc = $request->getPost('metadesc');
            $date_submitted = $request->getPost('date_submitted');
            $date_published = $request->getPost('date_published');
            // DATE FORMATTING
             /*if user used the default date and didn't change the date in frontend
             Already ('Y-m-d') format if unchange */
             if(strlen($date_submitted) > 10) {
                 $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                 $dates = explode(" ", $date_submitted);
                 $date_submitted = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
             }
             //DATE FORMATTING ends

             if(strlen($date_published) > 10) {
                 $mont0 = array('Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04', 'May' => '05', 'Jun' => '06', 'Jul' => '07', 'Aug' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dec' => '12');
                 $dates = explode(" ", $date_published);
                 $date_published = $dates[3].'-'.$mont0[$dates[1]].'-'.$dates[2];
             }

            if ($date_published == null || $date_published == undefined || $date_published == '' || $date_published == '0000-00-00' || $date_published == 'Invalid Date'){
                if($status == 1) {
                    $finaldate = date("Y-m-d");
                } else {
                    $finaldate = null;
                }
            }
            else{
                 $finaldate = $date_published;
            }


            $saveThis = Testimonies::findFirst("id = '".$id."' "); //source for saving
            $metatags = $saveThis->metatags;
            if($metatags == '' || $metatags == null || $metatitle == '' || $metatitle == null || $metadesc == '' || $metadesc == null)
            {
                $optimized = 0;
            } else
            {
                $optimized = 1;
            }
            // if($status == 1) {
                $saveThis->assign(array(
                    'photo' => $photo,
                    'author' => $author,
                    'email' => $email,
                    // 'age' => $age,
                    'subject' => $subject,
                    'details' => $details,
                    'status' => $status,
                    'center' => $center,
                    'state' => $state,
                    // 'center_status' => $center_status,
                    // 'spotlight' => $spotlight,
                    'metatitle' => $metatitle,
                    'metadesc' => $metadesc,
                    'optimized' => $optimized,
                    'date_submitted' => $date_submitted,
                    'date_updated' => date('Y-m-d'),
                    'date_published' => $finaldate
                ));
                if($saveThis->save()) {
                    $data['msg'] = "UPDATED!"; $data['type'] = "success"; $data['date_published'] = $finaldate;
                }
                else {
                    $errors[] = array();
                    foreach($saveThis->getMessages as $message) {
                        $errors[] = $message->getMessages();
                    }
                    $data['msg'] = $errors[0]; $data['type'] = "danger";
                }

            $stmt = $db->prepare("SELECT testimonies.*, center.centertitle FROM testimonies LEFT JOIN center on testimonies.center = center.centerid WHERE id = '$id' ");
            $stmt->execute();
            $getStory = $stmt->fetch(\PDO::FETCH_ASSOC);
            $data['story'] = $getStory;
            echo json_encode($data);
    	}
    }

    public function deleteStoryAction($storyid) {
        $thisStory = Testimonies::findFirst("id = '$storyid' ");
        if($thisStory) {
            if($thisStory->delete()) {
                $data = array("result" => "STORY HAS BEEN DELETED!" );
            } else {
                $data = array("result" => "THERE IS AN ERROR DELETING THIS STORY!" );
            }
            echo json_encode($data);
        } else {
            echo json_encode("NO STORY FOUND!");
        }
    }

    public function FEsuccessstoriespageviewAction($storyid) {
        // $mainnews = News::find(" status = 1 ORDER BY views DESC LIMIT 5 ");
        $story = Testimonies::findFirst("ssid = '$storyid' ");

        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt1 = $db->prepare("SELECT * FROM testimonies LEFT JOIN center ON testimonies.center = center.centerid WHERE testimonies.ssid = '$storyid' ");
        $stmt1->execute();
        $story = $stmt1->fetch(\PDO::FETCH_ASSOC);

        if($story['relatedworkshop'] != '' || $story['relatedworkshop'] != null || $story['metatags'] != '' || $story['metatags'] != null) {
            if($story['metatags'] == null || $story['metatags'] == '') {
                $room = 1;
                $related = preg_replace('/[^A-Za-z0-9\-\s]/', '|', $story['relatedworkshop']);
                $related = preg_replace('/^([^a-zA-Z0-9])*/', '', $related);
                $stmt2 = $db->prepare("SELECT * FROM testimonies WHERE ssid != '".$storyid."' AND relatedworkshop RLIKE '".$related."' AND status = 1 ORDER BY date_published DESC LIMIT 5");
            } elseif ($story['relatedworkshop'] == null || $story['relatedworkshop'] == '') {
                $room = 2;
                $relatedtags = preg_replace('/[^A-Za-z0-9\-\s]/', '|', $story['metatags']);
                $relatedtags = preg_replace('/^([^a-zA-Z0-9])*/', '', $relatedtags);
                $stmt2 = $db->prepare("SELECT * FROM testimonies WHERE ssid != '".$storyid."' AND metatags RLIKE '".$relatedtags."' AND status = 1 ORDER BY date_published DESC LIMIT 5");
            } else {
                $room = 3;
                $related = preg_replace('/[^A-Za-z0-9\-\s]/', '|', $story['relatedworkshop']);
                $related = preg_replace('/^([^a-zA-Z0-9])*/', '', $related);

                // $relatedtags = str_replace(' ', '', $story['metatags']);
                $relatedtags = preg_replace('/[^A-Za-z0-9\-\s]/', '|',  $story['metatags']);
                $relatedtags = preg_replace('/^([^a-zA-Z0-9])*/', '', $relatedtags);
                $stmt2 = $db->prepare("SELECT * FROM testimonies WHERE ssid != '".$storyid."'  AND (relatedworkshop RLIKE '".$related."' OR metatags RLIKE '".$relatedtags."') AND status = 1 ORDER BY date_published DESC LIMIT 5");
            }

            $stmt2->execute();
            $relatedstory = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            $relatedstory = array();
        }

        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND  status = 1 ORDER BY news.views DESC LIMIT 0,5");
        $stmt->execute();
        $popularnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($popularnews as $key => $value) {
            $popularnews[$key]['title'] = utf8_encode( $popularnews[$key]['title']);
            $popularnews[$key]["body"] = utf8_encode($popularnews[$key]["body"]);
            $popularnews[$key]['description'] = utf8_encode($popularnews[$key]['description']);
            $popularnews[$key]["newsslugs"] = utf8_encode($popularnews[$key]["newsslugs"]);
        }

        echo json_encode(array(
            "mainnews" => $popularnews,
            "story" => $story,
            "relatedstory" => $relatedstory
        ));
    }

    public function viewStoryDetailsAction($storyid) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $stmt1 = $db->prepare("SELECT * FROM testimonies LEFT JOIN center ON testimonies.center = center.centerid WHERE testimonies.ssid = '$storyid' ");
        $stmt1->execute();
        $story = $stmt1->fetch(\PDO::FETCH_ASSOC);

        if($story['relatedworkshop'] != '' || $story['relatedworkshop'] != null || $story['metatags'] != '' || $story['metatags'] != null) {
            if($story['metatags'] == null || $story['metatags'] == '') {
                $room = 1;
                $related = preg_replace('/[^A-Za-z0-9\-\s]/', '|', $story['relatedworkshop']);
                $related = preg_replace('/^([^a-zA-Z0-9])*/', '', $related);
                $stmt2 = $db->prepare("SELECT * FROM testimonies WHERE ssid != '".$storyid."' AND relatedworkshop RLIKE '".$related."' AND status = 1 ORDER BY date_published DESC LIMIT 5");
            } elseif ($story['relatedworkshop'] == null || $story['relatedworkshop'] == '') {
                $room = 2;
                $relatedtags = preg_replace('/[^A-Za-z0-9\-\s]/', '|', $story['metatags']);
                $relatedtags = preg_replace('/^([^a-zA-Z0-9])*/', '', $relatedtags);
                $stmt2 = $db->prepare("SELECT * FROM testimonies WHERE ssid != '".$storyid."' AND metatags RLIKE '".$relatedtags."' AND status = 1 ORDER BY date_published DESC LIMIT 5");
            } else {
                $room = 3;
                $related = preg_replace('/[^A-Za-z0-9\-\s]/', '|', $story['relatedworkshop']);
                $related = preg_replace('/^([^a-zA-Z0-9])*/', '', $related);

                // $relatedtags = str_replace(' ', '', $story['metatags']);
                $relatedtags = preg_replace('/[^A-Za-z0-9\-\s]/', '|',  $story['metatags']);
                $relatedtags = preg_replace('/^([^a-zA-Z0-9])*/', '', $relatedtags);
                $stmt2 = $db->prepare("SELECT * FROM testimonies WHERE ssid != '".$storyid."'  AND (relatedworkshop RLIKE '".$related."' OR metatags RLIKE '".$relatedtags."') AND status = 1 ORDER BY date_published DESC LIMIT 5");
            }

            $stmt2->execute();
            $relatedstory = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            $relatedstory = array();
        }

        echo json_encode(array("story" => $story, "relatedstory" => $relatedstory));
    }

    public function storyaddmetatagAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $storyid = $request->getPost('storyid');

            $newmetatag = preg_replace('/^([^a-zA-Z0-9])*/', '', $request->getPost('metatag'));

            $add = Testimonies::findFirst(" id = '$storyid' ");
            $metatitle = $add->metatitle;
            $metadesc = $add->metadesc;
            if($metatitle == '' || $metatitle == null || $metadesc == '' || $metadesc == null)
            {
                $optimized = 0;
            } else
            {
                $optimized = 1;
            }

            if($add) {
                $add->metatags = $add->metatags.",".$newmetatag;
                $add->optimized = $optimized;
                if($add->save()) {
                    echo json_encode(array("result" => "Successful!", "optimized" => $optimized));
                } else {
                    echo json_encode(array("result" => "Error!"));
                }
            }
        }
    }

    public function storymetatagsAction($storyid) {
        $select = Testimonies::findFirst(" id = '".$storyid."' ");
        echo json_encode($select->metatags);
    }

    public function storyupdatemetatagAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $storyid = $request->getPost('storyid');
            $metatags = $request->getPost('metatags');
            $update = Testimonies::findFirst(" id = '$storyid' ");
            $status = $update->status;
            $metatitle = $update->metatitle;
            $metadesc = $update->metadesc;
            if($metatags == '' || $metatags == null || $metatitle == '' || $metatitle == null || $metadesc == '' || $metadesc == null)
            {
                $optimized = 0;
                $status = 0;
            } else
            {
                $optimized = 1;
            }

            if($update) {
                $update->metatags = $metatags;
                $update->status = $status;
                $update->optimized = $optimized;
                if($update->save()) {
                    echo json_encode(array("result" => "Successful!", "optimized" => $optimized));
                } else {
                    echo json_encode(array("result" => "Error!"));
                }
            }
        }
    }

    public function workshoptitlesAction($storyid) {
        $workshoptitles = Workshoptitle::find();
        $titles = array();
        foreach($workshoptitles as $title) {
            $titles[] = $title->titleslugs;
        }

        $story = Testimonies::findFirst(" id = '$storyid' ");
        $related = $story->relatedworkshop;

        echo json_encode(array(
            "titles" => $titles,
            "related" => $related
        ));
    }

    public function storyaddrelatedAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $storyid = $request->getPost('storyid');
            $title = $request->getPost('title');

            $find = Testimonies::findFirst(" id = '$storyid' ");
            $find->relatedworkshop = $find->relatedworkshop .",".$title;
            if($find->save()) {
                echo json_encode(array("result" => "Successful!"));
            } else {
                echo json_encode(array("result" => "Error!"));
            }
        }
    }

    public function storyremoverelatedAction() {
        $request = new \Phalcon\Http\Request();
        if($request->isPost()) {
            $storyid = $request->getPost('storyid');
            $related = $request->getPost('related');

            $find = Testimonies::findFirst(" id = '$storyid' ");
            if($find) {
                $find->relatedworkshop = $related;
                if($find->save()) {
                    echo json_encode(array("result" => "Successful!"));
                } else {
                    echo json_encode(array("result" => "Error!"));
                }
            }
        }
    }

    public function fetagAction($tag, $offset) {
        $db = \Phalcon\DI::getDefault()->get('db');
        $finaloffset = ($offset * 10) - 10;

        //STORIES SLIDER
        $stmt1 = $db->prepare("SELECT testimonies.ssid,testimonies.id, testimonies.photo, testimonies.author, testimonies.subject, testimonies.state, testimonies.metadesc, center.centertitle, center.centerslugs FROM testimonies LEFT JOIN center ON testimonies.center = center.centerid WHERE testimonies.status = 1 AND testimonies.spotlight = 1 ORDER BY testimonies.date_published DESC LIMIT 10 ");
        $stmt1->execute();
        $spotlights = $stmt1->fetchAll(\PDO::FETCH_ASSOC);
            foreach($spotlights as $key => $value) {
                $spotlights[$key]['metadesc'] = utf8_encode($spotlights[$key]['metadesc']);
            }

        $stmt2 = $db->prepare("SELECT testimonies.ssid,testimonies.id, testimonies.photo, testimonies.author, testimonies.subject, testimonies.state, testimonies.metadesc, center.centertitle, center.centerslugs FROM testimonies LEFT JOIN center on testimonies.center = center.centerid WHERE testimonies.status = 1 AND testimonies.metatags LIKE '%".$tag."%' ORDER BY testimonies.date_published DESC LIMIT $finaloffset, 10 ");
        $stmt2->execute();
        $stories = $stmt2->fetchAll(\PDO::FETCH_ASSOC);
            foreach($stories as $key => $value) {
                $stories[$key]['metadesc'] = preg_replace('/[^A-Za-z0-9\-]/', ' ', $stories[$key]['metadesc']);
            }

        //TOTAL STORIES COUNT
        $stmt4 = $db->prepare("SELECT id FROM testimonies WHERE status = 1 AND metatags LIKE '%".$tag."%' ");
        $stmt4->execute();
        $totalstories = $stmt4->fetchAll(\PDO::FETCH_ASSOC);

        //show total news category item PAGINATION BAR
        $stmt3 = $db->prepare("SELECT id FROM testimonies WHERE status = 1 AND testimonies.metatags LIKE '%".$tag."%' ");
        $stmt3->execute();
        $totalpage = $stmt3->fetchAll(\PDO::FETCH_ASSOC);
        // echo json_encode(count($totalpage));

        // $mainnews = News::find(" status = 1 ORDER BY views DESC LIMIT 5 ");
        $stmt = $db->prepare("SELECT * FROM news INNER JOIN newscategory ON news.category=newscategory.categoryid WHERE featurednews = 0 and categoryid != 9 AND  status = 1 ORDER BY news.views DESC LIMIT 0,5");
        $stmt->execute();
        $popularnews = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($popularnews as $key => $value) {
            $popularnews[$key]['title'] = utf8_encode( $popularnews[$key]['title']);
            $popularnews[$key]["body"] = utf8_encode($popularnews[$key]["body"]);
            $popularnews[$key]['description'] = utf8_encode($popularnews[$key]['description']);
            $popularnews[$key]["newsslugs"] = utf8_encode($popularnews[$key]["newsslugs"]);
        }

        echo json_encode(array(
            "tag" => $tag,
            "totalstories" => count($totalstories),
            "spotlights" => $spotlights,
            "stories" => $stories,
            "totalpage" => count($totalpage),
            "mainnews" => $popularnews
        ));
    }

    public function _indexListAction($offset) {
        $finaloffset = ($offset * 10) - 10;
        $stories = CB::bnbQuery("SELECT testimonies.ssid, testimonies.photo, testimonies.author, testimonies.subject, testimonies.metadesc
                              FROM testimonies
                              WHERE testimonies.status = 1
                              ORDER BY testimonies.date_published DESC");

        if($stories == true) {
          foreach($stories as $key => $value) {
            $stories[$key]['metadesc'] = utf8_encode($stories[$key]['metadesc']);
          }
          $data['storiesCount'] = count($stories);

          $storiesByTen = array_slice($stories, $finaloffset, 10);
          if($storiesByTen == null) {
            $data['error'] = true;
          } else {
            $data['storiesByTen'] = $storiesByTen;
            $data['error'] = false;
          }
        } else {
          $data['error'] = true;
        }
        echo json_encode($data);
    }

    public function _searchResultListAction($keyword, $offset) {
      $finaloffset = ($offset * 10) - 10;
      $stories = CB::bnbQuery("SELECT ssid, photo, author, subject, metadesc
                            FROM testimonies
                            WHERE status = 1 AND (author LIKE '%".$keyword."%' OR subject LIKE '%".$keyword."%')
                            ORDER BY date_published DESC");
      if($stories == true) {

        foreach($stories as $key => $value) {
          $stories[$key]['metadesc'] = utf8_encode($stories[$key]['metadesc']);
          $stories[$key]['details']  = utf8_encode($stories[$key]['details']);
          $stories[$key]['subject']  = utf8_encode($stories[$key]['subject']);
        }

        $data['storiesCount'] = count($stories);
        $storiesByTen = array_slice($stories, $finaloffset, 10);
        if($storiesByTen == null) {
          $data['nulL'] = true;
        } else {
          $data['storiesByTen'] = $storiesByTen;
          $data['keyword']      = $keyword;
          $data['nulL']         = false;
        }
      } else {
        $data['nulL']    = true;
        $data['keyword'] = $keyword;
      }
      echo json_encode($data);
    }

    public function _detailsAction($ssid) {
      $data['error'] = false;
      $story = CB::bnbQueryFirst("SELECT ssid, photo, author, subject, metadesc, details FROM testimonies WHERE status = 1 AND ssid = $ssid ");
      if($story == true) {
          $story['subject'] = utf8_encode($story['subject']);
          $story['details'] = utf8_encode($story['details']);
          $story['metadesc'] = utf8_encode($story['metadesc']);
          $story['metatags'] = utf8_encode($story['metatags']);
        $data['story'] = $story;
      } else {
        $data['error'] = true;
      }
      echo json_encode($data);
    }
}
