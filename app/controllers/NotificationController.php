<?php

namespace Controllers;

use \Models\Centercalendar as Centercalendar;
use \Models\Beneplace as Beneplace;
use \Models\Groupclassintrosession as Groupclassintrosession;
use \Models\Centerlocation as Centerlocation;
use \Models\Introsession as Introsession;
use \Models\Notification as Notification;
use \Models\Centerregion as Centerregion;
use \Models\Centerphonenumber as Centerphonenumber;
use \Models\Center as Center;
use \Models\Users as Users;
use \Models\States as States;
use \Models\Cities as Cities;
use \Models\Notificationcount as Notificationcount;
use \Models\Cities_extended as Cities_extended;
use \Models\Testimonies as Testimonies;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class NotificationController extends \Phalcon\Mvc\Controller {

	public function listnotificationAction($userid, $offset, $filter){
		$timeago = new CB;
		$finalnotification = array();

		$usertype = Users::findFirst('id="'.$userid.'"');
		if($usertype){
			if($filter != 'none'){
				if($usertype->task == 'Administrator'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.adminstatus as notificationstatus FROM notification WHERE adminstatus='".$filter."' ORDER BY datecreated DESC LIMIT 0,".$offset."");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'admin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.adminstatus as notificationstatus FROM notification WHERE adminstatus='".$filter."' ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Administrator if

				else if($usertype->task == 'Region Manager'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.regionstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid  INNER JOIN centerregion on center.centerregion=centerregion.regionid && centerregion.regionmanager ='".$userid."' WHERE regionstatus='".$filter."' ORDER BY datecreated DESC LIMIT 0,".$offset."");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'notadmin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.regionstatus as notificationstatus,center.centerid as centercenterid FROM notification INNER JOIN center on center.centerid=notification.centerid INNER JOIN centerregion on center.centerregion=centerregion.regionid && centerregion.regionmanager ='".$userid."' WHERE regionstatus='".$filter."' ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Region Manager if

				else if($usertype->task == 'Center Manager'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.centerstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid && center.centermanager ='".$userid."' WHERE centerstatus='".$filter."' ORDER BY datecreated DESC LIMIT 0,".$offset."");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'notadmin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.centerstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid && center.centermanager ='".$userid."' WHERE centerstatus='".$filter."' ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Region Manager if
			} //end of filter if
			else {
				if($usertype->task == 'Administrator'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.adminstatus as notificationstatus FROM notification ORDER BY datecreated DESC LIMIT 0,".$offset."");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'admin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.adminstatus as notificationstatus FROM notification WHERE adminstatus = 0 ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Administrator if

				else if($usertype->task == 'Region Manager'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.regionstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid  INNER JOIN centerregion on center.centerregion=centerregion.regionid && centerregion.regionmanager ='".$userid."' ORDER BY datecreated DESC LIMIT 0,".$offset."");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'notadmin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.regionstatus as notificationstatus,center.centerid as centercenterid FROM notification INNER JOIN center on center.centerid=notification.centerid INNER JOIN centerregion on center.centerregion=centerregion.regionid && centerregion.regionmanager ='".$userid."' WHERE regionstatus = 0 ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Region Manager if

				else if($usertype->task == 'Center Manager'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.centerstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid && center.centermanager ='".$userid."' ORDER BY datecreated DESC LIMIT 0,".$offset."");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'notadmin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.centerstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid && center.centermanager ='".$userid."' WHERE centerstatus = 0 ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Region Manager if
			} //end of else filter if
		} //end of usertype if

		echo json_encode(array('notification'=>$finalnotification,'notificationcount'=> count($notificationcount)));

		$finduserid = Notificationcount::findFirst('userid="'.$userid.'"');
		$finalnotificationtemp = array();
		if($finduserid){
			foreach ($notificationcount as $noti) {
				if($finduserid->lastnotificationdate < $noti['datecreated']){
					array_push($noti);
					$finalnotificationtemp[] = $noti;
				}
				
			}
			if($finduserid->lastnotificationdate < $finalnotification[0]['datecreated']){
				$finduserid->count = count($finalnotificationtemp);
				$finduserid->lastnotificationdate = $finalnotificationtemp[0]['datecreated'];
				if($finduserid->save()){
					// echo json_encode($data['success'] = 'success');
				}
			}
		}
		else{
			$addnoticount = new Notificationcount();
			$addnoticount->userid = $userid;
			$addnoticount->count = count($notificationcount);
			$addnoticount->lastnotificationdate = $finalnotification[0]['datecreated'];
			if($addnoticount->save()){
				// echo json_encode($data['success'] = 'success');
			}
		}
		
		
	}

	public function changestatusAction($notificationid,$userid){
		$usertype = Users::findFirst('id="'.$userid.'"');
		if($usertype){

			if($usertype->task == 'Administrator'){
				$changestatus = Notification::findFirst('notificationid="'.$notificationid.'"');
				if($changestatus){
					$changestatus->adminstatus = 1;
					if($changestatus->save()){
						echo json_encode('done');
					}
				}
			} //end Administrator if

			else if($usertype->task == 'Region Manager'){
				$changestatus = Notification::findFirst('notificationid="'.$notificationid.'"');
				if($changestatus){
					$changestatus->regionstatus = 1;
					if($changestatus->save()){
						echo json_encode('done');
					}
				}
			} //end Administrator if

			else if($usertype->task == 'Center Manager'){
				$changestatus = Notification::findFirst('notificationid="'.$notificationid.'"');
				if($changestatus){
					$changestatus->centerstatus = 1;
					if($changestatus->save()){
						echo json_encode('done');
					}
				}
			} //end Administrator if

		} //end of usertype if

	}

	public function notificationcountAction($userid){
		$count = Notificationcount::findFirst('userid="'.$userid.'"');
		if($count){
			echo json_encode($count);
		}
	}

	public function notificationcountzeroAction($userid){
		$count = Notificationcount::findFirst('userid="'.$userid.'"');
		if($count){
			$count->count = 0;
			if($count->save()){
				echo json_encode($data['success'] = 'success');
			}
		}
	}

	public function loadnotificationAction($num, $page, $keyword,$userid){
		$timeago = new CB;
		$finalnotification = array();
		$usertype = Users::findFirst('id="'.$userid.'"');

  		if ($keyword == 'null' || $keyword == 'undefined') {
                $offsetfinal = ($page * 10) - 10;
                if($usertype->task == 'Administrator'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.adminstatus as notificationstatus FROM notification  ORDER BY datecreated DESC LIMIT " . $offsetfinal . ",10");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'admin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.adminstatus as notificationstatus FROM notification  ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Administrator if

				else if($usertype->task == 'Region Manager'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.regionstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid  INNER JOIN centerregion on center.centerregion=centerregion.regionid && centerregion.regionmanager ='".$userid."'  ORDER BY datecreated DESC LIMIT " . $offsetfinal . ",10");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'notadmin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.regionstatus as notificationstatus,center.centerid as centercenterid FROM notification INNER JOIN center on center.centerid=notification.centerid INNER JOIN centerregion on center.centerregion=centerregion.regionid && centerregion.regionmanager ='".$userid."' ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Region Manager if

				else if($usertype->task == 'Center Manager'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.centerstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid && center.centermanager ='".$userid."' ORDER BY datecreated DESC LIMIT " . $offsetfinal . ",10");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'notadmin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.centerstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid && center.centermanager ='".$userid."' ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Region Manager if
               
            } 
            else {

                 $offsetfinal = ($page * 10) - 10;

                 if($usertype->task == 'Administrator'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.adminstatus as notificationstatus FROM notification WHERE notification.name like '%".$keyword."%' or notification.type like '%".$keyword."%' ORDER BY datecreated DESC LIMIT " . $offsetfinal . ",10");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'admin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.adminstatus as notificationstatus FROM notification WHERE notification.name like '%".$keyword."%' or notification.type like '%".$keyword."%' ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Administrator if

				else if($usertype->task == 'Region Manager'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.regionstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid  INNER JOIN centerregion on center.centerregion=centerregion.regionid && centerregion.regionmanager ='".$userid."' WHERE notification.name like '%".$keyword."%' or notification.type like '%".$keyword."%' ORDER BY datecreated DESC LIMIT " . $offsetfinal . ",10");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'notadmin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.regionstatus as notificationstatus,center.centerid as centercenterid FROM notification INNER JOIN center on center.centerid=notification.centerid INNER JOIN centerregion on center.centerregion=centerregion.regionid && centerregion.regionmanager ='".$userid."' WHERE notification.name like '%".$keyword."%' or notification.type like '%".$keyword."%' ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Region Manager if

				else if($usertype->task == 'Center Manager'){

					$db = \Phalcon\DI::getDefault()->get('db');
					$stmt = $db->prepare("SELECT notification.*,notification.centerstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid && center.centermanager ='".$userid."' WHERE notification.name like '%".$keyword."%' or notification.type like '%".$keyword."%' ORDER BY datecreated DESC LIMIT " . $offsetfinal . ",10");

					$stmt->execute();
					$notification = $stmt->fetchAll(\PDO::FETCH_ASSOC);

					foreach ($notification as $noti) {
						array_push($noti, array("timeago"=> $timeago->time_elapsed_string($noti['datecreated']),"uigo" => 'notadmin'));
						$finalnotification[]=$noti;
					} //end foreach

					$stmt = $db->prepare("SELECT notification.*,notification.centerstatus as notificationstatus,center.centerid as centercenterid  FROM notification INNER JOIN center on center.centerid=notification.centerid && center.centermanager ='".$userid."' WHERE notification.name like '%".$keyword."%' or notification.type like '%".$keyword."%' ORDER BY datecreated DESC");

					$stmt->execute();
					$notificationcount = $stmt->fetchAll(\PDO::FETCH_ASSOC);

				} //end Region Manager if

                 
                
            }

            echo json_encode(array('notification'=>$finalnotification,'notificationcount'=> count($notificationcount),'index' =>$page));

    }

    public function deletenotificationAction($notificationid){
    	$notification = Notification::findFirst('notificationid = "'.$notificationid.'"');
    	if($notification){
    		if($notification->delete()){
    			$data['status'] = 1;
    		}
    		else{
    			$data['status'] = 2;
    		}
    	}
    	else{
    		$data['status'] = 2;
    	}
    	echo json_encode($data);
    }

	public function deleteoldnotiAction(){
		$centers = CB::bnbQuery("SELECT centerid FROM center");
		if($centers){
			foreach ($centers as $center) {
				$getlatestnotification = CB::bnbQuery("SELECT datecreated FROM notification WHERE centerid = '".$center['centerid']."' ORDER BY datecreated DESC LIMIT 1");
				if($getlatestnotification){
					$latestdatefromnoti = strtotime($getlatestnotification[0]['datecreated']. ' -7 days');
					$newdate = date( 'Y-m-d', $latestdatefromnoti );
					$deleteoldnoti = CB::bnbQuerydelete("DELETE FROM notification WHERE centerid = '".$center['centerid']."' AND datecreated < '".$newdate."'");
				}
			}
		}
	}
	

}