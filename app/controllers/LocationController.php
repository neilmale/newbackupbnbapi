<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Models\Center as Center;
use \Models\Timezonebyzipcode as Timezonebyzipcode;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class LocationController extends \Phalcon\Mvc\Controller {

    public function fe_centermainpropAction() {
      $request = new Request();
      if($request->isPost()) {
        $datenow = date("Y-m-d");
        $centerslugs = $request->getPost("centerslugs");

        $center = CB::bnbQueryFirst("SELECT center.centerid, center.centertitle, center.centeraddress, center.centerzip, centerlocation.lat, centerlocation.lon FROM center
          LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid
          WHERE center.centerslugs ='".$centerslugs."' OR center.centerslugs2 = '" . $centerslugs . "' OR center.centerslugs3 = '" . $centerslugs . "'");
        if($center == true) {
          $data['center'] = $center;
        } else {
          $data['centerMsg'] = $center->getMessages()[0]->getMessage();
        }

        // $data['centerevents'] = CB::bnbQuery("SELECT * FROM(
        //     SELECT 'dummy' as ecenterslugs, activityid as eid,'dummy' as evenue,centerid as ecenterid,'dummy' as ecentertitle,activitytitle as etitle,activitydate as edate,description as edesc,activitydate as edatestart,activitytimefrom as estarthour,'dummy' as estartminute,activitytimefromformat as estarttimeformat,activitytimeto as eendhour,'dummy' as eendminute,activitytimetoformat as eendtimeformat, 'act' as type  FROM centercalendar WHERE centerid ='" . $center['centerid'] . "' and activitydate >= '".$datenow."'  and status = 1
        //     UNION
        //     SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.center ='" . $center['centerid'] . "' AND workshop.datestart >= '".$datenow."'
        //     UNION
        //     SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop INNER JOIN workshopassociatedcenter ON workshopassociatedcenter.workshopid=workshop.workshopid LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshopassociatedcenter.centerid ='". $center['centerid'] ."' AND workshop.datestart >= '".$datenow."'
        //     )
        //     AS event ORDER BY event.edatestart
        //     ");

        echo json_encode($data);
      }
    }

    public function regionsAction() {
      $states = CB::bnbQuery("SELECT states.state_code, states.state FROM center INNER JOIN states ON center.centerstate=states.state_code GROUP BY states.state_code ORDER BY states.state ASC");
      echo json_encode($states);
    }

    public function selectstateAction() {
      $request = new Request();
      if($request->isPost()) {
        $state_code = $request->getPost("state_code");

        $states = CB::bnbQuery("SELECT center.centerid, centertitle, centerslugs, centeraddress, centerstate, centercity, centerzip, phonenumber, centertype, lat, lon
                              FROM center
                              LEFT JOIN centerphonenumber ON center.centerid = centerphonenumber.centerid
                              LEFT JOIN centerlocation ON center.centerid = centerlocation.centerid
                              WHERE centerstate = '".$state_code."'
                              AND center.status = 1
                              ORDER BY centertitle ");
        echo json_encode($states);
      }
    }

    public function monthlyschedAction() {
      $request = new Request();
      if($request->isPost()) {
        $centerslugs = $request->getPost("centerslugs");
        $centerid = CB::bnbQueryFirst("SELECT centerid FROM center WHERE centerslugs = '".$centerslugs."'");
        $date = $request->getPost("date");
        $days = array();
        $days = ['mo','tu','we','th','fr','sa','su'];

        $schedule = array();
        $dateadd = array();
        $counter = 0;
        foreach ($days as $day) {
            $schedulelist = CB::bnbQuery("SELECT *, ".$day." as classandday FROM centerschedule where centerid = '".$centerid['centerid']."' and  ".$day." != '' ORDER BY STR_TO_DATE(starttime, '%l:%i %p')");
            if($schedulelist){
                if($day == 'mo'){
                  $day = 'Monday';
                } else if($day == 'tu'){
                  $day = 'Tuesday';
                } else if($day == 'we'){
                  $day = 'Wednesday';
                } else if($day == 'th'){
                  $day = 'Thursday';
                } else if($day == 'fr'){
                  $day = 'Friday';
                } else if($day == 'sa'){
                  $day = 'Saturday';
                } else if($day == 'su'){
                  $day = 'Sunday';
                }
                $schedulelist['day'] = $day;
                $schedule[] = $schedulelist;
            } else {
                break;
            }

        }

       echo json_encode($schedule);
      }
    }

    public function feCenternewsAction($centerslugs, $centernewslugs) {
      $center = Center::findFirst(" centerslugs = '".$centerslugs."' OR centerslugs2 = '".$centerslugs."' OR centerslugs3 = '".$centerslugs."'");
      $centerid = $center->centerid;
      $centerzip = $center->centerzip;

      $data['centertitle'] = $center->centertitle;

      $timezone = Timezonebyzipcode::findFirst("zip = ".$centerzip."");
      $TZ = date_default_timezone_set($timezone->timezone);
      $datenow = date('Y-m-d');

      $data['existingNews'] = false;
      $fullnews = CB::bnbQueryFirst("SELECT centernews.title, centernews.description, centernews.body, centernews.banner, centernews.metatitle, centernews.date
                  FROM centernews
                  LEFT JOIN center ON centernews.newslocation = center.centerid
                  WHERE centernews.newslocation = '".$centerid."'
                        AND centernews.newsslugs ='".$centernewslugs."'
                        AND centernews.date <= '".$datenow."'
                        AND centernews.status = 1");

      if($fullnews == true) {
        $data['existingNews'] = true;
        $fullnews['title'] = utf8_encode($fullnews['title']);
        $fullnews['description'] = utf8_encode($fullnews['description']);
        $fullnews['body'] = utf8_encode($fullnews['body']);
      }
      $data['centernews'] = $fullnews;
      echo json_encode($data);
    }

    public function feCenterFullnewsAction($centerslugs) {
      // $timezone = Timezonebyzipcode::findFirst("zip = ".$centerzip."");
      // $TZ = date_default_timezone_set($timezone->timezone);
      $datenow = date('Y-m-d');

      $data['centerslugs'] = $centerslugs;
      $centerid = CB::bnbQueryFirst("SELECT centerid FROM center WHERE (centerslugs = '".$centerslugs."' OR centerslugs2='".$centerslugs."' OR centerslugs3='".$centerslugs."') AND status = 1")['centerid'];
      $fullnews = CB::bnbQuery("SELECT * FROM centernews WHERE newslocation = '".$centerid."' AND date <= '".$datenow."' AND status = 1 ORDER BY date DESC LIMIT 10");
      foreach($fullnews as $news) {
        $news['title'] = utf8_encode($news['title']);
        $news['description'] = utf8_encode($news['description']);
        $news['body'] = utf8_encode($news['body']);
      }
      $data['fullnews'] = $fullnews;

      echo json_encode($data);
    }

    public function feCenterFulltestimonialsAction($centerslugs, $offset) {
      $datenow = date('Y-m-d');
      $finaloffset = $offset * 10 - 10;
      $centerid = CB::bnbQueryFirst("SELECT centerid FROM center WHERE (centerslugs = '".$centerslugs."' OR centerslugs2='".$centerslugs."' OR centerslugs3='".$centerslugs."') AND status = 1")['centerid'];
      $fulltestimonials = CB::bnbQuery("SELECT ssid, author, photo, subject, metadesc FROM testimonies WHERE center = '".$centerid."' AND status = 1 ORDER BY date_published DESC");
      foreach($fulltestimonials as $key => $value) {
        $fulltestimonials[$key]['metadesc'] = utf8_encode($fulltestimonials[$key]['metadesc']);
      }

      $data['totalItems'] = count($fulltestimonials);
      $data['fulltestimonials'] = array_slice($fulltestimonials, $finaloffset, 10);

      echo json_encode($data);
    }

    public function femembershipAction() {
      $data['nobanner'] = false;
  		$banners = CB::bnbQuery("SELECT url, image FROM images WHERE page = 'locations' AND status = 1 ORDER BY sequence ");
  		if($banners == true) {
  				if(count($banners) > 1) { $data['slider'] = true; }
  				else { $data['slider'] = false; }
  				$data['locationsbanners'] = $banners;
  		} else {
  			$data['nobanner'] = true;
  		}

      $workshoptitles = CB::bnbQuery("SELECT * FROM workshoptitle ORDER BY date_updated DESC");
  		if(count($workshoptitles) > 0 ) {
  			$data['notitles'] = false;
  			$data['workshoptitles'] = $workshoptitles;
  		} else {
  			$data['notitle'] = true;
  		}

      $datenow = date('Y-m-d');

      $data['centereventspopular'] = CB::bnbQuery("SELECT * FROM(
          SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, activityid as eid,'dummy' as evenue, center.centerid as ecenterid,centertitle as ecentertitle,activitytitle as etitle,activitydate as edate,description as edesc,activitydate as edatestart,activitytimefrom as estarthour,'dummy' as estartminute,activitytimefromformat as estarttimeformat,activitytimeto as eendhour,'dummy' as eendminute,activitytimetoformat as eendtimeformat, 'act' as type  FROM centercalendar
          INNER JOIN center ON centercalendar.centerid = center.centerid WHERE activitydate >= '".$datenow."'  and centercalendar.status = 1
          UNION
          SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.datestart >= '".$datenow."'
          UNION
          SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop INNER JOIN workshopassociatedcenter ON workshopassociatedcenter.workshopid=workshop.workshopid LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.datestart >= '".$datenow."'
          )
          AS event ORDER BY event.edatestart LIMIT 2
          ");

      $data['centerevents'] = CB::bnbQuery("SELECT * FROM(
          SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, activityid as eid,'dummy' as evenue, center.centerid as ecenterid,centertitle as ecentertitle,activitytitle as etitle,activitydate as edate,description as edesc,activitydate as edatestart,activitytimefrom as estarthour,'dummy' as estartminute,activitytimefromformat as estarttimeformat,activitytimeto as eendhour,'dummy' as eendminute,activitytimetoformat as eendtimeformat, 'act' as type  FROM centercalendar
          INNER JOIN center ON centercalendar.centerid = center.centerid WHERE activitydate >= '".$datenow."'  and centercalendar.status = 1
          UNION
          SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.datestart >= '".$datenow."'
          UNION
          SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop INNER JOIN workshopassociatedcenter ON workshopassociatedcenter.workshopid=workshop.workshopid LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.datestart >= '".$datenow."'
          )
          AS event ORDER BY event.edatestart LIMIT 2
          ");

  		echo json_encode($data);
    }

    public function fegeteventsAction($keyword) {
      $datenow = date('Y-m-d');

      if($keyword != "undefined" && $keyword != "null"){
        $where1 = "AND (centertitle LIKE '%" . $keyword . "%' OR
        activitytitle LIKE '%" . $keyword . "%' OR
        activitydate LIKE '%" . $keyword . "%' OR
        center.centerstate LIKE '%" . $keyword . "%')";

        $where2 = "AND (centertitle LIKE '%" . $keyword . "%' OR
        workshoptitle.title LIKE '%" . $keyword . "%' OR
        workshop.datestart LIKE '%" . $keyword . "%' OR
        workshop.datestart LIKE '%" . $keyword . "%' OR
        center.centerstate LIKE '%" . $keyword . "%')";

        $query = "SELECT * FROM(
            SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, activityid as eid,'dummy' as evenue, center.centerid as ecenterid,centertitle as ecentertitle,activitytitle as etitle,activitydate as edate,description as edesc,activitydate as edatestart,activitytimefrom as estarthour,'dummy' as estartminute,activitytimefromformat as estarttimeformat,activitytimeto as eendhour,'dummy' as eendminute,activitytimetoformat as eendtimeformat, 'act' as type  FROM centercalendar
            INNER JOIN center ON centercalendar.centerid = center.centerid WHERE activitydate >= '".$datenow."'  and centercalendar.status = 1 $where1
            UNION
            SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.datestart >= '".$datenow."' $where2
            UNION
            SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop INNER JOIN workshopassociatedcenter ON workshopassociatedcenter.workshopid=workshop.workshopid LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.datestart >= '".$datenow."' $where2
            ) AS event ORDER BY event.edatestart LIMIT 2";
      } else {
        $query = "SELECT * FROM(
            SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, activityid as eid,'dummy' as evenue, center.centerid as ecenterid,centertitle as ecentertitle,activitytitle as etitle,activitydate as edate,description as edesc,activitydate as edatestart,activitytimefrom as estarthour,'dummy' as estartminute,activitytimefromformat as estarttimeformat,activitytimeto as eendhour,'dummy' as eendminute,activitytimetoformat as eendtimeformat, 'act' as type  FROM centercalendar
            INNER JOIN center ON centercalendar.centerid = center.centerid WHERE activitydate >= '".$datenow."'  and centercalendar.status = 1
            UNION
            SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.datestart >= '".$datenow."'
            UNION
            SELECT center.centerslugs as ecenterslugs, center.centerstate as ecenterstate, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop INNER JOIN workshopassociatedcenter ON workshopassociatedcenter.workshopid=workshop.workshopid LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.datestart >= '".$datenow."'
            )
            AS event ORDER BY event.edatestart LIMIT 2
            ";
      }

      $data = CB::bnbQuery($query);

        echo json_encode($data);
    }

    public function geteventAction() {
      $request = new Request();
      if($request->isPost()) {
        $datenow = date("Y-m-d");
        $centerslugs = $request->getPost("centerslugs");
        $center = CB::bnbQuery("SELECT centerid FROM center WHERE centerslugs='" . $centerslugs. "' OR centerslugs2='" . $centerslugs. "' OR centerslugs3='" . $centerslugs. "'")[0];

        $data = CB::bnbQuery("SELECT * FROM(
            SELECT 'dummy' as ecenterslugs, activityid as eid,'dummy' as evenue,centerid as ecenterid,'dummy' as ecentertitle,activitytitle as etitle,activitydate as edate,description as edesc,activitydate as edatestart,activitytimefrom as estarthour,'dummy' as estartminute,activitytimefromformat as estarttimeformat,activitytimeto as eendhour,'dummy' as eendminute,activitytimetoformat as eendtimeformat, 'act' as type  FROM centercalendar WHERE centerid ='" . $center['centerid'] . "' and activitydate >= '".$datenow."'  and status = 1
            UNION
            SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshop.center ='" . $center['centerid'] . "' AND workshop.datestart >= '".$datenow."'
            UNION
            SELECT center.centerslugs as ecenterslugs, workshop.workshopid as eid,workshop.venue as evenue,workshop.center as ecenterid,centertitle as ecentertitle,workshoptitle.title as etitle,workshop.datestart as edate,workshop.about as edesc,workshop.datestart as edatestart,workshop.starthour as estarthour,workshop.startminute as estartminute,workshop.starttimeformat as estarttimeformat,workshop.endhour as eendhour,workshop.endminute as eendminute,workshop.endtimeformat as eendtimeformat, 'wrk' as type FROM workshop INNER JOIN workshopassociatedcenter ON workshopassociatedcenter.workshopid=workshop.workshopid LEFT JOIN center ON workshop.center=center.centerid LEFT JOIN workshoptitle ON workshop.title=workshoptitle.workshoptitleid WHERE workshopassociatedcenter.centerid ='". $center['centerid'] ."' AND workshop.datestart >= '".$datenow."'
            )
            AS event ORDER BY event.edatestart
            ");

        echo json_encode($data);
      }
    }
}
