<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Models\Members as Members;
use \Controllers\ControllerBase as CB;
use \Models\States as States;
use \Models\Center as Center;
use \Models\Pwdrequest as Pwdrequest;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class MemberController extends \Phalcon\Mvc\Controller {

  public function loginAction() {
    $request = new \Phalcon\Http\Request();
    $app = new CB();
    if($request->isPost()){
      $email = $request->getPost('email');
      $password = sha1($request->getPost('password'));

      $m = Members::findFirst("(email='$email' OR username='$email') AND password='$password'");
      if($m){
        $jwt = new \Security\Jwt\JWT();
        $payload = array(
                "id"        => $m->memberid,
                "username"  => $m->username,
                "email"     => $m->email,
                "lastname"  => $m->lastname,
                "firstname" => $m->firstname,
                "level"     => $app->config->clients->member,
                "exp"       => time() + (60 * 60)); // time in the future

        $token = $jwt->encode($payload, $app->config->hashkey);
        $app->storeRedis($m->memberid, $token);

        $data = array('success' => $token);
      }else {
        $data = array('error' => 'Incorrect email or password');
      }

      echo json_encode($data);
    }
  }

  public function validateemailAction($email) {
    $m = Members::findFirst("email='$email'");
    if($m){
      $data = array('exist' => true );
    }else {
      $data = array('exist' => false );
    }

    echo json_encode($data);
  }

  public function registerAction() {
    $request = new \Phalcon\Http\Request();
    $app = new CB();

    if($request->isPost()){
      $m = new Members();
      $guid = new \Utilities\Guid\Guid();

      $m->assign(array(
        'memberid' => $guid->GUID(),
        'email' => $request->getPost('email'),
        'username' => substr($request->getPost('email'), 0, strrpos($request->getPost('email'), "@")),
        'password' => sha1($request->getPost('password')),
        'firstname' => $request->getPost('firstname'),
        'lastname' => $request->getPost('lastname'),
        'phoneno' => $request->getPost('phoneno'),
        'center' => $request->getPost('center')['centerid'],
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s"),
        'confirmationcode' => $guid->GUID()));

      if($m->save() == false){
        $data = [];
        foreach ($m->getMessages() as $message) {
          $data[] = $message;
        }
      }else {
        // $body = 'Welcome to Body and Brain, '.$request->getPost('firstname'). ' '. $request->getPost('lastname') . ' <br>  <br> Please click the confirmation link below to complete and activate your account.
        //   <br><br>  Link: <br><a href="' . $app->getConfig()->application->baseURL . '/activation/'.$m->confirmationcode.'">' . $app->getConfig()->application->baseURL . '/activation/'.$m->confirmationcode.'</a><br><br> <br><br>Thanks,<br><br>PI Staff';

        // $app->sendMail($request->getPost('email'), 'Body and Brain Registration Confirmation', $body);
        $jwt = new \Security\Jwt\JWT();
        $payload = array(
                "id" => $m->memberid,
                "username" => substr($request->getPost('email'), 0, strrpos($request->getPost('email'), "@")),
                "email" => $m->email,
                "lastname" => $m->lastname,
                "firstname" => $m->firstname,
                "level" => $app->config->clients->member,
                "exp" => time() + (60 * 60)); // time in the future

        $token = $jwt->encode($payload, $app->config->hashkey);
        $app->storeRedis($m->memberid, $token);

        $data = array('success' => $token);
        // $data = array('success' => 'Please check your email address inbox to confirm your registration.' );
      }

      echo json_encode($data);
    }
  }

  public function loginfbAction() {
    $request = new \Phalcon\Http\Request();
    $jwt = new \Security\Jwt\JWT();
    $app = new CB();

    if($request->isPost()){
      $id = $request->getPost('id');
      $m = Members::findFirst("facebookid='$id'");
      if($m){
        $payload = array(
              "id" => $m->memberid,
              "email" => $m->email,
              "lastname" => $m->lastname,
              "firstname" => $m->firstname,
              "level" => $app->config->clients->member,
              "exp" => time() + (60 * 60)); // time in the future

        $token = $jwt->encode($payload, $app->config->hashkey);
        $app->storeRedis($m->memberid, $token);

        $data = array('success' => $token);
      }else {
        $guid = new \Utilities\Guid\Guid();
        $m = new Members();
        $m->assign(array(
        'memberid' => $guid->GUID(),
        'email' => $request->getPost('email'),
        'facebookid' => $id,
        'firstname' => $request->getPost('first_name'),
        'lastname' => $request->getPost('last_name'),
        'created_at' => date("Y-m-d H:i:s"),
        'updated_at' => date("Y-m-d H:i:s")));

        if($m->create()){
          $payload = array(
              "id" => $m->memberid,
              "email" => $m->email,
              "lastname" => $m->lastname,
              "firstname" => $m->firstname,
              "level" => $app->config->clients->member,
              "exp" => time() + (60 * 60)); // time in the future

          $token = $jwt->encode($payload, $app->config->hashkey);
          $app->storeRedis($m->memberid, $token);

          $data = array('success' => $token);
        }else {
          $data = [];
          foreach ($m->getMessages() as $message) {
            $data[] = $message;
          }
        }
      }

      echo json_encode($data);
    }
  }

  public function logingplusAction() {
    $request = new \Phalcon\Http\Request();
    $jwt = new \Security\Jwt\JWT();
    $app = new CB();

    if($request->isPost()){
      $id = $request->getPost('id');
      $m = Members::findFirst("googleplusid='$id'");
      if($m){
        $payload = array(
              "id" => $m->memberid,
              "email" => $m->email,
              "lastname" => $m->lastname,
              "firstname" => $m->firstname,
              "level" => $app->config->clients->member,
              "exp" => time() + (60 * 60)); // time in the future

        $token = $jwt->encode($payload, $app->config->hashkey);
        $app->storeRedis($m->memberid, $token);

        $data = array('success' => $token);
      } else {
        $guid = new \Utilities\Guid\Guid();
        $m = new Members();
        $m->assign(array(
          'memberid' => $guid->GUID(),
          'googleplusid' => $id,
          'firstname' => $request->getPost('given_name'),
          'lastname' => $request->getPost('family_name'),
          'created_at' => date("Y-m-d H:i:s"),
          'updated_at' => date("Y-m-d H:i:s")
        ));

        if($m->create()) {
          $payload = array(
              "id" => $m->memberid,
              "email" => $m->email,
              "lastname" => $m->lastname,
              "firstname" => $m->firstname,
              "level" => $app->config->clients->member,
              "exp" => time() + (60 * 60)); // time in the future

          $token = $jwt->encode($payload, $app->config->hashkey);
          $app->storeRedis($m->memberid, $token);

          $data = array('success' => $token);
        } else {
          $data = [];
          foreach ($m->getMessages() as $message) {
            $data[] = $message;
          }
        }
      }
      echo json_encode($data);
    }
  }

  public function getstatesAction() {
    echo json_encode(States::find(array('order' => 'state ASC' ))->toArray());
  }

  public function getcenterAction($state) {
    echo json_encode(CB::bnbQuery("SELECT centertitle FROM center WHERE centerstate='" . $state . "' ORDER BY centertitle ASC"));
  }


  public function feGetcurrentuserAction($memberid) {
    echo json_encode(CB::bnbQueryFirst("SELECT memberid as id, firstname, lastname, username, email FROM members WHERE memberid = '".$memberid."'"));
  }

  public function account_personalinfo_update() {
    $request = new Request();
    if($request->isPost()) {
        $memberid  = $request->getPost("id");
        $username  = $request->getPost("username");
        $email     = $request->getPost("email");
        $firstname = $request->getPost("firstname");
        $lastname  = $request->getPost("lastname");
        //
        $member = Members::findFirst("memberid = '".$memberid."'");
        $member->username  = $username;
        $member->email     = $email;
        $member->firstname = $firstname;
        $member->lastname  = $lastname;
        if($member->save()) {
            $data["status"] = "ok";
            $data['member'] = $member;
        } else {
            $data["status"] = "error";
        };
    } else {
      $data["status"] = "error";
    }
    echo json_encode($data);
  }

  public function feResetPasswordAction() {
    $request = new Request();
    if($request->getPost()) {
      $memberid = $request->getPost("memberid");
      $current = $request->getPost("password")['current'];
      $new = $request->getPost("password")['new'];

      $member = Members::findFirst("memberid = '".$memberid."'");
      if($member == true) {
        if(sha1($current) == $member->password) {
          $member->password = sha1($new);
          if($member->save()) { $data['success'] = true; }
          else { $data['error'] = true; }

        } else {
          $data['warning'] = true;
          $data['current'] = sha1($current);
          $data['old'] = $member->password;
          $data['myid'] = $memberid;
        }
      }
    } else {
      $data['error'] = true;
    }
    echo json_encode($data);
  }

  public function feForgotPasswordAction() {
    $request = new Request();
    if($request->isPost()) {
      $memberid   = $request->getPost("id");
      $memberemail= $request->getPost("email");

      $code                = CB::genGUID();
      $pwdreq              = new Pwdrequest();

      $check = Pwdrequest::findFirst("userid = '".$memberid."' AND status = 'usable' ");
      if($check == true) {
        $check->status = 'expired';
        $check->save();

        $pwdreq->id          = $code;
        $pwdreq->userid      = $memberid;
        $pwdreq->email       = $memberemail;
        $pwdreq->status      = "usable";
        $pwdreq->date_created= date("Y-m-d H:i:s");
      } else {
        $pwdreq->id          = $code;
        $pwdreq->userid      = $memberid;
        $pwdreq->email       = $memberemail;
        $pwdreq->status      = "usable";
        $pwdreq->date_created= date("Y-m-d H:i:s");
      }

      if($pwdreq->save()) {
        $data['success'] = true;

        $baseURL = CB::conf()->application->baseURL;

        $body = "
            To change your password please visit the link below: <br><br>
            ".$baseURL."/changepassword/".$code.".
            <br><br>
            NOTE: This request will expire if unused after 5 minutes or replaced with another request.
        ";

        CB::sendMail($memberemail, "Body&Brain: Change Password Request", $body);

      } else {
        $data['error'] = true;
      }
    } else {
      $data['error'] = true;
    }
    echo json_encode($data);
  }

  public function feChangepasswordValidateAction($requestcode) {
    $request = Pwdrequest::findFirst("id = '".$requestcode."' AND status = 'usable' ");
    if($request == true) {
        $data['valid'] = true;
    } else {
        $data['valid'] = false;
    }
    echo json_encode($data);
  }

  public function feChangepasswordAction() {
    $request = new Request();
    $data['status'] = "error";

    if($request->isPost()) {
        $requestcode = $request->getPost("requestcode");
        $newpwd = $request->getPost("newpwd");
        $pwd = Pwdrequest::findFirst("id = '".$requestcode."' AND status = 'usable'");
        if($pwd == true) {
            $memberid = $pwd->userid;
            $member = Members::findFirst("memberid = '".$memberid."'");
            if($member == true) {
                $member->password = sha1($newpwd);
                if($member->save()){
                    $data['status'] = "success";

                    $pwd->status = "finished";
                    $pwd->save();
                }
            }
        }
    }
    echo json_encode($data);
  }

}
