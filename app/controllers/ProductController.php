<?php

namespace Controllers;

use \Phalcon\Http\Request;
use \Models\Product as Product;
use \Models\Productinvoice as Productinvoice;
use \Models\Inventory as Inventory;
use \Models\Productimages as Productimages;
use \Models\Producttags as Producttags;
use \Models\Prodtags as Prodtags;
use \Models\Producttype as Producttype;
use \Models\Prodtype as Prodtype;
use \Models\Prodtype2 as Prodtype2;
use \Models\Productcategory as Productcategory;
use \Models\Prodcat as Prodcat;
use \Models\Productsize as Productsize;
use \Models\Prodsize as Prodsize;
use \Models\Productlog as Productlog;
use \Models\Productsubcategory as Productsubcategory;
use \Models\Prodsubcat as Prodsubcat;
use \Models\Prodsubcat2 as Prodsubcat2;
use \Controllers\ControllerBase as CB;
use \Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;

class ProductController extends \Phalcon\Mvc\Controller {

    public function addproductAction() {
        $request = new \Phalcon\Http\Request();
        $app = new CB();
        if($request->isPost()){
            $invoice = $request->getPost('invoice');
            $product = $request->getPost('product');

            if($invoice!=null){
                try {
                    $transactionManager = new TransactionManager();
                    $transaction = $transactionManager->get();

                    $pi = new Productinvoice();
                    $pi->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();
                    $invoice_id = $guid->GUID();
                    $pi->assign(array(
                        'id' => $invoice_id,
                        'invoice_number' => $invoice['number'],
                        'distname' => $invoice['distname'],
                        'distnumber' => $invoice['distnumber']
                        ));

                    if ($pi->save() == false) {
                        $data = [];
                        foreach ($pi->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }else {
                        foreach ($product as $key => $value) {
                            $p = Product::findFirst("productid='" . $value['id'] . "'");
                            if($p){
                                $p->setTransaction($transaction);
                                $p->quantity = $value['quantity'] + $p->quantity;
                            }else {
                                $p = new Product();
                                $p->setTransaction($transaction);
                                $p->assign(array(
                                    'productid' => $value['id'],
                                    'productcode' => $value['code'],
                                    'name' => $value['name'],
                                    'shortdesc' => $value['shortdesc'],
                                    'description' => $value['description'],
                                    'quantity' => $value['quantity'],
                                    'price' => $value['price'],
                                    'discount' => $value['discount'],
                                    'discount_from' => $value['discount_from'],
                                    'discount_to' => $value['discount_to'],
                                    'belowquantity' => $value['belowquantity'],
                                    'minquantity' => $value['minquantity'],
                                    'maxquantity' => $value['maxquantity'],
                                    'status' => $value['status'] == true ? 1 : 0,
                                    'color' => isset($value['color']) ? $value['color'] : null,
                                    'slugs' => $value['slugs']
                                    ));
                                $action = "Added " . $value['name'] . " in the inventory";
                            }

                            if ($p->save() == false) {
                                $data = [];
                                foreach ($p->getMessages() as $message) {
                                    $data[] = $message;
                                }
                                $transaction->rollback();
                            }else {
                                $app->insertproducttags($value['tags'], $value['id']);
                                $app->insertproducttype($value['type'], $value['id']);
                                $app->insertproductcat($value['category'], $value['id']);
                                $app->insertproductsubcat($value['subcategory'], $value['id']);
                                $app->insertproductsize($value['size'], $value['id']);

                                $h = new Productlog();
                                $h->setTransaction($transaction);
                                $guid = new \Utilities\Guid\Guid();
                                $action = "Added " . $value['quantity'] . " qty";
                                $h->assign(array(
                                    'id' => $guid->GUID(),
                                    'action' => $action,
                                    'productid' => $p->productid,
                                    'datetime' => date("Y-m-d H:i:s")));

                                if ($h->save() == false) {
                                    $data = [];
                                    foreach ($p->getMessages() as $message) {
                                        $data[] = $message;
                                    }
                                    $transaction->rollback();
                                }
                            }

                            $inv = new Inventory();
                            $inv->setTransaction($transaction);
                            $guid = new \Utilities\Guid\Guid();
                            $inv->assign(array(
                                'id' => $guid->GUID(),
                                'productid' => $p->productid,
                                'invoiceid' => $invoice_id,
                                'quantity' => $value['quantity'],
                                'date_added' => $value['date']));

                            if ($inv->save() == false) {
                                $data = [];
                                foreach ($p->getMessages() as $message) {
                                    $data[] = $message;
                                }
                                $transaction->rollback();
                            }
                        }
                    }

                    $transaction->commit();
                    $data = array('type' => 'success', 'msg' => 'Inventory has been added successfully');
                } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                    die( json_encode(array('401' => $e->getMessage())) );
                }
            }else {
                try {
                    $transactionManager = new TransactionManager();
                    $transaction = $transactionManager->get();
                    $p = Product::findFirst("productid='" . $product['id'] . "'");
                    if($p){
                        $p->setTransaction($transaction);
                        $p->quantity = $product['quantity'] + $p->quantity;
                    }else {
                        $p = new Product();
                        $p->setTransaction($transaction);
                        $p->assign(array(
                            'productid' => $product['id'],
                            'productcode' => $product['code'],
                            'name' => $product['name'],
                            'shortdesc' => $product['shortdesc'],
                            'description' => $product['description'],
                            'quantity' => $product['quantity'],
                            'price' => $product['price'],
                            'discount' => $product['discount'],
                            'discount_from' => $product['discount_from'],
                            'discount_to' => $product['discount_to'],
                            'belowquantity' => $product['belowquantity'],
                            'minquantity' => $product['minquantity'],
                            'maxquantity' => $product['maxquantity'],
                            'status' => $product['status'] == true ? 1 : 0,
                            'color' => isset($product['color']) ? $product['color'] : null,
                            'slugs' => $product['slugs']
                            ));
                    }

                    if ($p->save() == false) {
                        $data = [];
                        foreach ($p->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    $inv = new Inventory();
                    $inv->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();
                    $inv->assign(array(
                        'id' => $guid->GUID(),
                        'productid' => $p->productid,
                        'invoiceid' => null,
                        'quantity' => $product['quantity'],
                        'date_added' => $product['date']));

                    if ($inv->save() == false) {
                        $data = [];
                        foreach ($p->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    } else {
                        $app->insertproducttags($product['tags'], $product['id']);
                        $app->insertproducttype($product['type'], $product['id']);
                        $app->insertproductsubcat($product['subcategory'], $product['id']);
                        $app->insertproductcat($product['category'], $product['id']);
                        $app->insertproductsize($product['size'], $product['id']);
                        $action = "Added " . $product['name'] . " in the inventory";
                        $h = new Productlog();
                        $h->setTransaction($transaction);
                        $guid = new \Utilities\Guid\Guid();
                        $h->assign(array(
                            'id' => $guid->GUID(),
                            'action' => $action,
                            'productid' => $p->productid,
                            'datetime' => date("Y-m-d H:i:s")));

                        if ($h->save() == false) {
                            $data = [];
                            foreach ($p->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }

                    $transaction->commit();
                    $data = array('type' => 'success', 'msg' => 'Product has been successfully added.');
                } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                    die( json_encode(array('401' => $e->getMessage())) );
                }

            }
            echo json_encode($data);
        }
    }

    public function loadimagesAction($id) {
        $images = productimages::find("productid='$id'");
        if($images){
            echo json_encode($images->toArray());
        }
    }

    public function saveimageAction($id) {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $i = Productimages::findFirst("productid='$id'");
            if($i){
                $status = 0;
            }else {
                $status = 1;
            }
            $image = new Productimages();
            $guid = new \Utilities\Guid\Guid();
            $image->assign(array(
                'id' => $guid->GUID(),
                'productid' => $id,
                'filename' => $request->getPost('filename'),
                'status' => $status ));
            $image->save();
        }
    }

    public function deleteimageAction($id) {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $image = Productimages::findFirst("productid='$id' AND filename='" . $request->getPost('filename') . "'");
            if($image){
                $image->delete();
            }
        }
    }

    public function loadproductAction() {
        $app = new CB();
        $pt = $app->bnbQuery("SELECT tags FROM producttags");
        $tags = [];
        if($pt){
            foreach ($pt as $key => $value) {
                $tags[] = $value['tags'];
            }
        }

        $itemname = $app->bnbQuery("SELECT name FROM product ORDER BY name");
        $name = [];
        if($itemname){
            foreach ($itemname as $key => $value) {
                $name[] = $value['name'];
            }
        }

        $categories = $app->bnbQuery("SELECT category FROM productcategory ORDER BY category");
        $category = [];
        if($categories){
            foreach ($categories as $key => $value) {
                $category[] = $value['category'];
            }
        }

        $sizes = $app->bnbQuery("SELECT size FROM productsize ORDER BY size");
        $size = [];
        if($sizes){
            foreach ($sizes as $key => $value) {
                $size[] = $value['size'];
            }
        }

        echo json_encode(array('tags' => $tags, 'itemname' => $name, 'category' => $category, 'size' => $size));
    }

    public function autocompleteAction($data) {
        $app = new CB();

        $data = addslashes($data);

        $product = $app->bnbQuery("SELECT * FROM product WHERE name='" . $data . "'");

        $images = $app->bnbQuery("SELECT * FROM productimages WHERE productid='" . $product[0]['productid'] . "'");

        $pt = $app->bnbQuery("SELECT tags FROM producttags INNER JOIN prodtags ON producttags.id=prodtags.tagid WHERE prodtags.productid='" . $product[0]['productid'] . "'");
        $tags = [];
        if($pt){
            foreach ($pt as $key => $value) {
                $tags[] = $value['tags'];
            }
        }

        $ptype = $app->bnbQuery("SELECT type FROM producttype INNER JOIN prodtype2 ON producttype.id=prodtype2.typeid WHERE prodtype2.productid='" . $product[0]['productid'] . "'");
        $type = [];
        if($ptype){
            foreach ($ptype as $key => $value) {
                $type[] = $value['type'];
            }
        }

        $pcat = $app->bnbQuery("SELECT category FROM productcategory INNER JOIN prodcat ON productcategory.id=prodcat.categoryid WHERE prodcat.productid='" . $product[0]['productid'] . "'");
        $category = [];
        if($pcat){
            foreach ($pcat as $key => $value) {
                $category[] = $value['category'];
            }
        }

        $psize = $app->bnbQuery("SELECT size FROM productsize INNER JOIN prodsize ON productsize.id=prodsize.sizeid WHERE prodsize.productid='" . $product[0]['productid'] . "'");
        $size = [];
        if($psize){
            foreach ($psize as $key => $value) {
                $size[] = $value['size'];
            }
        }

        $psubcat = $app->bnbQuery("SELECT subcategory FROM productsubcategory INNER JOIN prodsubcat2 ON productsubcategory.id=prodsubcat2.subcategoryid WHERE prodsubcat2.productid='" . $product[0]['productid'] . "'");
        $subcat = [];
        if($psubcat){
            foreach ($psubcat as $key => $value) {
                $subcat[] = $value['subcategory'];
            }
        }

        echo json_encode(array('product' => $product, 'images' => $images, 'tags' => $tags, 'type' => $type, 'category' => $category, 'size' => $size, 'subcategory' =>$subcat), JSON_NUMERIC_CHECK);
    }

    public function getlistAction($offset, $page, $keyword, $find) {
        $app = new CB();

        // offsetting
        $offsetfinal = ($page * 10) - 10;
        $sql = 'SELECT product.* FROM product';

        if($offsetfinal < 0){
            $offsetfinal = 0;
        }

        if($find!="null"){
           if($find == "os"){
                $sql .= " WHERE quantity=0";
            }else if($find == "nos"){
                $sql = "SELECT product.* FROM product";
                if($keyword != "null"){
                    $sql .= " WHERE product.name LIKE '%" . $keyword . "%' OR product.productcode LIKE '%" . $keyword ."%' OR product.quantity LIKE'%" . $keyword . "%' OR product.price LIKE '%" . $keyword ."%'";
                    $sql .= " OR product.color LIKE '%" . $keyword . "%' OR product.slugs LIKE '%" . $keyword . "%'";
                }
                $sql .= "  ORDER BY product.name ASC";
                $p = $app->bnbQuery($sql);
                $data = [];
                $y = 0;
                $w = 1;
                foreach ($p as $key => $value) {
                    if($value['belowquantity'] >= $value['quantity'] && $y >= $offsetfinal && $w <=10){
                        array_push($data, $value);
                        $y++;
                        $w++;
                    }else if($value['belowquantity'] >= $value['quantity'] && $y < $offsetfinal) {
                        $y++;
                    }
                }
            }else if($find == "at"){
                $sql = "SELECT product.* FROM product LEFT JOIN inventory ON product.productid=inventory.productid WHERE inventory.date_added='" . date("Y-m-d") . "'";
            }

            if($keyword != "null"){
                $sql .= " AND (product.name LIKE '%" . $keyword . "%' OR product.productcode LIKE '%" . $keyword ."%' OR product.quantity LIKE'%" . $keyword . "%' OR product.price LIKE '%" . $keyword ."%'";
                $sql .= " OR product.color LIKE '%" . $keyword . "%' OR product.slugs LIKE '%" . $keyword . "%')";
            }
        } else {
            if($keyword != "null"){
                $sql .= " WHERE product.name LIKE '%" . $keyword . "%' OR product.productcode LIKE '%" . $keyword ."%' OR product.quantity LIKE'%" . $keyword . "%' OR product.price LIKE '%" . $keyword ."%'";
                $sql .= " OR product.color LIKE '%" . $keyword . "%' OR product.slugs LIKE '%" . $keyword . "%'";
            }
        }

        if($find=="at"){
            $sql .= " GROUP BY product.productid";
        }
        $sqlCount = $sql;
        $sql .= "  ORDER BY product.name ASC LIMIT " . $offsetfinal . ", $offset";
       
        if($find == "nos"){
            $searchresult = $data;
        }else {
            $searchresult = $app->bnbQuery($sql);
        }

        foreach ($searchresult as $key => $value) {
            $searchresult[$key]['category'] = $app->bnbQuery("SELECT category FROM productcategory INNER JOIN prodcat ON productcategory.id=prodcat.categoryid WHERE prodcat.productid='" . $value['productid'] . "'");
        }

        $outofstock = $app->bnbQuery("SELECT COUNT(*) FROM product WHERE product.quantity=0")[0]['COUNT(*)'];
        $addedtoday = $app->bnbQuery("SELECT * FROM product LEFT JOIN inventory ON product.productid=inventory.productid WHERE inventory.date_added='" . date("Y-m-d") . "' GROUP BY product.productid");

        $p = $app->bnbQuery("SELECT name, quantity, belowquantity FROM product");
        $x = 0;
        foreach ($p as $key => $value) {
            if($value['belowquantity'] >= $value['quantity']){
                $x++;
            }
        }

        if($find != "nos"){
            $total_items = count($app->bnbQuery($sqlCount));
        }else {
            $total_items = $x;
        }

        $total = $app->bnbQuery("SELECT COUNT(*) FROM product")[0]['COUNT(*)'];
        echo json_encode(array('products' => $searchresult, 'index' =>$page, 'total_items' => $total_items, 'total' => $total, 'out_of_stock' => $outofstock, 'added_today' =>count($addedtoday), 'nearly_out_of_stock' => $x), JSON_NUMERIC_CHECK);
    }

    public function validatepcodeAction($id, $name) {
        $p = Product::findFirst("productcode='$id'");
        if($p){
            if($p->name == $name){
                echo json_encode(array('exist' => false ));
            }else {
                echo json_encode(array('exist' => true ));
            }
        }
    }

    public function deleteAction($id) {
        try {
            $condition = "productid='$id'";
            $p = Product::findFirst($condition);
            if($p){
                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();
                $p->setTransaction($transaction);

                if ($p->delete() == false) {
                    $data = [];
                    foreach ($p->getMessages() as $message) {
                        $data[] = $message;
                    }
                    $transaction->rollback();
                }

                $pc = Prodcat::find($condition);
                if($pc){
                    foreach($pc as $pc){
                        $pc->setTransaction($transaction);
                        if ($pc->delete() == false) {
                            $data = [];
                            foreach ($p->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }
                }

                $ps = Prodsize::find($condition);
                if($ps){
                    foreach($ps as $ps){
                        $ps->setTransaction($transaction);
                        if ($ps->delete() == false) {
                            $data = [];
                            foreach ($p->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }
                }

                $prodtags = Prodtags::find($condition);
                if($prodtags){
                    foreach($prodtags as $prodtags) {
                        $prodtags->setTransaction($transaction);
                        if ($ps->delete() == false) {
                            $data = [];
                            foreach ($p->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }
                }

                $prodtype2 = Prodtype2::find($condition);
                if($prodtype2){
                    foreach($prodtype2 as $prodtype2) {
                        $prodtype2->setTransaction($transaction);
                        if ($ps->delete() == false) {
                            $data = [];
                            foreach ($p->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }
                }
            }

            $transaction->commit();
            echo json_encode(array('type' => 'success', 'msg' => 'Product has been successfully deleted.'));
        } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
            die( json_encode(array('401' => $e->getMessage())) );
        }
    }

    public function loadeditAction($id) {
        $app = new CB();
        $product = $app->bnbQuery("SELECT * FROM product WHERE productid='" . $id . "'")[0];
        $product['images'] = $app->bnbQuery("SELECT * FROM productimages WHERE productid='" . $id . "'");

        $pt = $app->bnbQuery("SELECT tags FROM producttags INNER JOIN prodtags ON producttags.id=prodtags.tagid WHERE prodtags.productid='" . $id . "'");
        $product['tags'] = [];
        if($pt){
            foreach ($pt as $key => $value) {
                $product['tags'][] = $value['tags'];
            }
        }
        $ptype = $app->bnbQuery("SELECT type FROM producttype INNER JOIN prodtype2 ON producttype.id=prodtype2.typeid WHERE prodtype2.productid='" . $id . "'");
        $product['type'] = [];
        if($ptype){
            foreach ($ptype as $key => $value) {
                $product['type'][] = $value['type'];
            }
        }
        $pcat = $app->bnbQuery("SELECT category FROM productcategory INNER JOIN prodcat ON productcategory.id=prodcat.categoryid WHERE prodcat.productid='" . $id . "'");
        $product['category'] = [];
        if($pcat){
            foreach ($pcat as $key => $value) {
                $product['category'][] = $value['category'];
            }
        }
        $psubcat = $app->bnbQuery("SELECT subcategory FROM productsubcategory INNER JOIN prodsubcat2 ON productsubcategory.id=prodsubcat2.subcategoryid WHERE prodsubcat2.productid='" . $id . "'");
        $product['subcategory'] = [];
        if($psubcat){
            foreach ($psubcat as $key => $value) {
                $product['subcategory'][] = $value['subcategory'];
            }
        }
        $psize = $app->bnbQuery("SELECT size FROM productsize INNER JOIN prodsize ON productsize.id=prodsize.sizeid WHERE prodsize.productid='" . $id . "'");
        $product['size'] = [];
        if($psize){
            foreach ($psize as $key => $value) {
                $product['size'][] = $value['size'];
            }
        }
        $pt = $app->bnbQuery("SELECT tags FROM producttags");
        $tags = [];
        if($pt){
            foreach ($pt as $key => $value) {
                $tags[] = $value['tags'];
            }
        }
        $types = $app->bnbQuery("SELECT type FROM producttype ORDER BY type");
        $type = [];
        if($types){
            foreach ($types as $key => $value) {
                $type[] = $value['type'];
            }
        }
        $categories = $app->bnbQuery("SELECT category FROM productcategory ORDER BY category");
        $category = [];
        if($categories){
            foreach ($categories as $key => $value) {
                $category[] = $value['category'];
            }
        }
        $sizes = $app->bnbQuery("SELECT size FROM productsize ORDER BY size");
        $size = [];
        if($sizes){
            foreach ($sizes as $key => $value) {
                $size[] = $value['size'];
            }
        }
        echo json_encode(array('product' => $product, 'tags'=>$tags, 'type' => $type, 'category' => $category, 'size' => $size), JSON_NUMERIC_CHECK);
    }

    public function fe_listofproductsAction() {
      $products = CB::bnbQuery("SELECT product.productid, product.productcode, product.name, product.shortdesc, product.quantity, product.price, product.discount, product.minquantity, product.maxquantity, product.color,
                                        productimages.filename,
                                        producttype.type,
                                        productsize.size
                                FROM product
                                LEFT JOIN productimages ON product.productid = productimages.productid
                                LEFT JOIN prodtype2 ON product.productid = prodtype2.productid
                                LEFT JOIN producttype ON prodtype2.typeid = producttype.id
                                LEFT JOIN prodsize ON product.productid = prodsize.productid
                                LEFT JOIN productsize ON prodsize.sizeid = productsize.id
                                WHERE product.status = 1
                                GROUP BY product.productcode");

        echo json_encode($products);
    }

    public function fe_addtocartAction() {
      $request = new Request();
      if($request->isPost()) {
        $productid = $request->getPost("productid");
        $cart = $request->getPost("cart");

        $data['product'] = CB::bnbQueryFirst("SELECT product.productid, product.productcode, product.name, product.shortdesc, product.quantity, product.price, product.discount, product.minquantity, product.maxquantity, product.color,
                                              productimages.filename,
                                              producttype.type,
                                              productsize.size
                                      FROM product
                                      LEFT JOIN productimages ON product.productid = productimages.productid
                                      LEFT JOIN prodtype2 ON product.productid = prodtype2.productid
                                      LEFT JOIN producttype ON prodtype2.typeid = producttype.id
                                      LEFT JOIN prodsize ON product.productid = prodsize.productid
                                      LEFT JOIN productsize ON prodsize.sizeid = productsize.id
                                      WHERE product.productid = '".$productid."' AND product.status = 1 ");

        if($data['product']['discount'] != undefined && $data['product']['discount'] != null && $data['product']['discount'] > 0) {
          $data['product']['finalprice'] = $data['product']['price'] - ($data['product']['price'] * ($data['product']['discount'] / 100));
        } else {
          $data['product']['finalprice'] = $data['product']['price'];
        }

        $data['total'] = 0;
        foreach($cart as $id) {
          $item = Product::findFirst("productid = '".$id."' ");
          if($item->discount != undefined && $item->discount != null && $item->discount > 0) {
              $price = $item->price - ($item->price * ($item->discount / 100));
          } else {
              $price = $item->price;
          }
          $data['total'] = $data['total'] + $price;
        }

        echo json_encode($data);
      }
    }

    public function fe_mycartAction() {
      $request = new Request();
      if($request->isPost()) {
        $cart = $request->getPost("cart");

        $data['mycart'] = array();
        $data['total'] = 0;
        foreach($cart as $key => $value) {
          $data['mycart'][] = CB::bnbQueryFirst("SELECT product.productid, product.productcode, product.name, product.shortdesc, product.quantity, product.price, product.discount, product.minquantity, product.maxquantity, product.color,
                                                  productimages.filename,
                                                  producttype.type,
                                                  productsize.size
                                        FROM product
                                        LEFT JOIN productimages ON product.productid = productimages.productid
                                        LEFT JOIN prodtype2 ON product.productid = prodtype2.productid
                                        LEFT JOIN producttype ON prodtype2.typeid = producttype.id
                                        LEFT JOIN prodsize ON product.productid = prodsize.productid
                                        LEFT JOIN productsize ON prodsize.sizeid = productsize.id
                                        WHERE product.productid = '".$value."' AND product.status = 1 ");
          if($data['mycart'][$key]['discount'] != undefined && $data['mycart'][$key]['discount'] != null && $data['mycart'][$key]['discount'] > 0) {
            $data['mycart'][$key]['price'] = $data['mycart'][$key]['price'] - ($data['mycart'][$key]['price'] * ($data['mycart'][$key]['discount'] / 100));
          }

          $data['total'] = $data['total'] + $data['mycart'][$key]['price'];
        }
        echo json_encode($data);
      }
    }

    public function fe_productAction($productcode) {
      $data['product'] = CB::bnbQueryFirst("SELECT product.*, productimages.filename, producttype.type, productsize.size
                                            FROM product
                                            LEFT JOIN productimages ON product.productid = productimages.productid
                                            LEFT JOIN prodtype2 ON product.productid = prodtype2.productid
                                            LEFT JOIN producttype ON prodtype2.typeid = producttype.id
                                            LEFT JOIN prodsize ON product.productid = prodsize.productid
                                            LEFT JOIN productsize ON prodsize.sizeid = productsize.id
                                            WHERE product.productcode = '".$productcode."' AND product.status = 1 ");

      $data['productimages'] = CB::bnbQuery("SELECT filename FROM productimages WHERE productid = '".$data['product']['productid']."' ");
      echo json_encode($data);
    }

    public function fe_placeorderAction() {
      $request = new Request();
      if($request->isPost()) {
        $token = $request->getPost("token");
        $payment = $request->getPost("payment");

        if($paymethod['method'] == 'CC') {
          $expimonth = sprintf("%02d", $payment['cc_expimonth']);
          $expiyear = $payment['cc_expiyear'];

          $post_url = "https://test.authorize.net/gateway/transact.dll";
          // $post_url = "https://secure.authorize.net/gateway/transact.dll";
          $post_values = array(
            "x_login"           => "65k4YRwP",
            "x_tran_key"        => "8pV7Th75h5vP5a3M",
            "x_version"         => "3.1",
            "x_delim_data"      => "TRUE",
            "x_delim_char"      => "|",
            "x_relay_response"  => "FALSE",
            "x_type"            => "AUTH_CAPTURE",
            "x_method"          => "CC",
            "x_card_num"        => $payment['creditcardnumber'],
            "x_exp_date"        => $expimonth.$expiyear,
            "x_amount"          => $payment['grandtotal'],
            "x_description"     => "SEDONA SHOP",
            "x_first_name"      => $token['firstname'],
            "x_last_name"       => $token['lastname'],
            "x_card_code"		    => $payment['ccvnumber']
            // Additional fields can be added here as outlined in the AIM integration
            // guide at: http://developer.authorize.net
          );

          $post_string = "";
          foreach( $post_values as $key => $value )
          { $post_string .= "$key=" . urlencode( $value ) . "&"; }
          $post_string = rtrim( $post_string, "& " );

          $request = curl_init($post_url); // initiate curl object
          curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
          curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
          curl_setopt($request, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
          curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
          curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
          $post_response = curl_exec($request); // execute curl post and store results in $post_response
          // additional options may be required depending upon your server configuration
          // you can find documentation on curl options at http://www.php.net/curl_setopt
          curl_close ($request); // close curl object

          $response_array = explode($post_values["x_delim_char"],$post_response);
          $data['authorize'] = $response_array;
          $data['status'] = $response_array[0]; // 1 -> success | 3 -> failed

          $transactionID = $response_array[6];
          $paid = $response_array[9]; // $money
          $cardtype = $response_array[51]; //mastercard, visa, etc
         } // end of $paymethod['method'] == 'CC'

        $customerid = $token['id'];
        $paymentmethod = $payment['method']; // CC -> Creditcard;

        //THEN SAVE THE DATA TO DB THANKYOU :)

      echo json_encode($data);
      }
    }

    public function editAction() {
        $request = new \Phalcon\Http\Request();
        $app = new CB();
        if($request->isPost()){
            $id = $request->getPost('productid');
            $pr = Product::findFirst("productid='$id'");
            if($pr){
                try {
                    $data = array(
                        'name' => $request->getPost('name'),
                        'shortdesc' => $request->getPost("shortdesc"),
                        'description' => $request->getPost('description'),
                        'quantity' => $request->getPost('quantity'),
                        'price' => $request->getPost('price'),
                        'discount' => $request->getPost('discount'),
                        'discount_from' => $request->getPost('discount_from'),
                        'discount_to' => $request->getPost('discount_to'),
                        'belowquantity' => $request->getPost('belowquantity'),
                        'minquantity' => $request->getPost('minquantity'),
                        'maxquantity' => $request->getPost('maxquantity'),
                        'status' => $request->getPost('status'),
                        'color' => $request->getPost('color'),
                        'slugs' => $request->getPost('slugs'));

                    $action = $app->genLogs($pr, $data);

                    $transactionManager = new TransactionManager();
                    $transaction = $transactionManager->get();
                    $pr->setTransaction($transaction);
                    $pr->name = $data['name'];
                    $pr->shortdesc = $data['shortdesc'];
                    $pr->description = $data['description'];
                    $pr->quantity = $data['quantity'];
                    $pr->price = $data['price'];
                    $pr->discount = $data['discount'];
                    $pr->discount_from = $data['discount_from'];
                    $pr->discount_to = $data['discount_to'];
                    $pr->belowquantity = $data['belowquantity'];
                    $pr->minquantity = $data['minquantity'];
                    $pr->maxquantity = $data['maxquantity'];
                    $pr->status = $data['status'] == 'true' ? 1 : 0;
                    $pr->color = $data['color'];
                    $pr->slugs = $data['slugs'];

                    if ($pr->save() == false) {
                        $data = [];
                        foreach ($pr->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }else {
                        $action .= $app->insertproductcat($request->getPost('category'), $request->getPost('productid'));
                        $action .= $app->insertproductsubcat($request->getPost('subcategory'), $request->getPost('productid'));
                        $action .= $app->insertproducttype($request->getPost('type'), $request->getPost('productid'));
                        $action .= $app->insertproducttags($request->getPost('tags'), $request->getPost('productid'));
                        $action .= $app->insertproductsize($request->getPost('size'), $request->getPost('productid'));

                        if(strpos($action,'</li>') != false){
                            $h = new Productlog();
                            $h->setTransaction($transaction);
                            $guid = new \Utilities\Guid\Guid();

                            $action .= addslashes("</ul>");
                            $h->assign(array(
                                'id' => $guid->GUID(),
                                'action' => $action,
                                'productid' => $request->getPost('productid'),
                                'datetime' => date("Y-m-d H:i:s")));

                            if ($h->save() == false) {
                                $data = [];
                                foreach ($p->getMessages() as $message) {
                                    $data[] = $message;
                                }
                                $transaction->rollback();
                            }
                        }
                    }

                    $transaction->commit();
                    echo json_encode(array('type' => 'success', 'msg' => 'Product has been updated successfully' ));
                } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                    die( json_encode(array('401' => $e->getMessage())) );
                }
            }
        }
    }

    public function validatenameAction($id, $name) {
        $p = Product::findFirst("name='$name' AND productid != '$id'");
        if($p){
            echo json_encode(array('exist' => true ));
        }else {
            echo json_encode(array('exist' => false ));
        }
    }

    public function updatestatusAction($id, $status) {
        $p = Product::findFirst("productid='$id'");
        if($p){
            $p->status = $status;
            if ($p->save() == false) {
                $data = [];
                foreach ($p->getMessages() as $message) {
                    $data[] = $message;
                }
            }else {
                $stat = $status==1 ? "enabled" : "disabled";
                $action = "Updated this products status to " . $stat;
                $h = new Productlog();
                $guid = new \Utilities\Guid\Guid();
                $h->assign(array(
                    'id' => $guid->GUID(),
                    'action' => $action,
                    'productid' => $id,
                    'datetime' => date("Y-m-d H:i:s")));

                if ($h->save() == false) {
                    $data = [];
                    foreach ($p->getMessages() as $message) {
                        $data[] = $message;
                    }
                }
            }
            echo json_encode($data);
        }
    }

    public function validateslugAction($id, $slugs) {
        $p = Product::findFirst("slugs='$slugs' AND productid !='$id'");
        if($p){
            echo json_encode(array('exist' => true ));
        }else {
            echo json_encode(array('exist' => false ));
        }
    }

    public function addquantityAction($id) {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()) {
            try {
                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();

                $p = Product::findFirst("productid='$id'");
                if($p){
                    $guid = new \Utilities\Guid\Guid();
                    $inv = new Inventory();
                    $inv->setTransaction($transaction);
                    $inv->assign(array(
                        'id' => $guid->GUID(),
                        'productid' => $id,
                        'invoiceid' => null,
                        'quantity' => $request->getPost('quantity'),
                        'date_added' => $request->getPost('date') ));

                    if ($inv->save() == false) {
                        $data = [];
                        foreach ($inv->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    $action = "Updated quantity " . $p->quantity . " to " . ($request->getPost('quantity') + $p->quantity);
                    $h = new Productlog();
                    $h->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();
                    $h->assign(array(
                        'id' => $guid->GUID(),
                        'action' => $action,
                        'productid' => $id,
                        'datetime' => date("Y-m-d H:i:s")));

                    if ($h->save() == false) {
                        $data = [];
                        foreach ($p->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    $p->setTransaction($transaction);
                    $p->quantity = $request->getPost('quantity') + $p->quantity;

                    if ($p->save() == false) {
                        foreach ($p->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    $transaction->commit();
                    echo json_encode(array('type' => 'success', 'msg' => 'Product quantity has been added successfully'));
                }
            } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
        }
    }

    public function adddiscountAction($id) {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $p = Product::findFirst("productid='$id'");
            if($p){
                $action = "Updated the discount to " . $p->discount . "% to " . $request->getPost('discount') . "%";

                $p->discount = $request->getPost('discount');
                $p->discount_from = $request->getPost('discount_from');
                $p->discount_to = $request->getPost('discount_to');
                if ($p->save() == false) {
                    foreach ($p->getMessages() as $message) {
                        $data[] = $message;
                    }
                }else {
                    $h = new Productlog();
                    $h->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();
                    $h->assign(array(
                        'id' => $guid->GUID(),
                        'action' => $action,
                        'productid' => $id,
                        'datetime' => date("Y-m-d H:i:s")));

                    if ($h->save() == false) {
                        $data = [];
                        foreach ($p->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    $data = array('type' => 'success', 'msg' => 'Product discount has been added successfully');
                }

                echo json_encode($data);
            }
        }
    }

    public function viewproductAction($id) {
        $app = new CB();
        $product = $app->bnbQuery("SELECT * FROM product WHERE productid='" . $id . "'")[0];
        $product['images'] = $app->bnbQuery("SELECT * FROM productimages WHERE productid='" . $id . "'");

        $pt = $app->bnbQuery("SELECT tags FROM producttags INNER JOIN prodtags ON producttags.id=prodtags.tagid WHERE prodtags.productid='" . $id . "'");
        $product['tags'] = [];
        if($pt){
            foreach ($pt as $key => $value) {
                $product['tags'][] = $value['tags'];
            }
        }

        $ptype = $app->bnbQuery("SELECT type FROM producttype INNER JOIN prodtype2 ON producttype.id=prodtype2.typeid WHERE prodtype2.productid='" . $id . "'");
        $product['type'] = [];
        if($ptype){
            foreach ($ptype as $key => $value) {
                $product['type'][] = $value['type'];
            }
        }

        $pt = $app->bnbQuery("SELECT category FROM productcategory INNER JOIN prodcat ON productcategory.id=prodcat.categoryid WHERE prodcat.productid='" . $id . "'");
        $product['category'] = [];
        if($pt){
            foreach ($pt as $key => $value) {
                $product['category'][] = $value['category'];
            }
        }

        $pt = $app->bnbQuery("SELECT subcategory FROM productsubcategory INNER JOIN prodsubcat2 ON productsubcategory.id=prodsubcat2.subcategoryid WHERE prodsubcat2.productid='" . $id . "'");
        $product['subcategory'] = [];
        if($pt){
            foreach ($pt as $key => $value) {
                $product['subcategory'][] = $value['subcategory'];
            }
        }

        $page = 1;
        $offsetfinal = ($page * 10) - 10;
        $totalreportdirty_inv = $app->bnbQuery("SELECT COUNT(*) FROM productinvoice INNER JOIN inventory ON productinvoice.id=inventory.invoiceid WHERE inventory.productid='" . $id . "'");
        $invoice = $app->bnbQuery("SELECT productinvoice.invoice_number, inventory.date_added, inventory.quantity, productinvoice.distname FROM productinvoice INNER JOIN inventory ON productinvoice.id=inventory.invoiceid WHERE inventory.productid='" . $id . "' ORDER BY inventory.date_added DESC LIMIT 10");

        $totalreportdirty_hist = $app->bnbQuery("SELECT COUNT(*) FROM productlog WHERE productid='" . $id . "'");
        $history = $app->bnbQuery("SELECT * FROM productlog WHERE productid='" . $id . "' ORDER BY datetime DESC LIMIT 10");
        echo json_encode(
            array('product' => $product,
                'invoice' => $invoice,
                'history' => $history,
                'index' =>$page,
                'total_items_invoice' => $totalreportdirty_inv[0]["COUNT(*)"],
                'total_items_hist' => $totalreportdirty_hist[0]["COUNT(*)"])
            , JSON_NUMERIC_CHECK);
    }

    public function viewdetailsAction($id, $type, $page) {
        $app = new CB();
        $offsetfinal = ($page * 10) - 10;

        if($type=='invoice'){
            $sql = "SELECT
                productinvoice.invoice_number,
                inventory.date_added,
                inventory.quantity,
                productinvoice.distname FROM productinvoice INNER JOIN inventory ON productinvoice.id=inventory.invoiceid WHERE inventory.productid='" . $id . "' ORDER BY inventory.date_added DESC";
            $sqlCount = "SELECT COUNT(*) FROM productinvoice INNER JOIN inventory ON productinvoice.id=inventory.invoiceid WHERE inventory.productid='" . $id . "' ORDER BY inventory.date_added DESC";
            $totalreportdirty = $app->bnbQuery($sqlCount);

            $sql .= " LIMIT " .$offsetfinal. ",10";
            $data = $app->bnbQuery($sql);
        } else if($type=='history') {
            $totalreportdirty = $app->bnbQuery("SELECT COUNT(*) FROM productlog WHERE productid='" . $id . "'");
            $data = $app->bnbQuery("SELECT * FROM productlog WHERE productid='" . $id . "' ORDER BY datetime DESC LIMIT " . $offsetfinal . ",10");
        }

        echo json_encode(array('data' => $data, 'total_items' => $totalreportdirty[0]["COUNT(*)"], 'index' => $page), JSON_NUMERIC_CHECK);
    }

    public function selectimageAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()) {
            $id = $request->getPost('id');
            $productid = $request->getPost('productid');

            $pr = Productimages::findFirst("productid='$productid' AND status=1");
            if($pr){
                $pr->status = 0;
                if ($pr->save() == false) {
                    $data = [];
                    foreach ($p->getMessages() as $message) {
                        $data[] = $message;
                    }
                    die(json_encode($data));
                }
            }

            $pr = Productimages::findFirst("id='$id'");
            if($pr){
                $pr->status = 1;
                if ($pr->save() == false) {
                    $data = [];
                    foreach ($p->getMessages() as $message) {
                        $data[] = $message;
                    }
                    die(json_encode($data));
                }else {
                    $pr = Productimages::find("productid='$productid'");
                    if($pr){
                        echo json_encode($pr->toArray(), JSON_NUMERIC_CHECK);
                    }
                }
            }
        }
    }

    public function loadcategorylistAction($off, $page, $keyword) {
        $app = new CB();

        $offsetfinal = ($page * 10) - 10;
        $sql = "SELECT productcategory.category, productcategory.id FROM productcategory";
        $sqlCount = "SELECT COUNT(*) FROM productcategory";
        if($keyword!="null"){
            $sql .= " WHERE productcategory.category LIKE '%" . $keyword . "%'";
            $sqlCount .= " WHERE productcategory.category LIKE '%" . $keyword . "%'";
        }

        $sql .= " ORDER BY updated_at DESC LIMIT $offsetfinal, 10";

        $category = $app->bnbQuery($sql);

        foreach ($category as $key => $value) {
            $category[$key]['subcategory'] = $app->bnbQuery("SELECT productsubcategory.subcategory FROM productsubcategory INNER JOIN prodsubcat ON productsubcategory.id=prodsubcat.subcategoryid WHERE prodsubcat.categoryid='" . $value['id'] . "'");
        }

        $total_items = $app->bnbQuery($sqlCount)[0]['COUNT(*)'];

        echo json_encode(array('category' => $category, 'index' =>$page, 'total_items' => $total_items), JSON_NUMERIC_CHECK);
    }

    public function validatecategoryAction($id, $category) {
        if($id!="null"){
            $condition = "category='$category' AND id!='$id'";
        }else {
            $condition = "category='$category'";
        }
        $pc = Productcategory::findFirst($condition);
        if($pc){
            echo json_encode(array('exist' => true));
        }else {
            echo json_encode(array('exist' => false));
        }
    }

    public function loadsubcategoryAction() {
        $app = new CB();
        $sc = $app->bnbQuery("SELECT subcategory FROM productsubcategory");
        $subcategory = [];
        if($sc){
            foreach ($sc as $key => $value) {
                $subcategory[] = $value['subcategory'];
            }
        }
        echo json_encode($subcategory);
    }

    public function savecategoryAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            try {
                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();

                $pr = new Productcategory();
                $pr->setTransaction($transaction);
                $guid = new \Utilities\Guid\Guid();

                $pr->assign(array(
                    'id' => $guid->GUID(),
                    'category' => $request->getPost('category'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s') ));

                if ($pr->save() == false) {
                    $data = [];
                    foreach ($pr->getMessages() as $message) {
                        $data[] = $message;
                    }
                    $transaction->rollback();
                }

                $psc = Prodsubcat::find("categoryid='$pr->id'");
                if($psc){
                    $psc->delete();
                }
                foreach ($request->getPost('subcategory') as $key => $value) {
                    $subcat = Productsubcategory::findFirst("subcategory='" . $value . "'");
                    if(!$subcat){
                        $subcat = new Productsubcategory();
                        $subcat->setTransaction($transaction);
                        $guid = new \Utilities\Guid\Guid();

                        $subcat->assign(array(
                            'id' => $guid->GUID(),
                            'subcategory' => $value,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s') ));

                        if ($subcat->save() == false) {
                            $data = [];
                            foreach ($subcat->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }

                    $psc = new Prodsubcat();
                    $psc->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();

                    $psc->assign(array(
                        'id' => $guid->GUID(),
                        'categoryid' => $pr->id,
                        'subcategoryid' => $subcat->id
                        ));

                    if ($psc->save() == false) {
                        $data = [];
                        foreach ($psc->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }
                }

                $transaction->commit();
                $data = array('type' => 'success', 'msg' => 'Product category has been added successfully.'); 
                echo json_encode($data);
            } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
        }
    }

    public function deletecategoryAction($id) {
        $pc = Productcategory::findFirst("id='$id'");
        if($pc){
            if($pc->delete()){
                $pr = Prodcat::find("categoryid='$id'");
                if($pr){
                    $pr->delete();
                }

                echo json_encode(array('type' => 'success', 'msg' => 'Product category has been deleted successfully.'));
            }else {
                echo json_encode(array('type' => 'danger', 'msg' => 'An error occurred please try again later.'));
            }
        }
    }

    public function loadeditcategoryAction($id) {
        $app = new CB();

        $sql = "SELECT productcategory.category, productcategory.id FROM productcategory WHERE productcategory.id='$id'";
        $category = $app->bnbQuery($sql);

        $subcat = $app->bnbQuery("SELECT productsubcategory.subcategory FROM productsubcategory INNER JOIN prodsubcat ON productsubcategory.id=prodsubcat.subcategoryid WHERE prodsubcat.categoryid='$id'");

        $sc = $app->bnbQuery("SELECT subcategory FROM productsubcategory");
        $subcategory = [];
        if($sc){
            foreach ($sc as $key => $value) {
                $subcategory[] = $value['subcategory'];
            }
        }

        echo json_encode(array('category' => $category[0], 'subcategory' => $subcategory, 'subcat' => $subcat));
    }

    public function editcategoryAction() {
        $request = new \Phalcon\Http\Request();

        if($request->getPost()){
            $id = $request->getPost('id');
            try {
                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();
                $pc = Productcategory::findFirst("id='$id'");
                if($pc){
                    $pc->setTransaction($transaction);
                    $pc->category = $request->getPost('category');
                    $pc->updated_at = date("Y-m-d H:i:s");
                    if ($pc->save() == false) {
                        $data = [];
                        foreach ($pc->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    $prsc = Prodsubcat::find("categoryid='$id'");
                    if($prsc){
                        if ($prsc->delete() == false) {
                            $data = [];
                            foreach ($prsc->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }

                    foreach ($request->getPost('subcategory') as $key => $value) {
                        $sc = Productsubcategory::findFirst("subcategory='$value'");
                        if(!$sc){
                            $sc = new Productsubcategory();
                            $sc->setTransaction($transaction);
                            $guid = new \Utilities\Guid\Guid();

                            $sc->assign(array(
                                'id' => $guid->GUID(),
                                'subcategory' => $value,
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s") ));

                            if ($sc->save() == false) {
                                $data = [];
                                foreach ($sc->getMessages() as $message) {
                                    $data[] = $message;
                                }
                                $transaction->rollback();
                            }
                        }else {
                            $sc->setTransaction($transaction);
                        }

                        $prsc = new Prodsubcat();
                        $prsc->setTransaction($transaction);
                        $guid = new \Utilities\Guid\Guid();
                        $prsc->assign(array(
                                'id' => $guid->GUID(),
                                'categoryid' => $id,
                                'subcategoryid' => $sc->id ));
                        if ($prsc->save() == false) {
                            $data = [];
                            foreach ($prsc->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }

                    $transaction->commit();
                    $data = array('type' => 'success', 'msg' => 'Category has been edited successfully.');
                }else {
                    $data = array('type' => 'danger', 'msg' => 'An error occurred please try again later.' );
                }

                echo json_encode($data);

            } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
        }
    }

    public function loadtagslistAction($num, $page, $keyword) {
        $app = new CB();

        $offsetfinal = ($page * 10) - 10;
        $sql = "SELECT producttags.tags, producttags.id FROM producttags";
        $sqlCount = "SELECT COUNT(*) FROM producttags";
        if($keyword!="null"){
            $sql .= " WHERE producttags.tags LIKE '%" . $keyword . "%'";
            $sqlCount .= " WHERE producttags.tags LIKE '%" . $keyword . "%'";
        }

        $sql .= " ORDER BY updated_at DESC LIMIT $offsetfinal, 10";

        $tags = $app->bnbQuery($sql);

        $total_items = $app->bnbQuery($sqlCount)[0]['COUNT(*)'];

        echo json_encode(array('tags' => $tags, 'index' =>$page, 'total_items' => $total_items), JSON_NUMERIC_CHECK);
    }

    public function deletetagsAction($id) {
        $pt = Producttags::findFirst("id='$id'");
        if($pt){
            if ($pt->delete() == false) {
                $data = array('type' => 'danger', 'msg' => 'An error occurred please try again later.' );
            }else {
                $data = array('type' => 'success', 'msg' => 'Product tags has been successfully deleted.' );
                $pt = Prodtags::find("tagid='$id'");
                if($pt){
                    if ($pt->delete() == false) {
                        $data = array('type' => 'danger', 'msg' => 'An error occurred please try again later.' );
                    }
                }
            }
            echo json_encode($data);
        }
    }

    public function validatetagsAction($id, $tags) {
        if($id!="null"){
            $condition = "tags='$tags' AND id!='$id'";
        }else {
            $condition = "tags='$tags'";
        }
        $pc = Producttags::findFirst($condition);
        if($pc){
            echo json_encode(array('exist' => true));
        }else {
            echo json_encode(array('exist' => false));
        }
    }

    public function savetagAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $tag = $request->getPost('tag');
            $pt = Producttags::findFirst("tags='$tag'");
            if(!$pt){
                $data = array('type' => 'success', 'msg' => 'Product tag has been added successfully.' );
                $pt = new Producttags();
                $guid = new \Utilities\Guid\Guid();
                $pt->assign(array(
                    'id' => $guid->GUID(),
                    'tags' => $tag,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s") ));

                if($pt->save() == false){
                    $data = array('type' => 'danger', 'msg' => 'Product tag already exist.' );
                }
            }else {
                $data = array('type' => 'danger', 'msg' => 'Product tag already exist.' );
            }

            echo json_encode($data);
        }
    }

    public function updatetagsAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $id = $request->getPost("id");
            $tags = $request->getPost("tags");

            $pt = Producttags::findFirst("tags='$tags' AND id!='$id'");
            if($pt){
                $data = array('type' => 'danger', 'msg' => 'Product tag already exist.');
            } else {
                $pt = Producttags::findFirst("id='$id'");
                if($pt){
                    $data = array('type' => 'success', 'msg' => 'Product tags has been successfully edited.');
                    $pt->tags = $tags;
                    if($pt->save() == false){
                        $data = array('type' => 'danger', 'msg' => 'An error occured please try again later.');
                    }
                }else {
                    $data = array('type' => 'danger', 'msg' => 'An error occured please try again later.');
                }
            } 

            echo json_encode($data);
        }
    }

    public function loadsubcatAction($off, $page, $keyword) {
        $app = new CB();

        $offsetfinal = ($page * 10) - 10;
        $sql = "SELECT productsubcategory.subcategory, productsubcategory.id FROM productsubcategory";
        $sqlCount = "SELECT COUNT(*) FROM productsubcategory";
        if($keyword!="null"){
            $sql .= " WHERE productsubcategory.category LIKE '%" . $keyword . "%'";
            $sqlCount .= " WHERE productsubcategory.category LIKE '%" . $keyword . "%'";
        }

        $sql .= " ORDER BY updated_at DESC LIMIT $offsetfinal, 10";

        $subcategory = $app->bnbQuery($sql);

        foreach ($subcategory as $key => $value) {
            $subcategory[$key]['types'] = $app->bnbQuery("SELECT producttype.type FROM producttype INNER JOIN prodtype ON producttype.id=prodtype.typeid WHERE prodtype.subcategoryid='" . $value['id'] . "'");
        }

        $total_items = $app->bnbQuery($sqlCount)[0]['COUNT(*)'];

        echo json_encode(array('subcategory' => $subcategory, 'index' =>$page, 'total_items' => $total_items), JSON_NUMERIC_CHECK);
    }

    public function validatesubcatAction($id, $subcategory) {
        if($id!="null"){
            $condition = "subcategory='$subcategory' AND id!='$id'";
        }else {
            $condition = "subcategory='$subcategory'";
        }
        $pc = Productsubcategory::findFirst($condition);
        if($pc){
            echo json_encode(array('exist' => true));
        }else {
            echo json_encode(array('exist' => false));
        }
    }

    public function loadtypesAction(){
        $app = new CB();
        $pt = $app->bnbQuery("SELECT type FROM producttype");
        $producttype = [];
        if($pt){
            foreach ($pt as $key => $value) {
                $producttype[] = $value['type'];
            }
        }
        echo json_encode($producttype);
    }

    public function addsubcatAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            try {
                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();

                $pr = new Productsubcategory();
                $pr->setTransaction($transaction);
                $guid = new \Utilities\Guid\Guid();

                $pr->assign(array(
                    'id' => $guid->GUID(),
                    'subcategory' => $request->getPost('subcategory'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s') ));

                if ($pr->save() == false) {
                    $data = [];
                    foreach ($pr->getMessages() as $message) {
                        $data[] = $message;
                    }
                    $transaction->rollback();
                }

                $psc = Prodtype::find("subcategoryid='$pr->id'");
                if($psc){
                    $psc->delete();
                }
                foreach ($request->getPost('type') as $key => $value) {
                    $type = Producttype::findFirst("type='" . $value . "'");
                    if(!$type){
                        $type = new Producttype();
                        $type->setTransaction($transaction);
                        $guid = new \Utilities\Guid\Guid();

                        $type->assign(array(
                            'id' => $guid->GUID(),
                            'type' => $value,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s') ));

                        if ($type->save() == false) {
                            $data = [];
                            foreach ($type->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }

                    
                    $psc = new Prodtype();
                    $psc->setTransaction($transaction);
                    $guid = new \Utilities\Guid\Guid();

                    $psc->assign(array(
                        'id' => $guid->GUID(),
                        'subcategoryid' => $pr->id,
                        'typeid' => $type->id
                        ));

                    if ($psc->save() == false) {
                        $data = [];
                        foreach ($psc->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }
                }

                $transaction->commit();
                $data = array('type' => 'success', 'msg' => 'Product sub category has been added successfully.'); 
                echo json_encode($data);
            } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
        }
    }

    public function deletesubcategoryAction($id){
        $pc = Productsubcategory::findFirst("id='$id'");
        if($pc){
            if($pc->delete()){
                $pr = Prodsubcat::find("subcategoryid='$id'");
                if($pr){
                    $pr->delete();
                }
                $pr = Prodsubcat2::find("subcategoryid='$id'");
                if($pr){
                    $pr->delete();
                }
                $pt = Prodtype::find("subcategoryid='$id'");
                if($pt){
                    $pt->delete();
                }
                echo json_encode(array('type' => 'success', 'msg' => 'Product category has been deleted successfully.'));
            }else {
                echo json_encode(array('type' => 'danger', 'msg' => 'An error occurred please try again later.'));
            }
        }
    }

    public function loadeditsubcategoryAction($id){
        $app = new CB();
        $pt = $app->bnbQuery("SELECT type FROM producttype");
        $producttype = [];
        if($pt){
            foreach ($pt as $key => $value) {
                $producttype[] = $value['type'];
            }
        }

        $subcategory = $app->bnbQuery("SELECT productsubcategory.subcategory, productsubcategory.id FROM productsubcategory WHERE productsubcategory.id='$id'")[0];

        $type = $app->bnbQuery("SELECT producttype.type FROM producttype INNER JOIN prodtype ON producttype.id=prodtype.typeid WHERE prodtype.subcategoryid='" . $subcategory['id'] . "'");
        $subcategory['type'] = [];
        foreach ($type as $key => $value) {
            $subcategory['type'][] = $value['type'];
        }

        echo json_encode(array('subcategory' => $subcategory , 'type' => $producttype));
    }

    public function updatesubcategoryAction() {
        $request = new \Phalcon\Http\Request();

        if($request->getPost()){
            $id = $request->getPost('id');
            try {
                $transactionManager = new TransactionManager();
                $transaction = $transactionManager->get();
                $pc = Productsubcategory::findFirst("id='$id'");
                if($pc){
                    $pc->setTransaction($transaction);
                    $pc->subcategory = $request->getPost('subcategory');
                    $pc->updated_at = date("Y-m-d H:i:s");
                    if ($pc->save() == false) {
                        $data = [];
                        foreach ($pc->getMessages() as $message) {
                            $data[] = $message;
                        }
                        $transaction->rollback();
                    }

                    $prsc = Prodtype::find("subcategoryid='$id'");
                    if($prsc){
                        if ($prsc->delete() == false) {
                            $data = [];
                            foreach ($prsc->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }

                    foreach ($request->getPost('type') as $key => $value) {
                        $sc = Producttype::findFirst("type='$value'");
                        if(!$sc){
                            $sc = new Producttype();
                            $sc->setTransaction($transaction);
                            $guid = new \Utilities\Guid\Guid();

                            $sc->assign(array(
                                'id' => $guid->GUID(),
                                'type' => $value,
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s") ));

                            if ($sc->save() == false) {
                                $data = [];
                                foreach ($sc->getMessages() as $message) {
                                    $data[] = $message;
                                }
                                $transaction->rollback();
                            }
                        }else {
                            $sc->setTransaction($transaction);
                        }

                        $prsc = new Prodtype();
                        $prsc->setTransaction($transaction);
                        $guid = new \Utilities\Guid\Guid();
                        $prsc->assign(array(
                                'id' => $guid->GUID(),
                                'subcategoryid' => $id,
                                'typeid' => $sc->id ));
                        if ($prsc->save() == false) {
                            $data = [];
                            foreach ($prsc->getMessages() as $message) {
                                $data[] = $message;
                            }
                            $transaction->rollback();
                        }
                    }

                    $transaction->commit();
                    $data = array('type' => 'success', 'msg' => 'Category has been edited successfully.');
                }else {
                    $data = array('type' => 'danger', 'msg' => 'An error occurred please try again later.' );
                }

                echo json_encode($data);

            } catch(\Phalcon\Mvc\Model\Transaction\Failed $e) {
                die( json_encode(array('401' => $e->getMessage())) );
            }
        }
    }

    public function loadtypelistAction($off, $page, $keyword){
        $app = new CB();

        $offsetfinal = ($page * 10) - 10;
        $sql = "SELECT producttype.type, producttype.id FROM producttype";
        $sqlCount = "SELECT COUNT(*) FROM producttype";
        if($keyword!="null"){
            $sql .= " WHERE producttype.type LIKE '%" . $keyword . "%'";
            $sqlCount .= " WHERE producttype.type LIKE '%" . $keyword . "%'";
        }

        $sql .= " ORDER BY updated_at DESC LIMIT $offsetfinal, 10";

        $type = $app->bnbQuery($sql);

        $total_items = $app->bnbQuery($sqlCount)[0]['COUNT(*)'];

        echo json_encode(array('type' => $type, 'index' =>$page, 'total_items' => $total_items), JSON_NUMERIC_CHECK);
    }

    public function deletetypeAction($id){
        $pt = Producttype::findFirst("id='$id'");
        if($pt){
            if ($pt->delete() == false) {
                $data = array('type' => 'danger', 'msg' => 'An error occurred please try again later.' );
            }else {
                $data = array('type' => 'success', 'msg' => 'Product tags has been successfully deleted.' );
                $pt = Prodtype::find("typeid='$id'");
                if($pt){
                    if ($pt->delete() == false) {
                        $data = array('type' => 'danger', 'msg' => 'An error occurred please try again later.' );
                    }
                }

                $pt = Prodtype2::find("typeid='$id'");
                if($pt){
                    if ($pt->delete() == false) {
                        $data = array('type' => 'danger', 'msg' => 'An error occurred please try again later.' );
                    }
                }
            }
            echo json_encode($data);
        }
    }

    public function updatetypeAction(){
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $id = $request->getPost("id");
            $type = $request->getPost("type");

            $pt = Producttype::findFirst("type='$type' AND id!='$id'");
            if($pt){
                $data = array('type' => 'danger', 'msg' => 'Product type already exist.');
            } else {
                $pt = Producttype::findFirst("id='$id'");
                if($pt){
                    $data = array('type' => 'success', 'msg' => 'Product type has been successfully edited.');
                    $pt->type = $type;
                    if($pt->save() == false){
                        $data = array('type' => 'danger', 'msg' => 'An error occured please try again later.');
                    }
                }else {
                    $data = array('type' => 'danger', 'msg' => 'An error occured please try again later.');
                }
            } 

            echo json_encode($data);
        }
    }

    public function validatetypeAction($type) {
        $type = Producttype::findFirst("type='$type'");
        if($type){
            echo json_encode(array('exist' => true, ));
        }else {
            echo json_encode(array('exist' => false, ));        
        }
    }

    public function savetypeAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $type = $request->getPost('type');
            $pt = Producttype::findFirst("type='$type'");
            if(!$pt){
                $data = array('type' => 'success', 'msg' => 'Product type has been added successfully.' );
                $pt = new Producttype();
                $guid = new \Utilities\Guid\Guid();
                $pt->assign(array(
                    'id' => $guid->GUID(),
                    'type' => $type,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s") ));

                if($pt->save() == false){
                    $data = array('type' => 'danger', 'msg' => 'Product type already exist.' );
                }
            }else {
                $data = array('type' => 'danger', 'msg' => 'Product type already exist.' );
            }

            echo json_encode($data);
        }
    }

    public function getsubcatAction() {
        $request = new \Phalcon\Http\Request();
        $app = new CB();

        if($request->isPost()){
            $category = $request->getPost('category');
            $subcategory = [];
            foreach ($category as $key => $value) {
                $cat = Productcategory::findFirst("category='$value'");
                if($cat){
                    $subcat = $app->bnbQuery("SELECT productsubcategory.subcategory 
                        FROM productsubcategory INNER JOIN prodsubcat ON 
                        productsubcategory.id=prodsubcat.subcategoryid WHERE prodsubcat.categoryid='$cat->id'");

                    foreach ($subcat as $key => $value) {
                        if(!in_array($value['subcategory'], $subcategory)){
                            $subcategory[] = $value['subcategory'];
                        }
                    }
                }
            }

            echo json_encode($subcategory);
        }
    }

    public function gettypesAction(){
        $request = new \Phalcon\Http\Request();
        $app = new CB();

        if($request->isPost()){
            $subcategory = $request->getPost('subcategory');
            $types = [];
            foreach ($subcategory as $key => $value) {
                $subcat = Productsubcategory::findFirst("subcategory='$value'");
                if($subcat){
                    $type = $app->bnbQuery("SELECT producttype.type 
                        FROM producttype INNER JOIN prodtype ON 
                        producttype.id=prodtype.typeid WHERE prodtype.subcategoryid='$subcat->id'");

                    foreach ($type as $key => $value) {
                        if(!in_array($value['type'], $types)){
                            $types[] = $value['type'];
                        }
                    }
                }
            }

            echo json_encode($types);
        }
    }

    public function getreportAction($id) {
        $app = new CB();
        $invoice = $app->bnbQuery("SELECT productinvoice.invoice_number, inventory.date_added, inventory.quantity, productinvoice.distname FROM productinvoice INNER JOIN inventory ON productinvoice.id=inventory.invoiceid WHERE inventory.productid='" . $id . "' ORDER BY inventory.date_added DESC");
        $history = $app->bnbQuery("SELECT * FROM productlog WHERE productid='" . $id . "' ORDER BY datetime DESC");

        foreach ($history as $key => $value) {
            $value['action'] = str_replace("<ul><li>", '', $value['action']);
            $value['action'] = str_replace("</li><li>", ', ', $value['action']);
            $value['action'] = str_replace("</li></ul>", '', $value['action']);
            $history[$key]['action'] = $value['action'];
        }
        echo json_encode(array('invoice' => $invoice, 'history' => $history));
    }

    public function loadtagssizeAction() {
        $app = new CB();
        
        //tags
        $sql = "SELECT producttags.tags, producttags.id FROM producttags";
        $sqlCount = "SELECT COUNT(*) FROM producttags";
        $sql .= " ORDER BY updated_at DESC LIMIT 0, 10";

        $tags = $app->bnbQuery($sql);
        $total_items = $app->bnbQuery($sqlCount)[0]['COUNT(*)'];

        //size
        $sql = "SELECT productsize.size, productsize.id FROM productsize";
        $sqlCount = "SELECT COUNT(*) FROM productsize";
        $sql .= " LIMIT 0, 10";

        $size = $app->bnbQuery($sql);
        $total_items2 = $app->bnbQuery($sqlCount)[0]['COUNT(*)'];

        echo json_encode(array('tags' => $tags, 'size'=>$size, 'total_items' => $total_items, 'total_items2' => $total_items2), JSON_NUMERIC_CHECK);
    }

    public function loadsizeAction($num, $page, $keyword) {
        $app = new CB();

        $offsetfinal = ($page * 10) - 10;
        $sql = "SELECT productsize.size, productsize.id FROM productsize";
        $sqlCount = "SELECT COUNT(*) FROM productsize";
        if($keyword!="null"){
            $sql .= " WHERE productsize.size LIKE '%" . $keyword . "%'";
            $sqlCount .= " WHERE productsize.size LIKE '%" . $keyword . "%'";
        }

        $sql .= " LIMIT $offsetfinal, 10";

        $size = $app->bnbQuery($sql);

        $total_items = $app->bnbQuery($sqlCount)[0]['COUNT(*)'];

        echo json_encode(array('size' => $size, 'index' =>$page, 'total_items' => $total_items), JSON_NUMERIC_CHECK);
    }

    public function deletesizeAction($id) {
        $pt = Productsize::findFirst("id='$id'");
        if($pt){
            if ($pt->delete() == false) {
                $data = array('type' => 'danger', 'msg' => 'An error occurred please try again later.' );
            }else {
                $data = array('type' => 'success', 'msg' => 'Product size has been successfully deleted.' );
                $pt = Prodsize::find("sizeid='$id'");
                if($pt){
                    if ($pt->delete() == false) {
                        $data = array('type' => 'danger', 'msg' => 'An error occurred please try again later.' );
                    }
                }
            }
            echo json_encode($data);
        }
    }

    public function updatesizeAction() {
        $request = new \Phalcon\Http\Request();

        if($request->isPost()){
            $id = $request->getPost("id");
            $size = $request->getPost("size");

            $pt = Productsize::findFirst("size='$size' AND id!='$id'");
            if($pt){
                $data = array('type' => 'danger', 'msg' => 'Product size already exist.');
            } else {
                $pt = Productsize::findFirst("id='$id'");
                if($pt){
                    $data = array('type' => 'success', 'msg' => 'Product size has been successfully edited.');
                    $pt->size = $size;
                    if($pt->save() == false){
                        $data = array('type' => 'danger', 'msg' => 'An error occured please try again later.');
                    }
                }else {
                    $data = array('type' => 'danger', 'msg' => 'An error occured please try again later.');
                }
            } 

            echo json_encode($data);
        }
    }

    public function productreportAction(){ 
        $app = new CB();

        $searchresult = $app->bnbQuery("SELECT * FROM product");
        foreach ($searchresult as $key => $value) {
            $searchresult[$key]['category'] = $app->bnbQuery("SELECT category FROM productcategory INNER JOIN prodcat ON productcategory.id=prodcat.categoryid WHERE prodcat.productid='" . $value['productid'] . "'");
        }

        echo json_encode($searchresult);
    }
}
